#ifndef _UserNotFoundException_h_
#define _UserNotFoundException_h_

#include <exception>

namespace exception
{
	namespace domain
	{
		class UserNotFoundException : public std::exception
		{
		public:
			UserNotFoundException();
			virtual ~UserNotFoundException() throw();

			const char* what() const throw();
		};
	}
}

#endif
