#ifndef _JsonOutOfRange_h_
#define _JsonOutOfRange_h_

#include "JsonError.h"

namespace json
{
	class JsonOutOfRange : public JsonError
	{
	public:
		JsonOutOfRange(const size_t indx, const size_t len);
		~JsonOutOfRange();

		const char* what() const throw();

	private:
		/****************************************************************************************************/
		/************************************JsonOutOfRange members******************************************/
		/****************************************************************************************************/
		const size_t index;										/* out of range index */
		const size_t lenght;									/* length */
		/****************************************************************************************************/
		/************************************JsonOutOfRange members******************************************/
		/****************************************************************************************************/
	};
}

#endif
