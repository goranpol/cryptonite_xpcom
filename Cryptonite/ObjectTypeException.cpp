#include <mozilla/Char16.h>

#include "ObjectTypeException.h"

exception::domain::ObjectTypeException::ObjectTypeException(const int givTypNum) : typeNumber(-1), givenTypeNumber(givTypNum)
{
}

exception::domain::ObjectTypeException::ObjectTypeException(const int typNum, const int givTypNum) : typeNumber(typNum), givenTypeNumber(givTypNum)
{
}

exception::domain::ObjectTypeException::~ObjectTypeException() throw()
{
}

const char* exception::domain::ObjectTypeException::what() const throw()
{
	return "";
}