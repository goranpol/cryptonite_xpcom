#ifndef _Logger_h_
#define _Logger_h_

#include "nsISupports.h"

class Logger
{
public:
	
	static void logMessage(char* object, char* function, char* action);
	static void errorMessage(char* object, char* function, char* action, nsresult errorCode);
	
private:
	static char* getTime();
};

#endif
