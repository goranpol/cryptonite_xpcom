#ifndef _ObjectNotFoundException_h_
#define _ObjectNotFoundException_h_

#include <exception>
#include <string>

namespace exception
{
	namespace domain
	{
		class ObjectNotFoundException : public std::exception
		{
		public:
			ObjectNotFoundException(const std::string ID);
			ObjectNotFoundException(const std::string ID, const std::string chWd);
			virtual ~ObjectNotFoundException() throw();

			const char* what() const throw();

		private:
			const std::string id;
			const std::string checkWord;
		};
	}
}

#endif
