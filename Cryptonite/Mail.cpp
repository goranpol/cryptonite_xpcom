#include <mozilla/Char16.h>

#include "Mail.h"
#include "AESRijndael.h"
#include "Logger.h"

#include <sstream>
#include "Parser.h"

#include "ObjectNotFoundException.h"
#include "NotImplementedException.h"
#include "AESException.h"
#include "AlgorithmException.h"

base::Mail::Mail(const std::string objId, const std::string objKey, const std::string objInitChain, const std::string objCheckWord, const int objType) : base::Object(objType, objId, objKey), objectCheckWord(objCheckWord), objectInitChain(objInitChain), objectDraftId(""), attachments(std::vector<std::string>())
{
}

base::Mail::Mail(const std::string objId, const std::string objKey, const std::string objInitChain, const std::string objCheckWord, const int objType, const std::string objDraftId) : base::Object(objType, objId, objKey), objectCheckWord(objCheckWord), objectInitChain(objInitChain), objectDraftId(objDraftId), attachments(std::vector<std::string>())
{
}

base::Mail::Mail(const std::string objId, const std::string objKey, const std::string objInitChain, const std::string objCheckWord, const int objType, const std::string objDraftId, std::vector<std::string> objAttachmentIds) : base::Object(objType, objId, objKey), objectCheckWord(objCheckWord), objectInitChain(objInitChain), objectDraftId(objDraftId), attachments(objAttachmentIds)
{
}

base::Mail::Mail(const base::Object &obj) : base::Object(obj.getObjectType(), obj.getObjectId(), obj.getObjectKey()), objectInitChain(getInitChain(obj)), objectCheckWord(getCheckWord(obj))
{
}

base::Mail::Mail(const base::Mail &obj) : base::Object(obj.objectType, obj.objectId, obj.objectKey), objectCheckWord(obj.objectCheckWord), objectInitChain(obj.objectInitChain), users(obj.users), attachments(obj.attachments)
{
}

base::Mail::~Mail()
{
}

std::string base::Mail::getObjectCheckWord()
{
	return objectCheckWord;
}

std::string base::Mail::getObjectInitChain()
{
	return objectInitChain;
}

std::string base::Mail::getCheckWord(const base::Object &obj)
{
	const base::Mail mail = static_cast<const base::Mail&>(obj);
	return mail.objectCheckWord;
}

std::string base::Mail::getInitChain(const base::Object &obj)
{
	const base::Mail mail = static_cast<const base::Mail&>(obj);
	return mail.objectInitChain;
}

std::string base::Mail::generateCheckWord()
{
	size_t length = (size_t)((unsigned char)*randomNumbers(1) / 25.5) + 10;
	return toBase64(std::string(randomNumbers(length)), length);
}

base::Mail &base::Mail::operator=(base::Object &obj)
{
	base::Mail mail = static_cast<base::Mail&>(obj);
	if (this == &mail)
		return *this;

	const_cast<int&>(objectType) = mail.getObjectType();
	const_cast<std::string&>(objectId) = mail.getObjectId();
	const_cast<std::string&>(objectKey) = mail.getObjectKey();
	const_cast<std::string&>(objectInitChain) = mail.getObjectInitChain();
	const_cast<std::string&>(objectCheckWord) = mail.getObjectCheckWord();

	return *this;
}

base::Mail &base::Mail::operator=(base::Mail &obj)
{
	if (this == &obj)
		return *this;

	const_cast<int&>(objectType) = obj.getObjectType();
	const_cast<std::string&>(objectId) = obj.getObjectId();
	const_cast<std::string&>(objectKey) = obj.getObjectKey();
	const_cast<std::string&>(objectInitChain) = obj.getObjectInitChain();
	const_cast<std::string&>(objectCheckWord) = obj.getObjectCheckWord();

	return *this;
}

bool base::Mail::operator==(base::Mail &obj)
{
	if(objectType != obj.objectType)
		return false;
	else if(objectCheckWord.compare(obj.objectCheckWord) != 0)
		return false;
	else if (objectId.compare(obj.objectId) != 0)
		return false;
	else if (objectInitChain.compare(obj.objectInitChain) != 0)
		return false;
	else if (objectKey.compare(obj.objectKey) != 0)
		return false;
	else
		return true;
}

bool base::Mail::operator!=(base::Mail &obj)
{
	return !(*this == obj);
}

std::string base::Mail::encryptSubject(std::string subject)
{
	try
	{
		size_t whiteSpaceIndex = 0;
		while (true)
		{
			if (subject.size() - whiteSpaceIndex == 0)
				break;

			// iswhitespace
			if (subject[subject.size() - whiteSpaceIndex - 1] == ' ' || subject[subject.size() - whiteSpaceIndex - 1] == '\t' || subject[subject.size() - whiteSpaceIndex - 1] == '\n' || subject[subject.size() - whiteSpaceIndex - 1] == '\r')
				whiteSpaceIndex++;

			break;
		}

		subject = subject.substr(0, subject.size() - whiteSpaceIndex);
	}
	catch (...)
	{
	}

	std::string returnString = "*" + objectCheckWord + "*";
	returnString += encryptText(subject.substr(0, 30)) + "*";
	if(subject.size() > 29)
	{
		subject = subject.substr(30);
		for (size_t j = 0; j<subject.size(); j = j + 14)
			returnString += encryptText(subject.substr(j ,14)) + "*";
	}

	return returnString;
}

std::string base::Mail::decryptSubject(std::string subject)
{
	std::string returnString = "";
	std::vector<std::string> splittedSubject = split(subject, "*");

	returnString += splittedSubject[0];
	if(splittedSubject[1].compare(objectCheckWord) != 0)
		throw std::exception();		// ObjectNotFoundException or ObjectMisatchException

	for(size_t i=2; i<splittedSubject.size()-1; i++)
	{
		if(!splittedSubject[i].empty())
			returnString += decryptText(splittedSubject[i]);
	}

	return returnString;
}

std::string base::Mail::encryptBody(std::string body)
{
	return "*" + encryptText(body) + "*";
}

std::string base::Mail::decryptBody(std::string body)
{
	std::string returnString = "";
	std::vector<std::string> splitted = split(body, "*");

	if (splitted[0].size() != 0)
		returnString += splitted[0];

	for(size_t i=1; i<splitted.size()-1; i++)
	{
		if (splitted[i].size() != 0)
		{
			if (isBase64(splitted[i]))
			{
				if (splitted[i].compare(this->objectCheckWord) != 0)
					returnString += decryptText(splitted[i]);
			}
			else
			{
				returnString += splitted[i];
			}
		}
	}

	if (splitted[splitted.size() - 1].size() != 0)
		returnString += splitted[splitted.size()-1];

	return returnString;
}

std::string base::Mail::encryptText(std::string textString)
{
	std::string paddedText;
	std::string result;

	size_t textStringSize = textString.size();
	size_t blockLeftOver = textStringSize % 16;
	size_t padding;
	if (blockLeftOver == 0)
	{
		paddedText = textString + "*************";
		paddedText += "/16";
		padding = 16;
	}
	else if(blockLeftOver == 15)
	{
		paddedText = textString + "**************";
		paddedText += "/17";
		padding = 17;
	}
	else
	{
		padding = 16 - blockLeftOver;

		if(padding > 9)
		{
			paddedText = textString;
			for (size_t i = 0; i<padding - 3; i++)
				paddedText += "*";
		}
		else
		{
			paddedText = textString;
			for (size_t i = 0; i<padding - 2; i++)
				paddedText += "*";
		}

		paddedText += "/";
		std::ostringstream convertNumber;
		convertNumber << padding;
		paddedText += convertNumber.str();
	}

	return encrypt(paddedText);
}

std::string base::Mail::decryptText(std::string cipherTextString)
{
	return decrypt(cipherTextString);
}

std::string base::Mail::encrypt(std::string textString)
{
	std::string result;

	char *cipherText = (char*)NS_Alloc(sizeof(char) * textString.size() + 1);
	memset(cipherText, 0, textString.size() + 1);

	base::AESRijndael r;

	// only for cbc, fbc !!!not ebc
	r.makeKey((char*)fromBase64(objectKey).c_str(), (char*)fromBase64(objectInitChain).c_str(), 32);
	r.encrypt((char*)textString.c_str(), cipherText, textString.size(), r.CBC);
	
	char *tempBuf = cipherText;
	
	for (size_t i = 0; i<textString.size(); i++)
	{
		result.push_back(*tempBuf);
		tempBuf++;
	}

	NS_Free(cipherText);

	return toBase64(result, textString.size());
}

std::string base::Mail::decrypt(std::string cipherTextString)
{
	std::string cipherString = fromBase64(cipherTextString);
	size_t length = cipherString.size();

	if (length % 16 != 0)
		return cipherTextString;

	char *plainText = (char*)NS_Alloc(sizeof(char) * length + 1);
	memset(plainText, 0, length + 1);
	base::AESRijndael r;

	r.makeKey((char*)fromBase64(objectKey).c_str(), (char*)fromBase64(objectInitChain).c_str(), 32);
	r.decrypt((char*)cipherString.c_str(), plainText, length, r.CBC);

	std::string tempResult;
	char* tempBuf = plainText;
	for (size_t i = 0; i<length; i++)
	{
		tempResult.push_back(*tempBuf);
		tempBuf++;
	}

	NS_Free(plainText);

	size_t paddingSizeIndex = tempResult.rfind("/");
	std::string paddingNumberString = tempResult.substr(paddingSizeIndex + 1);

	size_t padding;
	std::istringstream (paddingNumberString) >> padding;

	std::string result = tempResult.substr(0, tempResult.size() - padding);

	return result;
}

json::Value &base::Mail::toJson() const
{
	json::JSON jsonObj = json::JSON(ObjectType);
	jsonObj.pushBack("ooid", objectId);
	jsonObj.pushBack("k", objectKey);
	jsonObj.pushBack("ot", objectType);

	jsonObj.pushBackObject("od");
	jsonObj["od"].pushBack("ic", objectInitChain);
	jsonObj["od"].pushBack("cw", objectCheckWord);

	if (!objectDraftId.empty())
		jsonObj["od"].pushBack("did", objectDraftId);

	if (!attachments.empty())
	{
		jsonObj["od"].pushBackArray("att");
		for (size_t i = 0; i < attachments.size(); i++)
			jsonObj["od"]["att"].pushBack(attachments[i]);
	}

	return jsonObj.get();
}