#include <mozilla/Char16.h>

#include "GDocument.h"
#include "AESRijndael.h"
#include "Logger.h"

#include "Parser.h"
#include "JsonError.h"

#include "ObjectRequest.h"
#include "UsersList.h"
#include "ObjectStorage.h"

#include <math.h>

#include "UnknownException.h"
#include "AlgorithmException.h"
#include "ObjectNotFoundException.h"
#include "AESException.h"

gdocument::GDocument::GDocument(const std::string objID) : base::Object(NITE_GDOC, objID, generateKey())
{
}

gdocument::GDocument::GDocument(const std::string objID, const std::string objKey) : base::Object(NITE_GDOC, objID, objKey)
{
}

gdocument::GDocument::GDocument(base::Object &obj) : base::Object(NITE_GDOC, obj.getObjectId(), obj.getObjectKey())
{
}

gdocument::GDocument::GDocument(gdocument::GDocument &obj) : base::Object(NITE_GDOC, obj.objectId, obj.objectKey)
{
}

gdocument::GDocument::~GDocument()
{
}

gdocument::GDocument &gdocument::GDocument::operator=(base::Object &obj)
{
	gdocument::GDocument gDoc = static_cast<gdocument::GDocument&>(obj);
	if (this == &gDoc)
		return *this;

	const_cast<int&>(objectType) = NITE_GDOC;
	const_cast<std::string&>(objectId) = gDoc.objectId;
	const_cast<std::string&>(objectKey) = gDoc.objectKey;

	return *this;
}

bool gdocument::GDocument::operator==(base::Object &obj)
{
	if (objectType != NITE_GDOC)
		return false;

	gdocument::GDocument gDoc = static_cast<gdocument::GDocument&>(obj);

	if (this->objectId.compare(gDoc.objectId) != 0)
		return false;
	if (this->objectKey.compare(gDoc.objectId) != 0)
		return false;

	return true;
}

bool gdocument::GDocument::operator!=(base::Object &obj)
{
	return !(*this == obj);
}

std::string gdocument::GDocument::encryptOutgoingString(std::string data)
{
	return encryptSendingMessage(data);
}

std::string gdocument::GDocument::decryptIncomingString(std::string data)
{
	std::vector<std::string> DOCS_mutationsStrings;
	std::vector<std::string> postDOCS_mutationsStrings;
	std::vector<std::string> postDOCS_mutationsStringsUndefined;
	std::string preString;

	std::string spliter = "DOCS_modelChunk =";
	std::vector<std::string> parsedData = split(data, spliter);

	std::string decryptedData;
	preString = parsedData[0];

	for(size_t i=1; i<parsedData.size(); i++)
	{
		parsedData[i].insert(0, spliter);

		if(parsedData[i].find("undefined") == parsedData[i].npos)
		{
			DOCS_mutationsStrings.push_back(parsedData[i].substr(0, parsedData[i].find(";")));
			postDOCS_mutationsStrings.push_back(parsedData[i].substr(parsedData[i].find(";"), parsedData[i].npos));
		}
		else
		{
			postDOCS_mutationsStringsUndefined.push_back(parsedData[i]);
		}
	}
	
	decryptedData += preString;
	for (size_t j = 0; j<DOCS_mutationsStrings.size(); j++)
	{
		try
		{
			json::JSON DOCS_mutationsJson = json::JSON(DOCS_mutationsStrings[j]);
			for (size_t i = 0; i<DOCS_mutationsJson.size(); i++)
			{
				if(DOCS_mutationsJson[i].getString("ty").compare("is") == 0)
				{
					if (j == 0)
					{
						decryptIS(DOCS_mutationsJson[i]);
					}
					else
					{
						DOCS_mutationsJson[i].replace("s", DOCS_mutationsJson[i].getString("s").substr(21));
						decryptIS(DOCS_mutationsJson[i]);
					}

				}
				else if(DOCS_mutationsJson[i].getString("ty").compare("as") == 0)
				{
					decryptASStart(DOCS_mutationsJson[i]);
				}
				else
				{
					Logger::logMessage("GDocument", "decryptIncomingString", "some other ty");
				}
			}

			decryptedData += DOCS_mutationsJson.print();
			decryptedData += postDOCS_mutationsStrings[j];
			decryptedData += postDOCS_mutationsStringsUndefined[j];
		}
		catch(json::JsonError)
		{
			throw exception::domain::AlgorithmException(NITE_GDOC, decryption);
		}
	}

	return decryptedData;
}

std::string gdocument::GDocument::encryptSendingMessage(std::string message)
{
	// check this again for throwing unexpected exceptions
	if(message.find("bundles=") == -1)
	{
		return message;
	}
	std::vector<std::string> variables;

	variables = split(message, "&");
	std::string encrypted;
	std::string bundles;
	size_t stay = 0;

	for (size_t i = 0; i<variables.size(); i++)
	{
		if (variables[i].find("bundles=") == 0)
		{
			bundles = variables[i];
			stay = i;
			break;
		}
		else
		{
			encrypted += variables[i];
			if(i < variables.size() - 1)
				encrypted += "&";
		}
	}

	std::string bundlesCopy = bundles;
	try
	{
		json::JSON bundlesJson = json::JSON(bundlesCopy);
		for (size_t i = 0; i<bundlesJson[(size_t)0]["commands"].size(); i++)
		{
			if (bundlesJson[(size_t)0]["commands"][i].getString("ty").compare("is") == 0)
			{
				encryptIS(bundlesJson[(size_t)0]["commands"][i]);
			}
			else if (bundlesJson[(size_t)0]["commands"][i].getString("ty").compare("ds") == 0)
			{
				encryptDS(bundlesJson[(size_t)0]["commands"][i]);
			}
			else if (bundlesJson[(size_t)0]["commands"][i].getString("ty").compare("as") == 0)
			{
				encryptAS(bundlesJson[(size_t)0]["commands"][i]);
			}
			else if (bundlesJson[(size_t)0]["commands"][i].getString("ty").compare("mlti") == 0)
			{
				encryptMLTI(bundlesJson[(size_t)0]["commands"][i]);
			}
			else
			{
				Logger::logMessage("GDocument", "encryptSendingMessage", "some other ty");
			}
		}
		
		encrypted += bundlesJson.print();

		for(size_t i=stay+1; i<variables.size(); i++)
		{
			if(i < variables.size())
				encrypted += "&";
			encrypted += variables[i];
			stay = i;
		}

		return encrypted;
	}
	catch(json::JsonError)
	{
		throw exception::domain::AlgorithmException(NITE_GDOC, encryption);
	}
}

void gdocument::GDocument::encryptIS(json::Value &object)
{
	std::string cipherBlock = encryptText(object.getString("s"));
	object.replace("s", cipherBlock);
	object.replace("ibi", computeStartIndexEncryption(object.getInt64("ibi")));
}

void gdocument::GDocument::encryptDS(json::Value &object)
{
	object.replace("si", computeStartIndexEncryption(object.getInt64("si")));
	object.replace("ei", computeEndIndexEncryption(object.getInt64("ei")));
}

void gdocument::GDocument::encryptAS(json::Value &object)
{
	if(object.getInt64("si") == object.getInt64("ei"))
		object.replace("si", computeStartIndexEncryption(object.getInt64("si")));
	else
		object.replace("si", int64_t(computeStartIndexEncryption(object.getInt64("si") - 1) + 1));

	object.replace("ei", computeStartIndexEncryption(object.getInt64("ei")));
}

void gdocument::GDocument::encryptMLTI(json::Value &object)
{
	for (size_t i = 0; i<object["mts"].size(); i++)
	{
		if(object["mts"][i].getString("ty").compare("is") == 0)
		{
			encryptIS(object["mts"][i]);
		}
		else if(object["mts"][i].getString("ty").compare("as") == 0)
		{
			encryptAS(object["mts"][i]);
			if (i > size_t(0) && object["mts"].size() > i + 1)
			{
				if(object["mts"][i-1].getString("ty").compare("is") == 0 && object["mts"][i].getString("st").compare("text") == 0)
				{
					// check for problems that may occur with i-1, i+1, i+3 (out of bounds !!!)
					std::string text = object["mts"][i-1].getString("s");
					std::string firstPart;
					std::string secondPart;

					if(text[0] != '\\')
					{
						firstPart = text.substr(0, 1);
						secondPart = text.substr(1);
					}
					else
					{
						firstPart = text.substr(0, 2);
						secondPart = text.substr(2);
					}

					int64_t index = object["mts"][i-1].getInt64("ibi");
					object["mts"][i-1].replace("s", firstPart);

					if(object["mts"][i+1].getString("ty").compare("as") == 0 && object["mts"][i+1].getString("st").compare("paragraph") == 0)
					{
						object["mts"].insert(i+3, object["mts"][i-1]);
						object["mts"][i+3].replace("s", secondPart);
						object["mts"][i+3].replace("ibi", int64_t(index + 1));
						
						encryptAS(object["mts"][i+1]);
						encryptAS(object["mts"][i+2]);

						i = i + 3;
					}
					else
					{
						object["mts"].insert(i+1, object["mts"][i-1]);
						object["mts"][i+1].replace("s", secondPart);
						object["mts"][i+1].replace("ibi", int64_t(index + 1));

						i++;
					}
				}
			}
		}
		else if(object["mts"][i].getString("ty").compare("ds") == 0)
		{
			encryptDS(object["mts"][i]);
		}
		else if(object["mts"][i].getString("ty").compare("is") == 0)
		{
			/* probably never going to happen */
			encryptMLTI(object["mts"][i]);
		}
		else
		{
			Logger::logMessage("GDocument", "encryptMLTI", "some other ty");
		}
	}
}

void gdocument::GDocument::decryptIS(json::Value &object)
{
	object.replace("s", decryptText(object.getString("s")));
	object.replace("ibi", computeStartIndexDecryption(object.getInt64("ibi")));
}

void gdocument::GDocument::decryptDS(json::Value &object)
{
	object.replace("si", computeStartIndexDecryption(object.getInt64("si")));
	object.replace("ei", computeEndIndexDecryption(object.getInt64("ei")));
}

void gdocument::GDocument::decryptASStart(json::Value &object)
{
	object.replace("si", computeStartIndexDecryption(object.getInt64("si")));
	object.replace("ei", computeStartIndexDecryption(object.getInt64("ei")));
}

void gdocument::GDocument::decryptASEnd(json::Value &object)
{
	object.replace("si", computeEndIndexDecryption(object.getInt64("si")));
	object.replace("ei", computeEndIndexDecryption(object.getInt64("ei")));
}

void gdocument::GDocument::decryptAS(json::Value &object)
{
	object.replace("si", computeStartIndexDecryption(object.getInt64("si")));
	object.replace("ei", computeEndIndexDecryption(object.getInt64("ei")));
}

int64_t gdocument::GDocument::computeStartIndexEncryption(int64_t index)
{
	if (index == 0)
		return 0;
	else
		return (((index - 1) * 22) + 1);							// base64 = 22
}

int64_t gdocument::GDocument::computeEndIndexEncryption(int64_t index)
{
	if (index == 0)
		return 0;
	else
		return (index * 22);										// base64 = 22
}

int64_t gdocument::GDocument::computeStartIndexDecryption(int64_t index)
{
	if (index == 0)
		return 0;
	else
	{
		double endIndex = (((double)index - 1) / 22) + 1;			// base64 = 22
		endIndex = ceil(endIndex);

		return (int64_t)endIndex;
	}
}

int64_t gdocument::GDocument::computeEndIndexDecryption(int64_t index)
{
	if (index == 0)
		return 0;
	else
	{
		double endIndex = (double)index / 22;						// base64 = 22
		endIndex = ceil(endIndex);
		return (int64_t)endIndex;
	}
}

std::string gdocument::GDocument::encryptText(const std::string textString)
{
	std::string result;
	std::string tempResult;
		
	for (size_t i = 0; i<textString.size(); i++)
	{
		tempResult.clear();
		
		if(textString[i] == '\\')
		{
			if(i == (textString.size() - 1))
			{
				tempResult.push_back(textString[i]);
				tempResult += randomNumbers(15);
				result += encrypt(tempResult);
			}
			else if(textString[i+1] == 'u')
			{
				tempResult.push_back(textString[i]);
				tempResult.push_back(textString[i+1]);
				tempResult.push_back(textString[i+2]);
				tempResult.push_back(textString[i+3]);
				tempResult.push_back(textString[i+4]);
				tempResult.push_back(textString[i+5]);
				tempResult += randomNumbers(10);
				i = i + 5;
				result += encrypt(tempResult);
			}
			else if(textString[i+1] == 'n' || textString[i+1] == '\"' || textString[i+1] == '\'')
			{
				result.push_back(textString[i]);
				result.push_back(textString[i+1]);
				std::string newLinePadding = toBase64(randomNumbers(16), 16);
				newLinePadding = newLinePadding.substr(0, 21);
				result += newLinePadding;
				i++;
				continue;
			}
			else if(textString[i+1] == '\\' || textString[i+1] == '/' || textString[i+1] == 't')
			{
				// something wrong here
				tempResult.push_back(textString[i]);
				tempResult += randomNumbers(15);
				i++;
				result += encrypt(tempResult);
			}
		}
		else
		{
			if((textString[i] & 0x80) == 0)
			{
				tempResult.push_back(textString[i]);
				tempResult += randomNumbers(15);
				result += encrypt(tempResult);
			}
			else if((textString[i] & 0xE0) == 0xC0)
			{
				tempResult.push_back(textString[i]);
				tempResult.push_back(textString[i+1]);
				tempResult += randomNumbers(14);
				result += encrypt(tempResult);
				i++;
			}
			else if((textString[i] & 0xF0) == 0xE0)
			{
				tempResult.push_back(textString[i]);
				tempResult.push_back(textString[i+1]);
				tempResult.push_back(textString[i+2]);
				tempResult += randomNumbers(13);
				result += encrypt(tempResult);
				i = i + 2;
			}
			else if((textString[i] & 0xF8) == 0xF0)
			{
				tempResult.push_back(textString[i]);
				tempResult.push_back(textString[i+1]);
				tempResult.push_back(textString[i+2]);
				tempResult.push_back(textString[i+3]);
				tempResult += randomNumbers(12);
				result += encrypt(tempResult);
				i = i + 3;
			}
		}
	}

	return result;
}

std::string gdocument::GDocument::decryptText(const std::string cipherTextString)
{
	if(cipherTextString.empty())
	{
		throw exception::domain::AESException(cipherstrempty);
	}	

	std::vector<std::string> cipherLines = split(cipherTextString, "\\n");
	std::string result;
	for (size_t i = 0; i<cipherLines.size(); i++)
	{
		if ((!cipherLines[i].empty()) && i > 0)
			cipherLines[i] = cipherLines[i].substr(21);
		
		if(cipherLines[i].empty())
		{
			if(i < cipherLines.size() - 1)
				result += "\\n";
			continue;
		}

		std::string tempPlainText = decrypt(cipherLines[i]);
		for (size_t j = 0; j<tempPlainText.size(); j = j + 16)
		{
			result.push_back(tempPlainText[j]);

			if(tempPlainText[j] == '\\')
			{
				if(tempPlainText[j+1] == 'u')
				{
					result.push_back(tempPlainText[j+1]);
					result.push_back(tempPlainText[j+2]);
					result.push_back(tempPlainText[j+3]);
					result.push_back(tempPlainText[j+4]);
					result.push_back(tempPlainText[j+5]);
				}
				else if(tempPlainText[j+1] == '\\' || tempPlainText[j+1] == '/' || tempPlainText[j+1] == 't')
				{
					result.push_back(tempPlainText[j+1]);
				}
			}
			else
			{
				if((tempPlainText[j] & 0xE0) == 0xC0)
				{
					result.push_back(tempPlainText[j+1]);
				}
				else if((tempPlainText[j] & 0xF0) == 0xE0)
				{
					result.push_back(tempPlainText[j+1]);
					result.push_back(tempPlainText[j+2]);
				}
				else if((tempPlainText[j] & 0xF8) == 0xF0)
				{
					result.push_back(tempPlainText[j+1]);
					result.push_back(tempPlainText[j+2]);
					result.push_back(tempPlainText[j+3]);
				}
			}
		}
		
		if(i < cipherLines.size() - 1)
		{
			result.append("\\n");
		}
	}
			
	return result;
}

std::string gdocument::GDocument::encrypt(std::string textString)
{
	std::string result;

	char *cipherText = (char*)NS_Alloc(sizeof(char) * 17);
	memset(cipherText, 0, 17);

	base::AESRijndael r;

	r.makeKey((char*)fromBase64(objectKey).c_str(), r.sm_chain0, 32);		// only for cbc, fbc !!!not ebc
	r.encrypt((char*)textString.c_str(), cipherText, 16);
	
	char *tempBuf = cipherText;
	
	for (size_t i = 0; i<16; i++)
	{
		result.push_back(*tempBuf);
		tempBuf++;
	}

	NS_Free(cipherText);
	return toBase64(result, 16);
}

std::string gdocument::GDocument::decrypt(std::string cipherTextString)
{
	std::string cipherString = "";
	size_t cipherTextStringLength = cipherTextString.size();
	for (size_t i = 0; i<cipherTextStringLength; i = i + 22)
	{
		cipherString += fromBase64(cipherTextString.substr(0, 22));
		cipherTextString = cipherTextString.substr(22);
	}
	size_t length = cipherString.size();

	char *plainText = (char*)NS_Alloc(sizeof(char) * length + 1);
	memset(plainText, int(0), length +1);
	base::AESRijndael r;

	r.makeKey((char*)fromBase64(objectKey).c_str(), r.sm_chain0, 32);
	r.decrypt((char*)cipherString.c_str(), plainText, length);

	std::string result;
	
	char *tempBuf = plainText;
	
	for (size_t i = 0; i<length; i++)
	{
		result.push_back(*tempBuf);
		tempBuf++;
	}
	
	NS_Free(plainText);
	return result;
}