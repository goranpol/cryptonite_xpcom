#include <mozilla/Char16.h>

#include "HotmailObserver.h"

#include "Logger.h"
#include "HotmailUIWrapper.h"
#include "HotmailListener.h"
#include "HotmailAttachment.h"
#include "ObjectStorage.h"
#include "UsersList.h"

#include "NSException.h"

NS_IMPL_ISUPPORTS(hotmail::HotmailObserver, nsIObserver)

hotmail::HotmailObserver::HotmailObserver(bool encrypt) : HttpObserver(encrypt)
{
}

hotmail::HotmailObserver::~HotmailObserver()
{
}

NS_IMETHODIMP hotmail::HotmailObserver::Observe(nsISupports *aSubject, const char *aTopic, const char16_t *aData)
{
	nsresult rv;

	nsCOMPtr<nsIHttpChannel> httpChannel = do_QueryInterface(aSubject, &rv);
	if(NS_FAILED(rv))
	{
		// inform javascript
		Logger::errorMessage("HotmailObserver", "Observe", "Observe FAILED: couldn't query interface to nsIHttpChannel", rv);
		return NS_OK;
	}

	nsAutoCString urlPath;
	rv = getUri(httpChannel, urlPath);
	if(NS_FAILED(rv))
	{
		// inform javascript
		Logger::errorMessage("HotmailObserver", "Observe", "Observe FAILED: couldn't get uri", rv);
		return NS_OK;
	}
		
	if(!strcmp(aTopic, "http-on-modify-request"))
	{
		if(urlPath.Find("cnmn=Microsoft.Msn.Hotmail.Ui.Fpp.MailBox.SendMessage_ec") != -1 && doEncrypt)
		{
			nsCOMPtr<nsIUploadChannel> uploadChannel = do_QueryInterface(httpChannel, &rv);
			if(NS_FAILED(rv))
			{
				// inform javascript
				Logger::errorMessage("HotmailObserver", "Observe:http-on-modify-request", "Observe FAILED: couldn't query interface to nsIUploadChannel", rv);
				return NS_OK;
			}

			try
			{
				std::string data = readSendingData(uploadChannel);

				try
				{
					std::string encryptedData = hotmail::HotmailUIWrapper().encryptOutgoingString(data, NITE_mail);
					replaceSendingData(uploadChannel, encryptedData);
					return NS_OK;
				}
				catch (...)
				{
					replaceSendingData(uploadChannel, data);
					return NS_OK;
				}
			}
			catch(...)
			{
				// infrom javascript
			}

			return NS_OK;
		}
		else if(urlPath.Find("cnmn=Microsoft.Msn.Hotmail.Ui.Fpp.MailBox.SaveDraftM4") != -1 && doEncrypt)
		{
			nsCOMPtr<nsIUploadChannel> uploadChannel = do_QueryInterface(httpChannel, &rv);
			if(NS_FAILED(rv))
			{
				// inform javascript
				Logger::errorMessage("HotmailObserver", "Observe:http-on-modify-request", "Observe FAILED: couldn't query interface to nsIUploadChannel", rv);
				return NS_OK;
			}

			try
			{
				std::string data = readSendingData(uploadChannel);

				try
				{
					std::string encryptedData = hotmail::HotmailUIWrapper().encryptOutgoingString(data, NITE_draft);
					replaceSendingData(uploadChannel, encryptedData);
					return NS_OK;
				}
				catch (...)
				{
					replaceSendingData(uploadChannel, data);
					return NS_OK;
				}
			}
			catch(...)
			{
				// inform javascript
			}

			return NS_OK;
		}
		else if(urlPath.Find("cnmn=Microsoft.Msn.Hotmail.Ui.Fpp.MailBox.MoveMessagesToFolder") != -1)
		{
			nsCOMPtr<nsIUploadChannel> uploadChannel = do_QueryInterface(httpChannel, &rv);
			if(NS_FAILED(rv))
			{
				// inform javascript
				Logger::errorMessage("HotmailObserver", "Observe:http-on-modify-request", "Observe FAILED: couldn't query interface to nsIUploadChannel", rv);
				return NS_OK;
			}

			try
			{
				std::string data = readSendingData(uploadChannel);

				try
				{
					hotmail::HotmailUIWrapper().deleteMails(data);
					replaceSendingData(uploadChannel, data);
					return NS_OK;
				}
				catch (...)
				{
					replaceSendingData(uploadChannel, data);
					return NS_OK;
				}
			}
			catch(...)
			{
				// inform javascript
			}

			return NS_OK;
		}
		else if (urlPath.Find("SilverlightAttachmentUploader.aspx") != -1 && doEncrypt)
		{
			nsCOMPtr<nsIUploadChannel> uploadChannel = do_QueryInterface(httpChannel, &rv);
			if (NS_FAILED(rv))
			{
				// inform javascript
				Logger::errorMessage("HotmailObserver", "Observe:http-on-modify-request", "Observe FAILED: couldn't query interface to nsIUploadChannel", rv);
				return NS_OK;
			}

			try
			{
				std::string data = readSendingData(uploadChannel);
				if (data.length() == 0)
				{
					replaceSendingData(uploadChannel, data);
					return NS_OK;
				}

				try
				{
					std::string encryptedData;
					hotmail::HotmailAttachment hotmailAttachment("-1");
					encryptedData = hotmailAttachment.encryptOutgoingString(data);

					gui::GUIObserver::getInstance().notifyObserver(NITE_ENCRYPT, "microsoft attachment");

					base::ObjectStorage::getInstance().addTempObject(hotmailAttachment);

					std::string dataLength = getRequestHeader(httpChannel, "Content-Length");
					std::string recalculatedDataLength = recalculateFileLength(dataLength);
					setRequestHeader(httpChannel, "Content-Length", recalculatedDataLength);

					replaceSendingData(uploadChannel, encryptedData);
					return NS_OK;
				}
				catch (...)
				{
					Logger::errorMessage("GMailObserver", "Observe:http-on-modify-request", "...", nsresult(1));
					replaceSendingData(uploadChannel, data);
					return NS_OK;
					// notify javascript
				}
			}
			catch (...)
			{
				// inform javascript
			}

			return NS_OK;
		}

		return NS_OK;
	}
	else if(!strcmp(aTopic, "http-on-examine-response"))
	{
		RefPtr<hotmail::HotmailListener> hotmailListener;

		if(urlPath.Find("mail.live.com") != -1)
		{
			if(urlPath.Find("cnmn=Microsoft.Msn.Hotmail.Ui.Fpp.MailBox.GetInboxData") != -1 || urlPath.Find("cnmn=Microsoft.Msn.Hotmail.Ui.Fpp.MailBox.MarkMessagesReadState") != -1)
			{
				hotmailListener = new hotmail::HotmailListener(NITE_decryptJavascript);
				gui::GUIObserver::getInstance().notifyObserver(NITE_DECRYPT, "microsoft mails");
			}
			else if(urlPath.Find("cnmn=Microsoft.Msn.Hotmail.Ui.Fpp.MailBox.GetComposeData") != -1)
			{
				hotmailListener = new hotmail::HotmailListener(NITE_decryptCompose);
				gui::GUIObserver::getInstance().notifyObserver(NITE_DECRYPT, "microsoft mails");
			}
			else if(urlPath.Find("fid=") != -1 || urlPath.Find("Mail.mvc") != -1 || urlPath.Find("default") != -1)
			{
				hotmailListener = new hotmail::HotmailListener(NITE_decryptHtml);
				gui::GUIObserver::getInstance().notifyObserver(NITE_DECRYPT, "microsoft mails");
			}
			else if(urlPath.Find("cnmn=Microsoft.Msn.Hotmail.Ui.Fpp.MailBox.SaveDraftM4") != -1 && doEncrypt)
			{
				hotmailListener = new hotmail::HotmailListener(NITE_sendDraft);
			}
			else if(urlPath.Find("cnmn=Microsoft.Msn.Hotmail.Ui.Fpp.MailBox.SendMessage_ec") != -1 && doEncrypt)
			{
				hotmailListener = new hotmail::HotmailListener(NITE_sendMail);
			}
			else if (urlPath.Find("mail.live.com/") == urlPath.Length() - 14)
			{
				hotmailListener = new hotmail::HotmailListener(NITE_decryptHtml);
				gui::GUIObserver::getInstance().notifyObserver(NITE_DECRYPT, "microsoft mails");
			}
			else if (urlPath.Find("SilverlightAttachmentUploader.aspx") != -1 && doEncrypt)
			{
				hotmailListener = new hotmail::HotmailListener(NITE_sendAttachment);
			}
			else
			{
				return NS_OK;
			}
		}

		nsCOMPtr<nsIHttpChannel> httpChannel = do_QueryInterface(aSubject, &rv);
		if(NS_FAILED(rv))
		{
			// notify javascript
			Logger::errorMessage("HotmailObserver", "Observe:http-on-examine-response", "Observe FAILED: couldn't query interface aSubject to nsIHttpChannel", rv);
			return NS_OK;
		}
		
		nsCOMPtr<nsITraceableChannel> traceableChannel = do_QueryInterface(aSubject, &rv);
		if(NS_FAILED(rv))
		{
			// notify javascript
			Logger::errorMessage("HotmailObserver", "Observe:http-on-examine-response", "Observe FAILED: couldn't query interface to nsITraceableChannel", rv);
			return NS_OK;
		}

		try
		{
			replaceStreamListener(traceableChannel, hotmailListener);
		}
		catch(exception::technical::NSException)
		{
			// notify javascript
		}
		catch(...)
		{
			// notify javascript
		}
	}

	return NS_OK;
}

std::string hotmail::HotmailObserver::recalculateFileLength(std::string data)
{
	size_t length = atoi(data.c_str());
	if (length % 16 == 0 || length % 16 == 15)
		length = length + 16;

	// Calculate size of encrypted file
	size_t newSize = ceil(double(length) / 16) * 16; // encrypted size
	size_t remainder = newSize % 3;
	newSize = newSize / 3 * 4;					// base64 size without remainder
	if (remainder == 1)
		newSize += 2;
	else if (remainder == 2)
		newSize += 3;							// base64 size with remainder
	newSize += 86;								// with added checkword

	return std::to_string(newSize);
}
