#include <mozilla/Char16.h>

#include "JsonBadAlloc.h"

json::JsonBadAlloc::JsonBadAlloc()
{
}

json::JsonBadAlloc::~JsonBadAlloc()
{
}

const char* json::JsonBadAlloc::what() const throw()
{
	std::string returnString = "Json error: bad allocation error";
	return fromStdString(returnString);
}