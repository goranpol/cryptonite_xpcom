#ifndef _GMailListener_h
#define _GMailListener_h

#include "HttpListener.h"
#include <string>
#include <vector>

#define NITE_SendMailOrDraft		0
#define NITE_DeleteMail			1
#define NITE_DecryptMail			2
#define NITE_DecryptMailSummary	3
#define NITE_AttachmentToDrive	4

namespace gmail
{
	class GMailListener : public base::HttpListener
	{
	public:
		GMailListener(const int type);
		GMailListener(const std::string requestID, const bool setMail);

		NS_DECL_ISUPPORTS
		NS_DECL_NSISTREAMLISTENER
		NS_DECL_NSIREQUESTOBSERVER

	private:
		~GMailListener();

		const int urlType;
		const bool mail;
		const std::string requestId;
	};
}

#endif
