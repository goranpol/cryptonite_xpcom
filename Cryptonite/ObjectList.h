#ifndef _ObjectList_h_
#define _ObjectList_h_

#include "Object.h"
#include "Value.h"
#include <memory>

namespace base
{
	class ObjectList
	{
	public:
		ObjectList();
		~ObjectList();

		ObjectList &operator=(const ObjectList &objList);
		Object &operator[](const size_t index);
		Object &operator[](const std::string ooid);
		ObjectList &operator+(ObjectList &objList);
		ObjectList &operator+=(ObjectList &objList);

		Object &get(const size_t index);
		Object &get(const std::string ooid);
		void replace(const size_t index, Object &obj);
		void insert(const size_t index, Object &obj);
		void pushBack(const Object &obj);
		void remove(const size_t index);
		void remove(const std::string ooid);

		bool empty();
		void clear();
		size_t size();

		json::Value &toJson();

	private:

		/* ObjectList members */
		typedef std::shared_ptr<base::Object> objectPtr;
		
		std::vector<objectPtr> objectMembers;
	};
}

#endif
