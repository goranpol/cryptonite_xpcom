#ifndef _UsersList_h_
#define _UsersList_h_

#include <vector>
#include "Value.h"

namespace base
{
	class UsersList
	{
	public:
		UsersList();
		~UsersList();

		UsersList &operator=(const UsersList &list);
		std::string operator[](const size_t index) const;

		std::string get(const size_t index);
		void replace(const size_t index, const std::string user);
		void insert(const size_t index, const std::string user);
		void pushBack(const std::string user);
		void remove(const size_t index);
		void remove(const std::string user);

		bool empty() const;
		void clear();
		size_t size() const;

		json::Value &toJson();

	private:

		/* UsersList members */
		std::vector<std::string> users;
		size_t length;
	};
}

#endif