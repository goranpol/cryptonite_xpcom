#include <mozilla/Char16.h>

#include "SessionInfo.h"

#include "Logger.h"


base::SessionInfo::SessionInfo(std::string sessKey) : sessionKey(sessKey)
{
}

base::SessionInfo::~SessionInfo()
{
}

std::string base::SessionInfo::getSessionKey()
{
	return sessionKey;
}

void base::SessionInfo::setPersistentCookie(std::string persistentCookie)
{
	// implement this
}

std::string base::SessionInfo::getPersistentCookie()
{
	// implement this
	return "";
}

