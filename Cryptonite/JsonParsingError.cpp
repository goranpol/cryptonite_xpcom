#include <mozilla/Char16.h>

#include "JsonParsingError.h"

json::JsonParsingError::JsonParsingError(const int typ, const std::string jsonStr) : type(typ), jsonString(jsonStr)
{
}

json::JsonParsingError::~JsonParsingError()
{
}

const char* json::JsonParsingError::what() const throw()
{
	std::string returnString = "Json error: ";

	if(type == startQuoteNotFound)
	{
		returnString += "starting quote for string (\' or \") not found";
	}
	else if(type == endQuoteNotFound)
	{
		returnString += "ending quote for string (\' or \") not found";
	}
	else if(type == stringNotComplete)
	{
		returnString += "string not complete json (size = 0 before end of parsing)";
	}
	else if(type == startingArraySignNotFound)
	{
		returnString += "starting sign for array ([) not found";
	}
	else if(type == endingOrSeparatorArraySignNotFound)
	{
		returnString += "ending sign for array (]) not found or separator sign for array (,) not found";
	}
	else if(type == startingJSONSignNotFound)
	{
		returnString += "starting sign for new JSON ([ or {) not found";
	}
	else if(type == startingObjectSignNotFound)
	{
		returnString += "starting sign for object ({) not found";
	}
	else if(type == endingOrSeparatorObjectNotFound)
	{
		returnString += "ending sign for object (}) not found or separator sign for object (,) not found";
	}
	else if(type == startingSignKeyNotFound)
	{
		returnString += "starting sign for key (\") in object pair not found";
	}
	else if(type == endingSignKeyNotFound)
	{
		returnString += "ending sign for key (\") in object pair not found";
	}
	else if(type == keyValueSeparatorNotFound)
	{
		returnString += "separator for key value (:) not found";
	}
	else if(type == noValueFound)
	{
		returnString += "no value found after \"key\" :";
	}
	else if(type == numberParsingError)
	{
		returnString += "error parsing json number";
	}
	else if(type == notJson)
	{
		returnString += "string is not json (no object or array found)";
	}
	else
	{
		returnString += "undefined error";
	}

	size_t l = 100;
	if (jsonString.size() < 100)
		l = jsonString.size();

	returnString += " || json string: " + jsonString.substr(0, l);

	return fromStdString(returnString);
}
