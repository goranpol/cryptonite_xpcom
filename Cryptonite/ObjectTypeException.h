#ifndef _ObjectTypeException_h_
#define _ObjectTypeException_h_

#include <exception>

#define encryption	0
#define decryption	1
#define useraccess	2

namespace exception
{
	namespace domain
	{
		class ObjectTypeException : public std::exception
		{
		public:
			ObjectTypeException(const int givTypNum);
			ObjectTypeException(const int typNum, const int givTypNum);
			virtual ~ObjectTypeException() throw();

			const char* what() const throw();

		private:
			const int typeNumber;
			const int givenTypeNumber;
		};
	}
}

#endif
