#ifndef _AlgorithmException_h_
#define _AlgorithmException_h_

#include <exception>

#define encryption	0
#define decryption	1
#define useraccess	2
#define deletion	3

namespace exception
{
	namespace domain
	{
		class AlgorithmException : public std::exception
		{
		public:
			AlgorithmException(const int objTyp, const int algTyp);
			virtual ~AlgorithmException() throw();

			const char* what() const throw();

		private:
			const int objectType;
			const int algorithmType;
		};
	}
}

#endif
