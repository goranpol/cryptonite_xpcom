#ifndef _GAttachmentListener_h
#define _GAttachmentListener_h

#include "HttpListener.h"

#include <string>

namespace gmail
{
	class GAttachmentListener : public base::HttpListener
	{
	public:
		GAttachmentListener(const std::string attid);

		NS_DECL_ISUPPORTS
		NS_DECL_NSISTREAMLISTENER
		NS_DECL_NSIREQUESTOBSERVER

	private:
		~GAttachmentListener();

		const std::string attachmentId;
	};
}

#endif

