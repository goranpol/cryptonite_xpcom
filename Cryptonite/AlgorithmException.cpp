#include <mozilla/Char16.h>

#include "AlgorithmException.h"

exception::domain::AlgorithmException::AlgorithmException(const int objTyp, const int algTyp) : objectType(objTyp), algorithmType(algTyp)
{
}

exception::domain::AlgorithmException::~AlgorithmException() throw()
{
}

const char* exception::domain::AlgorithmException::what() const throw()
{
	return "";
}