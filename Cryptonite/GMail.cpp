#include <mozilla/Char16.h>

#include "GMail.h"
#include "AESRijndael.h"
#include "Logger.h"

#include "ObjectStorage.h"

#include "JsonTypeError.h"

#include <sstream>
#include "Parser.h"

#include "ObjectNotFoundException.h"
#include "NotImplementedException.h"
#include "AESException.h"
#include "AlgorithmException.h"
#include "ObjectTypeException.h"

gmail::GMail::GMail(const std::string reqId) : base::Mail(reqId, generateKey(), generateKey(), generateCheckWord(), NITE_GMAIL)
{
}

gmail::GMail::GMail(const std::string reqId, const std::string objDraftId) : base::Mail(reqId, generateKey(), generateKey(), generateCheckWord(), NITE_GMAIL, objDraftId)
{
}

gmail::GMail::GMail(const std::string reqId, const std::string objDraftId, std::vector<std::string> objAttachmentIds) : base::Mail(reqId, generateKey(), generateKey(), generateCheckWord(), NITE_GMAIL, objDraftId, objAttachmentIds)
{
}

gmail::GMail::GMail(const std::string objId, const std::string objKey, const std::string objInitChain, const std::string objCheckWord) : base::Mail(objId, objKey, objInitChain, objCheckWord, NITE_GMAIL)
{
}

gmail::GMail::GMail(const std::string objId, const std::string objKey, const std::string objInitChain, const std::string objCheckWord, const std::string objDraftId) : base::Mail(objId, objKey, objInitChain, objCheckWord, NITE_GMAIL, objDraftId)
{
}

gmail::GMail::GMail(const std::string objId, const std::string objKey, const std::string objInitChain, const std::string objCheckWord, const std::string objDraftId, std::vector<std::string> objAttachmentIds) : base::Mail(objId, objKey, objInitChain, objCheckWord, NITE_GMAIL, objDraftId, objAttachmentIds)
{
}

gmail::GMail::GMail(const base::Object &obj) : base::Mail(obj.getObjectId(), obj.getObjectKey(), getInitChain(obj), getCheckWord(obj), NITE_GMAIL)
{
}

gmail::GMail::GMail(const gmail::GMail &obj) : base::Mail(obj.objectId, obj.objectKey, obj.objectInitChain, obj.objectCheckWord, NITE_GMAIL, obj.objectDraftId)
{
	for (size_t i = 0; i<obj.users.size(); i++)
		users.pushBack(obj.users[i]);

	for (size_t i = 0; i<obj.attachments.size(); i++)
		attachments.push_back(obj.attachments[i]);
}

gmail::GMail::~GMail()
{
}

gmail::GMail &gmail::GMail::operator=(base::Object &obj)
{
	if (obj.getObjectType() != NITE_GMAIL)
		throw exception::domain::ObjectTypeException(NITE_GMAIL, obj.getObjectType());

	gmail::GMail gMail = static_cast<gmail::GMail&>(obj);

	if (this == &gMail)
		return *this;

	const_cast<int&>(objectType) = NITE_GMAIL;
	const_cast<std::string&>(objectId) = gMail.objectId;
	const_cast<std::string&>(objectKey) = gMail.objectKey;
	const_cast<std::string&>(objectInitChain) = gMail.objectInitChain;
	const_cast<std::string&>(objectCheckWord) = gMail.objectCheckWord;

	return *this;
}

gmail::GMail &gmail::GMail::operator=(gmail::GMail &obj)
{
	if (this == &obj)
		return *this;

	const_cast<int&>(objectType) = NITE_GMAIL;
	const_cast<std::string&>(objectId) = obj.objectId;
	const_cast<std::string&>(objectKey) = obj.objectKey;
	const_cast<std::string&>(objectInitChain) = obj.objectInitChain;
	const_cast<std::string&>(objectCheckWord) = obj.objectCheckWord;

	return *this;
}

bool gmail::GMail::operator==(base::Object &obj)
{
	if (objectType != NITE_GMAIL)
		return false;

	gmail::GMail gMail = static_cast<gmail::GMail&>(obj);

	if(objectId.compare(gMail.objectId) == 0 && objectKey.compare(gMail.objectKey) == 0 && objectInitChain.compare(gMail.objectInitChain) == 0 && objectCheckWord.compare(gMail.objectCheckWord) == 0)
		return true;

	return false;
}

bool gmail::GMail::operator!=(base::Object &obj)
{
	return !(*this == obj);
}

std::string gmail::GMail::encryptOutgoingString(std::string data)
{
	try
	{
		std::vector<std::string> splitedData = split(data, "&");
		
		std::string encryptedData = "";
		for (size_t i = 0; i<splitedData.size(); i++)
		{
			if (splitedData[i].find("to=") == 0)
			{
				if(!splitedData[i].substr(3).empty())
					users.pushBack(splitedData[i].substr(3).substr(splitedData[i].substr(3).find("<") + 1, splitedData[i].substr(3).find(">") - splitedData[i].substr(3).find("<") - 1));

				encryptedData += splitedData[i] + "&";
			}
			else if (splitedData[i].find("cc=") == 0)
			{
				if(!splitedData[i].substr(3).empty())
					users.pushBack(splitedData[i].substr(3).substr(splitedData[i].substr(3).find("<") + 1, splitedData[i].substr(3).find(">") - splitedData[i].substr(3).find("<") - 1));

				encryptedData += splitedData[i] + "&";
			}
			else if (splitedData[i].find("bcc=") == 0)
			{
				if(!splitedData[i].substr(4).empty())
					users.pushBack(splitedData[i].substr(3).substr(splitedData[i].substr(3).find("<") + 1, splitedData[i].substr(3).find(">") - splitedData[i].substr(3).find("<") - 1));

				encryptedData += splitedData[i] + "&";
			}
			else  if (splitedData[i].find("subjectbox=") == 0)
			{
				std::string encryptedSubject = "";
				std::string subjectTemp = "";
				if (splitedData[i].substr(11).find("Re: ") == 0)
				{
					encryptedSubject = splitedData[i].substr(0, 15);
					subjectTemp = splitedData[i].substr(15);
				}
				else if (splitedData[i].substr(11).find("Fwd: ") == 0)
				{
					encryptedSubject = splitedData[i].substr(0, 16);
					subjectTemp = splitedData[i].substr(16);
				}
				else
				{
					encryptedSubject = splitedData[i].substr(0, 11);
					subjectTemp = splitedData[i].substr(11);
				}
				encryptedData += encryptedSubject + encryptSubject(subjectTemp) + "&";
			}
			else if (splitedData[i].find("subject=") == 0)
			{
				std::string encryptedSubject = "";
				std::string subjectTemp = "";
				if (splitedData[i].substr(8).find("Re: ") == 0)
				{
					encryptedSubject = splitedData[i].substr(0, 12);
					subjectTemp = splitedData[i].substr(12);
				}
				else if (splitedData[i].substr(8).find("Fwd: ") == 0)
				{
					encryptedSubject = splitedData[i].substr(0, 13);
					subjectTemp = splitedData[i].substr(13);
				}
				else
				{
					encryptedSubject = splitedData[i].substr(0, 8);
					subjectTemp = splitedData[i].substr(8);
				}

				encryptedData += encryptedSubject + encryptSubject(subjectTemp) + "&";
			}
			else if (splitedData[i].find("body=") == 0)
			{
				encryptedData += splitedData[i].substr(0, 5) + encryptBody(splitedData[i].substr(5)) + "&";
			}
			else if (splitedData[i].find("uet=") == 0)
			{
				if (splitedData[i].substr(4).size() == 0)
				{
					encryptedData += splitedData[i] + "&";
					continue;
				}

				encryptedData += splitedData[i].substr(0, 4) + encryptBody(splitedData[i].substr(4)) + "&";
			}
			else if (splitedData[i].find("lt;") == 0 || splitedData[i].find("gt;") == 0)
			{
				encryptedData.erase(encryptedData.end() - 1);
				encryptedData += encryptBody("&" + splitedData[i]) + "&";
			}
			else
			{
				encryptedData += splitedData[i] + "&";
			}
		}
		
		encryptedData.erase(encryptedData.end() - 1);
		return encryptedData;
	}
	catch(...)
	{
		throw exception::domain::AlgorithmException(NITE_GMAIL, encryption);
	}
}

std::string gmail::GMail::decryptIncomingString(std::string data)
{
	throw exception::domain::NotImplementedException("GMail", "decryptIncomingString");
}

void gmail::GMail::decryptCs(json::Value &jsonArray)
{
	try
	{
		size_t subjectIndex;
		if(jsonArray.getInt(3) == 0)
			subjectIndex = 22;
		else
			subjectIndex = 23;

		jsonArray.replace(subjectIndex, decryptSubject(jsonArray.getString(subjectIndex)));
	}
	catch(...)
	{
		Logger::errorMessage("GMail", "decryptCs", (char*)jsonArray.print().c_str(), nsresult(1));
		throw exception::domain::AlgorithmException(NITE_GMAIL, decryption);
	}
}

void gmail::GMail::decryptMs(json::Value &jsonArray)
{
	try
	{
		std::string decryptedSubject = decryptSubject(jsonArray.getString(12));
		jsonArray.replace(12, decryptedSubject);
		jsonArray.replace(8, "Cryptonite encrypted message");

		try
		{
			jsonArray[13].replace(5, decryptedSubject);
			jsonArray[13].replace(21, decryptedSubject);

			std::string decryptedBody = decryptBody(jsonArray[13].getString(6));
			jsonArray[13].replace(6, decryptedBody);
		}
		catch (...)
		{
		}
	}
	catch(...)
	{
		Logger::errorMessage("GMail", "decryptMs", (char*)jsonArray.print().c_str(), nsresult(1));
		throw exception::domain::AlgorithmException(NITE_GMAIL, decryption);
	}
}

void gmail::GMail::decryptSummary(json::Value &jsonArray)
{
	try
	{
		jsonArray.replace(9, decryptSubject(jsonArray.getString(9)));
		jsonArray.replace(10, "Cryptonite encrypted message");
	}
	catch(...)
	{
		Logger::errorMessage("GMail", "decryptSummary", (char*)jsonArray.print().c_str(), nsresult(1));
		throw exception::domain::AlgorithmException(NITE_GMAIL, decryption);
	}
}

std::string gmail::GMail::decryptSubject(std::string subject)
{
	std::string prefix = subject.substr(0, subject.find("*"));
	std::string postfix = subject.substr(subject.rfind("*"));
	subject = subject.substr(prefix.size(), subject.size() - prefix.size() - postfix.size());

	// replace " with \"
	std::string decryptedSubject = Mail::decryptSubject(prefix + subject + postfix);
	std::vector<std::string> splitedDecryptedSubject = split(decryptedSubject, "\"");
	decryptedSubject = splitedDecryptedSubject[0];
	for(size_t i=1; i<splitedDecryptedSubject.size(); i++)
	{
		if(splitedDecryptedSubject[i-1][splitedDecryptedSubject[i-1].size()-1] == '\\')
			decryptedSubject += "\"" + splitedDecryptedSubject[i];
		else
			decryptedSubject += "\\\"" + splitedDecryptedSubject[i];
	}

	return decryptedSubject;
}

std::string gmail::GMail::decryptBody(std::string body)
{
	size_t first = body.find("*");
	size_t last = body.rfind("*") + 1;
	std::string prefix = body.substr(0, first);
	std::string postfix = body.substr(last);
	body = body.substr(first, last - first);

	body = replace(body, "\\u003cWBR\\u003e", "");
	body = replace(body, "\\u003cwbr\\u003e", "");

	// google puts \n in the end of mail so you delete it
	while(postfix.find("\\n") != -1)
		postfix.replace(postfix.find("\\n"), 2, "");

	// replace " with \"
	std::string decryptedBody = Mail::decryptBody(prefix + body + postfix);
	std::vector<std::string> splitedDecryptedBody = split(decryptedBody, "\"");
	decryptedBody = splitedDecryptedBody[0];
	for(size_t i=1; i<splitedDecryptedBody.size(); i++)
	{
		if(splitedDecryptedBody[i-1][splitedDecryptedBody[i-1].size()-1] == '\\')
			decryptedBody += "\"" + splitedDecryptedBody[i];
		else
			decryptedBody += "\\\"" + splitedDecryptedBody[i];
	}

	// change \n for \\n (<br>)
	decryptedBody = replace(decryptedBody, "\n", "<br>");
	return decryptedBody;
}
