#ifndef _JsonError_h_
#define _JsonError_h_

#include <exception>
#include <string>

namespace json
{
	class JsonError : public std::exception
	{
	public:
		JsonError();
		~JsonError();

		const char* what() const throw();
	protected:
		char* fromStdString(const std::string str) const;

	private:

	};
}

#endif
