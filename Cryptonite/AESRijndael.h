#ifndef _AESRijndael_h_
#define _AESRijndael_h_

#include <cstring>
/* this class is borrowed from http://www.codeproject.com/script/Articles/ViewDownloads.aspx?aid=1380 */

namespace base
{
	class AESRijndael
	{
	public:
		AESRijndael();
		virtual ~AESRijndael();

		enum { ECB=0, CBC=1, CFB=2 };

		void makeKey(char const* key, char const* chain, int keylength, int blockSize=DEFAULT_BLOCK_SIZE);
		void encryptBlock(char const* in, char* result);
		void decryptBlock(char const* in, char* result);

		void encrypt(char const* in, char* result, size_t n, int iMode=ECB);
		void decrypt(char const* in, char* result, size_t n, int iMode=ECB);

		static char const* sm_chain0;

	private:

		enum { DEFAULT_BLOCK_SIZE = 16 };

		static int mul(int a, int b)
		{
			return (a != 0 && b != 0) ? sm_alog[(sm_log[a & 0xFF] + sm_log[b & 0xFF]) % 255] : 0;
		}

		void aesxor(char* buff, char const* chain)
		{
			if(m_bKeyInit==false)
				return;
			for(int i=0; i<m_blockSize; i++)
				*(buff++) ^= *(chain++);    
		}

		void defEncryptBlock(char const* in, char* result);
		void defDecryptBlock(char const* in, char* result);

		void resetChain()
		{
			memcpy(m_chain, m_chain0, m_blockSize);
		}

		static const int sm_alog[256];
		static const int sm_log[256];
		static const char sm_S[256];
		static const char sm_Si[256];
		static const int sm_T1[256];
		static const int sm_T2[256];
		static const int sm_T3[256];
		static const int sm_T4[256];
		static const int sm_T5[256];
		static const int sm_T6[256];
		static const int sm_T7[256];
		static const int sm_T8[256];
		static const int sm_U1[256];
		static const int sm_U2[256];
		static const int sm_U3[256];
		static const int sm_U4[256];
		static const char sm_rcon[30];
		static const int sm_shifts[3][4][2];

		// Key Initialization Flag
		bool m_bKeyInit;
		// Encryption (m_Ke) round key
		int** m_Ke;
		// Decryption (m_Kd) round key
		int** m_Kd;
		// Key Length
		int m_keylength;
		// Block Size
		int m_blockSize;
		// Number of Rounds
		int m_iROUNDS;
		// Chain Block
		char* m_chain0;
		char* m_chain;
	};
}

#endif
