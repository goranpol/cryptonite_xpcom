#ifndef _HttpObserver_h_
#define _HttpObserver_h_

#include "nsIUploadChannel.h"
#include "nsIUploadChannel2.h"
#include "nsIObserver.h"
#include "nsIHttpChannel.h"
#include "nsITraceableChannel.h"
#include "nsStringAPI.h"
#include "HttpListener.h"
#include <vector>

#include "GUIObserver.h"

namespace base
{
	class HttpObserver: public nsIObserver
	{
	public:
		HttpObserver(bool encrypt);
			
		NS_DECL_ISUPPORTS
		NS_DECL_NSIOBSERVER

	protected:
		virtual ~HttpObserver();

		std::string readSendingData(nsIUploadChannel *aUploadChannel);
		void replaceSendingData(nsIUploadChannel *aUploadChannel, std::string encryptedMessage);
		void replaceStreamListener(nsITraceableChannel *traceableChannel, base::HttpListener *listener);

		nsresult getUri(nsIHttpChannel *httpChannel, nsACString& url);

		virtual void changeUsersWithAccess(std::string data);

		/* header helper functions */
		std::string getRequestHeader(nsIHttpChannel *httpChannel, const std::string header);
		void setRequestHeader(nsIHttpChannel *httpChannel, const std::string header, const std::string value);
		std::string getResponseHeader(nsIHttpChannel *httpChannel, const std::string header);
		void setResponseHeader(nsIHttpChannel *httpChannel, const std::string header, const std::string value);

		/* string helper functions */
		std::vector<std::string> split(const std::string str, const std::string delimiters = " ");
		std::string replace(const std::string str, const std::string from, const std::string to);

		nsAutoCString requestMethod;
		nsAutoCString contentType;
		bool empty;
		bool doEncrypt;
	};
}

#endif