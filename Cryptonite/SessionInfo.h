#ifndef _SessionInfo_h_
#define _SessionInfo_h_

#include <string>
#include "nsISupports.h"

namespace base
{
	class SessionInfo
	{
	public:
		SessionInfo(std::string sessKey);
		~SessionInfo();

		void setPersistentCookie(std::string persistentCookie);
		std::string getPersistentCookie();

		std::string getSessionKey();

	private:
		std::string sessionKey;

		// std::string sessionPublicKey;		// maybe not needed
		// std::string sessionPrivateKey;		// maybe not needed
	};
}

#endif