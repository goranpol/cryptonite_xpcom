#ifndef _HotmailNew_h
#define _HotmailNew_h

#include "UsersList.h"
#include "Mail.h"

#include "HotmailNewUIWrapper.h"

namespace hotmail
{
	class HotmailNew : public base::Mail
	{
	public:
		HotmailNew();
		HotmailNew(const std::string objId, const std::string objDraftId);
		HotmailNew(const std::string objId, const std::string objKey, const std::string objInitChain, const std::string objCheckWord);
		HotmailNew(const std::string objId, const std::string objKey, const std::string objInitChain, const std::string objCheckWord, const std::string objDraftId);
		HotmailNew(const std::string objId, const std::string objKey, const std::string objInitChain, const std::string objCheckWord, const std::string objDraftId, std::vector<std::string> objAttachmentIds);
		HotmailNew(const Object &obj);
		HotmailNew(const HotmailNew &obj);
		~HotmailNew();

		HotmailNew &operator=(Object &obj);
		HotmailNew &operator=(HotmailNew &obj);
		bool operator==(Object &obj);
		bool operator!=(Object &obj);

		std::string encryptOutgoingString(std::string data);
		std::string decryptIncomingString(std::string data);

		friend class HotmailNewUIWrapper;

	private:

		void decryptFull(json::Value &jsonFull);
		void decryptSummary(json::Value &jsonSummary);
		void decryptReply(json::Value &jsonReply);
		void decryptDraftFull(json::Value &jsonDraftFull);
		void decryptDraftSummary(json::Value &jsonDraftSummary);

		std::string decryptSubject(std::string subject);
		std::string decryptBody(std::string body);
	};
}

#endif
