#include <mozilla/Char16.h>

#include "nsStringAPI.h"

#include "EscapeEncoding.h"
#include <cctype>

#include <sstream>
#include <stdexcept>
#include <iomanip>
#include <algorithm>


std::string base::EscapeEncoding::charToHex(unsigned char c, bool caps)
{
	short i = c;
	std::stringstream s;

	s << "%" << std::setw(2) << std::setfill('0') << std::hex << i;

	std::string returnString = s.str();
	if (caps)
	{
		std::transform(returnString.begin(), returnString.end(), returnString.begin(), ::toupper);
		return returnString;
	}
	else
	{
		std::transform(returnString.begin(), returnString.end(), returnString.begin(), ::tolower);
		return returnString;
	}
}

unsigned char base::EscapeEncoding::hexToChar(const std::string str)
{
	short c = 0;

	if (!str.empty())
	{
		std::istringstream in(str);
		in >> std::hex >> c;

		if (in.fail())
		{
			throw std::runtime_error("stream decode failure");
		}
	}

	return static_cast<unsigned char>(c);
}

std::string base::EscapeEncoding::encode(std::string str, bool caps)
{
	std::ostringstream out;

	for (std::string::size_type i = 0; i < str.length(); ++i)
	{
		short t = str[i];
		
		// t == 45						==>	-
		// t >= 48 && t <= 57			==>	0-9
		// t >= 65 && t <= 90			==>	A-Z
		// t == 95						==>	_
		// t >= 97 && t <= 122			==>	a-z
		// t == 126						==> ~
		// t == 46						==> .
		if (t == 45 || (t >= 48 && t <= 57) || (t >= 65 && t <= 90) || t == 95 || (t >= 97 && t <= 122) || t == 126 || t == 46)
		{
			out << str[i];
		}
		else
		{
			out << charToHex(str[i], caps);
		}
	}

	return out.str();
}

std::string base::EscapeEncoding::decode(std::string str)
{
	std::ostringstream out;

	for (std::string::size_type i = 0; i < str.length(); ++i)
	{
		if (str[i] == '%')
		{
			if ((i == str.length() - 1) || (i == str.length() - 2))
			{
				out << str.substr(i);
				break;
			}

			std::string tmp(str.substr(i + 1, 2));
			out << hexToChar(tmp);
			i += 2;
		}
		else
		{
			out << str[i];
		}
	}

	return out.str();
}
