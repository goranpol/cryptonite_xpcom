#include <mozilla/Char16.h>

#include "ObjectRequest.h"

#include "Parser.h"

#include "ObjectRequestException.h"
#include "Logger.h"

base::ObjectRequest::ObjectRequest(const std::string objId) : ooid(objId), checkWord("")
{
	if(objId.empty())
		throw exception::domain::ObjectRequestException();
}

base::ObjectRequest::ObjectRequest(const std::string objId, const std::string chWd) : ooid(objId), checkWord(chWd)
{
	if(objId.empty() || chWd.empty())
		throw exception::domain::ObjectRequestException();
}

base::ObjectRequest::~ObjectRequest()
{
}

base::ObjectRequest &base::ObjectRequest::operator=(const base::ObjectRequest &objReq)
{
	if (this == &objReq)
		return *this;

	const_cast<std::string&>(ooid) = objReq.ooid;
	const_cast<std::string&>(checkWord) = objReq.checkWord;

	return *this;
}

json::Value &base::ObjectRequest::toJson() const
{
	json::JSON jsonObjReq = json::JSON(ObjectType);
	
	jsonObjReq.pushBack("ooid", ooid);
	if(!checkWord.empty())
		jsonObjReq.pushBack("cw", checkWord);

	return jsonObjReq.get();
}

std::string base::ObjectRequest::getOoid() const
{
	return ooid;
}

std::string base::ObjectRequest::getCheckWord() const
{
	return checkWord;
}
