#ifndef _HotmailUIWrapper_h_
#define _HotmailUIWrapper_h_

#include "Parser.h"
#include "MailUIWrapper.h"
#include "Hotmail.h"

#include <string>
#include <vector>

#define NITE_decryptHtml			0
#define NITE_decryptJavascript	1
#define NITE_decryptCompose		2

#define NITE_draft	0
#define NITE_mail	1

namespace hotmail
{
	class HotmailUIWrapper : public base::MailUIWrapper
	{
	public:
		HotmailUIWrapper();
		~HotmailUIWrapper();

		std::string encryptOutgoingString(std::string data, int encryptType);
		std::string decryptIncomingString(std::string data, const int decryptType);
		void changeUsersWithAccess(std::string data){};

		void getSentDraftObjectID(std::string data);
		void getSentMail();

		void deleteMails(std::string data);

	private:
		std::string encryptDraft(std::string data);

		void decryptArray(json::Value &jsonArray);
		void decryptComposeData(json::Value &jsonData);

		/*
		Helper function that searches the supplied string for replyToMail id. 
		It should be in 10th place when splitting with char ','
		*/
		std::string getReplyToMail(std::string data);
		std::string getDraftId(std::string data);
		std::string getReplyToMailDraft(std::string data);
		std::string getDraftIdDraft(std::string data);
		std::vector<std::string> getAttachments(std::string data);
		
		std::string encryptCheck(const std::string subject);

		std::string getId(std::string id);
	};
}

#endif
