#ifndef _ObjectRequest_h_
#define _ObjectRequest_h_

#include <string>
#include "Value.h"
#include "UsersList.h"

namespace base
{
	class ObjectRequest
	{
	public:
		ObjectRequest(const std::string objId);
		ObjectRequest(const std::string objId, const std::string chWd);
		~ObjectRequest();

		ObjectRequest &operator=(const ObjectRequest &objReq);

		json::Value &toJson() const;
		std::string getOoid() const;
		std::string getCheckWord() const;

	private:

		/* ObjectRequest members */
		const std::string ooid;
		const std::string checkWord;
	};
}

#endif