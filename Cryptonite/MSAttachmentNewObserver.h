#ifndef _MSAttachmentNewObserver_h_
#define _MSAttachmentNewObserver_h_

#include "HttpObserver.h"

namespace hotmail
{
	class MSAttachmentNewObserver : public base::HttpObserver
	{
	public:
		MSAttachmentNewObserver(bool encrypt);

		NS_DECL_ISUPPORTS
		NS_DECL_NSIOBSERVER

	protected:

	private:
		~MSAttachmentNewObserver();
	};
}

#endif
