#ifndef _AsyncListener_h_
#define _AsyncListener_h_

#include "nsIStreamListener.h"
#include "nsIChannelEventSink.h"

#include <string>

namespace network
{
	class AsyncListener : public nsIStreamListener
	{
	public:
		AsyncListener(const std::string msg);

		NS_DECL_ISUPPORTS
		NS_DECL_NSISTREAMLISTENER 
		NS_DECL_NSIREQUESTOBSERVER

	private:
		~AsyncListener();

		char *mData;
		PRUint32 mRead;
		const std::string message;
	};
}

#endif
