#include <mozilla/Char16.h>

#include "Hotmail.h"

#include "ObjectNotFoundException.h"
#include "NotImplementedException.h"
#include "AESException.h"
#include "AlgorithmException.h"
#include "ObjectTypeException.h"

#include "Logger.h"

hotmail::Hotmail::Hotmail() : base::Mail("-1", generateKey(), generateKey(), generateCheckWord(), NITE_HOTMAIL)
{
}

hotmail::Hotmail::Hotmail(const std::string objDraftId) : base::Mail("-1", generateKey(), generateKey(), generateCheckWord(), NITE_HOTMAIL, objDraftId)
{
}

hotmail::Hotmail::Hotmail(const std::string objDraftId, std::vector<std::string> objAttachmentIds) : base::Mail("-1", generateKey(), generateKey(), generateCheckWord(), NITE_HOTMAIL, objDraftId, objAttachmentIds)
{
}

hotmail::Hotmail::Hotmail(const std::string objId, const std::string objKey, const std::string objInitChain, const std::string objCheckWord) : base::Mail(objId, objKey, objInitChain, objCheckWord, NITE_HOTMAIL)
{
}

hotmail::Hotmail::Hotmail(const std::string objId, const std::string objKey, const std::string objInitChain, const std::string objCheckWord, const std::string objDraftId) : base::Mail(objId, objKey, objInitChain, objCheckWord, NITE_HOTMAIL, objDraftId)
{
}

hotmail::Hotmail::Hotmail(const std::string objId, const std::string objKey, const std::string objInitChain, const std::string objCheckWord, const std::string objDraftId, std::vector<std::string> objAttachmentIds) : base::Mail(objId, objKey, objInitChain, objCheckWord, NITE_HOTMAIL, objDraftId, objAttachmentIds)
{
}

hotmail::Hotmail::Hotmail(const base::Object &obj) : base::Mail(obj.getObjectId(), obj.getObjectKey(), getInitChain(obj), getCheckWord(obj), NITE_HOTMAIL)
{
}

hotmail::Hotmail::Hotmail(const hotmail::Hotmail &obj) : base::Mail(obj.objectId, obj.objectKey, obj.objectInitChain, obj.objectCheckWord, NITE_HOTMAIL, obj.objectDraftId)
{
	for (size_t i = 0; i<obj.users.size(); i++)
		users.pushBack(obj.users[i]);

	for (size_t i = 0; i<obj.attachments.size(); i++)
		attachments.push_back(obj.attachments[i]);
}

hotmail::Hotmail::~Hotmail()
{
}

hotmail::Hotmail &hotmail::Hotmail::operator=(base::Object &obj)
{
	if (obj.getObjectType() != NITE_HOTMAIL)
		throw exception::domain::ObjectTypeException(NITE_HOTMAIL, obj.getObjectType());

	hotmail::Hotmail hotMail = static_cast<hotmail::Hotmail&>(obj);

	if (this == &hotMail)
		return *this;

	const_cast<int&>(objectType) = NITE_HOTMAIL;
	const_cast<std::string&>(objectId) = hotMail.objectId;
	const_cast<std::string&>(objectKey) = hotMail.objectKey;
	const_cast<std::string&>(objectInitChain) = hotMail.objectInitChain;
	const_cast<std::string&>(objectCheckWord) = hotMail.objectCheckWord;

	return *this;
}

hotmail::Hotmail &hotmail::Hotmail::operator=(hotmail::Hotmail &obj)
{
	if (this == &obj)
		return *this;

	const_cast<int&>(objectType) = NITE_HOTMAIL;
	const_cast<std::string&>(objectId) = obj.objectId;
	const_cast<std::string&>(objectKey) = obj.objectKey;
	const_cast<std::string&>(objectInitChain) = obj.objectInitChain;
	const_cast<std::string&>(objectCheckWord) = obj.objectCheckWord;

	return *this;
}

bool hotmail::Hotmail::operator==(base::Object &obj)
{
	if (objectType != NITE_HOTMAIL)
		return false;

	hotmail::Hotmail hotMail = static_cast<hotmail::Hotmail&>(obj);

	if (objectId.compare(hotMail.objectId) == 0 && objectKey.compare(hotMail.objectKey) == 0 && objectInitChain.compare(hotMail.objectInitChain) == 0 && objectCheckWord.compare(hotMail.objectCheckWord) == 0)
		return true;

	return false;
}

bool hotmail::Hotmail::operator !=(base::Object &obj)
{
	return !(*this == obj);
}

std::string hotmail::Hotmail::encryptOutgoingString(std::string data)
{
	// 0 - to whom you are sending
	// 1 - who is sending
	// 5 - subject
	// 6 - mail body
	// 8 - draft id	// allways delete this one
	// 10 - replying mail id // if not equal "mgAAAAAAAAAAAAAAAAAAAAAA2"
	std::string returnString = "";
	std::vector<std::string> splittedTemp = split(data, ",");
	std::vector<std::string> splitted = std::vector<std::string>();
	std::string temp = "";

	for (size_t i = 0; i<splittedTemp.size(); i++)
	{
		if(splittedTemp[i][splittedTemp[i].size()-1] == '\\')
		{
			temp += splittedTemp[i] + ",";
		}
		else
		{
			splitted.push_back(temp + splittedTemp[i]);
			temp = "";
		}
	}

	for (size_t i = 0; i<splitted.size(); i++)
	{
		if (i == 0)
		{
			// get the adresses to whom you are sending the mail
			std::vector<std::string> splittedReceivers = split(splitted[i], ";");
			for (size_t j = 0; j<splittedReceivers.size(); j++)
			{
				users.pushBack(splittedReceivers[j].substr(splittedReceivers[j].find("<") + 1, splittedReceivers[j].find(">") - splittedReceivers[j].find("<") - 1));
			}

			returnString += splitted[i] + ",";
		}
		else if(i == 5)
		{
			// encryptSubject
			returnString += "\"";
			if (splitted[i].find("\"RE\\: ") == 0)
			{
				returnString += "RE\\: ";
				splitted[i] = splitted[i].substr(6);
				returnString += encryptSubject(splitted[i].substr(0, splitted[i].size() - 1)) + "\",";
			}
			else if (splitted[i].find("\"FW\\: ") == 0)
			{
				returnString += "FW\\: ";
				splitted[i] = splitted[i].substr(6);
				returnString += encryptSubject(splitted[i].substr(0, splitted[i].size() - 1)) + "\",";
			}
			else
			{
				returnString += encryptSubject(splitted[i].substr(1, splitted[i].size() - 2)) + "\",";
			}
		}
		else if(i == 6)
			// encryptBody
			returnString += "\"" + encryptBody(splitted[i].substr(1, splitted[i].size() - 2)) + "\",";
		else if(i == splitted.size() - 1)
			returnString += splitted[i];
		else
			returnString += splitted[i] + ",";
	}

	return returnString;
}

std::string hotmail::Hotmail::decryptIncomingString(std::string data)
{
	throw std::exception();
}

std::string hotmail::Hotmail::encryptDraft(std::string data)
{
	std::string returnString = "";
	std::vector<std::string> splittedTemp = split(data, ",");
	std::vector<std::string> splitted = std::vector<std::string>();
	std::string temp = "";

	for (size_t i = 0; i<splittedTemp.size(); i++)
	{
		if(splittedTemp[i][splittedTemp[i].size()-1] == '\\')
		{
			temp += splittedTemp[i] + ",";
		}
		else
		{
			splitted.push_back(temp + splittedTemp[i]);
			temp = "";
		}
	}

	for (size_t i = 0; i<splitted.size(); i++)
	{
		if(i == 6)
		{
			// encryptSubject
			returnString += "\"";
			if (splitted[i].find("\"RE\\: ") == 0)
			{
				returnString += "RE\\: ";
				splitted[i] = splitted[i].substr(6);
				returnString += encryptSubject(splitted[i].substr(0, splitted[i].size() - 1)) + "\",";
			}
			else if (splitted[i].find("\"FW\\: ") == 0)
			{
				returnString += "FW\\: ";
				splitted[i] = splitted[i].substr(6);
				returnString += encryptSubject(splitted[i].substr(0, splitted[i].size() - 1)) + "\",";
			}
			else
			{
				returnString += encryptSubject(splitted[i].substr(1, splitted[i].size() - 2)) + "\",";
			}
		}
		else if(i == 7)
			// encryptBody
			returnString += "\"" + encryptBody(splitted[i].substr(1, splitted[i].size() - 2)) + "\",";
		else if(i == splitted.size() - 1)
			returnString += splitted[i];
		else
			returnString += splitted[i] + ",";
	}

	return returnString;
}

void hotmail::Hotmail::decryptRollup(json::Value &jsonRollup)
{
	std::string subject1 = replace(jsonRollup.getString(9), "\\x2a", "*");
	std::string subject2 = replace(jsonRollup.getString(10), "\\x26\\x2342\\x3b", "*");

	std::string decryptedSubject = decryptSubject(subject1);

	// replace so it has the right characters if needed
	jsonRollup.replace(9, decryptedSubject);
	jsonRollup.replace(10, decryptedSubject);
}

void hotmail::Hotmail::decryptMessage(json::Value &jsonMessage)
{
	std::string subject1 = replace(jsonMessage.getString(10), "\\x2a", "*");
	std::string subject2 = replace(jsonMessage.getString(11), "\\x26\\x2342\\x3b", "*");

	std::string decryptedSubject = decryptSubject(subject1);

	// replace so it has the right characters if needed
	jsonMessage.replace(10, decryptedSubject);
	jsonMessage.replace(11, decryptedSubject);
	
	jsonMessage.replace(12, "Cryptonite encrypted message...");

	try
	{
		std::string body = replace(jsonMessage.getString(53), "\\x2a", "*");
		std::string decryptedBody = decryptBody(body);

		// replace so it has the right characters if needed
		jsonMessage.replace(53, decryptedBody);
	}
	catch (...)
	{
	}
}

std::string hotmail::Hotmail::decryptSubject(std::string subject)
{
	std::string prefix = subject.substr(0, subject.find("*"));
	std::string postfix = subject.substr(subject.rfind("*"));
	subject = subject.substr(prefix.size(), subject.size() - prefix.size() - postfix.size());

	// replace " with \"
	std::string decryptedSubject = Mail::decryptSubject(prefix + subject + postfix);
	std::vector<std::string> splitedDecryptedSubject = split(decryptedSubject, "\"");
	decryptedSubject = splitedDecryptedSubject[0];
	for(size_t i=1; i<splitedDecryptedSubject.size(); i++)
	{
		if(splitedDecryptedSubject[i-1][splitedDecryptedSubject[i-1].size()-1] == '\\')
			decryptedSubject += "\"" + splitedDecryptedSubject[i];
		else
			decryptedSubject += "\\\"" + splitedDecryptedSubject[i];
	}

	return decryptedSubject;
}

std::string hotmail::Hotmail::decryptBody(std::string body)
{
	size_t first = body.find("*");
	size_t last = body.rfind("*") + 1;
	std::string prefix = body.substr(0, first);
	std::string postfix = body.substr(last);
	body = body.substr(first, last - first);

	// replace " with \"
	std::string decryptedBody = Mail::decryptBody(prefix + body + postfix);
	std::vector<std::string> splitedDecryptedBody = split(decryptedBody, "\"");
	decryptedBody = splitedDecryptedBody[0];
	for(size_t i=1; i<splitedDecryptedBody.size(); i++)
	{
		if(splitedDecryptedBody[i-1][splitedDecryptedBody[i-1].size()-1] == '\\')
			decryptedBody += "\"" + splitedDecryptedBody[i];
		else
			decryptedBody += "\\\"" + splitedDecryptedBody[i];
	}

	// change \n for \\n (<br>)
	decryptedBody = replace(decryptedBody, "\n", "<br>");
	return decryptedBody;
}