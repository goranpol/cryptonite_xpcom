#include <mozilla/Char16.h>

#include "GMailListener.h"

#include "Logger.h"

#include "GMailUIWrapper.h"
#include "Object.h"
#include "ObjectStorage.h"
#include "GDriveFile.h"

#include "Parser.h"

#include "NSException.h"
#include "AlgorithmException.h"

NS_IMPL_ISUPPORTS(gmail::GMailListener, nsIRequestObserver, nsIStreamListener)

gmail::GMailListener::GMailListener(const int type) : urlType(type), mail(false), requestId("")
{
}

gmail::GMailListener::GMailListener(const std::string requestID, const bool setMail) : urlType(NITE_SendMailOrDraft), mail(setMail), requestId(requestID)
{
}

gmail::GMailListener::~GMailListener()
{
}

NS_IMETHODIMP gmail::GMailListener::OnStartRequest(nsIRequest *aRequest, nsISupports *aContext)
{
	try
	{
		HttpListener::OnStartRequest(aRequest, aContext);
	}
	catch(exception::technical::NSException)
	{
		// notify javascript
	}

	return NS_OK;
}

NS_IMETHODIMP gmail::GMailListener::OnDataAvailable(nsIRequest *aRequest, nsISupports *aContext, nsIInputStream *aInputStream, uint64_t aOffset, uint32_t aCount)
{
	try
	{
		HttpListener::OnDataAvailable(aRequest, aContext, aInputStream, aOffset, aCount);
	}
	catch(exception::technical::NSException)
	{
		// notify javascript
	}

	return NS_OK;
}

NS_IMETHODIMP gmail::GMailListener::OnStopRequest(nsIRequest *aRequest, nsISupports *aContext, nsresult aStatusCode)
{	
	mData = (char*)NS_Realloc((void*)mData, sizeof(char) * (mRead + 1));
	if(!mData)
	{
		Logger::errorMessage("GMailListener", "OnStopRequest", "OnStopRequest FAILED: couldn't reallocate memory for mData", nsresult(-1));
		return NS_OK;
	}

	memcpy(mData + mRead, "\0", 1);

	if (urlType == NITE_SendMailOrDraft)
	{
		try
		{
			if(mail)
			{
				std::string decryptedMessage = gmail::GMailUIWrapper().getSentMailObjectID(requestId, mData);

				mDecryptedRead = decryptedMessage.size() + 1;
				mDecryptedData = (char*) NS_Alloc(sizeof(char) * mDecryptedRead);
				memcpy(mDecryptedData, decryptedMessage.c_str(), mDecryptedRead);
			}
			else
			{
				gmail::GMailUIWrapper().getSentDraftObjectID(requestId, mData);

				mDecryptedRead = mRead;
				mDecryptedData = (char*) NS_Alloc(sizeof(char) * mRead);
				memcpy(mDecryptedData, mData, mDecryptedRead);
			}
		}
		catch(exception::domain::AlgorithmException)
		{
			// notify javascript

			mDecryptedRead = mRead;
			mDecryptedData = (char*) NS_Alloc(sizeof(char) * mRead);
			memcpy(mDecryptedData, mData, mDecryptedRead);
		}
		catch(...)
		{
			// notify javascript

			mDecryptedRead = mRead;
			mDecryptedData = (char*) NS_Alloc(sizeof(char) * mRead);
			memcpy(mDecryptedData, mData, mDecryptedRead);
		}
		
	}
	else if (urlType == NITE_DecryptMail || urlType == NITE_DecryptMailSummary)
	{
		try
		{
			std::string decryptedData;

			if (urlType == NITE_DecryptMailSummary)
			{
				decryptedData = gmail::GMailUIWrapper().decryptIncomingString(mData, NITE_DecryptSummary);
			}
			else
			{
				// here it crashes sometimes
				decryptedData = gmail::GMailUIWrapper().decryptIncomingString(mData, NITE_Other);
			}

			mDecryptedRead = decryptedData.size();
			mDecryptedData = (char*) NS_Alloc(sizeof(char) * mDecryptedRead);
			memcpy(mDecryptedData, decryptedData.c_str(), mDecryptedRead);
		}
		catch(exception::domain::AlgorithmException)
		{
			// notify javascript

			mDecryptedRead = mRead;
			mDecryptedData = (char*)NS_Alloc(sizeof(char) * mRead);
			memcpy(mDecryptedData, mData, mDecryptedRead);
		}
		catch(...)
		{
			// notify javascript

			mDecryptedRead = mRead;
			mDecryptedData = (char*)NS_Alloc(sizeof(char) * mRead);
			memcpy(mDecryptedData, mData, mDecryptedRead);
		}
	}
	else if (urlType == NITE_AttachmentToDrive)
	{
		try
		{
			std::string data = mData;
			json::JSON responseJson(data);
			std::string id = responseJson["response"]["docs"][size_t(0)].getString("id");

			base::ObjectStorage::getInstance().addObject(gdrive::GDriveFile(id, "-1", "", ""), base::UsersList());
		}
		catch (...)
		{

		}

		mDecryptedRead = mRead;
		mDecryptedData = (char*)NS_Alloc(sizeof(char) * mRead);
		memcpy(mDecryptedData, mData, mDecryptedRead);
	}
	else
	{
		mDecryptedRead = mRead;
		mDecryptedData = (char*)NS_Alloc(sizeof(char) * mRead);
		memcpy(mDecryptedData, mData, mDecryptedRead);
	}	

	try
	{
		HttpListener::OnStopRequest(aRequest, aContext, aStatusCode);
	}
	catch(exception::technical::NSException)
	{
		// inform javascript
	}
	catch(...)
	{
		// inform javascript
	}

	return NS_OK;
}
