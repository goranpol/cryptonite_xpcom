#ifndef _HotmailNewListener_h
#define _HotmailNewListener_h

#include "HttpListener.h"

#define NITE_Summary			0
#define NITE_Full			1
#define NITE_Reply			2
#define NITE_SessionData		3
#define NITE_DraftSummary	4
#define NITE_DraftFull		5
#define NITE_attachment		6
#define NITE_Notification	7
#define NITE_MoveOrDelete	8

namespace hotmail
{
	class HotmailNewListener : public base::HttpListener
	{
	public:
		HotmailNewListener(const int type);

		NS_DECL_ISUPPORTS
		NS_DECL_NSISTREAMLISTENER
		NS_DECL_NSIREQUESTOBSERVER

	private:
		~HotmailNewListener();

		const int urlType;
	};
}

#endif
