#include <mozilla/Char16.h>

#include "ObjectStorage.h"
#include "Request.h"
#include "Logger.h"

#include "GDocument.h"
#include "GMail.h"
#include "Hotmail.h"
#include "GMailAttachment.h"
#include "HotmailAttachment.h"
#include "GDriveFile.h"
#include "OneDriveFile.h"
#include "HotmailNew.h"

#include "Parser.h"
#include "JsonTypeError.h"

#include "SingletoneException.h"
#include "ObjectNotFoundException.h"
#include "ObjectTypeException.h"
#include "UnknownException.h"
#include "ServerException.h"
#include "NSException.h"
#include "ObjectRequestException.h"

base::ObjectStorage *base::ObjectStorage::mObjectStorage = nullptr;

base::ObjectStorage::ObjectStorage(std::string sessKey) : session(*(new SessionInfo(sessKey)))
{
}

base::ObjectStorage::~ObjectStorage()
{
	objects.clear();
	tempObjects.clear();
	notEncryptedObjects.clear();

	delete mObjectStorage;
	mObjectStorage = nullptr;
}

base::ObjectStorage &base::ObjectStorage::getInstance()
{
	if(mObjectStorage == nullptr)
		throw exception::technical::SingletoneException("ObjectStorage");

	return *mObjectStorage;
}

void base::ObjectStorage::addObject(const base::Object &obj, const base::UsersList &users)
{
	/* try
	{
		// here is the problem
		if(obj.getObjectId().compare("-1") == 0)
		{
			objects.pushBack(obj);
			saveKey(obj, users);
		}
		else
			getObject(base::ObjectRequest(obj.getObjectId()), obj.getObjectType());
	}
	catch(...)
	{
		objects.pushBack(obj);
		saveKey(obj, users);
	} */

	if (obj.getObjectKey().compare("-1") != 0)
	{
		objects.pushBack(obj);
		for (size_t i = 0; i < notEncryptedObjects.size(); i++)
		{
			if (notEncryptedObjects[i].first.compare(obj.getObjectId()) == 0)
			{
				notEncryptedObjects.erase(notEncryptedObjects.begin() + i);
				break;
			}
		}
	}

	saveKey(obj, users);
}

void base::ObjectStorage::addObjects(base::ObjectList &objs, std::vector<UsersList> users)
{
	if(objs.empty())
		return;

	if(objs.size() != users.size())
	{
		// infrom javascript
		// return
		// throw base::ObjectsUsersLengthException();
	}

	std::vector<ObjectRequest> objReqs;
	for (size_t i = 0; i < objs.size(); i++)
	{
		if (objs[i].getObjectId().compare("-1") != 0)
			objReqs.push_back(objs[i].getObjectId());
	}

	base::ObjectList existingObjects;
	try
	{
		existingObjects = getObjects(objReqs, objs[0].getObjectType());
	}
	catch(exception::technical::NSException)
	{
		// do nothing
	}
	catch(exception::domain::ServerException)
	{
		// do nothing
	}
	catch(...)
	{
		// do nothing
	}

	size_t i = 0;
	while(i < objs.size())
	{
		try
		{
			existingObjects[objs[i].getObjectId()];
			objs.remove(i);
		}
		catch(exception::domain::ObjectNotFoundException)
		{
			i++;
			continue;
		}
	}

	objects += objs;
	try
	{
		saveKeys(objs, users);
	}
	catch(exception::technical::NSException)
	{
		// infrom javacript
	}
	catch(...)
	{
		// inform javascript
	}
}

base::Object &base::ObjectStorage::getObject(const base::ObjectRequest &objReq, const int objType)
{
	/********************************************************************************************************************************/
	// in notEncryptedObjects there are recent ids of objects that are not on server (so they are not encrypted)
	// here you check if objects and notEncryptedObjects have the same ooid in and erase it from notEncryptedObjects if they are

	// this is no good
	for (size_t i = 0; i<notEncryptedObjects.size(); i++)
	{
		if(notEncryptedObjects[i].first.compare(objReq.getOoid()) == 0 && notEncryptedObjects[i].second == objType)
		{
			try
			{
				objects[objReq.getOoid()];
				notEncryptedObjects.erase(notEncryptedObjects.begin() + i);
			}
			catch(exception::domain::ObjectNotFoundException)
			{
				break;
			}
		}
	}
	/********************************************************************************************************************************/

	try
	{
		return objects[objReq.getOoid()];
	}
	catch(exception::domain::ObjectNotFoundException)
	{
		try
		{
			return getKey(objReq, objType);
		}
		catch(exception::domain::ServerException &se)
		{
			notEncryptedObjects.push_back(std::pair<std::string, int>(objReq.getOoid(), objType));
			// inform javascript
			throw se;
		}
		catch(exception::domain::ObjectTypeException &ote)
		{
			notEncryptedObjects.push_back(std::pair<std::string, int>(objReq.getOoid(), objType));
			// inform javascript
			throw ote;
		}
		catch(exception::technical::NSException &nse)
		{
			notEncryptedObjects.push_back(std::pair<std::string, int>(objReq.getOoid(), objType));
			// infrom javacript
			throw nse;
		}
		catch(...)
		{
			notEncryptedObjects.push_back(std::pair<std::string, int>(objReq.getOoid(), objType));
			// inform javascript
			throw exception::domain::UnknownException("ObjectStorage::getObject");
		}
	}
	catch (...)
	{
		notEncryptedObjects.push_back(std::pair<std::string, int>(objReq.getOoid(), objType));
		throw exception::domain::UnknownException("ObjectStorage::getObject");
	}
}

base::ObjectList &base::ObjectStorage::getObjects(std::vector<ObjectRequest> objReqs, const int objType)
{
	/********************************************************************************************************************************/
	// in notEncryptedObjects there are recent ids of objects that are not on server (so they are not encrypted)
	// here you check if objects and notEncryptedObjects have the same ooid in and erase it from notEncryptedObjects if there are

	// this is no good
	for (size_t i = 0; i<notEncryptedObjects.size(); i++)
	{
		for (size_t j = 0; j<objReqs.size(); j++)
		{
			if(notEncryptedObjects[i].first.compare(objReqs[j].getOoid()) == 0 && notEncryptedObjects[i].second == objType)
			{
				try
				{
					objects[objReqs[j].getOoid()];
					notEncryptedObjects.erase(notEncryptedObjects.begin() + i);
				}
				catch(exception::domain::ObjectNotFoundException)
				{
					objReqs.erase(objReqs.begin() + j);
				}
			}
		}
	}
	/********************************************************************************************************************************/

	std::shared_ptr<base::ObjectList> objs = std::make_shared<base::ObjectList>();
	std::vector<ObjectRequest> remainingObjReqs;
	for (size_t i = 0; i<objReqs.size(); i++)
	{
		try
		{
			objs->pushBack(objects[objReqs[i].getOoid()]);
		}
		catch(exception::domain::ObjectNotFoundException)
		{
			remainingObjReqs.push_back(objReqs[i]);
		}
		catch(exception::domain::ObjectTypeException)
		{
			remainingObjReqs.push_back(objReqs[i]);
		}
		catch(...)
		{
			// maybe only this catch
			remainingObjReqs.push_back(objReqs[i]);
		}
	}

	try
	{
		if(!remainingObjReqs.empty())
		{
			*objs += getKeys(remainingObjReqs, objType);
		}
	}
	catch(exception::domain::ServerException &se)
	{
		// inform javascript
		throw se;
	}
	catch(exception::technical::NSException &nse)
	{
		// infrom javascript
		throw nse;
	}
	catch(json::JsonError &je)
	{
		Logger::errorMessage("ObjectStorage", "getObjects", (char*)je.what(), nsresult(1));
		throw je;
	}
	catch(...)
	{
		throw exception::domain::UnknownException("ObjectStorage::getObjects");
	}

	this->returnObjects = objs;

	if(objs->size() == objReqs.size())
		return *this->returnObjects;


	for (size_t i = 0; i<objReqs.size(); i++)
	{
		bool isInObjs = false;
		for (size_t j = 0; j<objs->size(); j++)
		{
			if(objs->get(j).getObjectId().compare(objReqs[i].getOoid()) == 0)
			{
				isInObjs = true;
				break;
			}
		}

		if(isInObjs == false)
		{
			notEncryptedObjects.push_back(std::pair<std::string, int>(objReqs[i].getOoid(), objType));
		}
	}

	return *this->returnObjects;
}

void base::ObjectStorage::removeObject(const base::ObjectRequest &objReq, const int objType)
{
	try
	{
		objects.remove(objReq.getOoid());
	}
	catch(exception::domain::ObjectNotFoundException)
	{
		// do nothing
	}
	catch(...)
	{
	}

	try
	{
		deleteKey(objReq, objType);
	}
	catch(exception::technical::NSException)
	{
		// inform javascript
	}
	catch(...)
	{
		// inform javascript unknown
	}
}

void base::ObjectStorage::removeObjects(const std::vector<ObjectRequest> objReqs, const int objType)
{
	if(objReqs.empty())
		return;

	for (size_t i = 0; i<objReqs.size(); i++)
	{
		try
		{
			objects.remove(objReqs[i].getOoid());
		}
		catch(exception::domain::ObjectNotFoundException)
		{
			
			continue;
		}
	}

	try
	{
		deleteKeys(objReqs, objType);
	}
	catch(exception::technical::NSException)
	{
		// infrom javascript
	}
	catch(...)
	{
		// inform javascript unknown
	}
}

void base::ObjectStorage::changeAccess(base::ObjectRequest objReq, base::UsersList &usrList, const std::string type, const int objType)
{
	try
	{
		changeUsersWithAccess(objReq, usrList, type, objType);
	}
	catch(exception::technical::NSException)
	{
		// infrom javascript
	}
	catch(...)
	{
		// inform javascript unknown
	}
}

void base::ObjectStorage::changeAccess(std::vector<ObjectRequest> objReqs, base::UsersList &usrList, const std::string type, const int objType)
{
	if(objReqs.empty())
		return;

	try
	{
		changeUsersWithAccess(objReqs, usrList, type, objType);
	}
	catch(exception::technical::NSException)
	{
		// inform javascript
	}
	catch(...)
	{
		// inform javascript unknown
	}
}

void base::ObjectStorage::addTempObject(base::Object &obj)
{
	tempObjects.pushBack(obj);
}

void base::ObjectStorage::removeTempObject(const std::string reqId)
{
	try
	{
		tempObjects.remove(reqId);
	}
	catch (...)
	{
		Logger::errorMessage("ObjectStorage", "removeTempObject", "Couldn't remove wrong id", nsresult(1));
	}
}

base::Object &base::ObjectStorage::getTempObject(const std::string reqId, int objType)
{
	if (objType == NITE_GMAIL)
	{
		// try catch maybe
		returnObject = std::make_shared<gmail::GMail>(static_cast<gmail::GMail&>(tempObjects[reqId]));
		return *returnObject;
	}
	else if (objType == NITE_HOTMAIL)
	{
		// try catch maybe ...
		returnObject = std::make_shared<hotmail::Hotmail>(static_cast<hotmail::Hotmail&>(tempObjects[reqId]));
		return *returnObject;
	}
	else if (objType == NITE_GATTACH)
	{
		returnObject = std::make_shared<gmail::GMailAttachment>(static_cast<gmail::GMailAttachment&>(tempObjects[reqId]));
		return *returnObject;
	}
	else if (objType == NITE_HOTATTACH)
	{
		returnObject = std::make_shared<hotmail::HotmailAttachment>(static_cast<hotmail::HotmailAttachment&>(tempObjects[reqId]));
		return *returnObject;
	}
	else if (objType == NITE_GDRIVEFILE)
	{
		returnObject = std::make_shared<gdrive::GDriveFile>(static_cast<gdrive::GDriveFile&>(tempObjects[reqId]));
		return *returnObject;
	}
	else if (objType == NITE_ONEDRIVEFILE)
	{
		returnObject = std::make_shared<onedrive::OneDriveFile>(static_cast<onedrive::OneDriveFile&>(tempObjects[reqId]));
		return *returnObject;
	}
	else if (objType == NITE_HOTMAILNEW)
	{
		returnObject = std::make_shared<hotmail::HotmailNew>(static_cast<hotmail::HotmailNew&>(tempObjects[reqId]));
		return *returnObject;
	}
	// others as they come

	throw exception::domain::ObjectNotFoundException(reqId);
}

void base::ObjectStorage::saveKey(const base::Object &obj, base::UsersList userList)
{
	json::JSON saveKeyJson = json::JSON(ObjectType);
	saveKeyJson.pushBack("rty", "nk");
	saveKeyJson.pushBack("sek", session.getSessionKey());
	saveKeyJson.pushBackArray("objs");

	saveKeyJson["objs"].pushBackObject();
	saveKeyJson["objs"][(size_t)0].pushBack("obj", obj.toJson());
	saveKeyJson["objs"][(size_t)0].pushBack("uwa", userList.toJson());

	network::Request(saveKeyJson.print()).sendAsyncRequest();
}

void base::ObjectStorage::saveKeys(base::ObjectList &objList, std::vector<UsersList> usrsList)
{
	json::JSON saveKeysJson = json::JSON(ObjectType);
	saveKeysJson.pushBack("rty", "nk");
	saveKeysJson.pushBack("sek", session.getSessionKey());
	saveKeysJson.pushBackArray("objs");

	for (size_t i = size_t(0); i<objList.size(); i++)
	{
		saveKeysJson["objs"].pushBackObject();
		saveKeysJson["objs"][i].pushBack("obj", objList[i].toJson());
		saveKeysJson["objs"][i].pushBack("uwa", usrsList[i].toJson());
	}

	network::Request(saveKeysJson.print()).sendAsyncRequest();
}

base::Object &base::ObjectStorage::getKey(const base::ObjectRequest &objReq, const int objType)
{
	json::JSON getKeyJson = json::JSON(ObjectType);
	getKeyJson.pushBack("rty", "gk");
	getKeyJson.pushBack("sek", session.getSessionKey());
	getKeyJson.pushBack("ot", objType);
	getKeyJson.pushBackArray("objs");

	getKeyJson["objs"].pushBack(objReq.toJson());
	
	std::string response = network::Request(getKeyJson.print()).sendRequest();
	json::JSON responseJson = json::JSON(response);
	try
	{
		if (objType == NITE_GDOC)
		{
			returnObject = std::make_shared<gdocument::GDocument>(objReq.getOoid(), responseJson[(size_t)0].getString("k"));
			objects.pushBack(*returnObject);
			return *returnObject;
		}
		else if (objType == NITE_GMAIL)
		{
			returnObject = std::make_shared<gmail::GMail>(objReq.getOoid(), responseJson[(size_t)0].getString("k"), responseJson[(size_t)0]["od"].getString("ic"), objReq.getCheckWord());
			objects.pushBack(*returnObject);
			return *returnObject;
		}
		else if (objType == NITE_HOTMAIL)
		{
			returnObject = std::make_shared<hotmail::Hotmail>(objReq.getOoid(), responseJson[(size_t)0].getString("k"), responseJson[(size_t)0]["od"].getString("ic"), objReq.getCheckWord());
			objects.pushBack(*returnObject);
			return *returnObject;
		}
		else if (objType == NITE_GATTACH)
		{
			returnObject = std::make_shared<gmail::GMailAttachment>(objReq.getOoid(), responseJson[(size_t)0].getString("k"), responseJson[(size_t)0]["od"].getString("ic"), objReq.getCheckWord());
			objects.pushBack(*returnObject);
			return *returnObject;
		}
		else if (objType == NITE_HOTATTACH)
		{
			returnObject = std::make_shared<hotmail::HotmailAttachment>(objReq.getOoid(), responseJson[(size_t)0].getString("k"), responseJson[(size_t)0]["od"].getString("ic"), objReq.getCheckWord());
			objects.pushBack(*returnObject);
			return *returnObject;
		}
		else if (objType == NITE_GDRIVEFILE)
		{
			returnObject = std::make_shared<gdrive::GDriveFile>(objReq.getOoid(), responseJson[(size_t)0].getString("k"), objReq.getCheckWord(), responseJson[(size_t)0]["od"].getString("ic"));
			objects.pushBack(*returnObject);
			return *returnObject;
		}
		else if (objType == NITE_ONEDRIVEFILE)
		{
			returnObject = std::make_shared<onedrive::OneDriveFile>(objReq.getOoid(), responseJson[(size_t)0].getString("k"), objReq.getCheckWord() , responseJson[(size_t)0]["od"].getString("ic"));
			objects.pushBack(*returnObject);
			return *returnObject;
		}
		else if (objType == NITE_HOTMAILNEW)
		{
			returnObject = std::make_shared<hotmail::HotmailNew>(objReq.getOoid(), responseJson[(size_t)0].getString("k"), responseJson[(size_t)0]["od"].getString("ic"), objReq.getCheckWord());
			objects.pushBack(*returnObject);
			return *returnObject;
		}
		// do others as they come
		else
			throw exception::domain::ObjectTypeException(objType);
	}
	catch(json::JsonTypeError)
	{
		Logger::errorMessage("ObjectStorage", "getKey", "json type error", nsresult(1));
		throw exception::domain::ServerException(responseJson.getInt("errn"), responseJson.getString("errm"));
	}
	catch (...)
	{
		Logger::errorMessage("ObjectStorage", "getKey", (char*)objReq.getOoid().c_str(), nsresult(2));
		throw exception::domain::ObjectNotFoundException(objReq.getOoid(), objReq.getCheckWord());
	}
}

base::ObjectList &base::ObjectStorage::getKeys(std::vector<ObjectRequest> objReqs, const int objType)
{
	json::JSON getKeysJson = json::JSON(ObjectType);
	getKeysJson.pushBack("rty", "gk");
	getKeysJson.pushBack("sek", session.getSessionKey());
	getKeysJson.pushBack("ot", objType);
	getKeysJson.pushBackArray("objs");

	for (size_t i = 0; i < objReqs.size(); i++)
	{
		getKeysJson["objs"].pushBack(objReqs[i].toJson());
	}

	std::string response = network::Request(getKeysJson.print()).sendRequest();
	json::JSON responseJson = json::JSON(response);
	try
	{
		returnObjects = std::make_shared<ObjectList>();

		for (size_t i = 0; i<responseJson.size(); i++)
		{
			if(responseJson[i].empty())
			{
				Logger::errorMessage("ObjectStorage", "getKeys", (char*)objReqs[i].toJson().print().c_str(), nsresult(-1));
				continue;
			}

			if (objType == NITE_GDOC)
			{
				gdocument::GDocument gDoc(objReqs[i].getOoid(), responseJson[i].getString("k"));
				returnObjects->pushBack(gDoc);
				objects.pushBack(gDoc);
			}
			else if (objType == NITE_GMAIL)
			{
				gmail::GMail gMail(objReqs[i].getOoid(), responseJson[i].getString("k"), responseJson[i]["od"].getString("ic"), objReqs[i].getCheckWord());
				returnObjects->pushBack(gMail);
				objects.pushBack(gMail);
			}
			else if (objType == NITE_HOTMAIL)
			{
				hotmail::Hotmail hotmail(objReqs[i].getOoid(), responseJson[i].getString("k"), responseJson[i]["od"].getString("ic"), objReqs[i].getCheckWord());
				returnObjects->pushBack(hotmail);
				objects.pushBack(hotmail);
			}
			else if (objType == NITE_GATTACH)
			{
				gmail::GMailAttachment gAttachment(objReqs[i].getOoid(), responseJson[i].getString("k"), responseJson[i]["od"].getString("ic"), objReqs[i].getCheckWord());
				returnObjects->pushBack(gAttachment);
				objects.pushBack(gAttachment);
			}
			else if (objType == NITE_HOTATTACH)
			{
				hotmail::HotmailAttachment hotmailAttachment(objReqs[i].getOoid(), responseJson[i].getString("k"), responseJson[i]["od"].getString("ic"), objReqs[i].getCheckWord());
				returnObjects->pushBack(hotmailAttachment);
				objects.pushBack(hotmailAttachment);
			}
			else if (objType == NITE_GDRIVEFILE)
			{
				gdrive::GDriveFile gDriveFile(objReqs[i].getOoid(), responseJson[(size_t)0].getString("k"), objReqs[i].getCheckWord(), responseJson[(size_t)0]["od"].getString("ic"));
				returnObjects->pushBack(gDriveFile);
				objects.pushBack(gDriveFile);
			}
			else if (objType == NITE_ONEDRIVEFILE)
			{
				onedrive::OneDriveFile oneDriveFile(objReqs[i].getOoid(), responseJson[(size_t)0].getString("k"), objReqs[i].getCheckWord(), responseJson[(size_t)0]["od"].getString("ic"));
				returnObjects->pushBack(oneDriveFile);
				objects.pushBack(oneDriveFile);
			}
			else if (objType == NITE_HOTMAILNEW)
			{
				hotmail::HotmailNew hotmailNew(objReqs[i].getOoid(), responseJson[i].getString("k"), responseJson[i]["od"].getString("ic"), objReqs[i].getCheckWord());
				returnObjects->pushBack(hotmailNew);
				objects.pushBack(hotmailNew);
			}

			// do others as they come
			else
				// maybe log or not
				continue;
		}

		return *returnObjects;
	}
	catch(json::JsonTypeError)
	{
		Logger::errorMessage("ObjectStorage", "getKeys", "json type error", nsresult(1));
		throw exception::domain::ServerException(responseJson.getInt("errn"), responseJson.getString("errm"));
	}
	catch (...)
	{
		Logger::errorMessage("ObjectStorage", "getKeys", "...", nsresult(2));
		throw exception::domain::ObjectNotFoundException("more then one not found");
	}
}

void base::ObjectStorage::deleteKey(const base::ObjectRequest &objReq, const int objType)
{
	json::JSON deleteKeyJson = json::JSON(ObjectType);
	deleteKeyJson.pushBack("rty", "dk");
	deleteKeyJson.pushBack("sek", session.getSessionKey());
	deleteKeyJson.pushBackArray("objs");

	deleteKeyJson["objs"].pushBackObject();
	deleteKeyJson["objs"][(size_t)0].pushBack("ot", objType);
	deleteKeyJson["objs"][(size_t)0].pushBack("ooid", objReq.getOoid());

	network::Request(deleteKeyJson.print()).sendAsyncRequest();
}

void base::ObjectStorage::deleteKeys(const std::vector<ObjectRequest> objReqs, const int objType)
{
	json::JSON deleteKeysJson = json::JSON(ObjectType);
	deleteKeysJson.pushBack("rty", "dk");
	deleteKeysJson.pushBack("sek", session.getSessionKey());
	deleteKeysJson.pushBackArray("objs");

	for (size_t i = 0; i<objReqs.size(); i++)
	{
		deleteKeysJson["objs"].pushBackObject();
		deleteKeysJson["objs"][i].pushBack("ot", objType);
		deleteKeysJson["objs"][i].pushBack("ooid", objReqs[i].getOoid());
	}

	network::Request(deleteKeysJson.print()).sendAsyncRequest();
}

void base::ObjectStorage::changeUsersWithAccess(base::ObjectRequest objReq, base::UsersList &usrList, const std::string type, const int objType)
{
	json::JSON changeAccessJson = json::JSON(ObjectType);
	changeAccessJson.pushBack("rty", "cuwa");
	changeAccessJson.pushBack("sek", session.getSessionKey());
	changeAccessJson.pushBackArray("objs");

	changeAccessJson["objs"].pushBackObject();
	changeAccessJson["objs"][(size_t)0].pushBack("ooid", objReq.getOoid());
	changeAccessJson["objs"][(size_t)0].pushBack("t", type);
	changeAccessJson["objs"][(size_t)0].pushBackArray("uwa");

	for (size_t j = 0; j<usrList.size(); j++)
		changeAccessJson["objs"][(size_t)0]["uwa"].pushBack(usrList[j]);

	changeAccessJson.pushBack("ot", objType);

	network::Request(changeAccessJson.print()).sendAsyncRequest();
}

void base::ObjectStorage::changeUsersWithAccess(std::vector<ObjectRequest> objReqs, UsersList &usrList, const std::string type, const int objType)
{
	json::JSON changeAccessJson = json::JSON(ObjectType);
	changeAccessJson.pushBack("rty", "cuwa");
	changeAccessJson.pushBack("sek", session.getSessionKey());
	changeAccessJson.pushBackArray("objs");

	for (size_t i = 0; i<objReqs.size(); i++)
	{
		changeAccessJson["objs"].pushBackObject();
		changeAccessJson["objs"][i].pushBack("ooid", objReqs[i].getOoid());
		changeAccessJson["objs"][i].pushBack("t", type);
		changeAccessJson["objs"][i].pushBackArray("uwa");

		for (size_t j = 0; j<usrList.size(); j++)
			changeAccessJson["objs"][i]["uwa"].pushBack(usrList[j]);
	}

	changeAccessJson.pushBack("ot", objType);

	network::Request(changeAccessJson.print()).sendAsyncRequest();
}

/************splits string str with delimiters************/
std::vector<std::string> base::ObjectStorage::split(const std::string str, const std::string delimiters)
{
	std::string::size_type pos;
	std::string::size_type lastPos = 0;

	std::vector<std::string> result;
	while(true)
	{
		pos = str.find(delimiters, lastPos);

		if(pos == std::string::npos)
		{
			pos = str.size();
			if(pos != lastPos)
				result.push_back(str.substr(lastPos, pos - lastPos));
			else
				result.push_back(std::string(""));
				
			break;
		}
		else
		{
			if(pos != lastPos)
				result.push_back(str.substr(lastPos, pos - lastPos));
			else
				result.push_back(std::string(""));
		}

		lastPos = pos + delimiters.size();
	}

	return result;
}

