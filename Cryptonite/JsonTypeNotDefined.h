#ifndef _JsonTypeNotDefined_h_
#define _JsonTypeNotDefined_h_

#include "JsonError.h"

namespace json
{
	class JsonTypeNotDefined : public JsonError
	{
	public:
		JsonTypeNotDefined(const int typeNumber);
		~JsonTypeNotDefined();

		const char* what() const throw();

	private:
		/********************************************************************************************************/
		/************************************JsonTypeNotDefined members******************************************/
		/********************************************************************************************************/
		const int type;										/* error type */
		/********************************************************************************************************/
		/************************************JsonTypeNotDefined members******************************************/
		/********************************************************************************************************/

	};
}

#endif
