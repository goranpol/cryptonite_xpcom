#ifndef _EscapeEncoding_
#define _EscapeEncoding_

#include <string>

namespace base
{
	class EscapeEncoding
	{
	public:
		static std::string encode(std::string str, bool caps = false);
		static std::string decode(std::string str);
		
	private:
		static std::string charToHex(unsigned char c, bool caps = false);
		static unsigned char hexToChar(const std::string str);
	};
}

#endif
