#include <mozilla/Char16.h>

#include "ServerException.h"

exception::domain::ServerException::ServerException(const int errNum, const std::string errMsg) : errorNumber(errNum), errorMessage(errMsg)
{
}

exception::domain::ServerException::~ServerException() throw()
{
}

const char* exception::domain::ServerException::what() const throw()
{
	return "";
}