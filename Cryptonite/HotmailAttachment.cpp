#include <mozilla/Char16.h>

#include "HotmailAttachment.h"

#include "ObjectNotFoundException.h"
#include "NotImplementedException.h"
#include "AESException.h"
#include "AlgorithmException.h"
#include "ObjectTypeException.h"


hotmail::HotmailAttachment::HotmailAttachment(const std::string objId) : base::Attachment(objId, generateKey(), generateKey(), generateCheckWord(), NITE_HOTATTACH)
{
}

hotmail::HotmailAttachment::HotmailAttachment(const std::string objId, const std::string objKey, const std::string objInitChain, const std::string objCheckWord) : base::Attachment(objId, objKey, objInitChain, objCheckWord, NITE_HOTATTACH)
{
}

hotmail::HotmailAttachment::HotmailAttachment(const base::Object &obj) : base::Attachment(obj.getObjectId(), obj.getObjectKey(), getInitChain(obj), getCheckWord(obj), NITE_HOTATTACH)
{
}

hotmail::HotmailAttachment::HotmailAttachment(const hotmail::HotmailAttachment &obj) : base::Attachment(obj.objectId, obj.objectKey, obj.objectInitChain, obj.objectCheckWord, NITE_HOTATTACH)
{
}

hotmail::HotmailAttachment::~HotmailAttachment()
{
}

hotmail::HotmailAttachment &hotmail::HotmailAttachment::operator=(Object &obj)
{
	if (obj.getObjectType() != NITE_HOTATTACH)
		throw exception::domain::ObjectTypeException(NITE_HOTATTACH, obj.getObjectType());

	hotmail::HotmailAttachment hotmailAttachment = static_cast<hotmail::HotmailAttachment&>(obj);

	if (this == &hotmailAttachment)
		return *this;

	const_cast<int&>(objectType) = NITE_HOTATTACH;
	const_cast<std::string&>(objectId) = hotmailAttachment.objectId;
	const_cast<std::string&>(objectKey) = hotmailAttachment.objectKey;
	const_cast<std::string&>(objectInitChain) = hotmailAttachment.objectInitChain;
	const_cast<std::string&>(objectCheckWord) = hotmailAttachment.objectCheckWord;

	return *this;
}

hotmail::HotmailAttachment &hotmail::HotmailAttachment::operator=(HotmailAttachment &obj)
{
	if (this == &obj)
		return *this;

	const_cast<int&>(objectType) = NITE_HOTATTACH;
	const_cast<std::string&>(objectId) = obj.objectId;
	const_cast<std::string&>(objectKey) = obj.objectKey;
	const_cast<std::string&>(objectInitChain) = obj.objectInitChain;
	const_cast<std::string&>(objectCheckWord) = obj.objectCheckWord;

	return *this;
}

bool hotmail::HotmailAttachment::operator==(Object &obj)
{
	if (objectType != NITE_HOTATTACH)
		return false;

	hotmail::HotmailAttachment hotmailAttachment = static_cast<hotmail::HotmailAttachment&>(obj);

	if (objectId.compare(hotmailAttachment.objectId) == 0 && objectKey.compare(hotmailAttachment.objectKey) == 0 && objectInitChain.compare(hotmailAttachment.objectInitChain) == 0 && objectCheckWord.compare(hotmailAttachment.objectCheckWord) == 0)
		return true;

	return false;
}

bool hotmail::HotmailAttachment::operator!=(Object &obj)
{
	return !(*this == obj);
}


std::string hotmail::HotmailAttachment::getAttachmentId(std::string data)
{
	data = replace(data, "&#124;", "|");
	std::vector<std::string> splittedData = split(data, "|");

	return splittedData[2];
}