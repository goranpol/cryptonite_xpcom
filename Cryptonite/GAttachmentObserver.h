#ifndef _GAttachmentObserver_h_
#define _GAttachmentObserver_h_

#include "HttpObserver.h"

namespace gmail
{
	class GAttachmentObserver : public base::HttpObserver
	{
	public:
		GAttachmentObserver(bool encrypt);

		NS_DECL_ISUPPORTS
		NS_DECL_NSIOBSERVER

	protected:

	private:
		~GAttachmentObserver();
	};
}

#endif