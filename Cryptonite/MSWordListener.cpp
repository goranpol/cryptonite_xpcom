#include <mozilla/Char16.h>

#include "MSWordListener.h"

NS_IMPL_ISUPPORTS(msword::MSWordListener, nsIRequestObserver, nsIStreamListener)

msword::MSWordListener::MSWordListener()
{
}

msword::MSWordListener::~MSWordListener()
{
}

NS_IMETHODIMP msword::MSWordListener::OnStartRequest(nsIRequest *aRequest, nsISupports *aContext)
{
	return NS_OK;
}

NS_IMETHODIMP msword::MSWordListener::OnStopRequest(nsIRequest *aRequest, nsISupports *aContext, nsresult aStatusCode)
{
	return NS_OK;
}

NS_IMETHODIMP msword::MSWordListener::OnDataAvailable(nsIRequest *aRequest, nsISupports *aContext, nsIInputStream *aInputStream, uint64_t aOffset, uint32_t aCount)
{
	return NS_OK;
}