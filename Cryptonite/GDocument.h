#ifndef _GDocument_h
#define _GDocument_h

#include "Object.h"

namespace gdocument
{
	class GDocument : public base::Object
	{
	public:
		GDocument(const std::string objID);
		GDocument(const std::string objID, const std::string objKey);
		GDocument(Object &obj);
		GDocument(GDocument &obj);
		~GDocument();

		GDocument &operator=(Object &obj);
		bool operator==(Object &obj);
		bool operator!=(Object &obj);

		std::string encryptOutgoingString(std::string data);
		std::string decryptIncomingString(std::string data);

	private:
		std::string encryptSendingMessage(std::string message);

		void encryptIS(json::Value &object);
		void encryptDS(json::Value &object);
		void encryptAS(json::Value &object);
		void encryptMLTI(json::Value &object);

		void decryptIS(json::Value &object);
		void decryptDS(json::Value &object);
		void decryptASStart(json::Value  &object);
		void decryptASEnd(json::Value &object);
		void decryptAS(json::Value &object);
		
		int64_t computeStartIndexEncryption(int64_t index);
		int64_t computeEndIndexEncryption(int64_t index);
		int64_t computeStartIndexDecryption(int64_t index);
		int64_t computeEndIndexDecryption(int64_t index);

		std::string encryptText(std::string textString);
		std::string decryptText(std::string cipherTextString);

		std::string encrypt(std::string textString);
		std::string decrypt(std::string cipherTextString);
	};
}

#endif
