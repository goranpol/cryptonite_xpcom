#ifndef _MSWord_h
#define _MSWord_h

#include "Object.h"

namespace msword
{
	class MSWord : public base::Object
	{
	public:
		MSWord();
		~MSWord();

		MSWord &operator=(Object &obj);
		bool operator==(Object &obj);
		bool operator!=(Object &obj);

		std::string encryptOutgoingString(std::string data);
		std::string decryptIncomingString(std::string data);
		void changeUsersWithAccess(std::string data);

	protected:

	private:

		std::string encryptText(std::string textString);
		std::string decryptText(std::string cipherTextString);

		std::string encrypt(std::string textString);
		std::string decrypt(std::string cipherTextString);
	};
}

#endif
