#include <mozilla/Char16.h>

#include "GDocumentListener.h"

#include "GDocument.h"

#include "Logger.h"

#include "NSException.h"
#include "AlgorithmException.h"
#include "AESException.h"

NS_IMPL_ISUPPORTS(gdocument::GDocumentListener, nsIRequestObserver, nsIStreamListener)

gdocument::GDocumentListener::GDocumentListener() : GDocument("", "")
{
}

gdocument::GDocumentListener::GDocumentListener(base::Object &gDoc) : GDocument(*(new gdocument::GDocument(gDoc)))
{
}

gdocument::GDocumentListener::~GDocumentListener()
{
}

NS_IMETHODIMP gdocument::GDocumentListener::OnStartRequest(nsIRequest *aRequest, nsISupports *aContext)
{	
	try
	{
		HttpListener::OnStartRequest(aRequest, aContext);
	}
	catch(exception::technical::NSException)
	{
		// notify javascript
	}

	return NS_OK;
}

NS_IMETHODIMP gdocument::GDocumentListener::OnDataAvailable(nsIRequest *aRequest, nsISupports *aContext, nsIInputStream *aInputStream, uint64_t aOffset, uint32_t aCount)
{	
	try
	{
		HttpListener::OnDataAvailable(aRequest, aContext, aInputStream, aOffset, aCount);
	}
	catch(exception::technical::NSException)
	{
		// notify javascript
	}
	
	return NS_OK;
}

NS_IMETHODIMP gdocument::GDocumentListener::OnStopRequest(nsIRequest *aRequest, nsISupports *aContext, nsresult aStatusCode)
{
	mData = (char*)NS_Realloc((void*)mData, sizeof(char) * (mRead + 1));
	if(!mData)
	{
		// notify javascript
		Logger::errorMessage("GDocumentListener", "OnStopRequest", "OnStopRequest FAILED: couldn't reallocate memory for mData", nsresult(-1));
		return NS_OK;
	}

	memcpy(mData + mRead, "\0", 1);

	try
	{
		std::string decryptedData = GDocument.decryptIncomingString(mData);

		mDecryptedRead = decryptedData.size();
		mDecryptedData = (char*)NS_Alloc(sizeof(char) * mDecryptedRead);
		memcpy(mDecryptedData, decryptedData.c_str(), mDecryptedRead);
		
		HttpListener::OnStopRequest(aRequest, aContext, aStatusCode);
	}
	catch(exception::domain::AlgorithmException)
	{
		// infrom javascript

		mDecryptedRead = mRead;
		mDecryptedData = (char*)NS_Alloc(sizeof(char) * mRead);
		memcpy(mDecryptedData, mData, mDecryptedRead);
	}
	catch(exception::domain::AESException)
	{
		// infrom javascript

		mDecryptedRead = mRead;
		mDecryptedData = (char*)NS_Alloc(sizeof(char) * mRead);
		memcpy(mDecryptedData, mData, mDecryptedRead);
	}
	catch(exception::technical::NSException)
	{
		// notify javascript

		mDecryptedRead = mRead;
		mDecryptedData = (char*)NS_Alloc(sizeof(char) * mRead);
		memcpy(mDecryptedData, mData, mDecryptedRead);
	}
	catch(...)
	{
		// infrom javascript

		mDecryptedRead = mRead;
		mDecryptedData = (char*)NS_Alloc(sizeof(char) * mRead);
		memcpy(mDecryptedData, mData, mDecryptedRead);
	}

	return NS_OK;
}
