#ifndef _MSAttachmentListener_h
#define _MSAttachmentListener_h

#include "HttpListener.h"

#include <string>

namespace hotmail
{
	class MSAttachmentListener : public base::HttpListener
	{
	public:
		MSAttachmentListener();

		NS_DECL_ISUPPORTS
		NS_DECL_NSISTREAMLISTENER
		NS_DECL_NSIREQUESTOBSERVER

	private:
		~MSAttachmentListener();
	};
}

#endif