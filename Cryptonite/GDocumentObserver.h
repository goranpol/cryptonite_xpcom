#ifndef _GDocumentObserver_h_
#define _GDocumentObserver_h_

#include "GDriveObserver.h"

namespace gdocument
{
	class GDocumentObserver : public gdrive::GDriveObserver
	{
	public:
		GDocumentObserver(bool encrypt);

		NS_DECL_ISUPPORTS
		NS_DECL_NSIOBSERVER

	protected:

	private:
		~GDocumentObserver();

		std::string getDocumentId(std::string url);
	};
}

#endif
