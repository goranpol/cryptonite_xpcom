#include <mozilla/Char16.h>

#include "Object.h"

#include "nsIComponentManager.h"
#include "nsIRandomGenerator.h"
#include "nsCOMPtr.h"

#include "Logger.h"

#include "Parser.h"

#include "GDocument.h"
#include "GMail.h"
#include "Hotmail.h"

#include "NotImplementedException.h"
#include "ObjectNotFoundException.h"
#include "UnknownException.h"
#include "NSException.h"
#include "ObjectTypeException.h"

static const std::string base64Chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
										"abcdefghijklmnopqrstuvwxyz"
										"0123456789-_";


base::Object::Object(const int objType, const std::string objId, const std::string objKey) : objectType(objType), objectId(objId), objectKey(objKey)
{
}

base::Object::~Object()
{
}

base::Object &base::Object::operator=(base::Object &obj)
{
	if (this == &obj)
		return *this;

	const_cast<int&>(objectType) = obj.objectType;
	const_cast<std::string&>(objectId) = obj.objectId;
	const_cast<std::string&>(objectKey) = obj.objectKey;
}

bool base::Object::operator==(base::Object &obj)
{
	if (this->objectType != obj.objectType)
		return false;
	if (this->objectId.compare(obj.objectId) != 0)
		return false;
	if (this->objectKey.compare(obj.objectKey) != 0)
		return false;

	return true;
}

bool base::Object::operator!=(base::Object &obj)
{
	return !(*this == obj);
}

std::string base::Object::encryptOutgoingString(std::string data)
{
	throw exception::domain::NotImplementedException("base::Object", "encryptOutgoingString(std::string data)");
}

std::string base::Object::decryptIncomingString(std::string data)
{
	throw exception::domain::NotImplementedException("base::Object", "decryptIncomingString(std::string data)");
}

std::string base::Object::getObjectId() const
{
	return objectId;
}

std::string base::Object::getObjectKey() const
{
	return objectKey;
}

int base::Object::getObjectType() const
{
	return objectType;
}

std::string base::Object::encryptText(std::string textString)
{
	throw exception::domain::NotImplementedException("Object", "encryptText");
}

std::string base::Object::decryptText(std::string cipherTextString)
{
	throw exception::domain::NotImplementedException("Object", "decryptText");
}

std::string base::Object::encrypt(std::string textString)
{
	throw exception::domain::NotImplementedException("Object", "encrypt");
}

std::string base::Object::decrypt(std::string cipherTextString)
{
	throw exception::domain::NotImplementedException("Object", "decrypt");
}

char* base::Object::randomNumbers(const size_t num)
{
	std::string action = "generating random numbers";
	nsresult rv;
	nsCOMPtr<nsIComponentManager> componentManager;
	rv = NS_GetComponentManager(getter_AddRefs(componentManager));
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	nsCOMPtr<nsIRandomGenerator> randomGenerator;
	rv = componentManager->CreateInstanceByContractID("@mozilla.org/security/random-generator;1", nullptr, NS_GET_IID(nsIRandomGenerator), getter_AddRefs(randomGenerator));
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	PRUint8 *randomBytes;
	rv = randomGenerator->GenerateRandomBytes(num, &randomBytes);
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	// memory leak
	char *result = (char*)NS_Alloc(sizeof(char) * num + 1);
	memset(result, 0, num + 1);
	char *pbuf = result;

	for(size_t i=0; i<num; i++)
	{
		*pbuf = *randomBytes;
		randomBytes++;
		pbuf++;
	}
	*pbuf = '\0';

	return result;
}

std::string base::Object::generateKey()
{
	std::string action = "generating key";
	nsresult rv;
	nsCOMPtr<nsIComponentManager> componentManager;
	rv = NS_GetComponentManager(getter_AddRefs(componentManager));
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	nsCOMPtr<nsIRandomGenerator> randomGenerator;
	rv = componentManager->CreateInstanceByContractID("@mozilla.org/security/random-generator;1", nullptr, NS_GET_IID(nsIRandomGenerator), getter_AddRefs(randomGenerator));
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	PRUint8 *randomBytes;
	rv = randomGenerator->GenerateRandomBytes(32, &randomBytes);
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	std::string result("");

	for (size_t i = 0; i<32; i++)
	{
		result.push_back(*randomBytes);
		randomBytes++;
	}

	return toBase64(result, 32);
}

/************splits string str with delimiters and returns the vector of splited strings************/
std::vector<std::string> base::Object::split(const std::string str, const std::string delimiters)
{
	std::string::size_type pos;
	std::string::size_type lastPos = 0;

	std::vector<std::string> result;
	while(true)
	{
		pos = str.find(delimiters, lastPos);

		if(pos == std::string::npos)
		{
			pos = str.size();
			if(pos != lastPos)
				result.push_back(str.substr(lastPos, pos - lastPos));
			else
				result.push_back(std::string(""));
				
			break;
		}
		else
		{
			if(pos != lastPos)
				result.push_back(str.substr(lastPos, pos - lastPos));
			else
				result.push_back(std::string(""));
		}

		lastPos = pos + delimiters.size();
	}

	return result;
}

/* replaces from substrings with to substrings and returns changed string */
std::string base::Object::replace(const std::string str, const std::string from, const std::string to)
{
	if(str.find(from) == -1)
		return str;

	std::vector<std::string> strTmp = split(str, from);
	std::string returnString = strTmp[0];
	for(size_t i=1; i<strTmp.size(); i++)
	{
		returnString.append(to);
		returnString.append(strTmp[i]);
	}
	
	return returnString;
}

/* this function is borrowed from http://www.adp-gmbh.ch/cpp/common/base64.html and changed */
std::string base::Object::toBase64(const std::string input, size_t len)
{
	std::string ret;
	int i = 0;
	int j = 0;
	unsigned char char_array_3[3];
	unsigned char char_array_4[4];
	unsigned char *inputCString = (unsigned char*)input.c_str();
	
	while(len--)
	{
		char_array_3[i++] = *(inputCString++);
		if(i == 3)
		{
			char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
			char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
			char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
			char_array_4[3] = char_array_3[2] & 0x3f;

			for (i = 0; i < 4; i++)
				ret += base64Chars[char_array_4[i]];
			i = 0;
		}
	}

	if(i)
	{
		for(j = i; j < 3; j++)
			char_array_3[j] = '\0';

		char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
		char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
		char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
		char_array_4[3] = char_array_3[2] & 0x3f;

		for (j = 0; j < i + 1; j++)
			ret += base64Chars[char_array_4[j]];
	}

	return ret;
}

/* this function is borrowed from http://www.adp-gmbh.ch/cpp/common/base64.html and changed */
std::string base::Object::fromBase64(const std::string input)
{
	int len = input.size();
	int i = 0;
	int j = 0;
	int in_ = 0;
	unsigned char char_array_4[4];
	unsigned char char_array_3[3];
	std::string ret;

	while(len--)
	{
		char_array_4[i++] = input[in_];
		j++;

		in_++;
		if(i == 4)
		{
			for (i = 0; i < 4; i++)
				char_array_4[i] = base64Chars.find(char_array_4[i]);

			char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
			char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
			char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

			for (i = 0; i < j - 1; i++)
				ret += char_array_3[i];
			i = 0;
			j = 0;
		}
	}

	if(i)
	{
		for(j = i; j < 4; j++)
			char_array_4[j] = 0;

		for (j = 0; j < 4; j++)
			char_array_4[j] = base64Chars.find(char_array_4[j]);

		char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
		char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
		char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

		for (j = 0; j < i - 1; j++)
			ret += char_array_3[j];
	}

	return ret;
}

bool base::Object::isBase64(const std::string input)
{
	for (size_t i = 0; i < input.size(); i++)
	{
		if (isalnum(input[i]) == 0 && input[i] != '-' && input[i] != '_')
			return false;
	}

	return true;
}

json::Value &base::Object::toJson() const
{
	json::JSON jsonObj = json::JSON(ObjectType);
	jsonObj.pushBack("ooid", objectId);
	jsonObj.pushBack("k", objectKey);
	jsonObj.pushBack("ot", objectType);

	jsonObj.pushBackObject("od");
	jsonObj["od"].pushBack("ic");
	jsonObj["od"].pushBack("cw");

	return jsonObj.get();
}

