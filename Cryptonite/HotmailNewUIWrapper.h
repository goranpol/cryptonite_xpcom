#ifndef _HotmailNewUIWrapper_h_
#define _HotmailNewUIWrapper_h_

#include "MailUIWrapper.h"
#include "Parser.h"

#define NITE_DecryptSummary		0
#define NITE_DecryptFull			1
#define NITE_DecryptSessionData	2
#define NITE_DecryptReply		3
#define NITE_DecryptDraftSummary	4
#define NITE_DecryptDraftFull	5
#define NITE_DecryptNotification	6


namespace hotmail
{
	class HotmailNewUIWrapper : public base::MailUIWrapper
	{
	public:
		HotmailNewUIWrapper();
		~HotmailNewUIWrapper();

		std::string encryptOutgoingString(std::string data, int encryptType);
		std::string decryptIncomingString(std::string data, const int decryptType);
		void changeUsersWithAccess(std::string data){};

		void getAttachmentIds(std::string data);

		void deleteMails(std::string data);
		void moveMails(std::string data);
		void getNewIds(std::string data);

	private:
		void decryptSummary(json::Value &jsonData);
		void decryptFull(json::Value &jsonData);
		void decryptReply(json::Value &jsonData);
		void decryptDraftSummary(json::Value &jsonData);
		void decryptDraftFull(json::Value &jsonData);
		std::string decryptNotification(std::string data);

		std::string fixId(std::string id);
	};
}

#endif
