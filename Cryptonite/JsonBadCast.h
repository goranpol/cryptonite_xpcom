#ifndef _JsonBadCast_h_
#define _JsonBadCast_h_

#include "JsonError.h"

namespace json
{
	class JsonBadCast : public JsonError
	{
	public:
		JsonBadCast();
		~JsonBadCast();

		const char* what() const throw();

	private:

	};
}

#endif
