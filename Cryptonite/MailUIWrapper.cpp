#include <mozilla/Char16.h>

#include "MailUIWrapper.h"
#include "NotImplementedException.h"

#include "Logger.h"

base::MailUIWrapper::MailUIWrapper()
{
}

base::MailUIWrapper::~MailUIWrapper()
{
}

std::string base::MailUIWrapper::encryptCheck(const std::string subject)
{
	std::string subjectBuffer = subject;
	if(subjectBuffer.find("*") != -1)
	{
		subjectBuffer = subjectBuffer.substr(subjectBuffer.find("*") + 1);
		if(subjectBuffer.find("*") < 27 && subjectBuffer.find("*") > 13)
		{
			std::string chWd = subjectBuffer.substr(size_t(0), subjectBuffer.find("*"));
			if (isBase64(chWd))
				return chWd;
		}
	}

	throw std::exception();		// notEncryptedException
}

/************splits string str with delimiters and returns the vector of splited strings************/
std::vector<std::string> base::MailUIWrapper::split(const std::string str, const std::string delimiters)
{
	std::string::size_type pos;
	std::string::size_type lastPos = 0;

	std::vector<std::string> result;
	while(true)
	{
		pos = str.find(delimiters, lastPos);

		if(pos == std::string::npos)
		{
			pos = str.size();
			if(pos != lastPos)
				result.push_back(str.substr(lastPos, pos - lastPos));
			else
				result.push_back(std::string(""));
				
			break;
		}
		else
		{
			if(pos != lastPos)
				result.push_back(str.substr(lastPos, pos - lastPos));
			else
				result.push_back(std::string(""));
		}

		lastPos = pos + delimiters.size();
	}

	return result;
}

/* replaces from substrings with to substrings and returns changed string */
std::string base::MailUIWrapper::replace(const std::string str, const std::string from, const std::string to)
{
	if(str.find(from) == -1)
		return str;

	std::vector<std::string> strTmp = split(str, from);
	std::string returnString = strTmp[0];
	for(size_t i=1; i<strTmp.size(); i++)
	{
		returnString.append(to);
		returnString.append(strTmp[i]);
	}
	
	return returnString;
}

/* base64 helper functions */
bool base::MailUIWrapper::isBase64(const std::string input)
{
	for (size_t i = 0; i < input.size(); i++)
	{
		if (isalnum(input[i]) == 0 && input[i] != '-' && input[i] != '_')
			return false;
	}

	return true;
}
