#ifndef _OneDriveFile_h
#define _OneDriveFile_h

#include "DriveFile.h"

namespace onedrive
{
	class OneDriveFile : public base::DriveFile
	{
	public:
		OneDriveFile();
		OneDriveFile(const std::string objID, bool single = false);
		OneDriveFile(const std::string objID, const std::string objKey, const std::string checkWord, const std::string initChain);
		OneDriveFile(const base::Object &obj);
		OneDriveFile(const onedrive::OneDriveFile &obj);
		~OneDriveFile();

		OneDriveFile &operator=(Object &obj);
		OneDriveFile &operator=(OneDriveFile &obj);
		bool operator==(Object &obj);
		bool operator!=(Object &obj);

		std::string encryptOutgoingString(std::string data);
		std::string encryptText(std::string textString);
		std::string encrypt(std::string textString);

		/* for fragmented data */
		void setIsFirst(const bool val);
		void setIsLast(const bool val);

	private:

		/* for fragmented data */
		bool isSingle;
		bool isFirst;
		bool isLast;
		std::string lastBlock;
		std::string initBlock;
		std::string remainder;
	};
}

#endif