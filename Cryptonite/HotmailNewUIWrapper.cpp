#include <mozilla/Char16.h>

#include "HotmailNewUIWrapper.h"
#include "HotmailNew.h"
#include "HotmailAttachment.h"
#include "ObjectStorage.h"
#include "GUIObserver.h"
#include "ObjectRequest.h"

#include "ServerException.h"

#include "Logger.h"


hotmail::HotmailNewUIWrapper::HotmailNewUIWrapper()
{
}


hotmail::HotmailNewUIWrapper::~HotmailNewUIWrapper()
{
}

std::string hotmail::HotmailNewUIWrapper::encryptOutgoingString(std::string data, int encryptType)
{
	std::string returnString = data;
	std::string dataCopy = data;

	try
	{
		json::JSON dataJson(dataCopy);

		// if you are creating or updating the item (mail or draft)
		bool create = false;
		if (dataJson["Body"].getString("__type").compare("CreateItemRequest:#Exchange") == 0)
			create = true;
		else if (dataJson["Body"].getString("__type").compare("UpdateItemRequest:#Exchange") == 0)
			create = false;

		// if you are sending a draft or email
		bool saveAndSend = false;
		if (dataJson["Body"].getString("MessageDisposition").compare("SendAndSaveCopy") == 0)
			saveAndSend = true;
		else if (dataJson["Body"].getString("MessageDisposition").compare("SaveOnly") == 0)
			saveAndSend = false;

		try
		{
			// new mail
			if (dataJson["Body"].getString("ComposeOperation").compare("newMail") == 0)
			{
				try
				{					
					hotmail::HotmailNew hotmailNew = hotmail::HotmailNew();
					returnString = hotmailNew.encryptOutgoingString(data);			// this one changes the hotmailNew.users (if it has to)

					if (saveAndSend && create)
					{
						base::ObjectStorage::getInstance().addObject(hotmailNew, hotmailNew.users);
					}

					else if (!saveAndSend && create)
					{
						base::ObjectStorage::getInstance().addTempObject(hotmailNew);
					}
					else
					{
						// create is allways false here
						std::string mailId = fixId(dataJson["Body"]["ItemChanges"][(size_t)0]["ItemId"].getString("Id"));

						if (saveAndSend)
						{
							hotmail::HotmailNew hotmailNewOne = hotmail::HotmailNew("-1", hotmailNew.getObjectKey(), hotmailNew.getObjectInitChain(), hotmailNew.getObjectCheckWord(), mailId);
							base::ObjectStorage::getInstance().addObject(hotmailNewOne, hotmailNew.users);
						}
						else
						{
							try
							{
								base::ObjectStorage::getInstance().getObject(mailId, NITE_HOTMAILNEW);
								base::ObjectStorage::getInstance().removeObject(mailId, NITE_HOTMAILNEW);
							}
							catch (...)
							{
								Logger::logMessage("HotmailNewUIWrapper", "encryptOutgoingString", "couldn't find the mail with the id (new mail)");
							}

							hotmail::HotmailNew hotmailNewOne = hotmail::HotmailNew(mailId, hotmailNew.getObjectKey(), hotmailNew.getObjectInitChain(), hotmailNew.getObjectCheckWord());
							base::ObjectStorage::getInstance().addObject(hotmailNewOne, base::UsersList());
						}
					}
				}
				catch (...)
				{
					Logger::logMessage("HotmailNewUIWrapper", "encryptOutgoingString error (new mail)", (char*)dataJson["Body"].print().c_str());
					return returnString;
				}
			}
			// edit
			else if (dataJson["Body"].getString("ComposeOperation").compare("edit") == 0)
			{
				// create is allways false here
				try
				{
					std::string mailId = fixId(dataJson["Body"]["ItemChanges"][(size_t)0]["ItemId"].getString("Id"));
					bool replyOrForward = false;

					for (size_t i = 0; i < dataJson["Body"]["ItemChanges"][(size_t)0]["Updates"].size(); i++)
					{
						try
						{
							std::string fieldURI = dataJson["Body"]["ItemChanges"][(size_t)0]["Updates"][i]["Path"].getString("FieldURI");
							if (fieldURI.compare("Subject") == 0)
							{
								std::string subject = dataJson["Body"]["ItemChanges"][(size_t)0]["Updates"][i]["Item"].getString("Subject");

								if (subject.find("Re: ") == 0 || subject.find("Fw: ") == 0)
								{
									replyOrForward = true;
								}

								break;
							}
						}
						catch (...)
						{
						}
					}

					if (replyOrForward == true)
					{
						hotmail::HotmailNew hotmailNew = hotmail::HotmailNew(base::ObjectStorage::getInstance().getObject(mailId, NITE_HOTMAILNEW));
						returnString = hotmailNew.encryptOutgoingString(data);			// this one changes the hotmailNew.users (if it has to)

						if (saveAndSend)
						{
							hotmail::HotmailNew hotmailNewOne = hotmail::HotmailNew("-1", hotmailNew.getObjectKey(), hotmailNew.getObjectInitChain(), hotmailNew.getObjectCheckWord(), mailId);
							base::ObjectStorage::getInstance().addObject(hotmailNewOne, hotmailNew.users);
						}
						else
						{
							try
							{
								base::ObjectStorage::getInstance().getObject(mailId, NITE_HOTMAILNEW);
								base::ObjectStorage::getInstance().removeObject(mailId, NITE_HOTMAILNEW);
							}
							catch (...)
							{
								Logger::logMessage("HotmailNewUIWrapper", "encryptOutgoingString", "couldn't find the mail with the id (edit mail)");
							}

							hotmail::HotmailNew hotmailNewOne = hotmail::HotmailNew(mailId, hotmailNew.getObjectKey(), hotmailNew.getObjectInitChain(), hotmailNew.getObjectCheckWord());
							base::ObjectStorage::getInstance().addObject(hotmailNewOne, base::UsersList());
						}
					}
					else
					{
						hotmail::HotmailNew hotmailNew = hotmail::HotmailNew();
						returnString = hotmailNew.encryptOutgoingString(data);			// this one changes the hotmailNew.users (if it has to)

						if (saveAndSend)
						{
							hotmail::HotmailNew hotmailNewOne = hotmail::HotmailNew("-1", hotmailNew.getObjectKey(), hotmailNew.getObjectInitChain(), hotmailNew.getObjectCheckWord(), mailId);
							base::ObjectStorage::getInstance().addObject(hotmailNewOne, hotmailNew.users);
						}
						else
						{
							try
							{
								base::ObjectStorage::getInstance().getObject(mailId, NITE_HOTMAILNEW);
								base::ObjectStorage::getInstance().removeObject(mailId, NITE_HOTMAILNEW);
							}
							catch (...)
							{
								Logger::logMessage("HotmailNewUIWrapper", "encryptOutgoingString", "couldn't find the mail with the id (edit mail)");
							}

							hotmail::HotmailNew hotmailNewOne = hotmail::HotmailNew(mailId, hotmailNew.getObjectKey(), hotmailNew.getObjectInitChain(), hotmailNew.getObjectCheckWord());
							base::ObjectStorage::getInstance().addObject(hotmailNewOne, base::UsersList());
						}
					}
				}
				catch (...)
				{
					Logger::logMessage("HotmailNewUIWrapper", "encryptOutgoingString error (edit)", (char*)dataJson["Body"].print().c_str());
					return returnString;
				}
			}
			// reply - forward
			else if (dataJson["Body"].getString("ComposeOperation").compare("reply") == 0 || dataJson["Body"].getString("ComposeOperation").compare("forward") == 0)
			{
				// create true
				if (create)
				{
					try
					{
						std::string mailId = fixId(dataJson["Body"]["Items"][(size_t)0]["ReferenceItemId"].getString("Id"));
						hotmail::HotmailNew hotmailNew(base::ObjectStorage::getInstance().getObject(mailId, NITE_HOTMAILNEW));
						returnString = hotmailNew.encryptOutgoingString(data);							// because this one changes the hotmailNew.users

						if (saveAndSend)
						{
							hotmail::HotmailNew hotmailNewOne = hotmail::HotmailNew("-1", hotmailNew.getObjectKey(), hotmailNew.getObjectInitChain(), hotmailNew.getObjectCheckWord(), mailId);
							base::ObjectStorage::getInstance().addObject(hotmailNewOne, hotmailNew.users);
						}
						else
						{
							try
							{
								base::ObjectStorage::getInstance().getObject(mailId, NITE_HOTMAILNEW);
								base::ObjectStorage::getInstance().removeObject(mailId, NITE_HOTMAILNEW);
							}
							catch (...)
							{
								Logger::logMessage("HotmailNewUIWrapper", "encryptOutgoingString", "couldn't find the mail with the id (reply or forward)");
							}

							hotmail::HotmailNew hotmailNewOne = hotmail::HotmailNew(mailId, hotmailNew.getObjectKey(), hotmailNew.getObjectInitChain(), hotmailNew.getObjectCheckWord());
							base::ObjectStorage::getInstance().addObject(hotmailNewOne, base::UsersList());
						}
					}
					catch (...)
					{
						Logger::logMessage("HotmailNewUIWrapper", "encryptOutgoingString error reply or forward create", (char*)dataJson["Body"].print().c_str());
						return returnString;
					}
				}
				// create false
				else
				{
					try
					{
						std::string mailId = fixId(dataJson["Body"]["ItemChanges"][(size_t)0]["ItemId"].getString("Id"));
						hotmail::HotmailNew hotmailNew(base::ObjectStorage::getInstance().getObject(mailId, NITE_HOTMAILNEW));
						returnString = hotmailNew.encryptOutgoingString(data);							// because this one changes the hotmailNew.users

						if (saveAndSend)
						{
							hotmail::HotmailNew hotmailNewOne = hotmail::HotmailNew("-1", hotmailNew.getObjectKey(), hotmailNew.getObjectInitChain(), hotmailNew.getObjectCheckWord(), mailId);
							base::ObjectStorage::getInstance().addObject(hotmailNewOne, hotmailNew.users);
						}
						else
						{
							try
							{
								base::ObjectStorage::getInstance().getObject(mailId, NITE_HOTMAILNEW);
								base::ObjectStorage::getInstance().removeObject(mailId, NITE_HOTMAILNEW);
							}
							catch (...)
							{
								Logger::logMessage("HotmailNewUIWrapper", "encryptOutgoingString", "couldn't find the mail with the id (reply or forward)");
							}

							hotmail::HotmailNew hotmailNewOne = hotmail::HotmailNew(mailId, hotmailNew.getObjectKey(), hotmailNew.getObjectInitChain(), hotmailNew.getObjectCheckWord());
							base::ObjectStorage::getInstance().addObject(hotmailNewOne, base::UsersList());
						}
					}
					catch (...)
					{
						Logger::logMessage("HotmailNewUIWrapper", "encryptOutgoingString error reply or forward update", (char*)dataJson["Body"].print().c_str());
						return returnString;
					}
				}
			}
		}
		catch (...)
		{
			try
			{
				// reply - forward (create allways true here)
				if (dataJson["Body"]["ComposeOperation"].getType() == JsonNull)
				{
					std::string replyEmailId = fixId(dataJson["Body"]["Items"][(size_t)0]["ReferenceItemId"].getString("Id"));
					hotmail::HotmailNew tmp(base::ObjectStorage::getInstance().getObject(replyEmailId, NITE_HOTMAILNEW));
					hotmail::HotmailNew hotmailNew("-1", tmp.getObjectKey(), tmp.getObjectInitChain(), tmp.getObjectCheckWord());

					returnString = hotmailNew.encryptOutgoingString(data);
					base::ObjectStorage::getInstance().addTempObject(hotmailNew);
				}
			}
			catch (...)
			{
				Logger::logMessage("HotmailNewUIWrapper", "encryptOutgoingString error reply or forward create", (char*)data.c_str());
				return data;
			}
		}
	}
	catch (...)
	{
		return returnString;
	}

	return returnString;
}

std::string hotmail::HotmailNewUIWrapper::decryptIncomingString(std::string data, const int decryptType)
{
	std::string returnString = data;
	try
	{
		if (decryptType == NITE_DecryptSummary)
		{
			json::JSON jsonData = json::JSON(data);
			decryptSummary(jsonData["Body"]);
			returnString = jsonData.print();
		}
		else if (decryptType == NITE_DecryptFull)
		{
			json::JSON jsonData = json::JSON(data);
			decryptFull(jsonData["Body"]);
			returnString = jsonData.print();
		}
		else if (decryptType == NITE_DecryptSessionData)
		{
			if (data.find("Content-Name: SessionData") != -1)
			{
				returnString = data.substr(0, data.find("Content-Name: SessionData"));
				std::string remainder = data.substr(data.find("Content-Name: SessionData"));
				json::JSON jsonData = json::JSON(remainder);
				decryptSummary(jsonData["findConversation"]["Body"]);
				returnString = jsonData.print() + remainder;
			}
			else
			{
				json::JSON jsonData = json::JSON(data);
				decryptSummary(jsonData["findConversation"]["Body"]);
				returnString = jsonData.print();
			}
		}
		else if (decryptType == NITE_DecryptReply)
		{
			json::JSON jsonData = json::JSON(data);
			decryptReply(jsonData["Body"]);
			returnString = jsonData.print();
		}
		else if (decryptType == NITE_DecryptDraftSummary)
		{
			json::JSON jsonData = json::JSON(data);
			decryptDraftSummary(jsonData["Body"]);
			returnString = jsonData.print();
		}
		else if (decryptType == NITE_DecryptDraftFull)
		{
			json::JSON jsonData = json::JSON(data);
			decryptDraftFull(jsonData["Body"]);
			returnString = jsonData.print();
		}
		else if (NITE_DecryptNotification)
		{
			returnString = decryptNotification(data);
		}
		else
		{
			Logger::errorMessage("HotmailNewUIWrapper", "decryptIncomingString", "else error", nsresult(1));
			return returnString;
		}

		return returnString;
	}
	catch (...)
	{
		Logger::errorMessage("HotmailNewUIWrapper", "decryptIncomingString", "...", nsresult(2));
		return returnString;
	}
}

void hotmail::HotmailNewUIWrapper::deleteMails(std::string data)
{
	std::vector<base::ObjectRequest> objReqs;
	try
	{
		json::JSON jsonData = json::JSON(data);
		if (jsonData["Body"].getString("DeleteType").compare("HardDelete") == 0)
		{
			try
			{
				for (size_t i = 0; i < jsonData["Body"]["ItemIds"].size(); i++)
				{
					objReqs.push_back(fixId(jsonData["Body"]["ItemIds"][i].getString("Id")));
				}
			}
			catch (...)
			{
				Logger::errorMessage("HotmailNewUIWrapper", "deleteMails", "hard delete", nsresult(2));
			}

			base::ObjectStorage::getInstance().removeObjects(objReqs, NITE_HOTMAILNEW);

			return;
		}
		else if (jsonData["Body"].getString("DeleteType").compare("MoveToDeletedItems") == 0)
		{
			try
			{
				size_t convActionsSize = jsonData["Body"]["ItemIds"].size();
				for (size_t i = 0; i < convActionsSize; i++)
				{
					objReqs.push_back(fixId(jsonData["Body"]["ItemIds"][i].getString("Id")));
				}

				base::ObjectList newHotmails;
				if (!objReqs.empty())
				{
					newHotmails = base::ObjectStorage::getInstance().getObjects(objReqs, NITE_HOTMAILNEW);
				}

				base::ObjectList newHotmailsNew;
				std::vector<base::UsersList> usersLists;
				for (size_t i = 0; i < convActionsSize; i++)
				{
					try
					{
						hotmail::HotmailNew hotmailNew(newHotmails[fixId(jsonData["Body"]["ItemIds"][i].getString("Id"))]);

						newHotmailsNew.pushBack(hotmail::HotmailNew("-1", hotmailNew.getObjectKey(), hotmailNew.getObjectInitChain(), hotmailNew.getObjectCheckWord()));
						usersLists.push_back(base::UsersList());
					}
					catch (...)
					{
						continue;
					}
				}

				base::ObjectStorage::getInstance().addObjects(newHotmailsNew, usersLists);

				return;
			}
			catch (...)
			{
				Logger::errorMessage("HotmailNewUIWrapper", "deleteMails", "move to deleted items", nsresult(2));
			}

			base::ObjectStorage::getInstance().removeObjects(objReqs, NITE_HOTMAILNEW);

			return;
		}
	}
	catch (...)
	{
		Logger::errorMessage("HotmailNewUIWrapper", "deleteMails", "outer catch ...", nsresult(1));
	}
}

void hotmail::HotmailNewUIWrapper::moveMails(std::string data)
{
	std::vector<base::ObjectRequest> objReqs;

	try
	{
		json::JSON jsonData = json::JSON(data);
		if ((jsonData["Body"]["ConversationActions"][size_t(0)].getString("Action").compare("Delete") == 0 && jsonData["Body"]["ConversationActions"][size_t(0)].getString("DeleteType").compare("MoveToDeletedItems") == 0) || jsonData["Body"]["ConversationActions"][size_t(0)].getString("Action").compare("Move") == 0)
		{
			try
			{
				size_t convActionsSize = jsonData["Body"]["ConversationActions"].size();
				for (size_t i = 0; i < convActionsSize; i++)
				{
					objReqs.push_back(fixId(jsonData["Body"]["ConversationActions"][i]["ConversationId"].getString("Id")));
				}

				base::ObjectList newHotmails;
				if (!objReqs.empty())
				{
					newHotmails = base::ObjectStorage::getInstance().getObjects(objReqs, NITE_HOTMAILNEW);
				}

				for (size_t i = 0; i < convActionsSize; i++)
				{
					try
					{
						hotmail::HotmailNew hotmailNew(newHotmails[fixId(jsonData["Body"]["ConversationActions"][i]["ConversationId"].getString("Id"))]);
						hotmail::HotmailNew temp(std::to_string(i), hotmailNew.getObjectKey(), hotmailNew.getObjectInitChain(), hotmailNew.getObjectCheckWord());
						base::ObjectStorage::getInstance().addTempObject(temp);
					}
					catch (...)
					{
						continue;
					}
				}
			}
			catch (...)
			{
				Logger::errorMessage("HotmailNewUIWrapper", "moveMails", "move to deleted items", nsresult(2));
			}

			base::ObjectStorage::getInstance().removeObjects(objReqs, NITE_HOTMAILNEW);

			return;
		}
		else if (jsonData["Body"]["ConversationActions"][size_t(0)].getString("Action").compare("Delete") == 0 && jsonData["Body"]["ConversationActions"][size_t(0)].getString("DeleteType").compare("SoftDelete") == 0)
		{
			try
			{
				size_t convActionsSize = jsonData["Body"]["ConversationActions"].size();
				for (size_t i = 0; i < convActionsSize; i++)
				{
					objReqs.push_back(fixId(jsonData["Body"]["ConversationActions"][i]["ConversationId"].getString("Id")));
				}
			}
			catch (...)
			{
				Logger::errorMessage("HotmailNewUIWrapper", "moveMails", "move to deleted items", nsresult(4));
			}

			base::ObjectStorage::getInstance().removeObjects(objReqs, NITE_HOTMAILNEW);

			return;
		}
		// don't forget for emptyFolder
	}
	catch (...)
	{
		Logger::errorMessage("HotmailNewUIWrapper", "moveMails", "outer catch ...", nsresult(1));
	}
}

void hotmail::HotmailNewUIWrapper::getNewIds(std::string data)
{
	try
	{
		json::JSON jsonData = json::JSON(data);

		for (size_t i = 0; i < jsonData["Body"]["ResponseMessages"]["Items"].size(); i++)
		{
			try
			{
				hotmail::HotmailNew hotmailNew(base::ObjectStorage::getInstance().getTempObject(std::to_string(i), NITE_HOTMAILNEW));

				for (size_t j = 0; j < jsonData["Body"]["ResponseMessages"]["Items"][i]["MovedItemIds"].size(); j++)
				{
					std::string mailId = fixId(jsonData["Body"]["ResponseMessages"]["Items"][i]["MovedItemIds"][j].getString("Id"));
					base::ObjectStorage::getInstance().addObject(hotmail::HotmailNew(mailId, hotmailNew.getObjectKey(), hotmailNew.getObjectInitChain(), hotmailNew.getObjectCheckWord()), base::UsersList());
				}

				base::ObjectStorage::getInstance().removeTempObject(std::to_string(i));
			}
			catch (...)
			{
				continue;
			}
		}
	}
	catch (...)
	{
		Logger::errorMessage("HotmailNewUIWrapper", "getNewIds", "outer catch ...", nsresult(1));
	}
}

void hotmail::HotmailNewUIWrapper::decryptSummary(json::Value &jsonData)
{
	std::vector<size_t> indexes;
	std::vector<base::ObjectRequest> objReqs;
	std::vector<base::ObjectRequest> objReqsConvs;

	try
	{
		// get keys
		for (size_t i = 0; i < jsonData["Conversations"].size(); i++)
		{
			try
			{
				std::string checkWord = encryptCheck(jsonData["Conversations"][i].getString("ConversationTopic"));
				std::string id = fixId(jsonData["Conversations"][i]["ItemIds"][size_t(0)].getString("Id"));
				std::string idConv = fixId(jsonData["Conversations"][i]["ConversationId"].getString("Id"));

				indexes.push_back(i);
				objReqs.push_back(base::ObjectRequest(id, checkWord));
				objReqsConvs.push_back(base::ObjectRequest(idConv, checkWord));
			}
			catch (...)
			{
				continue;
			}
		}

		if (indexes.empty())
			return;

		base::ObjectList newHotmails;
		if (!objReqs.empty())
		{
			newHotmails = base::ObjectStorage::getInstance().getObjects(objReqs, NITE_HOTMAILNEW);
		}

		base::ObjectList newHotmailsConvs;
		if (!objReqsConvs.empty())
		{
			newHotmailsConvs = base::ObjectStorage::getInstance().getObjects(objReqsConvs, NITE_HOTMAILNEW);
		}

		// decrypt
		for (size_t i = 0; i < indexes.size(); i++)
		{
			try
			{
				HotmailNew hotmailNew(newHotmails[fixId(jsonData["Conversations"][indexes[i]]["ItemIds"][size_t(0)].getString("Id"))]);
				hotmailNew.decryptSummary(jsonData["Conversations"][indexes[i]]);

				try
				{
					newHotmailsConvs[fixId(jsonData["Conversations"][indexes[i]]["ConversationId"].getString("Id"))];
				}
				catch (...)
				{
					HotmailNew hotmailNewConv(fixId(jsonData["Conversations"][indexes[i]]["ConversationId"].getString("Id")), hotmailNew.getObjectKey(), hotmailNew.getObjectInitChain(), hotmailNew.getObjectCheckWord());
					base::ObjectStorage::getInstance().addObject(hotmailNewConv, base::UsersList());
				}
			}
			catch (...)
			{
				try
				{
					HotmailNew hotmailNew(newHotmailsConvs[fixId(jsonData["Conversations"][indexes[i]]["ConversationId"].getString("Id"))]);
					hotmailNew.decryptSummary(jsonData["Conversations"][indexes[i]]);
				}
				catch (...)
				{
				}

				continue;
			}
		}
	}
	catch (...)
	{
		Logger::errorMessage("HotmailNewUIWrapper", "decryptSummary", "outer ...", nsresult(1));
	}
}

void hotmail::HotmailNewUIWrapper::decryptFull(json::Value &jsonData)
{
	std::vector<size_t> indexes;
	std::vector<base::ObjectRequest> objReqs;

	try
	{
		// get keys
		for (size_t i = 0; i < jsonData["ResponseMessages"]["Items"][size_t(0)]["Conversation"]["ConversationNodes"].size(); i++)
		{
			try
			{
				std::string checkWord = encryptCheck(jsonData["ResponseMessages"]["Items"][size_t(0)]["Conversation"]["ConversationNodes"][i]["Items"][size_t(0)].getString("Subject"));
				std::string id = fixId(jsonData["ResponseMessages"]["Items"][size_t(0)]["Conversation"]["ConversationNodes"][i]["Items"][size_t(0)]["ItemId"].getString("Id"));

				objReqs.push_back(base::ObjectRequest(id, checkWord));
				indexes.push_back(i);
			}
			catch (...)
			{
				continue;
			}
		}

		if (indexes.empty())
			return;

		base::ObjectList newHotmails;
		if (!objReqs.empty())
		{
			newHotmails = base::ObjectStorage::getInstance().getObjects(objReqs, NITE_HOTMAILNEW);
		}

		// decrypt
		for (size_t i = 0; i < indexes.size(); i++)
		{
			try
			{
				try
				{
					HotmailNew hotmailNew(newHotmails[fixId(jsonData["ResponseMessages"]["Items"][size_t(0)]["Conversation"]["ConversationNodes"][indexes[i]]["Items"][size_t(0)]["ItemId"].getString("Id"))]);
					hotmailNew.decryptFull(jsonData["ResponseMessages"]["Items"][size_t(0)]["Conversation"]["ConversationNodes"][indexes[i]]["Items"][size_t(0)]);
				}
				catch (...)
				{
					continue;
				}
			}
			catch (...)
			{
				continue;
			}
		}
	}
	catch (...)
	{
		Logger::errorMessage("HotmailNewUIWrapper", "decryptFull", "outer ...", nsresult(1));
	}
}

void hotmail::HotmailNewUIWrapper::decryptReply(json::Value &jsonData)
{
	try
	{
		if (jsonData["ResponseMessages"]["Items"][size_t(0)]["Items"][(size_t)0].getType() == JsonNull)
			return;

		if (jsonData["ResponseMessages"]["Items"][size_t(0)].getString("__type").compare("ItemInfoResponseMessage:#Exchange") != 0)
			return;

		// get key
		std::string checkWord = encryptCheck(jsonData["ResponseMessages"]["Items"][size_t(0)]["Items"][(size_t)0].getString("Subject"));
		std::string id = fixId(jsonData["ResponseMessages"]["Items"][size_t(0)]["Items"][size_t(0)]["ItemId"].getString("Id"));

		// decrypt
		try
		{
			HotmailNew tmp(base::ObjectStorage::getInstance().getTempObject("-1", NITE_HOTMAILNEW));
			HotmailNew hotmailNew(id, tmp.getObjectKey(), tmp.getObjectInitChain(), tmp.getObjectCheckWord());

			hotmailNew.decryptReply(jsonData["ResponseMessages"]["Items"][size_t(0)]["Items"][size_t(0)]);
			base::ObjectStorage::getInstance().addObject(hotmailNew, base::UsersList());
		}
		catch (...)
		{
			Logger::errorMessage("HotmailNewUIWrapper", "decryptReply", "error with decrypting referenced mail of reply - forward", nsresult(1));
		}
	}
	catch (...)
	{
		try
		{
			std::string id = fixId(jsonData["ResponseMessages"]["Items"][size_t(0)]["Items"][size_t(0)]["ItemId"].getString("Id"));
			HotmailNew tmp(base::ObjectStorage::getInstance().getTempObject("-1", NITE_HOTMAILNEW));
			HotmailNew hotmailNew(id, tmp.getObjectKey(), tmp.getObjectInitChain(), tmp.getObjectCheckWord());

			base::ObjectStorage::getInstance().addObject(hotmailNew, base::UsersList());
		}
		catch (...)
		{
			Logger::errorMessage("HotmailNewUIWrapper", "decryptReply", "outer ...", nsresult(2));
		}
	}
}

void hotmail::HotmailNewUIWrapper::decryptDraftSummary(json::Value &jsonData)
{
	std::vector<size_t> indexes;
	std::vector<base::ObjectRequest> objReqs;

	try
	{
		// get keys
		for (size_t i = 0; i < jsonData["ResponseMessages"]["Items"][size_t(0)]["RootFolder"]["Items"].size(); i++)
		{
			try
			{
				std::string checkWord = encryptCheck(jsonData["ResponseMessages"]["Items"][size_t(0)]["RootFolder"]["Items"][i].getString("Subject"));
				std::string id = fixId(jsonData["ResponseMessages"]["Items"][size_t(0)]["RootFolder"]["Items"][i]["ItemId"].getString("Id"));
				indexes.push_back(i);
				objReqs.push_back(base::ObjectRequest(id, checkWord));
			}
			catch (...)
			{
				continue;
			}
		}

		if (indexes.empty())
			return;

		base::ObjectList newHotmails;
		if (!objReqs.empty())
		{
			newHotmails = base::ObjectStorage::getInstance().getObjects(objReqs, NITE_HOTMAILNEW);
		}

		// decrypt
		for (size_t i = 0; i < indexes.size(); i++)
		{
			try
			{
				HotmailNew hotmailNew(newHotmails[fixId(jsonData["ResponseMessages"]["Items"][size_t(0)]["RootFolder"]["Items"][indexes[i]]["ItemId"].getString("Id"))]);
				hotmailNew.decryptDraftSummary(jsonData["ResponseMessages"]["Items"][size_t(0)]["RootFolder"]["Items"][indexes[i]]);
			}
			catch (...)
			{
				continue;
			}
		}
	}
	catch (...)
	{
		Logger::errorMessage("HotmailNewUIWrapper", "decryptDraftSummary", "outer ...", nsresult(1));
	}
}

void hotmail::HotmailNewUIWrapper::decryptDraftFull(json::Value &jsonData)
{
	std::vector<size_t> indexes;
	std::vector<base::ObjectRequest> objReqs;

	try
	{
		// get keys
		for (size_t i = 0; i < jsonData["ResponseMessages"]["Items"][size_t(0)]["Items"].size(); i++)
		{
			try
			{
				std::string checkWord = encryptCheck(jsonData["ResponseMessages"]["Items"][(size_t)0]["Items"][i].getString("Subject"));
				std::string id = fixId(jsonData["ResponseMessages"]["Items"][size_t(0)]["Items"][i]["ItemId"].getString("Id"));
				objReqs.push_back(base::ObjectRequest(id, checkWord));
				indexes.push_back(i);
			}
			catch (...)
			{
				continue;
			}
		}

		if (indexes.empty())
			return;

		base::ObjectList newHotmails;
		if (!objReqs.empty())
		{
			newHotmails = base::ObjectStorage::getInstance().getObjects(objReqs, NITE_HOTMAILNEW);
		}

		// decrypt
		for (size_t i = 0; i < indexes.size(); i++)
		{
			try
			{
				HotmailNew hotmailNew(newHotmails[fixId(jsonData["ResponseMessages"]["Items"][size_t(0)]["Items"][indexes[i]]["ItemId"].getString("Id"))]);
				hotmailNew.decryptDraftFull(jsonData["ResponseMessages"]["Items"][size_t(0)]["Items"][indexes[i]]);
			}
			catch (...)
			{
				continue;
			}
		}
	}
	catch (...)
	{
		Logger::errorMessage("HotmailNewUIWrapper", "decryptFull", "outer ...", nsresult(1));
	}
}

void hotmail::HotmailNewUIWrapper::getAttachmentIds(std::string data)
{
	try
	{
		std::string dataCopy = data;
		
		json::JSON dataJson(dataCopy);
		std::string rootItemId = fixId(dataJson["Body"]["ResponseMessages"]["Items"][(size_t)0]["Attachments"][size_t(0)]["AttachmentId"].getString("RootItemId"));
		hotmail::HotmailNew hotmailNewTemp = static_cast<hotmail::HotmailNew&>(base::ObjectStorage::getInstance().getObject(rootItemId, NITE_HOTMAILNEW));
		hotmail::HotmailNew hotmailNew(rootItemId, hotmailNewTemp.getObjectKey(), hotmailNewTemp.getObjectInitChain(), hotmailNewTemp.getObjectCheckWord(), rootItemId);

		std::string attId = fixId(dataJson["Body"]["ResponseMessages"]["Items"][size_t(0)]["Attachments"][size_t(0)]["AttachmentId"].getString("Id"));
		attId = attId.substr(attId.size() - 29);
		hotmailNew.attachments.push_back(attId);

		hotmail::HotmailAttachment hotmailAttachmentTemp = static_cast<hotmail::HotmailAttachment&>(base::ObjectStorage::getInstance().getTempObject("hotmail_attachment", NITE_HOTATTACH));
		hotmail::HotmailAttachment hotmailAttachment(attId, hotmailAttachmentTemp.getObjectKey(), hotmailAttachmentTemp.getObjectInitChain(), hotmailAttachmentTemp.getObjectCheckWord());
		base::ObjectStorage::getInstance().addObject(hotmailAttachment, base::UsersList());
		base::ObjectStorage::getInstance().removeTempObject("hotmail_attachment");

		base::ObjectStorage::getInstance().addObject(hotmailNew, base::UsersList());
	}
	catch (...)
	{
		Logger::errorMessage("HotmailNewUIWrapper", "getAttachmentIds", "something went wrong with getting attachment ids", nsresult(1));
	}
}

std::string hotmail::HotmailNewUIWrapper::decryptNotification(std::string data)
{
	try
	{
		std::string returnString = "";
		std::vector<std::string> dataSplitted = split(data, "<script>");
		returnString = dataSplitted[0];
		if (dataSplitted.size() == 1)
			return data;

		for (size_t i = 1; i < dataSplitted.size(); i++)
		{
			std::string temp = dataSplitted[i];
			try
			{
				json::JSON dataJson(temp);
				try
				{
					if (dataJson[size_t(0)].getString("EventType").compare("0") == 0)
					{
						if (dataJson[size_t(0)].getString("id").compare("NewMailNotification") == 0)
						{
							std::string mailId = fixId(dataJson[size_t(0)].getString("ItemId"));
							std::string checkWord = encryptCheck(dataJson[size_t(0)].getString("Subject"));
							std::string subject = dataJson[size_t(0)].getString("Subject");

							dataJson[size_t(0)].replace("PreviewText", "Cryptonite encrypted\\u000d\\u000a");

							hotmail::HotmailNew hotmailNew(base::ObjectStorage::getInstance().getObject(base::ObjectRequest(mailId, checkWord), NITE_HOTMAILNEW));						
							dataJson[size_t(0)].replace("Subject", hotmailNew.decryptSubject(subject));
						}
					}
					else if (dataJson[size_t(0)].getString("EventType").compare("RowModified") == 0)
					{
						std::string checkWord = encryptCheck(dataJson[size_t(0)]["Conversation"].getString("ConversationTopic"));

						std::string mailId = fixId(dataJson[size_t(0)]["Conversation"]["ItemIds"][size_t(0)].getString("Id"));
						std::string convId = fixId(dataJson[size_t(0)]["Conversation"]["ConversationId"].getString("Id"));
						std::string subject = dataJson[size_t(0)]["Conversation"].getString("ConversationTopic");

						dataJson[size_t(0)]["Conversation"].replace("Preview", "Cryptonite encrypted");

						hotmail::HotmailNew hotmailNew(base::ObjectStorage::getInstance().getObject(base::ObjectRequest(mailId, checkWord), NITE_HOTMAILNEW));
						dataJson[size_t(0)]["Conversation"].replace("ConversationTopic", hotmailNew.decryptSubject(subject));

						try
						{
							base::ObjectStorage::getInstance().getObject(base::ObjectRequest(convId, checkWord), NITE_HOTMAILNEW);
						}
						catch (...)
						{
							HotmailNew hotmailNewConv(convId, hotmailNew.getObjectKey(), hotmailNew.getObjectInitChain(), hotmailNew.getObjectCheckWord());
							base::ObjectStorage::getInstance().addObject(hotmailNewConv, base::UsersList());
						}
					}
					else if (dataJson[size_t(0)].getString("EventType").compare("RowAdded") == 0)
					{
						std::string checkWord = encryptCheck(dataJson[size_t(0)]["Item"].getString("Subject"));

						std::string mailId = fixId(dataJson[size_t(0)]["Item"]["ItemId"].getString("Id"));
						std::string convId = fixId(dataJson[size_t(0)]["Item"]["ConversationId"].getString("Id"));
						std::string subject = dataJson[size_t(0)]["Item"].getString("Subject");

						dataJson[size_t(0)]["Item"].replace("Preview", "Cryptonite encrypted");

						hotmail::HotmailNew hotmailNew(base::ObjectStorage::getInstance().getObject(base::ObjectRequest(mailId, checkWord), NITE_HOTMAILNEW));
						dataJson[size_t(0)]["Item"].replace("Subject", hotmailNew.decryptSubject(subject));

						try
						{
							base::ObjectStorage::getInstance().getObject(base::ObjectRequest(convId, checkWord), NITE_HOTMAILNEW);
						}
						catch (...)
						{
							HotmailNew hotmailNewConv(convId, hotmailNew.getObjectKey(), hotmailNew.getObjectInitChain(), hotmailNew.getObjectCheckWord());
							base::ObjectStorage::getInstance().addObject(hotmailNewConv, base::UsersList());
						}
					}
				}
				catch (...)
				{
					// Logger::logMessage("HotmailNewUIWrapper", "decryptNotification catch", (char*)dataJson.print().c_str());
				}

				returnString += "<script>" + dataJson.print() + temp;
			}
			catch (...)
			{
				returnString += "<script>" + dataSplitted[i];
			}
		}

		return returnString;
	}
	catch (...)
	{
		Logger::errorMessage("HotmailNewUIWrapper", "decryptNotification", "error decrypting notifications", nsresult(2));
		return data;
	}
}

std::string hotmail::HotmailNewUIWrapper::fixId(std::string id)
{
	return replace(id, "\\", "");
}
