#ifndef _MSWordObserver_h_
#define _MSWordObserver_h_

#include "HttpObserver.h"

namespace msword
{
	class MSWordObserver : public base::HttpObserver
	{
	public:
		MSWordObserver(bool encrypt);

		NS_DECL_ISUPPORTS
		NS_DECL_NSIOBSERVER

	private:
		~MSWordObserver();
	};
}

#endif