#include <mozilla/Char16.h>

#include "JsonBadCast.h"

json::JsonBadCast::JsonBadCast()
{
}

json::JsonBadCast::~JsonBadCast()
{
}

const char* json::JsonBadCast::what() const throw()
{
	std::string returnString = "Json error: bad cast error";
	return fromStdString(returnString);
}
