#include <mozilla/Char16.h>

#include "GAttachmentListener.h"
#include "NSException.h"
#include "AlgorithmException.h"

#include "MSAttachmentListener.h"
#include "HotmailAttachment.h"

#include "ObjectStorage.h"

#include "Logger.h"

NS_IMPL_ISUPPORTS(hotmail::MSAttachmentListener, nsIRequestObserver, nsIStreamListener)

hotmail::MSAttachmentListener::MSAttachmentListener()
{
}


hotmail::MSAttachmentListener::~MSAttachmentListener()
{
}

NS_IMETHODIMP hotmail::MSAttachmentListener::OnStartRequest(nsIRequest *aRequest, nsISupports *aContext)
{
	try
	{
		HttpListener::OnStartRequest(aRequest, aContext);
	}
	catch (exception::technical::NSException)
	{
		// notify javascript
	}

	return NS_OK;
}

NS_IMETHODIMP hotmail::MSAttachmentListener::OnDataAvailable(nsIRequest *aRequest, nsISupports *aContext, nsIInputStream *aInputStream, uint64_t aOffset, uint32_t aCount)
{
	try
	{
		HttpListener::OnDataAvailable(aRequest, aContext, aInputStream, aOffset, aCount);
	}
	catch (exception::technical::NSException)
	{
		// notify javascript
	}

	return NS_OK;
}

NS_IMETHODIMP hotmail::MSAttachmentListener::OnStopRequest(nsIRequest *aRequest, nsISupports *aContext, nsresult aStatusCode)
{
	mData = (char*)NS_Realloc((void*)mData, sizeof(char) * (mRead + 1));
	if (!mData)
	{
		Logger::errorMessage("GAttachmentListener", "OnStopRequest", "OnStopRequest FAILED: couldn't reallocate memory for mData", nsresult(-1));
		return NS_OK;
	}

	memcpy(mData + mRead, "\0", 1);

	std::string data = mData;
	std::string checkWord = data.substr(0, 86);
	try
	{
		hotmail::HotmailAttachment hotmailAttachment = static_cast<hotmail::HotmailAttachment&>(base::ObjectStorage::getInstance().getObject(base::ObjectRequest(checkWord, checkWord), NITE_HOTATTACH));

		std::string decryptedData = hotmailAttachment.decryptIncomingString(data);

		mDecryptedRead = decryptedData.size();
		mDecryptedData = (char*)NS_Alloc(sizeof(char) * mDecryptedRead);
		memcpy(mDecryptedData, decryptedData.c_str(), mDecryptedRead);
	}
	catch (...)
	{
		mDecryptedRead = mRead;
		mDecryptedData = (char*)NS_Alloc(sizeof(char) * mRead);
		memcpy(mDecryptedData, mData, mDecryptedRead);
	}

	try
	{
		HttpListener::OnStopRequest(aRequest, aContext, aStatusCode);
	}
	catch (exception::technical::NSException)
	{
		// inform javascript
	}
	catch (...)
	{
		// inform javascript
	}

	return NS_OK;
}
