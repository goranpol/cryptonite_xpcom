#include <mozilla/Char16.h>

#include "JsonOutOfRange.h"

#include <sstream>

json::JsonOutOfRange::JsonOutOfRange(const size_t indx, const size_t len) : index(indx), lenght(len)
{
}

json::JsonOutOfRange::~JsonOutOfRange()
{
}

const char* json::JsonOutOfRange::what() const throw()
{
	std::stringstream tmp1;
	tmp1 << "Json error: index out of range!!! index: " << index;
	tmp1 << " length: " << lenght;

	return fromStdString(tmp1.str());
}