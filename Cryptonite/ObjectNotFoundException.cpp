#include <mozilla/Char16.h>

#include "ObjectNotFoundException.h"

exception::domain::ObjectNotFoundException::ObjectNotFoundException(const std::string ID) : id(ID), checkWord("")
{
}

exception::domain::ObjectNotFoundException::ObjectNotFoundException(const std::string ID, const std::string chWd) : id(ID), checkWord(chWd)
{
}

exception::domain::ObjectNotFoundException::~ObjectNotFoundException() throw()
{
}

const char* exception::domain::ObjectNotFoundException::what() const throw()
{
	return "";
}