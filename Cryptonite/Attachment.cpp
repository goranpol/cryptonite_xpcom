#include <mozilla/Char16.h>

#include "Attachment.h"
#include "AESRijndael.h"
#include "Logger.h"

#include <sstream>
#include "Parser.h"

#include "ObjectNotFoundException.h"
#include "NotImplementedException.h"
#include "AESException.h"
#include "AlgorithmException.h"


base::Attachment::Attachment(const std::string objId, const std::string objKey, const std::string objInitChain, const std::string objCheckWord, const int objType) : base::Object(objType, objId, objKey), objectCheckWord(objCheckWord), objectInitChain(objInitChain)
{
}

base::Attachment::Attachment(const base::Object &obj) : base::Object(obj.getObjectType(), obj.getObjectId(), obj.getObjectKey()), objectInitChain(getInitChain(obj)), objectCheckWord(getCheckWord(obj))
{
}

base::Attachment::Attachment(base::Attachment &obj) : base::Object(obj.getObjectType(), obj.getObjectId(), obj.getObjectKey()), objectCheckWord(obj.objectCheckWord), objectInitChain(obj.objectInitChain)
{
}

base::Attachment::~Attachment()
{
}

std::string base::Attachment::getObjectCheckWord()
{
	return objectCheckWord;
}

std::string base::Attachment::getObjectInitChain()
{
	return objectInitChain;
}

base::Attachment &base::Attachment::operator=(base::Object &obj)
{
	base::Attachment attachment = static_cast<base::Attachment&>(obj);
	if (this == &attachment)
		return *this;

	const_cast<int&>(objectType) = attachment.getObjectType();
	const_cast<std::string&>(objectId) = attachment.getObjectId();
	const_cast<std::string&>(objectKey) = attachment.getObjectKey();
	const_cast<std::string&>(objectInitChain) = attachment.getObjectInitChain();
	const_cast<std::string&>(objectCheckWord) = attachment.getObjectCheckWord();

	return *this;
}

base::Attachment &base::Attachment::operator=(base::Attachment &obj)
{
	if (this == &obj)
		return *this;

	const_cast<int&>(objectType) = obj.getObjectType();
	const_cast<std::string&>(objectId) = obj.getObjectId();
	const_cast<std::string&>(objectKey) = obj.getObjectKey();
	const_cast<std::string&>(objectInitChain) = obj.getObjectInitChain();
	const_cast<std::string&>(objectCheckWord) = obj.getObjectCheckWord();

	return *this;
}

bool base::Attachment::operator==(base::Attachment &obj)
{
	if (objectType != obj.objectType)
		return false;
	else if (objectCheckWord.compare(obj.objectCheckWord) != 0)
		return false;
	else if (objectId.compare(obj.objectId) != 0)
		return false;
	else if (objectInitChain.compare(obj.objectInitChain) != 0)
		return false;
	else if (objectKey.compare(obj.objectKey) != 0)
		return false;
	else
		return true;
}

bool base::Attachment::operator!=(base::Attachment &obj)
{
	return !(*this == obj);
}

std::string base::Attachment::getInitChain(const base::Object &obj)
{
	const base::Attachment attachment = static_cast<const base::Attachment&>(obj);
	return attachment.objectInitChain;
}

std::string base::Attachment::getCheckWord(const base::Object &obj)
{
	const base::Attachment attachment = static_cast<const base::Attachment&>(obj);
	return attachment.objectCheckWord;
}

std::string base::Attachment::generateCheckWord()
{
	return toBase64(randomNumbers(64), 64);
}

std::string base::Attachment::encryptOutgoingString(std::string data)
{
	return objectCheckWord + encryptText(data);
}

std::string base::Attachment::decryptIncomingString(std::string data)
{
	if (!isBase64(data))
		return data;

	std::string chWd = data.substr(0, 86);

	if (chWd.compare(objectCheckWord) != 0)
		throw exception::domain::ObjectNotFoundException(objectId, chWd);

	return decryptText(data.substr(86));
}

std::string base::Attachment::encryptText(std::string textString)
{
	std::string paddedText;
	std::string result;

	size_t textStringSize = textString.size();
	size_t blockLeftOver = textStringSize % 16;
	if (blockLeftOver == 0)
	{
		paddedText = textString + randomNumbers(13);
		paddedText += "/16";
	}
	else if (blockLeftOver == 15)
	{
		paddedText = textString + randomNumbers(14);
		paddedText += "/17";
	}
	else
	{
		size_t padding = 16 - blockLeftOver;

		if (padding > 9)
		{
			paddedText = textString;
			paddedText += randomNumbers(padding - 3);
		}
		else
		{
			paddedText = textString;
			paddedText += randomNumbers(padding - 2);
		}

		paddedText += "/";
		std::ostringstream convertNumber;
		convertNumber << padding;
		paddedText += convertNumber.str();
	}

	return encrypt(paddedText);
}

std::string base::Attachment::decryptText(std::string cipherTextString)
{
	return decrypt(cipherTextString);
}

std::string base::Attachment::encrypt(std::string textString)
{
	std::string result;

	char *cipherText = (char*)NS_Alloc(sizeof(char) * textString.size() + 1);
	memset(cipherText, 0, textString.size() + 1);

	base::AESRijndael r;

	// only for cbc, fbc !!!not ebc
	r.makeKey((char*)fromBase64(objectKey).c_str(), (char*)fromBase64(objectInitChain).c_str(), 32);
	r.encrypt((char*)textString.c_str(), cipherText, textString.size(), r.CBC);

	char *tempBuf = cipherText;

	for (size_t i = 0; i<textString.size(); i++)
	{
		result.push_back(*tempBuf);
		tempBuf++;
	}

	NS_Free(cipherText);

	return toBase64(result, textString.size());
}

std::string base::Attachment::decrypt(std::string cipherTextString)
{
	std::string cipherString = fromBase64(cipherTextString);
	size_t length = cipherString.size();

	if (length % 16 != 0)
		return cipherTextString;

	char *plainText = (char*)NS_Alloc(sizeof(char) * length + 1);
	memset(plainText, 0, length + 1);
	base::AESRijndael r;

	r.makeKey((char*)fromBase64(objectKey).c_str(), (char*)fromBase64(objectInitChain).c_str(), 32);
	r.decrypt((char*)cipherString.c_str(), plainText, length, r.CBC);

	std::string tempResult;
	char* tempBuf = plainText;
	for (size_t i = 0; i<length; i++)
	{
		tempResult.push_back(*tempBuf);
		tempBuf++;
	}

	NS_Free(plainText);

	size_t paddingSizeIndex = tempResult.rfind("/");
	std::string paddingNumberString = tempResult.substr(paddingSizeIndex + 1);

	size_t padding;
	std::istringstream(paddingNumberString) >> padding;

	std::string result = tempResult.substr(0, tempResult.size() - padding);

	return result;
}

json::Value &base::Attachment::toJson() const
{
	json::JSON jsonObj = json::JSON(ObjectType);
	jsonObj.pushBack("ooid", objectId);
	jsonObj.pushBack("k", objectKey);
	jsonObj.pushBack("ot", objectType);

	jsonObj.pushBackObject("od");
	jsonObj["od"].pushBack("ic", objectInitChain);
	jsonObj["od"].pushBack("cw", objectCheckWord);

	return jsonObj.get();
}
