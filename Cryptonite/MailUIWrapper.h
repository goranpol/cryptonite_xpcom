#ifndef _MailUIWrapper_h
#define _MailUIWrapper_h

#include <string>
#include <vector>

namespace base
{
	class MailUIWrapper
	{
	public:
		MailUIWrapper();

		/* virtual std::string encryptOutgoingString(std::string data);
		virtual std::string decryptIncomingString(std::string data, const int decryptType);
		virtual void changeUsersWithAccess(std::string data); */

	protected:
		virtual ~MailUIWrapper();

		// returns check word if encrypted exception if not
		virtual std::string encryptCheck(const std::string subject);

		/* base64 helper functions */
		bool isBase64(const std::string input);

		/* string helper functions */
		std::vector<std::string> split(const std::string str, const std::string delimiters = " ");
		std::string replace(const std::string str, const std::string from, const std::string to);
	};
}

#endif
