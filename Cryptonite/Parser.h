#ifndef _JSON_h_
#define _JSON_h_

#include "Enumeration.h"
#include <string>
#include <memory>

namespace json
{
	class JSON
	{
	public:
		JSON(std::string &jsonStr);
		JSON(const JSON &val);
		JSON(const int type);
		JSON(const std::string &prfx, const int type);
		JSON(const Enumeration &val);
		JSON(const std::string &prfx, const Enumeration &val);
		virtual ~JSON();
		
		JSON &operator=(const JSON &val);
		JSON &operator=(const Enumeration &val);

		bool operator==(const JSON &val);
		bool operator==(const Enumeration &val);
		bool operator!=(const JSON &val);
		bool operator!=(const Enumeration &val);

		/* access operators */
		Value &operator[](const size_t index);
		Value &operator[](const std::string key);
		Value &operator[](const char* key);

		/* get root */
		Value &get();

		/* gets if array */
		Value &get(const size_t index);
		std::string getString(const size_t index);
		bool getBool(const size_t index);
		int64_t getInt64(const size_t index);
		int getInt(const size_t index);
		double getDouble(const size_t index);

		/* gets if object */
		Value &get(const std::string &key);
		std::string getString(const std::string &key);
		bool getBool(const std::string &key);
		int64_t getInt64(const std::string &key);
		int getInt(const std::string &key);
		double getDouble(const std::string &key);

		/* replaces if array */
		void replace(const size_t index, const Value &val);
		void replace(const size_t index, const std::string val);
		void replace(const size_t index, const char* val);
		void replace(const size_t index, const bool val);
		void replace(const size_t index, const int64_t val);
		void replace(const size_t index, const int val);
		void replace(const size_t index, const float val);
		void replace(const size_t index, const double val);
		void replace(const size_t index);
		void replaceArray(const size_t index);
		void replaceObject(const size_t index);
		void replaceJavascript(const size_t index, const std::string name = "");

		/* replaces if object */
		void replace(const std::string key, const Value &val);
		void replace(const std::string key, const std::string val);
		void replace(const std::string key, const char* val);
		void replace(const std::string key, const bool val);
		void replace(const std::string key, const int64_t val);
		void replace(const std::string key, const int val);
		void replace(const std::string key, const float val);
		void replace(const std::string key, const double val);
		void replace(const std::string key);
		void replaceArray(const std::string key);
		void replaceObject(const std::string key);
		void replaceJavascript(const std::string key, const std::string name = "");

		/* inserts if array */
		void insert(const size_t index, const Value &val);
		void insert(const size_t index, const std::string val);
		void insert(const size_t index, const char* val);
		void insert(const size_t index, const bool val);
		void insert(const size_t index, const int64_t val);
		void insert(const size_t index, const int val);
		void insert(const size_t index, const float val);
		void insert(const size_t index, const double val);
		void insert(const size_t index);
		void insertArray(const size_t index);
		void insertObject(const size_t index);
		void insertJavascript(const size_t index, const std::string name = "");

		/* inserts if object */
		void insert(const std::string key, const Value &val, const size_t index);
		void insert(const std::string key, const std::string val, const size_t index);
		void insert(const std::string key, const char* val, const size_t index);
		void insert(const std::string key, const bool val, const size_t index);
		void insert(const std::string key, const int64_t val, const size_t index);
		void insert(const std::string key, const int val, const size_t index);
		void insert(const std::string key, const float val, const size_t index);
		void insert(const std::string key, const double val, const size_t index);
		void insert(const std::string key, const size_t index);
		void insertArray(const std::string key, const size_t index);
		void insertObject(const std::string key, const size_t index);
		void insertJavascript(const std::string key, const size_t index, const std::string name = "");

		/* pushBacks if array */
		void pushBack(const Value &val);
		void pushBack(const bool val);
		void pushBack(const int64_t val);
		void pushBack(const int val);
		void pushBack(const float val);
		void pushBack(const double val);
		void pushBack();
		void pushBackArray();
		void pushBackObject();

		/* pushBack for array (String, char*) or object (key and null) */
		void pushBack(const std::string val);
		void pushBack(const char* val);
		void pushBackJavascript(const std::string key, const std::string name = "");

		/* pushBacks if object */
		void pushBack(const std::string key, const Value &val);
		void pushBack(const std::string key, const std::string val);
		void pushBack(const std::string key, const char* val);
		void pushBack(const std::string key, const bool val);
		void pushBack(const std::string key, const int64_t val);
		void pushBack(const std::string key, const int val);
		void pushBack(const std::string key, const float val);
		void pushBack(const std::string key, const double val);
		void pushBackArray(const std::string key);
		void pushBackObject(const std::string key);

		/* removes for array and object */
		void remove(const size_t index);
		void remove(const std::string key);

		/* empty & clear */
		bool empty();
		void clear();

		/* check name if javascript function */
		bool checkName(const std::string nm);
		bool checkName(const char* nm);

		/* other */
		size_t size();
		std::string print();
		void parse(std::string &jsonStr);
		std::string getPrefix();
		void setPrefix(std::string prfx);

	protected:

	private:
		bool parsePrefix(std::string &jsonStr);

		/******************************************************************************************/
		/************************************JSON members******************************************/
		/******************************************************************************************/
		std::string prefix;
		Enumeration enumeration;
		/******************************************************************************************/
		/************************************JSON members******************************************/
		/******************************************************************************************/
	};
}

#endif