#ifndef _JsonBadAlloc_h_
#define _JsonBadAlloc_h_

#include "JsonError.h"

namespace json
{
	class JsonBadAlloc : public JsonError
	{
	public:
		JsonBadAlloc();
		~JsonBadAlloc();

		const char* what() const throw();

	private:

	};
}

#endif
