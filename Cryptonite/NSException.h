#ifndef _NSException_h_
#define _NSException_h_

#include "nsISupports.h"
#include <exception>
#include <string>

namespace exception
{
	namespace technical
	{
		class NSException : public std::exception
		{
		public:
			NSException(const nsresult errNum, const std::string act);
			virtual ~NSException() throw();

			const char* what() const throw();

		private:
			const nsresult errorNumber;
			const std::string action;
		};
	}
}

#endif
