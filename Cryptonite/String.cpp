#include <mozilla/Char16.h>

#include "String.h"
#include "Logger.h"

#include "JsonTypeError.h"
#include "JsonBadCast.h"
#include "JsonBadAlloc.h"

json::String::String() : Value(JsonString), singleQuotes(false), stringValue("")
{
}

json::String::String(const std::string &value, const bool singleQ) : Value(JsonString), singleQuotes(singleQ), stringValue(value)
{
}

json::String::String(const char* value, const bool singleQ) : Value(JsonString), singleQuotes(singleQ), stringValue(value)
{
}

json::String::String(const json::String &val) : Value(val.type), singleQuotes(val.singleQuotes), stringValue(val.stringValue)
{
}

json::String::~String()
{
}

json::String &json::String::operator=(const json::Value &val)
{
	if(val.getType() != JsonString)
		throw json::JsonTypeError(JsonString, val.getType());

	String stringVal = static_cast<String&>(const_cast<Value&>(val));

	if (&stringVal == this)
		return *this;

	const_cast<int&>(this->type) = JsonString;
	const_cast<bool&>(this->singleQuotes) = stringVal.singleQuotes;
	const_cast<std::string&>(this->stringValue) = stringVal.stringValue;

	return *this;
}

json::String &json::String::operator=(const json::String &val)
{
	if(val.getType() != JsonString)
		throw json::JsonTypeError(JsonString, val.getType());

	if (this == &val)
		return *this;

	const_cast<int&>(this->type) = JsonString;
	const_cast<bool&>(this->singleQuotes) = val.singleQuotes;
	const_cast<std::string&>(this->stringValue) = val.stringValue;
	
	return *this;
}

json::String &json::String::operator=(const std::string val)
{
	const_cast<int&>(this->type) = JsonString;
	const_cast<bool&>(this->singleQuotes) = false;
	const_cast<std::string&>(this->stringValue) = val;

	return *this;
}

json::String &json::String::operator=(const char* val)
{
	const_cast<int&>(this->type) = JsonString;
	const_cast<bool&>(this->singleQuotes) = false;
	const_cast<std::string&>(this->stringValue) = val;

	return *this;
}

bool json::String::operator==(const json::Value &val)
{
	if(type != JsonString || val.getType() != JsonString)
		return false;

	String stringVal = static_cast<String&>(const_cast<Value&>(val));

	if(singleQuotes == stringVal.singleQuotes)
		if(stringValue.compare(stringVal.stringValue) != int(0))
			return false;
	else
		return false;

	return true;
}

bool json::String::operator==(const json::String &val)
{
	if(type != JsonString || val.getType() != JsonString)
		return false;
	
	if(singleQuotes == val.singleQuotes)
		if(stringValue.compare(val.stringValue) != 0)
			return false;
	else
		return false;

	return true;
}

bool json::String::operator==(const std::string val)
{
	if(type != JsonString)
		return false;

	if(stringValue.compare(val) != 0)
		return false;

	return true;
}

bool json::String::operator==(const char* val)
{
	if(type != JsonString)
		return false;

	if(stringValue.compare(val) != 0)
		return false;

	return true;
}

bool json::String::operator!=(const json::Value &val)
{
	return !(*this == val);
}

bool json::String::operator!=(const json::String &val)
{
	return !(*this == val);
}

bool json::String::operator!=(const std::string val)
{
	return !(*this == val);
}

bool json::String::operator!=(const char* val)
{
	return !(*this == val);
}

std::string json::String::print()
{
	if(type != JsonString)
		throw json::JsonTypeError(JsonString, type);

	char *quotes;
	if(singleQuotes)
		quotes = "\'";
	else
		quotes = "\"";

	return quotes + stringValue + quotes;
}

std::string json::String::getStringValue() const
{
	return stringValue;
}
