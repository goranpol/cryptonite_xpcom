#ifndef _OneDriveObserver_h_
#define _OneDriveObserver_h_

#include "HttpObserver.h"

namespace onedrive
{
	class OneDriveObserver : public base::HttpObserver
	{
	public:
		OneDriveObserver(bool encrypt);

		NS_DECL_ISUPPORTS
		NS_DECL_NSIOBSERVER

	protected:
		~OneDriveObserver();
		void changeUsersWithAccess(std::string data);

	private:
		std::string recalculateContentRangeSingleFragment(std::string data);
		std::string recalculateContentRangeFirstFragment(std::string data);
		std::string recalculateContentRangeLastFragment(std::string data);
		std::string recalculateContentRange(std::string data);
		std::string recalculateXRequestStats(std::string data);
		bool isSingleFragment(std::string data);
		bool isFirstFragment(std::string data);
		bool isLastFragment(std::string data);
	};
}

#endif
