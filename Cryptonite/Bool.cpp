#include <mozilla/Char16.h>

#include "Bool.h"
#include "Logger.h"

#include "JsonTypeError.h"
#include "JsonBadCast.h"
#include "JsonBadAlloc.h"

json::Bool::Bool() : Value(JsonBoolean), boolValue(false)
{
}

json::Bool::Bool(const bool val) : Value(JsonBoolean), boolValue(val)
{
}

json::Bool::Bool(const json::Bool &val) : Value(val.type), boolValue(val.boolValue)
{
}

json::Bool::~Bool()
{
}

json::Bool &json::Bool::operator=(const json::Value &val)
{
	if(val.getType() != JsonBoolean)
		throw json::JsonTypeError(JsonBoolean, val.getType());

	Bool boolVal = static_cast<Bool&>(const_cast<Value&>(val));

	if (&boolVal == this)
		return *this;

	const_cast<int&>(this->type) = JsonBoolean;
	const_cast<bool&>(this->boolValue) = boolVal.boolValue;

	return *this;
}

json::Bool &json::Bool::operator=(const json::Bool &val)
{
	if(val.getType() != JsonBoolean)
		throw json::JsonTypeError(JsonBoolean, val.getType());

	if (this == &val)
		return *this;
	
	const_cast<int&>(this->type) = JsonBoolean;
	const_cast<bool&>(this->boolValue) = val.boolValue;

	return *this;
}

json::Bool &json::Bool::operator=(const bool val)
{
	const_cast<int&>(this->type) = JsonBoolean;
	const_cast<bool&>(this->boolValue) = val;

	return *this;
}

bool json::Bool::operator==(const json::Value &val)
{
	if(val.getType() != JsonBoolean || type != JsonBoolean)
		return false;

	Bool boolVal = static_cast<Bool&>(const_cast<Value&>(val));

	if(boolVal.boolValue != boolValue)
		return false;

	return true;
}

bool json::Bool::operator==(const json::Bool &val)
{
	if(val.getType() != JsonBoolean || type != JsonBoolean)
		return false;
	
	if(val.boolValue != boolValue)
		return false;

	return true;
}

bool json::Bool::operator==(const bool val)
{
	if(type != JsonBoolean)
		return false;

	if(boolValue != val)
		return false;

	return true;
}

bool json::Bool::operator!=(const json::Value &val)
{
	return !(*this == val);
}

bool json::Bool::operator!=(const json::Bool &val)
{
	return !(*this == val);
}

bool json::Bool::operator!=(const bool val)
{
	return !(*this == val);
}

std::string json::Bool::print()
{
	if(type != JsonBoolean)
		throw json::JsonTypeError(JsonBoolean, type);

	if(this->boolValue)
		return "true";
	else
		return "false";
}

bool json::Bool::getBoolValue() const
{
	return boolValue;
}
