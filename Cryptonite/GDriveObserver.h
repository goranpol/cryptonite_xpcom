#ifndef _GDriveObserver_h_
#define _GDriveObserver_h_

#include "HttpObserver.h"

namespace gdrive
{
	class GDriveObserver :	public base::HttpObserver
	{
	public:
		GDriveObserver(bool encrypt);
		
		NS_DECL_ISUPPORTS
		NS_DECL_NSIOBSERVER

	protected:
		~GDriveObserver();
		void changeUsersWithAccess(std::string data);

	private:

		std::string recalculateSize(std::string size);
		std::string recalculateContentRangeSingleFragment(std::string data);
		std::string recalculateContentRangeFirstFragment(std::string data);
		std::string recalculateContentRangeLastFragment(std::string data);
		std::string recalculateContentRange(std::string data);
		bool isSingleFragment(std::string data);
		bool isFirstFragment(std::string data);
		bool isLastFragment(std::string data);

		std::string toBase64WithPadding(const std::string input, size_t len);
		std::string fromBase64WithPadding(const std::string input);
	};
}

#endif