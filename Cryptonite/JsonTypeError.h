#ifndef _JsonTypeError_h_
#define _JsonTypeError_h_

#include "JsonError.h"

namespace json
{
	class JsonTypeError : public JsonError
	{
	public:
		JsonTypeError(const int expectedTyp, const int givenTyp);
		~JsonTypeError();

		const char* what() const throw();

	private:
		/***************************************************************************************************/
		/************************************JsonTypeError members******************************************/
		/***************************************************************************************************/
		const int expectedType;										/* type which is expected */
		const int givenType;										/* type that was given */
		/***************************************************************************************************/
		/************************************JsonTypeError members******************************************/
		/***************************************************************************************************/
	};
}

#endif
