#ifndef _GMailObserver_h_
#define _GMailObserver_h_

#include "HttpObserver.h"

namespace gmail
{
	class GMailObserver : public base::HttpObserver
	{
	public:
		GMailObserver(bool encrypt);

		NS_DECL_ISUPPORTS
		NS_DECL_NSIOBSERVER

	protected:

	private:
		~GMailObserver();
	};
}

#endif