#include <mozilla/Char16.h>

#include "GDocumentObserver.h"

#include "Logger.h"

#include "GDocumentListener.h"
#include "ObjectStorage.h"
#include "GDocument.h"

#include "NSException.h"
#include "ObjectNotFoundException.h"
#include "ObjectTypeException.h"
#include "AlgorithmException.h"
#include "SingletoneException.h"
#include "AESException.h"

NS_IMPL_ISUPPORTS(gdocument::GDocumentObserver, nsIObserver)

gdocument::GDocumentObserver::GDocumentObserver(bool encrypt) : GDriveObserver(encrypt)
{
}

gdocument::GDocumentObserver::~GDocumentObserver()
{
}

NS_IMETHODIMP gdocument::GDocumentObserver::Observe(nsISupports *aSubject, const char *aTopic, const char16_t *aData)
{
	nsresult rv;

	nsCOMPtr<nsIHttpChannel> httpChannel = do_QueryInterface(aSubject, &rv);
	if(NS_FAILED(rv))
	{
		// inform javascript
		Logger::errorMessage("GDocumentObserver", "Observe", "Observe FAILED: couldn't query interface to nsIHttpChannel", rv);
		return NS_OK;
	}

	nsAutoCString urlPath;
	rv = getUri(httpChannel, urlPath);
	if(NS_FAILED(rv))
	{
		// inform javascript
		Logger::errorMessage("GDocumentObserver", "Observe", "Observe FAILED: couldn't get uri", rv);
		return NS_OK;
	}
		
	if(!strcmp(aTopic, "http-on-modify-request"))
	{
		if(urlPath.Find("google.") != -1 && urlPath.Find("/document") != -1 && urlPath.Find("/d/") != -1)
		{
			nsCOMPtr<nsIUploadChannel> uploadChannel = do_QueryInterface(httpChannel, &rv);
			if(NS_FAILED(rv))
			{
				// inform javascript
				Logger::errorMessage("GDocumentObserver", "Observe:http-on-modify-request", "Observe FAILED: couldn't query interface to nsIUploadChannel", rv);
				return NS_OK;
			}

			if(urlPath.Find("save?") != -1)
			{
				try
				{
					base::Object obj = base::ObjectStorage::getInstance().getObject(base::ObjectRequest(getDocumentId(urlPath.get())), NITE_GDOC);
					gdocument::GDocument gDoc(obj);
					std::string data = readSendingData(uploadChannel);

					// Logger::logMessage("", "", (char*)data.c_str());
					// 1. FIX TABLES
					// bundles=[{"commands":[{"ty":"mlti","mts":[{"ty":"is","ibi":9812,"s":"\n\u0010\u0012\u001c\n\u001c\n\u001c\n\u001c\n\u0012\u001c\n\u001c\n\u001c\n\u001c\n\u0012\u001c\n\u001c\n\u001c\n\u001c\n\u0011"},{"ty":"as", ...
					// 2. FIX LINKS
					// the problem is when you split first encoded block because "as" "text"
					// 3. FIX FOOTNOTE
					// 4. FIX INSERT HORIZONTAL LINE
					// the problem is when you split first encoded block because "as" "text" (same as links)
					// 5. FIX INSERT PAGE NUMBER
					// 6. FIX HEADER FOOTER STUFF
					// 7. FIX TABLE OF CONTENT INSERT
					// 8. FIX INSERT PAGE BREAK

					try
					{
						gui::GUIObserver::getInstance().notifyObserver(NITE_ENCRYPT, "google document");
						std::string encryptedData = gDoc.encryptOutgoingString(data);

						replaceSendingData(uploadChannel, encryptedData);
						return NS_OK;
					}
					catch (...)
					{
						replaceSendingData(uploadChannel, data);
						return NS_OK;
					}
				}
				catch(exception::technical::NSException)
				{
					// inform javascript
				}
				catch(exception::domain::ObjectNotFoundException)
				{
					// inform javascript
				}
				catch(exception::domain::ObjectTypeException)
				{
					// inform javascript
				}
				catch(exception::domain::AlgorithmException)
				{
					// inform javascript
				}
				catch(exception::technical::SingletoneException)
				{
					// infrom javascript
				}
				catch(exception::domain::AESException)
				{
					// inform javascript
				}
				catch(...)
				{
					// inform javascript (use UnknownException)
				}

				return NS_OK;
			}
		}
		else if(((urlPath.Find("google.") != -1) && (urlPath.Find("/commonshare") != -1)))
		{
			nsCOMPtr<nsIUploadChannel> uploadChannel = do_QueryInterface(httpChannel, &rv);
			if(NS_FAILED(rv))
			{
				// inform javscript
				Logger::errorMessage("GDocumentObserver", "Observe:http-on-modify-request", "Observe FAILED: couldn't query interface to nsIUploadChannel", rv);
				return NS_OK;
			}

			try
			{
				std::string data = readSendingData(uploadChannel);
				
				try
				{
					changeUsersWithAccess(data);
					replaceSendingData(uploadChannel, data);
					return NS_OK;
				}
				catch (...)
				{
					replaceSendingData(uploadChannel, data);
					return NS_OK;
				}
			}
			catch(exception::technical::NSException)
			{
				// inform javascript
			}
			catch(exception::domain::AlgorithmException)
			{
				// inform javascript
			}
			catch(...)
			{
				// inform javascript
			}

			return NS_OK;
		}
	}
	else if(!strcmp(aTopic, "http-on-examine-response"))
	{
		if(urlPath.Find("google.") != -1 && urlPath.Find("document") != -1 && urlPath.Find("create") != -1 && doEncrypt)
		{
			nsCOMPtr<nsIHttpChannel> httpChannel = do_QueryInterface(aSubject, &rv);
			if(NS_FAILED(rv))
			{
				// inform javascript
				Logger::errorMessage("GDocumentObserver", "Observe:http-on-modify-request", "Observe FAILED: couldn't query interface aSubject to nsIHttpChannel", rv);
				return NS_OK;
			}

			try
			{
				nsAutoCString changedLocation;
				rv = httpChannel->GetResponseHeader(nsAutoCString("Location"), changedLocation);
				if(NS_FAILED(rv))
				{
					// inform javascript
					Logger::errorMessage("GDocumentObserver", "Observe:http-on-modify-request", "Observe FAILED: couldn't get Location header", rv);
					return NS_OK;
				}
				
				base::ObjectStorage::getInstance().addObject(gdocument::GDocument(getDocumentId(std::string(changedLocation.get()))), base::UsersList());
			}
			catch(exception::technical::NSException)
			{
				// inform javascript
			}
			catch(exception::domain::ObjectTypeException)
			{
				// inform javascript
			}
			catch(...)
			{
				// infrom javascript
			}

			return NS_OK;
		}
		else if(urlPath.Find("google.") != -1 && urlPath.Find("/document") != -1 && urlPath.Find("/d/") != -1)
		{
			nsCOMPtr<nsITraceableChannel> traceableChannel = do_QueryInterface(aSubject, &rv);
			if(NS_FAILED(rv))
			{
				// infrom javascript
				Logger::errorMessage("GDocumentObserver", "Observe:http-on-examine-response", "Observe FAILED: couldn't query interface to nsITraceableChannel", rv);
				return NS_OK;
			}

			try
			{
				/*******************************************************************************************************************************/
				base::Object obj = base::ObjectStorage::getInstance().getObject(base::ObjectRequest(getDocumentId(urlPath.get())), NITE_GDOC);

				RefPtr<base::HttpListener> gDocumentListener;
				if(urlPath.Find("edit") != -1)
				{
					gui::GUIObserver::getInstance().notifyObserver(NITE_DECRYPT, "google document");
					gDocumentListener = new gdocument::GDocumentListener(obj);
				}
				else
				{
					return NS_OK;
				}
				/*******************************************************************************************************************************/

				if(!gDocumentListener)
				{
					Logger::errorMessage("GDocumentObserver", "Observe:http-on-examine-response", "Observe FAILED: couldn't create GDocumentListener", nsresult(10000));
					// notify javascript
					return NS_OK;
				}

				replaceStreamListener(traceableChannel, gDocumentListener);
			}
			catch(exception::domain::ObjectNotFoundException)
			{
				// infrom javascript
			}
			catch(exception::technical::NSException)
			{
				// infrom javascript
			}
			catch(exception::technical::SingletoneException)
			{
				// infrom javascript
			}
			catch(exception::domain::ObjectTypeException)
			{
				// inform javascript
			}
			catch(...)
			{
				// infrom javascript
			}

			return NS_OK;
		}
	}

	return NS_OK;
}

std::string gdocument::GDocumentObserver::getDocumentId(std::string url)
{
	if (url.find("/d/") != -1)
	{
		size_t idStartPos = url.find("/d/") + 3;
		std::string documentId = url.substr(idStartPos);
		documentId = documentId.substr(0, documentId.find("/"));
		
		return documentId;
	}

	throw exception::domain::ObjectNotFoundException("no document id");
}