#include <mozilla/Char16.h>

#include "MSAttachmentObserver.h"
#include "MSAttachmentListener.h"

#include "NSException.h"

#include "Logger.h"

NS_IMPL_ISUPPORTS(hotmail::MSAttachmentObserver, nsIObserver)

hotmail::MSAttachmentObserver::MSAttachmentObserver(bool encrypt) : HttpObserver(encrypt)
{
}


hotmail::MSAttachmentObserver::~MSAttachmentObserver()
{
}

NS_IMETHODIMP hotmail::MSAttachmentObserver::Observe(nsISupports *aSubject, const char *aTopic, const char16_t *aData)
{
	nsresult rv;

	nsCOMPtr<nsIHttpChannel> httpChannel = do_QueryInterface(aSubject, &rv);
	if (NS_FAILED(rv))
	{
		// notify javascript
		Logger::errorMessage("MSAttachmentObserver", "Observe", "Observe FAILED: couldn't query interface to nsIHttpChannel", rv);
		return NS_OK;
	}

	nsAutoCString urlPath;
	rv = getUri(httpChannel, urlPath);
	if (NS_FAILED(rv))
	{
		// notify javascript
		Logger::errorMessage("MSAttachmentObserver", "Observe", "Observe FAILED: couldn't get uri", rv);
		return NS_OK;
	}

	if (!strcmp(aTopic, "http-on-examine-response"))
	{
		RefPtr<hotmail::MSAttachmentListener> hotmailAttachmentListener;
		if (urlPath.Find("GetAttachment.aspx") != -1)
		{
			gui::GUIObserver::getInstance().notifyObserver(NITE_DECRYPT, "microsoft attachment");
			hotmailAttachmentListener = new hotmail::MSAttachmentListener();
		}
		else
		{
			return NS_OK;
		}

		nsCOMPtr<nsIHttpChannel> httpChannel = do_QueryInterface(aSubject, &rv);
		if (NS_FAILED(rv))
		{
			// notify javascript
			Logger::errorMessage("MSAttachmentObserver", "Observe:http-on-examine-response", "Observe FAILED: couldn't query interface aSubject to nsIHttpChannel", rv);
			return NS_OK;
		}

		nsCOMPtr<nsITraceableChannel> traceableChannel = do_QueryInterface(aSubject, &rv);
		if (NS_FAILED(rv))
		{
			// notify javascript
			Logger::errorMessage("MSAttachmentObserver", "Observe:http-on-examine-response", "Observe FAILED: couldn't query interface to nsITraceableChannel", rv);
			return NS_OK;
		}

		try
		{
			replaceStreamListener(traceableChannel, hotmailAttachmentListener);
		}
		catch (exception::technical::NSException)
		{
			// notify javascript
		}
		catch (...)
		{
			// notify javascript
		}

		return NS_OK;
	}
	return NS_OK;
}
