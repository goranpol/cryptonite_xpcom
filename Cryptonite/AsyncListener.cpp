#include <mozilla/Char16.h>

#include "AsyncListener.h"

#include "nsCOMPtr.h"
#include "nsIBinaryInputStream.h"
#include "nsIBinaryOutputStream.h"
#include "nsIStorageStream.h"
#include "nsIComponentManager.h"

#include "Logger.h"

#include "Parser.h"

#include "ServerException.h"

NS_IMPL_ISUPPORTS(network::AsyncListener, nsIRequestObserver, nsIStreamListener)

network::AsyncListener::AsyncListener(const std::string msg) : message(msg)
{
}

network::AsyncListener::~AsyncListener()
{
}

NS_IMETHODIMP network::AsyncListener::OnDataAvailable(nsIRequest *aRequest, nsISupports *aContext, nsIInputStream *aInputStream, uint64_t aOffset, uint32_t aCount)
{
	nsresult rv;

	// if NS_FAILED send something to javascript (use ServerException)
	nsCOMPtr<nsIComponentManager> componentManager;
	rv = NS_GetComponentManager(getter_AddRefs(componentManager));
	if(NS_FAILED(rv))
	{
		return NS_OK;
	}

	nsCOMPtr<nsIBinaryInputStream> binaryInputStream;
	rv = componentManager->CreateInstanceByContractID("@mozilla.org/binaryinputstream;1", nullptr, NS_GET_IID(nsIBinaryInputStream), getter_AddRefs(binaryInputStream));
	if(NS_FAILED(rv))
	{
		return NS_OK;
	}

	rv = binaryInputStream->SetInputStream(aInputStream);
	if(NS_FAILED(rv))
	{
		return NS_OK;
	}

	mData = (char*)NS_Realloc((void*)mData, sizeof(char) * (aCount + mRead));
	if(!mData)
	{
		return NS_OK;
	}
	
	char* readData = (char*)NS_Alloc(sizeof(char) * aCount);
	if(!mData)
	{
		return NS_OK;
	}

	rv = binaryInputStream->ReadBytes(aCount, &readData);
	if(!mData)
	{
		return NS_OK;
	}

	memcpy(mData + mRead, readData, aCount);

	mRead += aCount;
	NS_Free(readData);

	return NS_OK;
}

NS_IMETHODIMP network::AsyncListener::OnStartRequest(nsIRequest *aRequest, nsISupports *aContext)
{
	mData = nullptr;
	mRead = 0;
	
	return NS_OK;
}

NS_IMETHODIMP network::AsyncListener::OnStopRequest(nsIRequest *aRequest, nsISupports *aContext, nsresult aStatusCode)
{
	try
	{
		Logger::logMessage("", "async return", mData);

		std::string response = mData;
		json::JSON responseJson = json::JSON(response);
		if(responseJson.getString("errm").compare("OK") == 0)
		{
			return NS_OK;
		}

		Logger::errorMessage("AsyncListener", "OnStopRequest", (char*)responseJson.getString("errm").c_str(), nsresult(responseJson.getInt("errn")));
		// try again and again
		// if it still fails send a error message to javascript to open a error dialog and (use ServerException)

		NS_Free(mData);
		return NS_OK;
	}
	catch(...)
	{
		// Logger::errorMessage("AsyncListener", "OnStopRequest", mData, nsresult(-1));
		NS_Free(mData);
		return NS_OK;
	}
}
