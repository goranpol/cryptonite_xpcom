#include <mozilla/Char16.h>

#include "MasterObserver.h"

#include "nsIRandomGenerator.h"
#include "nsIComponentManager.h"
#include "nsIURI.h"
#include "nsISeekableStream.h"
#include "nsIStringStream.h"
#include "nsIBinaryInputStream.h"

#include "nsIServiceManager.h"
#include "nsIObserverService.h"
#include "nsIURI.h"

#include "GDocumentObserver.h"
#include "GDriveObserver.h"
#include "GMailObserver.h"
#include "HotmailObserver.h"
#include "GAttachmentObserver.h"
#include "MSAttachmentObserver.h"
#include "OneDriveObserver.h"
#include "HotmailNewObserver.h"
#include "MSAttachmentNewObserver.h"

#include <nsStringAPI.h>

#include "Parser.h"

#include "Logger.h"

#include "NSException.h"
#include "SingletoneException.h"

RefPtr<base::MasterObserver> base::MasterObserver::mMasterObserver = nullptr;
NS_IMPL_ISUPPORTS(base::MasterObserver, nsIObserver)

// doEncrypt has to be false and changed in Observe method
base::MasterObserver::MasterObserver() : gMailFlag(false), gDriveFlag(false), hotmailFlag(false), oneDriveFlag(false), doEncrypt(true)
{
}

base::MasterObserver::~MasterObserver()
{
}

base::MasterObserver &base::MasterObserver::getInstance()
{
	if(mMasterObserver == nullptr)
		throw exception::technical::SingletoneException("MasterObserver");

	return *mMasterObserver;
}

void base::MasterObserver::registerObserver()
{
	std::string action = "registering master observer";
	nsresult rv;

	Logger::logMessage("MasterObserver", "registerObserver", "REGISTERING MasterObserver");
	nsCOMPtr<nsIServiceManager> servMan;
	rv = NS_GetServiceManager(getter_AddRefs(servMan));
	if(NS_FAILED(rv))
	{
		Logger::errorMessage("MasterObserver", "registerObserver", "registering MasterObserver FAILED: couldn't obtain nsIServiceManager", rv);
		throw exception::technical::NSException(rv, action);
	}

	nsCOMPtr<nsIObserverService> observerService;
	rv = servMan->GetServiceByContractID("@mozilla.org/observer-service;1", NS_GET_IID(nsIObserverService), getter_AddRefs(observerService));
	if(NS_FAILED(rv))
	{
		Logger::errorMessage("MasterObserver", "registerObserver", "registering MasterObserver FAILED: couldn't obtain nsIObserverService(CLASS @mozilla.org/observer-service;1)", rv);
		throw exception::technical::NSException(rv, action);
	}
	
	rv = observerService->AddObserver(&MasterObserver::getInstance(), "http-on-modify-request", false);
	if(NS_FAILED(rv))
	{
		Logger::errorMessage("MasterObserver", "registerObserver", "registering MasterObserver FAILED: couldn't register for http-on-modify-request", rv);
		throw exception::technical::NSException(rv, action);
	}
	
	rv = observerService->AddObserver(&MasterObserver::getInstance(), "http-on-examine-response", false);
	if(NS_FAILED(rv))
	{
		Logger::errorMessage("MasterObserver", "registerObserver", "registering MasterObserver FAILED: couldn't register for http-on-examine-response", rv);
		throw exception::technical::NSException(rv, action);
	}

	rv = observerService->AddObserver(&MasterObserver::getInstance(), "crypt-o-nite-change-flags", false);
	if(NS_FAILED(rv))
	{
		Logger::errorMessage("MasterObserver", "registerObserver", "registering MasterObserver FAILED: couldn't register for crypt-o-nite-change_flags", rv);
		throw exception::technical::NSException(rv, action);
	}

	Logger::logMessage("MasterObserver", "registerObserver", "registering MasterObserver SUCESESSFUL");
}

void base::MasterObserver::unregisterObserver()
{
	std::string action = "unregistering master observer";
	nsresult rv;

	Logger::logMessage("MasterObserver", "unregisterObserver", "UNREGISTERING MasterObserver");
	nsCOMPtr<nsIServiceManager> servMan;
	rv = NS_GetServiceManager(getter_AddRefs(servMan));
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	nsCOMPtr<nsIObserverService> observerService;
	rv = servMan->GetServiceByContractID("@mozilla.org/observer-service;1", NS_GET_IID(nsIObserverService), getter_AddRefs(observerService));
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	rv = observerService->RemoveObserver(&MasterObserver::getInstance(), "http-on-modify-request");
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	rv = observerService->RemoveObserver(&MasterObserver::getInstance(), "http-on-examine-response");
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	rv = observerService->RemoveObserver(&MasterObserver::getInstance(), "crypt-o-nite-change-flags");
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	Logger::logMessage("MasterObserver", "unregisterObserver", "unregistering MasterObserver SUCESESSFUL");
}

NS_IMETHODIMP base::MasterObserver::Observe(nsISupports *aSubject, const char *aTopic, const char16_t *aData)
{
	nsresult rv;

	// make one for doEncrypt flag too

	// change this topic maybe to something like cryptonite-activation-flags
	if(!strcmp(aTopic, "crypt-o-nite-change-flags"))
	{
		try
		{
			std::string tmp = toString(nsAutoString(aData));
			json::JSON flags = json::JSON(tmp);

			if (flags["name"] == "gdrive")
				gDriveFlag = flags.getBool("value");
			else if (flags["name"] == "onedrive")
				oneDriveFlag = flags.getBool("value");
			else if(flags["name"] == "gmail")
				gMailFlag = flags.getBool("value");
			else if(flags["name"] == "hotmail")
				hotmailFlag = flags.getBool("value");
			// others

		}
		catch(...)
		{
			// some error to javascript
		}

		return NS_OK;
	}

	nsCOMPtr<nsIHttpChannel> httpChannel = do_QueryInterface(aSubject, &rv);
	if(NS_FAILED(rv))
	{
		Logger::errorMessage("MasterObserver", "Observe", "Observe FAILED: couldn't query interface to nsIHttpChannel", rv);
		return NS_OK;
	}

	nsAutoCString urlPath;
	try
	{
		getUri(httpChannel, urlPath);
	}
	catch(exception::technical::NSException)
	{
		// notify javascript
	}
	catch(...)
	{
		// notify javascript
	}

	RefPtr<HttpObserver> observer;
	
	try
	{
		/* get this stuff from the server (put it in preferences) */
		if (urlPath.Find("google.com/upload/drive") != -1 && gDriveFlag)
		{
			observer = new gdrive::GDriveObserver(doEncrypt);
			observer->Observe(aSubject, aTopic, aData);
		}
		else if (urlPath.Find("google.com/batch/drive") != -1 && gDriveFlag)
		{
			observer = new gdrive::GDriveObserver(doEncrypt);
			observer->Observe(aSubject, aTopic, aData);
		}
		else if (urlPath.Find("docs.google.") != -1 && gDriveFlag)
		{
			observer = new gdocument::GDocumentObserver(doEncrypt);
			observer->Observe(aSubject, aTopic, aData);
		}
		else if (urlPath.Find("drive.google.") != -1 && gDriveFlag)
		{
			observer = new gdrive::GDriveObserver(doEncrypt);
			observer->Observe(aSubject, aTopic, aData);
		}
		else if (urlPath.Find("mail.google.") != -1 && gMailFlag)
		{
			observer = new gmail::GMailObserver(doEncrypt);
			observer->Observe(aSubject, aTopic, aData);
		}
		else if (urlPath.Find("mail-attachment.googleusercontent") != -1 && gMailFlag)
		{
			observer = new gmail::GAttachmentObserver(doEncrypt);
			observer->Observe(aSubject, aTopic, aData);
		}
		else if (urlPath.Find(".googleusercontent.") != -1 && gDriveFlag)
		{
			observer = new gdrive::GDriveObserver(doEncrypt);
			observer->Observe(aSubject, aTopic, aData);
		}
		else if (urlPath.Find("mail.live.") != -1 && hotmailFlag)
		{
			observer = new hotmail::HotmailObserver(doEncrypt);
			observer->Observe(aSubject, aTopic, aData);
		}
		else if (urlPath.Find("afx.ms") != -1 && hotmailFlag)
		{
			observer = new hotmail::MSAttachmentObserver(doEncrypt);
			observer->Observe(aSubject, aTopic, aData);
		}
		else if (urlPath.Find("users.storage.live.") != -1 && oneDriveFlag)
		{
			observer = new onedrive::OneDriveObserver(doEncrypt);
			observer->Observe(aSubject, aTopic, aData);
		}
		else if (urlPath.Find("onedrive.live") != -1 && urlPath.Find("download.aspx") != -1 && oneDriveFlag)
		{
			observer = new onedrive::OneDriveObserver(doEncrypt);
			observer->Observe(aSubject, aTopic, aData);
		}
		else if (urlPath.Find("files.1drv") != -1 && urlPath.Find("download") != -1 && oneDriveFlag)
		{
			observer = new onedrive::OneDriveObserver(doEncrypt);
			observer->Observe(aSubject, aTopic, aData);
		}
		else if (urlPath.Find("livefilestore") != -1 && urlPath.Find("download") != -1 && oneDriveFlag)
		{
			observer = new onedrive::OneDriveObserver(doEncrypt);
			observer->Observe(aSubject, aTopic, aData);
		}
		else if (urlPath.Find("skyapi.onedrive.live") != -1 && urlPath.Find("SetPermissions") != -1 && oneDriveFlag)
		{
			observer = new onedrive::OneDriveObserver(doEncrypt);
			observer->Observe(aSubject, aTopic, aData);
		}
		else if (urlPath.Find("attachment.outlook.office") != -1 && urlPath.Find("service.svc") != -1 && hotmailFlag)
		{
			observer = new hotmail::MSAttachmentNewObserver(doEncrypt);
			observer->Observe(aSubject, aTopic, aData);
		}
		else if (urlPath.Find("outlook.live") != -1 && hotmailFlag)
		{
			observer = new hotmail::HotmailNewObserver(doEncrypt);
			observer->Observe(aSubject, aTopic, aData);
		}
		// others

		return NS_OK;
	}
	catch (...)
	{
		Logger::errorMessage("MasterObserver", "Observe", "Observe FAILED: exception in one of the subclasses", nsresult(1));
		// notify javascript
	}

	return NS_OK;
}

void base::MasterObserver::getUri(nsIHttpChannel *httpChannel, nsACString &url)
{
	std::string action = "getting url";
	nsresult rv;
	nsIURI *uri;
	nsAutoCString urlPrePath;
	nsAutoCString urlPath;

	rv = httpChannel->GetURI(&uri);
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}
	rv = uri->GetPrePath(urlPrePath);
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}
	rv = uri->GetPath(urlPath);
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	url.Append(urlPrePath);
	url.Append(urlPath);
}

std::string base::MasterObserver::toString(nsEmbedString s)
{
	PRUnichar *start = (PRUnichar *)s.get();
	PRUnichar *end = (PRUnichar *)s.get() + s.Length();
	wchar_t wca[4096];
	wchar_t *wstart = wca;
	wchar_t *wpr = wstart;

	for(; start < end; ++start)
	{
		*wstart = (wchar_t) *start;
		++wstart;
	}
	*wstart = 0;


	std::string ptr;
	ptr.resize(4096);
	size_t size = wcstombs((char*)ptr.data(), wpr, 4096);
	ptr.resize(size);

	return ptr;
}
