#include <mozilla/Char16.h>

#include "JsonKeyNotFound.h"

json::JsonKeyNotFound::JsonKeyNotFound(const std::string k) : key(k)
{
}

json::JsonKeyNotFound::~JsonKeyNotFound()
{
}

const char* json::JsonKeyNotFound::what() const throw()
{
	std::string returnString = "Json error: key: " + key + " not found";
	return fromStdString(returnString);
}