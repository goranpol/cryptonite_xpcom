#include <mozilla/Char16.h>

#include "GMailAttachment.h"

#include "ObjectNotFoundException.h"
#include "NotImplementedException.h"
#include "AESException.h"
#include "AlgorithmException.h"
#include "ObjectTypeException.h"


gmail::GMailAttachment::GMailAttachment(const std::string objId) : base::Attachment(objId, generateKey(), generateKey(), generateCheckWord(), NITE_GATTACH)
{
}

gmail::GMailAttachment::GMailAttachment(const std::string objId, const std::string objKey, const std::string objInitChain, const std::string objCheckWord) : base::Attachment(objId, objKey, objInitChain, objCheckWord, NITE_GATTACH)
{
}

gmail::GMailAttachment::GMailAttachment(const base::Object &obj) : base::Attachment(obj.getObjectId(), obj.getObjectKey(), getInitChain(obj), getCheckWord(obj), NITE_GATTACH)
{
}

gmail::GMailAttachment::GMailAttachment(const gmail::GMailAttachment &obj) : base::Attachment(obj.objectId, obj.objectKey, obj.objectInitChain, obj.objectCheckWord, NITE_GATTACH)
{
}

gmail::GMailAttachment::~GMailAttachment()
{
}

gmail::GMailAttachment &gmail::GMailAttachment::operator=(Object &obj)
{
	if (obj.getObjectType() != NITE_GATTACH)
		throw exception::domain::ObjectTypeException(NITE_GATTACH, obj.getObjectType());

	gmail::GMailAttachment gMailAttachment = static_cast<gmail::GMailAttachment&>(obj);
	
	if (this == &gMailAttachment)
		return *this;

	const_cast<int&>(objectType) = NITE_GATTACH;
	const_cast<std::string&>(objectId) = gMailAttachment.objectId;
	const_cast<std::string&>(objectKey) = gMailAttachment.objectKey;
	const_cast<std::string&>(objectInitChain) = gMailAttachment.objectInitChain;
	const_cast<std::string&>(objectCheckWord) = gMailAttachment.objectCheckWord;

	return *this;
}

gmail::GMailAttachment &gmail::GMailAttachment::operator=(GMailAttachment &obj)
{
	if (this == &obj)
		return *this;

	const_cast<int&>(objectType) = NITE_GATTACH;
	const_cast<std::string&>(objectId) = obj.objectId;
	const_cast<std::string&>(objectKey) = obj.objectKey;
	const_cast<std::string&>(objectInitChain) = obj.objectInitChain;
	const_cast<std::string&>(objectCheckWord) = obj.objectCheckWord;

	return *this;
}

bool gmail::GMailAttachment::operator==(Object &obj)
{
	gmail::GMailAttachment gMailAttachment = static_cast<gmail::GMailAttachment&>(obj);

	if (objectId.compare(gMailAttachment.objectId) == 0 && objectKey.compare(gMailAttachment.objectKey) == 0 && objectInitChain.compare(gMailAttachment.objectInitChain) == 0 && objectCheckWord.compare(gMailAttachment.objectCheckWord) == 0)
		return true;

	return false;
}

bool gmail::GMailAttachment::operator!=(Object &obj)
{
	return !(*this == obj);
}
