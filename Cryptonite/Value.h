#ifndef _Value_h_
#define _Value_h_

#define JsonString			0
#define JsonNumber			1
#define JsonBoolean			2
#define JsonNull			3
#define JsonEmpty			4
#define JsonEnumeration		5

#include <inttypes.h>
#include <string>
#include <vector>

namespace json
{
	class Value
	{
	public:
		Value();
		Value(const Value &val);
		virtual ~Value();

		virtual Value &operator=(const Value &val);

		virtual bool operator==(const Value &val);
		virtual bool operator==(const std::string val);
		virtual bool operator==(const char* val);
		virtual bool operator==(const bool val);
		virtual bool operator==(const int64_t val);
		virtual bool operator==(const int val);
		virtual bool operator==(const float val);
		virtual bool operator==(const double val);

		virtual bool operator!=(const Value &val);
		virtual bool operator!=(const std::string val);
		virtual bool operator!=(const char* val);
		virtual bool operator!=(const bool val);
		virtual bool operator!=(const int64_t val);
		virtual bool operator!=(const int val);
		virtual bool operator!=(const float val);
		virtual bool operator!=(const double val);

		/* access operators */
		virtual Value &operator[](const size_t index);
		virtual Value &operator[](const std::string key);
		virtual Value &operator[](const char* key);

		/* gets if array */
		virtual Value &get(const size_t index);
		virtual std::string getString(const size_t index);
		virtual bool getBool(const size_t index);
		virtual int64_t getInt64(const size_t index);
		virtual int getInt(const size_t index);
		virtual double getDouble(const size_t index);

		/* gets if object */
		virtual Value &get(const std::string key);
		virtual std::string getString(const std::string key);
		virtual bool getBool(const std::string key);
		virtual int64_t getInt64(const std::string key);
		virtual int getInt(const std::string key);
		virtual double getDouble(const std::string key);

		/* replaces if array */
		virtual void replace(const size_t index, const Value &val);
		virtual void replace(const size_t index, const std::string val);
		virtual void replace(const size_t index, const char* val);
		virtual void replace(const size_t index, const bool val);
		virtual void replace(const size_t index, const int64_t val);
		virtual void replace(const size_t index, const int val);
		virtual void replace(const size_t index, const float val);
		virtual void replace(const size_t index, const double val);
		virtual void replace(const size_t index);
		virtual void replaceArray(const size_t index);
		virtual void replaceObject(const size_t index);
		virtual void replaceJavascript(const size_t index, const std::string name = "");

		/* replaces if object */
		virtual void replace(const std::string key, const Value &val);
		virtual void replace(const std::string key, const std::string val);
		virtual void replace(const std::string key, const char* val);
		virtual void replace(const std::string key, const bool val);
		virtual void replace(const std::string key, const int64_t val);
		virtual void replace(const std::string key, const int val);
		virtual void replace(const std::string key, const float val);
		virtual void replace(const std::string key, const double val);
		virtual void replace(const std::string key);
		virtual void replaceArray(const std::string key);
		virtual void replaceObject(const std::string key);
		virtual void replaceJavascript(const std::string key, const std::string name = "");

		/* inserts if array */
		virtual void insert(const size_t index, const Value &val);
		virtual void insert(const size_t index, const std::string val);
		virtual void insert(const size_t index, const char* val);
		virtual void insert(const size_t index, const bool val);
		virtual void insert(const size_t index, const int64_t val);
		virtual void insert(const size_t index, const int val);
		virtual void insert(const size_t index, const float val);
		virtual void insert(const size_t index, const double val);
		virtual void insert(const size_t index);
		virtual void insertArray(const size_t index);
		virtual void insertObject(const size_t index);
		virtual void insertJavascript(const size_t index, const std::string name = "");

		/* inserts if object */
		virtual void insert(const std::string key, const Value &val, const size_t index);
		virtual void insert(const std::string key, const std::string val, const size_t index);
		virtual void insert(const std::string key, const char* val, const size_t index);
		virtual void insert(const std::string key, const bool val, const size_t index);
		virtual void insert(const std::string key, const int64_t val, const size_t index);
		virtual void insert(const std::string key, const int val, const size_t index);
		virtual void insert(const std::string key, const float val, const size_t index);
		virtual void insert(const std::string key, const double val, const size_t index);
		virtual void insert(const std::string key, const size_t index);
		virtual void insertArray(const std::string key, const size_t index);
		virtual void insertObject(const std::string key, const size_t index);
		virtual void insertJavascript(const std::string key, const size_t index, const std::string name = "");

		/* pushBacks if array */
		virtual void pushBack(const Value &val);
		virtual void pushBack(const bool val);
		virtual void pushBack(const int64_t val);
		virtual void pushBack(const int val);
		virtual void pushBack(const float val);
		virtual void pushBack(const double val);
		virtual void pushBack();
		virtual void pushBackArray();
		virtual void pushBackObject();

		/* pushBack for array (String, char*) or object (key and null) */
		virtual void pushBack(const std::string val);
		virtual void pushBack(const char* val);
		virtual void pushBackJavascript(const std::string key, const std::string name = "");

		/* pushBacks if object */
		virtual void pushBack(const std::string key, const Value &val);
		virtual void pushBack(const std::string key, const std::string val);
		virtual void pushBack(const std::string key, const char* val);
		virtual void pushBack(const std::string key, const bool val);
		virtual void pushBack(const std::string key, const int64_t val);
		virtual void pushBack(const std::string key, const int val);
		virtual void pushBack(const std::string key, const float val);
		virtual void pushBack(const std::string key, const double val);
		virtual void pushBackArray(const std::string key);
		virtual void pushBackObject(const std::string key);

		/* removes for array and object */
		virtual void remove(const size_t index);
		virtual void remove(const std::string key);

		/* empty & clear */
		virtual bool empty();
		virtual void clear();

		/* check name if javascript function */
		virtual bool checkName(const std::string nm);
		virtual bool checkName(const char* nm);

		/* print */
		virtual std::string print();
		virtual size_t size();

		int getType() const;

		friend class Enumeration;

	protected:
		Value(const int type);

		void skipSpaces(std::string &jsonStr);

		/*******************************************************************************************/
		/************************************Value members******************************************/
		/*******************************************************************************************/
		const int type;					/* 0 - String, 1 - Number, 2 - Boolean, 3 - Null, 4 - Empty, 5 - Enumeration */
		/*******************************************************************************************/
		/************************************Value members******************************************/
		/*******************************************************************************************/

	private:

	};
}

#endif