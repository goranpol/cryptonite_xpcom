#include <mozilla/Char16.h>

#include "JsonTypeNotDefined.h"

#include <sstream>

json::JsonTypeNotDefined::JsonTypeNotDefined(const int typeNumber) : type(typeNumber)
{
}

json::JsonTypeNotDefined::~JsonTypeNotDefined()
{
}

const char* json::JsonTypeNotDefined::what() const throw()
{
	std::stringstream typeString("Json error: Error in json type number: ");
	typeString << type;
	typeString << " (not in range 0-5 --> 0 = JsonString, 1 = JsonNumber, 2 = JsonBoolean, 3 = JsonNull, 4 = JsonEmpty, 5 = JsonEnumeration) ";

	return fromStdString(typeString.str());
}

