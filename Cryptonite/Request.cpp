#include <mozilla/Char16.h>

#include "Request.h"

#include "nsIServiceManager.h"
#include "nsIComponentManager.h"
#include "nsIRandomGenerator.h"
#include "nsIURI.h"
#include "nsCOMPtr.h"

#include "nsIIOService.h"
#include "nsIChannel.h"
#include "nsIUploadChannel.h"
#include "nsIHttpChannel.h"

#include "nsIStringStream.h"
#include "nsISeekableStream.h"
#include "nsIInputStream.h"
#include "nsIBinaryInputStream.h"

#include "nsStringAPI.h"

#include "AsyncListener.h"

#include "Logger.h"

#include "NSException.h"

#include "nsComponentManagerUtils.h"


network::Request::Request(const std::string mess) : message(mess)
{
}

network::Request::~Request()
{
}

std::string network::Request::sendRequest()
{
	std::string action = "sending crypt-o-nite request";
	nsresult rv;

	nsCOMPtr<nsIServiceManager> servMan;
	rv = NS_GetServiceManager(getter_AddRefs(servMan));
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	nsCOMPtr<nsIIOService> ioService;
	rv = servMan->GetServiceByContractID("@mozilla.org/network/io-service;1", NS_GET_IID(nsIIOService), getter_AddRefs(ioService));
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	nsCOMPtr<nsIChannel> channel;
	NS_NAMED_LITERAL_CSTRING(url, "https://cryptonite.si:15987/");
	// NS_NAMED_LITERAL_CSTRING(url, "http://server.cryptonite.si/");
	// NS_NAMED_LITERAL_CSTRING(url, "https://213.157.239.72:15987/");
	// NS_NAMED_LITERAL_CSTRING(url, "https://192.168.1.105:15987/");
	nsCOMPtr<nsIURI> uri;
	rv = ioService->NewURI(url, nullptr, nullptr, getter_AddRefs(uri));
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	rv = ioService->NewChannelFromURI(uri, getter_AddRefs(channel));
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	nsCOMPtr<nsIStringInputStream> stringInputStream;
	rv = servMan->GetServiceByContractID("@mozilla.org/io/string-input-stream;1", NS_GET_IID(nsIStringInputStream), getter_AddRefs(stringInputStream));
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	rv = stringInputStream->SetData((char*)message.c_str(), message.size());
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	nsCOMPtr<nsISeekableStream> seekableStream = do_QueryInterface(stringInputStream, &rv);
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	rv = seekableStream->Seek(0,0);
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	nsCOMPtr<nsIInputStream> inputStream = do_QueryInterface(stringInputStream, &rv);
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}
	
	nsCOMPtr<nsIUploadChannel> uploadChannel = do_QueryInterface(channel, &rv);
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	NS_NAMED_LITERAL_CSTRING(mimeType, "text/plain");
	rv = uploadChannel->SetUploadStream(inputStream, mimeType, -1);
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	nsCOMPtr<nsIHttpChannel> httpChannel = do_QueryInterface(channel, &rv);
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	NS_NAMED_LITERAL_CSTRING(method, "POST");
	rv = httpChannel->SetRequestMethod(method);
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	NS_NAMED_LITERAL_CSTRING(connectionHeader, "Connection");
	NS_NAMED_LITERAL_CSTRING(close, "close");
	rv = httpChannel->SetRequestHeader(connectionHeader, close, false);
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	nsCOMPtr<nsIInputStream> receiveInputStream;

	rv = channel->Open(getter_AddRefs(receiveInputStream));
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	// read from receive input stream
	nsCOMPtr<nsIComponentManager> componentManager;
	rv = NS_GetComponentManager(getter_AddRefs(componentManager));
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	nsCOMPtr<nsIBinaryInputStream> receiveBinaryInputStream;

	rv = componentManager->CreateInstanceByContractID("@mozilla.org/binaryinputstream;1", nullptr, NS_GET_IID(nsIBinaryInputStream), getter_AddRefs(receiveBinaryInputStream));
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	rv = receiveBinaryInputStream->SetInputStream(receiveInputStream);
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	NS_NAMED_LITERAL_CSTRING(contentLengthHeader, "Content-Length");
	nsAutoCString len;
	rv = httpChannel->GetResponseHeader(contentLengthHeader, len);
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	PRUint32 responseLen = atol(len.get());
	char *readData = (char*) NS_Alloc(sizeof(char) * (responseLen) + 1);
	if(!readData)
	{
		throw exception::technical::NSException(nsresult(-1), action);
	}

	rv = receiveBinaryInputStream->ReadBytes(responseLen, &readData);
	if(!readData)
	{
		throw exception::technical::NSException(nsresult(-1), action);
	}

	return readData;
}

void network::Request::sendAsyncRequest()
{
	// for temporary use call sendRequest in here
	sendRequest();
	return;
	// temporary stuff end



	/* std::string action = "sending asincronous crypt-o-nite request";
	nsresult rv;

	nsCOMPtr<nsIServiceManager> servMan;
	rv = NS_GetServiceManager(getter_AddRefs(servMan));
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	nsCOMPtr<nsIIOService> ioService;
	rv = servMan->GetServiceByContractID("@mozilla.org/network/io-service;1", NS_GET_IID(nsIIOService), getter_AddRefs(ioService));
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	NS_NAMED_LITERAL_CSTRING(url, "https://213.157.239.72:15987/");
	//  NS_NAMED_LITERAL_CSTRING(url, "https://192.168.1.105:15987/");
	nsCOMPtr<nsIURI> uri;
	rv = ioService->NewURI(url, nullptr, nullptr, getter_AddRefs(uri));
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	nsCOMPtr<nsIChannel> channel;
	rv = ioService->NewChannelFromURI(uri, getter_AddRefs(channel));
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	nsCOMPtr<nsIStringInputStream> stringInputStream;
	rv = servMan->GetServiceByContractID("@mozilla.org/io/string-input-stream;1", NS_GET_IID(nsIStringInputStream), getter_AddRefs(stringInputStream));
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	rv = stringInputStream->SetData((char*)message.c_str(), message.size() + 1);
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	nsCOMPtr<nsISeekableStream> seekableStream = do_QueryInterface(stringInputStream, &rv);
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	rv = seekableStream->Seek(0,0);
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	nsCOMPtr<nsIInputStream> inputStream= do_QueryInterface(stringInputStream, &rv);
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}
	
	nsCOMPtr<nsIUploadChannel> uploadChannel = do_QueryInterface(channel, &rv);
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	// NS_NAMED_LITERAL_CSTRING(mimeType, "text/plain");
	NS_NAMED_LITERAL_CSTRING(mimeType, "application/json");
	rv = uploadChannel->SetUploadStream(inputStream, mimeType, message.size() + 1);
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	// nsCOMPtr<nsIHttpChannel> httpChannel = do_QueryInterface(channel, &rv);
	nsCOMPtr<nsIHttpChannel> httpChannel = do_QueryInterface(uploadChannel, &rv);
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	NS_NAMED_LITERAL_CSTRING(connectionHeader, "Connection");
	NS_NAMED_LITERAL_CSTRING(close, "close");
	rv = httpChannel->SetRequestHeader(connectionHeader, close, false);
	if (NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	NS_NAMED_LITERAL_CSTRING(method, "POST");
	rv = httpChannel->SetRequestMethod(method);
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	nsCOMPtr<network::AsyncListener> asyncListener = new network::AsyncListener(message);
	NS_IF_ADDREF(asyncListener);

	rv = httpChannel->AsyncOpen(asyncListener, nullptr);
	// rv = channel->AsyncOpen(asyncListener, nullptr);
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	} */
}

