#ifndef _String_h_
#define _String_h_

#include "Value.h"

namespace json
{
	class String : public Value
	{
	public:
		String();
		String(const std::string &value, const bool singleQ = false);
		String(const char* value, const bool singleQ = false);
		String(const String &val);
		virtual ~String();

		String &operator=(const Value &val);
		String &operator=(const String &val);
		String &operator=(const std::string val);
		String &operator=(const char* val);

		bool operator==(const Value &val);
		bool operator==(const String &val);
		bool operator==(const std::string val);
		bool operator==(const char* val);

		bool operator!=(const Value &val);
		bool operator!=(const String &val);
		bool operator!=(const std::string val);
		bool operator!=(const char* val);

		/* print */
		std::string print();

		std::string getStringValue() const;

		friend class Enumeration;

	protected:

	private:
		/********************************************************************************************/
		/************************************String members******************************************/
		/********************************************************************************************/
		const std::string stringValue;							/* the value of string json value */
		const bool singleQuotes;								/* if the string is in single qoutes */
		/********************************************************************************************/
		/************************************String members******************************************/
		/********************************************************************************************/
	};
}

#endif
