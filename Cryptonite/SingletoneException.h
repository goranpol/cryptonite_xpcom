#ifndef _SingletoneException_h_
#define _SingletoneException_h_

#include <exception>
#include <string>

namespace exception
{
	namespace technical
	{
		class SingletoneException
		{
		public:
			SingletoneException(const std::string sing);
			virtual ~SingletoneException() throw();

			const char* what() const throw();

		private:
			const std::string singletone;
		};
	}
}

#endif
