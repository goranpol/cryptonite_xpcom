#include <mozilla/Char16.h>

#include "OneDriveObserver.h"
#include "OneDriveListener.h"
#include "OneDriveFile.h"

#include "Logger.h"

#include "ObjectStorage.h"

#include "GDocument.h"

#include "NSException.h"
#include "AlgorithmException.h"
#include "SingletoneException.h"

#include "Parser.h"

#include <math.h>

NS_IMPL_ISUPPORTS(onedrive::OneDriveObserver, nsIObserver)

onedrive::OneDriveObserver::OneDriveObserver(bool encrypt) : HttpObserver(encrypt)
{
}


onedrive::OneDriveObserver::~OneDriveObserver()
{
}

NS_IMETHODIMP onedrive::OneDriveObserver::Observe(nsISupports *aSubject, const char *aTopic, const char16_t *aData)
{
	nsresult rv;

	nsCOMPtr<nsIHttpChannel> httpChannel = do_QueryInterface(aSubject, &rv);
	if (NS_FAILED(rv))
	{
		// notify javascript
		Logger::errorMessage("OneDriveObserver", "Observe", "Observe FAILED: couldn't query interface to nsIHttpChannel", rv);
		return NS_OK;
	}

	nsAutoCString requestMethodACString;
	httpChannel->GetRequestMethod(requestMethodACString);

	std::string method = requestMethodACString.get();
	if (method.compare("OPTIONS") == 0)
	{
		return NS_OK;
	}

	nsAutoCString urlPath;
	rv = getUri(httpChannel, urlPath);
	if (NS_FAILED(rv))
	{
		// notify javascript
		Logger::errorMessage("OneDriveObserver", "Observe", "Observe FAILED: couldn't get uri", rv);
		return NS_OK;
	}

	if (!strcmp(aTopic, "http-on-modify-request"))
	{
		if (urlPath.Find(".users.storage.live") != -1 && doEncrypt)
		{
			nsCOMPtr<nsIUploadChannel> uploadChannel = do_QueryInterface(httpChannel, &rv);
			if (NS_FAILED(rv))
			{
				// notify javascript
				Logger::errorMessage("OneDriveObserver", "Observe:http-on-modify-request", "Observe FAILED: couldn't query interface to nsIUploadChannel", rv);
				return NS_OK;
			}

			try
			{
				try
				{
					std::string type = getRequestHeader(httpChannel, "BITS-Packet-Type");

					if (type.compare("Create-Session") == 0)
					{
						std::string xRequestStats = getRequestHeader(httpChannel, "X-RequestStats");
						std::string xRequestStatsRecalculated = recalculateXRequestStats(xRequestStats);
						setRequestHeader(httpChannel, "X-RequestStats", xRequestStatsRecalculated);
					}
					else if (type.compare("Fragment") == 0)
					{
						std::string data = readSendingData(uploadChannel);
						if (data.length() == 0)
						{
							replaceSendingData(uploadChannel, data);
							return NS_OK;
						}

						std::string sessionId = getRequestHeader(httpChannel, "BITS-Session-Id");

						std::string contentRange = getRequestHeader(httpChannel, "Content-Range");
						if (isSingleFragment(contentRange))
						{
							try
							{
								gui::GUIObserver::getInstance().notifyObserver(NITE_ENCRYPT, "file");

								std::string encryptedData = "";
								onedrive::OneDriveFile oneDriveFile = onedrive::OneDriveFile(sessionId, true);
								encryptedData = oneDriveFile.encryptOutgoingString(data);

								base::ObjectStorage::getInstance().addTempObject(oneDriveFile);
								replaceSendingData(uploadChannel, encryptedData);

								std::string xRequestStats = getRequestHeader(httpChannel, "X-RequestStats");
								std::string xRequestStatsRecalculated = recalculateXRequestStats(xRequestStats);
								setRequestHeader(httpChannel, "X-RequestStats", xRequestStatsRecalculated);

								std::string contentRangeRecalculated = recalculateContentRangeSingleFragment(contentRange);
								setRequestHeader(httpChannel, "Content-Range", contentRangeRecalculated);

								return NS_OK;
							}
							catch (...)
							{
								Logger::errorMessage("OneDriveObserver", "Observe:http-on-modify-request in uploading single fragment", "...", nsresult(1));
								replaceSendingData(uploadChannel, data);
							}
						}
						else if (isFirstFragment(contentRange))
						{
							try
							{
								gui::GUIObserver::getInstance().notifyObserver(NITE_ENCRYPT, "file");

								std::string encryptedData = "";
								onedrive::OneDriveFile oneDriveFile = onedrive::OneDriveFile(sessionId);
								encryptedData = oneDriveFile.encryptOutgoingString(data);
								oneDriveFile.setIsFirst(false);

								base::ObjectStorage::getInstance().addTempObject(oneDriveFile);

								replaceSendingData(uploadChannel, encryptedData);

								std::string len = getRequestHeader(httpChannel, "Content-Length");

								std::string xRequestStats = getRequestHeader(httpChannel, "X-RequestStats");
								std::string xRequestStatsRecalculated = recalculateXRequestStats(xRequestStats);
								setRequestHeader(httpChannel, "X-RequestStats", xRequestStatsRecalculated);

								std::string contentRangeRecalculated = recalculateContentRangeFirstFragment(contentRange);
								setRequestHeader(httpChannel, "Content-Range", contentRangeRecalculated);

								return NS_OK;
							}
							catch (...)
							{
								Logger::errorMessage("OneDriveObserver", "Observe:http-on-modify-request in uploading first fragment", "...", nsresult(1));
								replaceSendingData(uploadChannel, data);
							}
						}
						else if (isLastFragment(contentRange))
						{
							try
							{
								gui::GUIObserver::getInstance().notifyObserver(NITE_ENCRYPT, "file");

								std::string encryptedData = "";
								onedrive::OneDriveFile oneDriveFile = static_cast<onedrive::OneDriveFile&>(base::ObjectStorage::getInstance().getTempObject(sessionId, NITE_ONEDRIVEFILE));
								base::ObjectStorage::getInstance().removeTempObject(sessionId);
								oneDriveFile.setIsLast(true);
								oneDriveFile.setIsFirst(false);
								encryptedData = oneDriveFile.encryptOutgoingString(data);

								base::ObjectStorage::getInstance().addTempObject(oneDriveFile);

								replaceSendingData(uploadChannel, encryptedData);

								std::string xRequestStats = getRequestHeader(httpChannel, "X-RequestStats");
								std::string xRequestStatsRecalculated = recalculateXRequestStats(xRequestStats);
								setRequestHeader(httpChannel, "X-RequestStats", xRequestStatsRecalculated);

								std::string contentRangeRecalculated = recalculateContentRangeLastFragment(contentRange);
								setRequestHeader(httpChannel, "Content-Range", contentRangeRecalculated);

								return NS_OK;
							}
							catch (...)
							{
								Logger::errorMessage("OneDriveObserver", "Observe:http-on-modify-request in uploading last fragment", "...", nsresult(1));
								replaceSendingData(uploadChannel, data);
							}
						}
						else
						{
							try
							{
								std::string encryptedData = "";
								onedrive::OneDriveFile oneDriveFile = static_cast<onedrive::OneDriveFile&>(base::ObjectStorage::getInstance().getTempObject(sessionId, NITE_ONEDRIVEFILE));
								base::ObjectStorage::getInstance().removeTempObject(sessionId);
								oneDriveFile.setIsLast(false);
								oneDriveFile.setIsFirst(false);
								encryptedData = oneDriveFile.encryptOutgoingString(data);

								base::ObjectStorage::getInstance().addTempObject(oneDriveFile);

								replaceSendingData(uploadChannel, encryptedData);

								std::string xRequestStats = getRequestHeader(httpChannel, "X-RequestStats");
								std::string xRequestStatsRecalculated = recalculateXRequestStats(xRequestStats);
								setRequestHeader(httpChannel, "X-RequestStats", xRequestStatsRecalculated);

								std::string contentRangeRecalculated = recalculateContentRange(contentRange);
								setRequestHeader(httpChannel, "Content-Range", contentRangeRecalculated);

								return NS_OK;
							}
							catch (...)
							{
								Logger::errorMessage("OneDriveObserver", "Observe:http-on-modify-request in uploading middle fragment", "...", nsresult(1));
								replaceSendingData(uploadChannel, data);
							}
						}
					}
					else if (type.compare("Close-Session") == 0)
					{
						try
						{
							std::string sessionId = getRequestHeader(httpChannel, "BITS-Session-Id");
							onedrive::OneDriveFile tempObject = static_cast<OneDriveFile&>(base::ObjectStorage::getInstance().getTempObject(sessionId, NITE_ONEDRIVEFILE));
							base::ObjectStorage::getInstance().removeTempObject(sessionId);
							onedrive::OneDriveFile newObject = onedrive::OneDriveFile(urlPath.get(), tempObject.getObjectKey(), tempObject.getObjectCheckWord(), tempObject.getObjectInitChain());
							base::ObjectStorage::getInstance().addTempObject(newObject);

							std::string xRequestStats = getRequestHeader(httpChannel, "X-RequestStats");
							std::string xRequestStatsRecalculated = recalculateXRequestStats(xRequestStats);
							setRequestHeader(httpChannel, "X-RequestStats", xRequestStatsRecalculated);
						}
						catch (...)
						{
						}
					}
				}
				catch (...)
				{
					// Logger::errorMessage("OneDriveObserver", "Observe:http-on-modify-request", "error changing size in uploading file", nsresult(6));
					return NS_OK;
				}
			}
			catch (exception::technical::NSException)
			{
				Logger::errorMessage("OneDriveObserver", "Observe:http-on-modify-request", "...", nsresult(2));
				// notify javascript
			}
			catch (exception::technical::SingletoneException)
			{
				Logger::errorMessage("OneDriveObserver", "Observe:http-on-modify-request", "...", nsresult(3));
				// notify javascript
			}
			catch (exception::domain::AlgorithmException)
			{
				Logger::errorMessage("OneDriveObserver", "Observe:http-on-modify-request", "...", nsresult(4));
				// notify javascript
			}
			catch (...)
			{
				Logger::errorMessage("OneDriveObserver", "Observe:http-on-modify-request", "...", nsresult(5));
				// notify javascript
			}

			return NS_OK;
		}
		else if (urlPath.Find("skyapi.onedrive.live") != -1 && urlPath.Find("SetPermissions") != -1)
		{
			nsCOMPtr<nsIUploadChannel> uploadChannel = do_QueryInterface(httpChannel, &rv);
			if (NS_FAILED(rv))
			{
				// notify javascript
				Logger::errorMessage("GDriveObserver", "Observe:http-on-modify-request", "Observe FAILED: couldn't query interface to nsIUploadChannel", rv);
				return NS_OK;
			}

			try
			{
				std::string data = readSendingData(uploadChannel);
				if (data.length() == 0)
				{
					replaceSendingData(uploadChannel, data);
					return NS_OK;
				}
				try
				{
					changeUsersWithAccess(data);
					replaceSendingData(uploadChannel, data);
				}
				catch (...)
				{
					replaceSendingData(uploadChannel, data);
					return NS_OK;
				}
			}
			catch (exception::technical::NSException)
			{
				// notify javascript
			}
			catch (exception::technical::SingletoneException)
			{
				// notify javascript
			}
			catch (exception::domain::AlgorithmException)
			{
				// notify javascript
			}
			catch (...)
			{
				// notify javascript
			}

			return NS_OK;
		}
		else if (urlPath.Find("onedrive.live") != -1 && urlPath.Find("download.aspx") != -1)
		{
			try
			{
				std::string id = urlPath.get();
				id = id.substr(id.find("resid=") + 6);
				id = id.substr(size_t(0), id.find("&"));

				id = replace(id, "%21", "!");

				onedrive::OneDriveFile oneDriveFile("-1", id, "-1", "-1");
				base::ObjectStorage::getInstance().addTempObject(oneDriveFile);
			}
			catch (exception::technical::NSException)
			{
				Logger::errorMessage("OneDriveObserver", "Observe:http-on-modify-request", "get id for download", nsresult(1));
				// notify javascript
			}
			catch (...)
			{
				Logger::errorMessage("OneDriveObserver", "Observe:http-on-modify-request", "get id for download", nsresult(2));
				// notify javascript
			}
		}
	}
	else if (!strcmp(aTopic, "http-on-examine-response"))
	{
		if ((urlPath.Find(".files.1drv") != -1 && urlPath.Find("download") != -1) || (urlPath.Find("livefilestore") != -1 && urlPath.Find("download") != -1))
		{
			nsCOMPtr<nsITraceableChannel> traceableChannel = do_QueryInterface(aSubject, &rv);
			if (NS_FAILED(rv))
			{
				// notify javascript
				Logger::errorMessage("OneDriveObserver", "Observe:http-on-examine-response", "Observe FAILED: couldn't query interface to nsITraceableChannel", rv);
				return rv;
			}

			try
			{
				std::string id = base::ObjectStorage::getInstance().getTempObject("-1", NITE_ONEDRIVEFILE).getObjectKey();
				base::ObjectStorage::getInstance().removeTempObject("-1");
				RefPtr<base::HttpListener> oneDriveListener = new onedrive::OneDriveListener(id);

				replaceStreamListener(traceableChannel, oneDriveListener);
			}
			catch (exception::technical::NSException)
			{
				Logger::errorMessage("OneDriveObserver", "Observe:http-on-examine-response", "download", nsresult(1));
				// notify javascript
				return NS_OK;
			}
			catch (...)
			{
				Logger::errorMessage("OneDriveObserver", "Observe:http-on-examine-response", "download", nsresult(2));
				// notify javascript
				return NS_OK;
			}
		}
		else if (urlPath.Find(".users.storage.live") != -1 && doEncrypt)
		{
			nsCOMPtr<nsITraceableChannel> traceableChannel = do_QueryInterface(aSubject, &rv);
			if (NS_FAILED(rv))
			{
				// notify javascript
				Logger::errorMessage("OneDriveObserver", "Observe:http-on-examine-response", "Observe FAILED: couldn't query interface to nsITraceableChannel", rv);
				return rv;
			}

			try
			{
				std::string id = getResponseHeader(httpChannel, "X-Resource-Id");

				OneDriveFile tempObj = static_cast<OneDriveFile&>(base::ObjectStorage::getInstance().getTempObject(urlPath.get(), NITE_ONEDRIVEFILE));
				base::ObjectStorage::getInstance().removeTempObject(urlPath.get());
				OneDriveFile newObj = OneDriveFile(id, tempObj.getObjectKey(), tempObj.getObjectCheckWord(), tempObj.getObjectInitChain());
				base::ObjectStorage::getInstance().addObject(newObj, base::UsersList());
			}
			catch (exception::technical::NSException)
			{
				Logger::errorMessage("OneDriveObserver", "Observe:http-on-modify-request", "download", nsresult(1));
				// notify javascript
				return NS_OK;
			}
			catch (...)
			{
				Logger::errorMessage("OneDriveObserver", "Observe:http-on-modify-request", "download", nsresult(2));
				// notify javascript
				return NS_OK;
			}
		}
	}

	return NS_OK;
}

void onedrive::OneDriveObserver::changeUsersWithAccess(std::string data)
{
	std::vector<base::ObjectRequest> reqs;
	base::UsersList users;
	std::string type;

	try
	{
		json::JSON dataJson(data);

		reqs.push_back(base::ObjectRequest(dataJson.getString("id")));
		if (dataJson.getInt("userAction") == 0)
			type = "a";
		else if (dataJson.getInt("userAction") == 1)
			type = "r";
		else
			return;

		for (size_t i = 0; i < dataJson["entities"].size(); i++)
		{
			users.pushBack(dataJson["entities"][i].getString("email"));
		}

		if (!users.empty())
		{
			if (reqs.empty())
				return;
			else if (reqs.size() == 1)
			{
				base::ObjectStorage::getInstance().changeAccess(reqs[0], users, type, NITE_ONEDRIVEFILE);
				return;
			}

			base::ObjectStorage::getInstance().changeAccess(reqs, users, type, NITE_ONEDRIVEFILE);
			return;
		}
	}
	catch (...)
	{
		throw exception::domain::AlgorithmException(NITE_ONEDRIVEFILE, useraccess);
	}

}

std::string onedrive::OneDriveObserver::recalculateContentRangeSingleFragment(std::string data)
{
	std::string returnString = "";
	std::vector<std::string> rangeSplitted = split(data, "-");

	returnString = rangeSplitted[0];

	std::string len = split(rangeSplitted[1], "/")[1];
	size_t length = atoi(len.c_str());
	if (length % 16 == 0 || length % 16 == 15)
		length = length + 16;

	// Calculate size of encrypted file
	size_t newSize = ceil(double(length) / 16) * 16; // encrypted size
	size_t remainder = newSize % 3;
	newSize = newSize / 3 * 4;					// base64 size without remainder
	if (remainder == 1)
		newSize += 2;
	else if (remainder == 2)
		newSize += 3;							// base64 size with remainder

	newSize += 86;								// with added checkword

	returnString += "-" + std::to_string(newSize - 1) + "/" + std::to_string(newSize);

	return returnString;
}

std::string onedrive::OneDriveObserver::recalculateContentRangeFirstFragment(std::string data)
{
	std::string dataCpy = data.substr(6);

	std::string from = "";
	size_t i = 0;
	while (dataCpy[i] != '-')
	{
		from.push_back(dataCpy[i]);
		i++;
	}

	if (from.compare("0") != 0)
	{
		Logger::errorMessage("OneDriveObserver", "recalculateContentRangeFirstFragment", "not first fragment", nsresult(1));
		throw std::exception();
	}

	dataCpy = dataCpy.substr(dataCpy.find("-") + 1);
	i = 0;
	std::string to = "";
	while (dataCpy[i] != '/')
	{
		to.push_back(dataCpy[i]);
		i++;
	}

	std::string length = dataCpy.substr(dataCpy.find("/") + 1);

	size_t toSizeT = atoi(to.c_str());
	size_t newToSizeT = floor(double(toSizeT + 1) / 48) * 48;	// encrypted size
	newToSizeT = (newToSizeT / 3 * 4) - 1;						// base64 size without remainder
	newToSizeT += 86;											// with added checkword
	std::string toRecalculated = std::to_string(newToSizeT);

	size_t len = atoi(length.c_str());
	if (len % 16 == 0 || len % 16 == 15)
		len = len + 16;

	// Calculate size of encrypted file
	size_t newSize = ceil(double(len) / 16) * 16; // encrypted size
	size_t remainder = newSize % 3;
	newSize = newSize / 3 * 4;					// base64 size without remainder
	if (remainder == 1)
		newSize += 2;
	else if (remainder == 2)
		newSize += 3;							// base64 size with remainder

	newSize += 86;								// with added checkword
	std::string lengthRecalculated = std::to_string(newSize);

	std::string returnString = "bytes " + from + "-" + toRecalculated + "/" + lengthRecalculated;

	return returnString;
}

std::string onedrive::OneDriveObserver::recalculateContentRangeLastFragment(std::string data)
{
	std::string dataCpy = data.substr(6);

	std::string from = "";
	size_t i = 0;
	while (dataCpy[i] != '-')
	{
		from.push_back(dataCpy[i]);
		i++;
	}

	if (from.compare("0") == 0)
	{
		Logger::errorMessage("OneDriveObserver", "recalculateContentRangeLastFragment", "is first fragment", nsresult(1));
		throw std::exception();
	}

	dataCpy = dataCpy.substr(dataCpy.find("-") + 1);
	i = 0;
	std::string to = "";
	while (dataCpy[i] != '/')
	{
		to.push_back(dataCpy[i]);
		i++;
	}

	std::string length = dataCpy.substr(dataCpy.find("/") + 1);

	size_t fromSizeT = atoi(from.c_str());
	size_t newFromSizeT = floor(double(fromSizeT) / 48) * 48;	// encrypted size
	newFromSizeT = (newFromSizeT / 3 * 4);						// base64 size without remainder
	newFromSizeT += 86;											// with added checkword
	std::string fromRecalculated = std::to_string(newFromSizeT);

	size_t len = atoi(length.c_str());
	if (len % 16 == 0 || len % 16 == 15)
		len = len + 16;

	// Calculate size of encrypted file
	size_t newSize = ceil(double(len) / 16) * 16; // encrypted size
	size_t remainder = newSize % 3;
	newSize = newSize / 3 * 4;					// base64 size without remainder
	if (remainder == 1)
		newSize += 2;
	else if (remainder == 2)
		newSize += 3;							// base64 size with remainder

	newSize += 86;								// with added checkword
	std::string lengthRecalculated = std::to_string(newSize);

	std::string returnString = "bytes " + fromRecalculated + "-" + std::to_string(newSize - 1) + "/" + lengthRecalculated;

	return returnString;
}

std::string onedrive::OneDriveObserver::recalculateContentRange(std::string data)
{
	std::string dataCpy = data.substr(6);

	std::string from = "";
	size_t i = 0;
	while (dataCpy[i] != '-')
	{
		from.push_back(dataCpy[i]);
		i++;
	}

	if (from.compare("0") == 0)
	{
		Logger::errorMessage("OneDriveObserver", "recalculateContentRange", "is first fragment", nsresult(1));
		throw std::exception();
	}

	dataCpy = dataCpy.substr(dataCpy.find("-") + 1);
	i = 0;
	std::string to = "";
	while (dataCpy[i] != '/')
	{
		to.push_back(dataCpy[i]);
		i++;
	}

	std::string length = dataCpy.substr(dataCpy.find("/") + 1);

	size_t fromSizeT = atoi(from.c_str());
	size_t newFromSizeT = floor(double(fromSizeT) / 48) * 48;	// encrypted size
	newFromSizeT = (newFromSizeT / 3 * 4);						// base64 size without remainder
	newFromSizeT += 86;											// with added checkword
	std::string fromRecalculated = std::to_string(newFromSizeT);

	size_t toSizeT = atoi(to.c_str());
	size_t newToSizeT = floor(double(toSizeT + 1) / 48) * 48;	// encrypted size
	newToSizeT = (newToSizeT / 3 * 4) - 1;					// base64 size without remainder
	newToSizeT += 86;										// with added checkword
	std::string toRecalculated = std::to_string(newToSizeT);

	size_t len = atoi(length.c_str());
	if (len % 16 == 0 || len % 16 == 15)
		len = len + 16;

	// Calculate size of encrypted file
	size_t newSize = ceil(double(len) / 16) * 16; // encrypted size
	size_t remainder = newSize % 3;
	newSize = newSize / 3 * 4;					// base64 size without remainder
	if (remainder == 1)
		newSize += 2;
	else if (remainder == 2)
		newSize += 3;							// base64 size with remainder

	newSize += 86;								// with added checkword
	std::string lengthRecalculated = std::to_string(newSize);

	std::string returnString = "bytes " + fromRecalculated + "-" + toRecalculated + "/" + lengthRecalculated;

	return returnString;
}

std::string onedrive::OneDriveObserver::recalculateXRequestStats(std::string data)
{
	std::string returnString = "";
	std::vector<std::string> statsSplitted = split(data, ",");

	for (size_t i = 0; i < statsSplitted.size(); i++)
	{
		if (statsSplitted[i].find("size=") == 0)
		{
			returnString += "size=";

			size_t length = atoi(statsSplitted[i].substr(5).c_str());
			if (length % 16 == 0 || length % 16 == 15)
				length = length + 16;

			// Calculate size of encrypted file
			size_t newSize = ceil(double(length) / 16) * 16; // encrypted size
			size_t remainder = newSize % 3;
			newSize = newSize / 3 * 4;					// base64 size without remainder
			if (remainder == 1)
				newSize += 2;
			else if (remainder == 2)
				newSize += 3;							// base64 size with remainder
			newSize += 86;								// with added checkword

			returnString += std::to_string(newSize) + ",";
		}
		else if (i != statsSplitted.size() - 1)
			returnString += statsSplitted[i] + ",";
		else
			returnString += statsSplitted[i];
	}

	return returnString;
}

bool onedrive::OneDriveObserver::isSingleFragment(std::string data)
{
	std::string range = data.substr(6);
	
	std::vector<std::string> rangeVector = split(range, "-");
	if (rangeVector.size() != 2)
		throw std::exception();

	std::vector<std::string>  rangeVector2 = split(rangeVector[1], "/");
	if (rangeVector2.size() != 2)
		throw std::exception();

	std::string from = rangeVector[0];
	std::string to = rangeVector2[0];
	std::string size = rangeVector2[1];

	if (from.compare("0") == 0)
	{
		size_t toUInt = atoi(to.c_str());
		size_t sizeUInt = atoi(size.c_str());

		if ((toUInt + 1) == sizeUInt)
			return true;
	}

	return false;
}

bool onedrive::OneDriveObserver::isFirstFragment(std::string data)
{
	std::string range = data.substr(6);

	std::vector<std::string> rangeVector = split(range, "-");
	if (rangeVector.size() != 2)
		throw std::exception();

	if (rangeVector[0].compare("0") == 0)
		return true;

	return false;
}

bool onedrive::OneDriveObserver::isLastFragment(std::string data)
{
	std::vector<std::string> rangeVector = split(data, "/");
	if (rangeVector.size() != 2)
		throw std::exception();

	std::vector<std::string> rangeVector2 = split(rangeVector[0], "-");
	if (rangeVector2.size() != 2)
		throw std::exception();

	std::string to = rangeVector2[1];
	std::string size = rangeVector[1];

	size_t toUInt = atoi(to.c_str());
	size_t sizeUInt = atoi(size.c_str());

	if ((toUInt + 1) == sizeUInt)
		return true;
	
	return false;
}
