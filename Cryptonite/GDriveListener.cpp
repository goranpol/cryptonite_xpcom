#include <mozilla/Char16.h>

#include "GDriveListener.h"
#include "GDriveFile.h"
#include "Logger.h"

#include "Parser.h"
#include "JsonError.h"

#include "ObjectStorage.h"
#include "ObjectRequest.h"

#include "NSException.h"
#include "SingletoneException.h"

NS_IMPL_ISUPPORTS(gdrive::GDriveListener, nsIRequestObserver, nsIStreamListener)

gdrive::GDriveListener::GDriveListener(const int typ) : type(typ), fileId("")
{
}

gdrive::GDriveListener::GDriveListener(const int typ, const std::string id) : type(typ), fileId(id)
{
}

gdrive::GDriveListener::GDriveListener(const std::string id) : type(NITE_downloadType), fileId(id)
{
}

gdrive::GDriveListener::~GDriveListener()
{
}

NS_IMETHODIMP gdrive::GDriveListener::OnStartRequest(nsIRequest *aRequest, nsISupports *aContext)
{
	nsresult rv;
	
	try
	{
		rv = HttpListener::OnStartRequest(aRequest, aContext);
	}
	catch(exception::technical::NSException)
	{
		// notify javascript
	}

	return NS_OK;
}

NS_IMETHODIMP gdrive::GDriveListener::OnDataAvailable(nsIRequest *aRequest, nsISupports *aContext, nsIInputStream *aInputStream, uint64_t aOffset, uint32_t aCount)
{
	nsresult rv;
	
	try
	{
		rv = HttpListener::OnDataAvailable(aRequest, aContext, aInputStream, aOffset, aCount);
	}
	catch(exception::technical::NSException)
	{
		// notify javascript
	}
	
	return NS_OK;
}

NS_IMETHODIMP gdrive::GDriveListener::OnStopRequest(nsIRequest *aRequest, nsISupports *aContext, nsresult aStatusCode)
{
	nsresult rv;
	
	mData = (char*)NS_Realloc((void*)mData, sizeof(char) * (mRead + 1));
	if(!mData)
	{
		// notify javascript
		Logger::errorMessage("GDriveListener", "OnStopRequest", "OnStopRequest FAILED: couldn't reallocate memory for mData", nsresult(-1));
		return NS_OK;
	}

	memcpy(mData + mRead, "\0", 1);

	if (type == NITE_deleteType)
	{
		/**************************************************************************************/
		mDecryptedRead = mRead;
		mDecryptedData = (char*)NS_Alloc(sizeof(char) * mDecryptedRead);
		memcpy(mDecryptedData, mData, mDecryptedRead);
		/**************************************************************************************/

		try
		{
			deleteFromGDrive();
			rv = HttpListener::OnStopRequest(aRequest, aContext, aStatusCode);
		}
		catch (exception::technical::NSException)
		{
			// notify javascript
		}
		catch (exception::technical::SingletoneException)
		{
			// notify javascript
		}
		catch (...)
		{
			// notify javascript
		}
	}
	else if (type == NITE_downloadType)
	{
		std::string data = mData;
		std::string checkWord = data.substr(0, 86);

		try
		{
			gdrive::GDriveFile gDriveFile = static_cast<gdrive::GDriveFile&>(base::ObjectStorage::getInstance().getObject(base::ObjectRequest(fileId, checkWord), NITE_GDRIVEFILE));

			std::string decryptedData = gDriveFile.decryptIncomingString(data);
			mDecryptedRead = decryptedData.size();
			mDecryptedData = (char*)NS_Alloc(sizeof(char) * mDecryptedRead);
			memcpy(mDecryptedData, decryptedData.c_str(), mDecryptedRead);
		}
		catch (...)
		{
			mDecryptedRead = mRead;
			mDecryptedData = (char*)NS_Alloc(sizeof(char) * mRead);
			memcpy(mDecryptedData, mData, mDecryptedRead);
		}

		try
		{
			rv = HttpListener::OnStopRequest(aRequest, aContext, aStatusCode);
		}
		catch (exception::technical::NSException)
		{
			// notify javascript
		}
		catch (exception::technical::SingletoneException)
		{
			// notify javascript
		}
		catch (...)
		{
			// notify javascript
		}
	}
	else if (type == NITE_getIdType)
	{
		std::string data = mData;
		
		try
		{
			gdrive::GDriveFile gDriveFile = static_cast<gdrive::GDriveFile&>(base::ObjectStorage::getInstance().getTempObject(fileId, NITE_GDRIVEFILE));
			base::ObjectStorage::getInstance().removeTempObject(fileId);

			json::JSON responseJson(data);
			std::string id = responseJson.getString("id");

			gdrive::GDriveFile newObj = gdrive::GDriveFile(id, gDriveFile.getObjectKey(), gDriveFile.getObjectCheckWord(), gDriveFile.getObjectInitChain());
			base::ObjectStorage::getInstance().addObject(newObj, base::UsersList());

			mDecryptedRead = mRead;
			mDecryptedData = (char*)NS_Alloc(sizeof(char) * mRead);
			memcpy(mDecryptedData, mData, mDecryptedRead);
		}
		catch (...)
		{
			mDecryptedRead = mRead;
			mDecryptedData = (char*)NS_Alloc(sizeof(char) * mRead);
			memcpy(mDecryptedData, mData, mDecryptedRead);
		}

		try
		{
			rv = HttpListener::OnStopRequest(aRequest, aContext, aStatusCode);
		}
		catch (exception::technical::NSException)
		{
			// notify javascript
		}
		catch (exception::technical::SingletoneException)
		{
			// notify javascript
		}
		catch (...)
		{
			// notify javascript
		}
	}

	return NS_OK;
}

// probably needs to be fixed
void gdrive::GDriveListener::deleteFromGDrive()
{
	std::string tmp = std::string(mData);
	json::JSON deleteJson = json::JSON(tmp);
	if(deleteJson["response"]["docs"].empty())
		return;
	else if(deleteJson["response"]["docs"].size() == 1)
	{
		try
		{
			if (deleteJson["response"]["docs"][(size_t)0]["attributes"].getBool("deleted"))
				base::ObjectStorage::getInstance().removeObject(base::ObjectRequest(deleteJson["response"]["docs"][(size_t)0].getString("id")), -1);

			return;
		}
		catch(json::JsonError)
		{
			return;
		}
	}

	std::vector<base::ObjectRequest> reqs;
	for (size_t i = size_t(0); i<deleteJson.size(); i++)
	{
		if(deleteJson["response"]["docs"][i]["attributes"].getBool("deleted"))
			reqs.push_back(base::ObjectRequest(deleteJson["response"]["docs"][i].getString("id")));
	}

	if(!reqs.empty())
		base::ObjectStorage::getInstance().removeObjects(reqs, -1);
}
