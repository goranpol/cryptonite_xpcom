#ifndef _HotmailAttachment_h_
#define _HotmailAttachment_h_

#include "Attachment.h"

namespace hotmail
{
	class HotmailAttachment : public base::Attachment
	{
	public:
		HotmailAttachment(const std::string id);
		HotmailAttachment(const std::string objId, const std::string objKey, const std::string objInitChain, const std::string objCheckWord);
		HotmailAttachment(const base::Object &obj);
		HotmailAttachment(const hotmail::HotmailAttachment &obj);
		~HotmailAttachment();

		HotmailAttachment &operator=(Object &obj);
		HotmailAttachment &operator=(HotmailAttachment &obj);
		bool operator==(Object &obj);
		bool operator!=(Object &obj);

		std::string getAttachmentId(std::string data);

	private:

	};
}

#endif
