#include <mozilla/Char16.h>

#include "UnknownException.h"

exception::domain::UnknownException::UnknownException(const std::string functSign) : functionSignature(functSign)
{
}

exception::domain::UnknownException::~UnknownException() throw()
{
}

const char* exception::domain::UnknownException::what() const throw()
{
	return "";
}