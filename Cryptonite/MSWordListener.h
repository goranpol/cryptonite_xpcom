#ifndef _MSWordListenerr_h
#define _MSWordListener_h

#include "HttpListener.h"

namespace msword
{
	class MSWordListener : public base::HttpListener
	{
	public:
		MSWordListener();

		NS_DECL_ISUPPORTS
		NS_DECL_NSISTREAMLISTENER
		NS_DECL_NSIREQUESTOBSERVER

	private:
		~MSWordListener();


	};
}

#endif
