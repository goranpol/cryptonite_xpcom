#include <mozilla/Char16.h>

#include "GMailUIWrapper.h"
#include "ObjectRequest.h"
#include "Object.h"
#include "ObjectStorage.h"
#include "ObjectList.h"

#include "JsonError.h"
#include "JsonTypeError.h"
#include "JsonParsingError.h"
#include "JsonOutOfRange.h"

#include "GUIObserver.h"

#include "Logger.h"
#include <sstream>

#include "ObjectNotFoundException.h"
#include "AlgorithmException.h"
#include "ObjectRequestException.h"

gmail::GMailUIWrapper::GMailUIWrapper()
{
}

gmail::GMailUIWrapper::~GMailUIWrapper()
{
}

std::string gmail::GMailUIWrapper::encryptOutgoingString(std::string data, const std::string reqId)
{
	data = replace(data, "\n", "");
	std::vector<std::string> splitedData = split(data, "&");
	
	std::string removingDraftId = "";
	std::string returnString = data;
	bool encrypt = false;
	for (size_t i = size_t(0); i<splitedData.size(); i++)
	{
		if (splitedData[i].find("draft=") == 0)
		{
			if (splitedData[i].find("undefined") == -1)
			{
				try
				{
					base::ObjectRequest objReq = base::ObjectRequest(splitedData[i].substr(6));
					base::ObjectStorage::getInstance().getObject(objReq, NITE_GMAIL);
					removingDraftId = splitedData[i].substr(6);
					encrypt = true;
				}
				catch (...)
				{
				}
			}
			else
			{
				try
				{
					encrypt = true;
				}
				catch(exception::domain::ObjectNotFoundException)
				{
				}
				catch (...)
				{
				}
			}
		}
		else if (splitedData[i].find("rm=") == 0)
		{
			if(splitedData[i].find("undefined") == -1 && splitedData[i].compare("rm=") != 0)
			{
				try
				{
					base::ObjectRequest objReq = base::ObjectRequest(splitedData[i].substr(3));
					gmail::GMail gMail = static_cast<gmail::GMail&>(base::ObjectStorage::getInstance().getObject(objReq, NITE_GMAIL));
					gmail::GMail tempGMail(reqId, gMail.getObjectKey(), gMail.getObjectInitChain(), gMail.getObjectCheckWord(), removingDraftId);
					returnString = tempGMail.encryptOutgoingString(data);
					
					base::ObjectStorage::getInstance().addTempObject(tempGMail);
					gui::GUIObserver::getInstance().notifyObserver(NITE_ENCRYPT, "google mail");
					break;
				}
				catch(...)
				{
					return data;
				}
			}

			if (encrypt == false)
			{
				return data;
			}

			gmail::GMail gMail(reqId, removingDraftId);
			returnString = gMail.encryptOutgoingString(data);
			base::ObjectStorage::getInstance().addTempObject(gMail);

			gui::GUIObserver::getInstance().notifyObserver(NITE_ENCRYPT, "google mail");
			break;
		}
	}

	return returnString;
}

std::string gmail::GMailUIWrapper::decryptIncomingString(std::string data, const int decryptType)
{
	// check for out of bound indexes
	// here is probably the crash

	std::string dataCopy = data;

	try
	{
		if (decryptType == NITE_DecryptSummary)
		{
			return decryptHtml(dataCopy);
		}
	}
	catch (...)
	{
		return data;
	}

	std::vector<json::JSON> jsons;
	while (dataCopy.size() > 0)
	{
		try
		{
			json::JSON json(dataCopy);
			jsons.push_back(json);
		}
		catch (json::JsonError)
		{
			break;
		}
		catch(...)
		{
			return data;
		}
	}

	if (jsons.empty())
		return data;

	try
	{
		if (jsons.size() > 1)
		{
			return decryptMailsMultipleJSONS(jsons);
		}
		else if (jsons.size() == 0)
		{
			return data;
		}
		else
		{
			return decryptMailsOneJSON(jsons[0]);
		}
	}
	catch (...)
	{
		Logger::logMessage("GMailUIWrapper", "decryptIncomingString", " decrypting catch ...");
		return data;
	}
}

void gmail::GMailUIWrapper::deleteMails(std::string data)
{
	std::vector<std::string> splitedData = split(data, "&");
	std::vector<base::ObjectRequest> objReqs;
	
	// make it in server side (key, initChain and check word match)
	try
	{
		for (size_t i = 0; i<splitedData.size(); i++)
		{
			if(splitedData[i].find("t=") == 0)
			{
				objReqs.push_back(base::ObjectRequest(splitedData[i].substr(2)));
			}
		}
		if(objReqs.empty())
			return;
		else if(objReqs.size() == 1)
			base::ObjectStorage::getInstance().removeObject(objReqs[0], NITE_GMAIL);
		else
			base::ObjectStorage::getInstance().removeObjects(objReqs, NITE_GMAIL);
	}
	catch(...)
	{
		throw exception::domain::AlgorithmException(NITE_GMAIL, deletion);
	}
}

std::string gmail::GMailUIWrapper::decryptHtml(std::string data)
{
	// check for out of bound indexes
	size_t length = 0;
	size_t endLength = 0;

	if(data.find("VIEW_DATA=[") == -1)
		return data;

	std::vector<std::string> splitedHTML = split(data, "VIEW_DATA=");
	std::string decryptedHTML = splitedHTML[0] + "VIEW_DATA=";

	try
	{
		// if splitedHTML.length() != 2 throw error
		json::JSON json = json::JSON(splitedHTML[1]);
		std::string HTMLEnding = splitedHTML[1];

		for (size_t i = 0; i<json.size(); i++)
		{
			if (json[i].getString(size_t(0)).compare("tb") == 0)
			{
				decryptSummary(json[i][2]);
			}
		}

		decryptedHTML += json.print() + HTMLEnding;
	}
	catch(...)
	{
		throw exception::domain::AlgorithmException(NITE_GMAIL, decryption);
	}

	return decryptedHTML;
}

std::string gmail::GMailUIWrapper::decryptMailsOneJSON(json::JSON json)
{
	// check for out of bound indexes
	try
	{
		decryptFull(json[(size_t)0]);
		return json.print();
	}
	catch(...)
	{
		Logger::logMessage("GMailUIWrapper", "decryptMailsOneJSONS", "ERROR!!!");
		throw exception::domain::AlgorithmException(NITE_GMAIL, decryption);
	}
}

std::string gmail::GMailUIWrapper::decryptMailsMultipleJSONS(std::vector<json::JSON> jsons)
{
	// check for out of bound indexes
	bool first = true;
	std::string returnString = "";

	try
	{
		for (size_t i = 0; i<jsons.size(); i++)
		{
			decryptFull(jsons[i].get());

			// for calculating size as prefix
			std::string recalculatedPrefix;
			if(first)
			{
				if (jsons[i].getPrefix().find(")]}'\n\n") == 0)
				{
					recalculatedPrefix = jsons[i].getPrefix().substr(0, 6);
				}
				first = false;
			}
			jsons[i].setPrefix("");

			size_t length = 0;
			length = utf8Length(jsons[i].print()) + 2;

			std::ostringstream convert;
			convert << length;
			std::string lengthString = convert.str();
			recalculatedPrefix += lengthString + "\n";
			jsons[i].setPrefix(recalculatedPrefix);

			returnString += jsons[i].print() + "\n";
		}

		return returnString;
	}
	catch(...)
	{
		Logger::logMessage("GMailUIWrapper", "decryptMailsMultipleJSONS", "ERROR!!!");
		throw exception::domain::AlgorithmException(NITE_GMAIL, decryption);
	}
}

void gmail::GMailUIWrapper::decryptSummary(json::Value &jsonArray)
{
	// check for out of bound indexes
	std::vector<base::ObjectRequest> objReqs;
	std::vector<size_t> indexes;

	try
	{
		for (size_t i = 0; i<jsonArray.size(); i++)
		{
			try
			{
				std::string subject = jsonArray[i].getString(9);
				
				try
				{
					objReqs.push_back(base::ObjectRequest(jsonArray[i].getString(size_t(0)), encryptCheck(subject)));
					indexes.push_back(i);
				}
				catch(...)		// notEncryptedException
				{
					continue;
				}
			}
			catch(...)
			{
				continue;
			}
		}
		if(indexes.empty())
			return;

		base::ObjectList gMails;
		if(!objReqs.empty())
			gMails = base::ObjectStorage::getInstance().getObjects(objReqs, NITE_GMAIL);

		if(gMails.empty())
			return;

		for (size_t i = 0; i<indexes.size(); i++)
		{
			try
			{
				// GMail gMail(gMails[jsonArray[indexes[i]].getString(size_t(0))]);
				GMail gMail = static_cast<gmail::GMail&>(gMails[jsonArray[indexes[i]].getString(size_t(0))]);
				gMail.decryptSummary(jsonArray[indexes[i]]);
			}
			catch(...)
			{
				continue;
			}
		}
	}
	catch(...)
	{
		Logger::logMessage("GMailUIWrapper", "decryptSummary", "ERROR!!!");
		throw exception::domain::AlgorithmException(NITE_GMAIL, decryption);
	}
}

void gmail::GMailUIWrapper::decryptFull(json::Value &jsonArray)
{
	// check for out of bound indexes
	size_t length = 0;
	size_t endLength = 0;
	std::vector<base::ObjectRequest> objReqs;
	std::vector<size_t> indexes;

	try
	{
		for (size_t i = 0; i<jsonArray.size(); i++)
		{
			try
			{
				if (jsonArray[i].getString(size_t(0)).compare("cs") == 0)
				{
					size_t subjectIndex;
					if (jsonArray[i].getInt(3) == 0)
						subjectIndex = 22;
					else
						subjectIndex = 23;

					try
					{
						std::string checkWord = encryptCheck(jsonArray[i].getString(subjectIndex));

						indexes.push_back(i);
						size_t len = (size_t)jsonArray[i].getInt64(3);
						size_t index = i;

						if (len == 0)
						{
							for (size_t j = 0; j < jsonArray[i][9].size(); j++)
							{
								objReqs.push_back(base::ObjectRequest(jsonArray[i][9].getString(j), checkWord));
							}
							i += jsonArray[i][9].size();
						}
						else
						{
							for (size_t j = 0; j < jsonArray[i][8].size(); j++)
							{
								objReqs.push_back(base::ObjectRequest(jsonArray[i][8].getString(j), checkWord));
							}
							i += jsonArray[i][8].size();
						}
					}
					catch (...)
					{
						continue;
					}
				}
				else if (jsonArray[i].getString(size_t(0)).compare("ms") == 0)
				{
					try
					{
						std::string checkWord = encryptCheck(jsonArray[i].getString(12));

						indexes.push_back(i);
						objReqs.push_back(base::ObjectRequest(jsonArray[i].getString(1), checkWord));
					}
					catch (...)
					{
						continue;
					}
				}
				else if (jsonArray[i].getString(0).compare("stu") == 0 && jsonArray[i][2].size() != 0)
				{
					bool decrypt = false;
					for (size_t j = size_t(0); j < jsonArray[i][2].size(); j++)
					{
						std::string subject = jsonArray[i][2][j][1].getString(9);

						try
						{
							objReqs.push_back(base::ObjectRequest(jsonArray[i][2][j][1].getString(size_t(0)), encryptCheck(subject)));
							decrypt = true;
						}
						catch (...)
						{
							continue;
						}
					}
					if (decrypt)
						indexes.push_back(i);
				}
				else if (jsonArray[i].getString(size_t(0)).compare("tb") == 0)
				{
					indexes.push_back(i);
				}
			}
			catch (...)
			{
				Logger::errorMessage("GMailUIWrapper", "decryptFull", "for outer first round", nsresult(1));
				continue;
			}
		}

		if (indexes.empty())
		{
			return;
		}

		base::ObjectList gMails;
		try
		{
			// here get keys from server (sometimes indexes is not empty and objReqs is (in the case of tb))
			if(!objReqs.empty())
				gMails = base::ObjectStorage::getInstance().getObjects(objReqs, NITE_GMAIL);
		}
		catch(...)
		{
			Logger::errorMessage("GMailUIWrapper", "decryptFull", "getObjects", nsresult(1));
			throw exception::domain::AlgorithmException(NITE_GMAIL, decryption);
		}

		for (size_t i = 0; i<indexes.size(); i++)
		{
			try
			{
				if (jsonArray[indexes[i]].getString(size_t(0)).compare("cs") == 0)
				{
					try
					{
						gmail::GMail gMail = static_cast<gmail::GMail&>(gMails[jsonArray[indexes[i]].getString(1)]);
						gMail.decryptCs(jsonArray[indexes[i]]);

						size_t j = 0;
						while (true)
						{
							j++;
							try
							{
								if (jsonArray[indexes[i] + j].empty())
									continue;

								if (jsonArray[indexes[i] + j].getString(0).compare("ms") != 0)
									break;

								gMail.decryptMs(jsonArray[indexes[i] + j]);
							}
							catch (...)
							{
								break;
							}
						}
					}
					catch (...)
					{
						continue;
					}
				}
				else if (jsonArray[indexes[i]].getString(size_t(0)).compare("ms") == 0)
				{
					try
					{
						gmail::GMail gMail = static_cast<gmail::GMail&>(gMails[jsonArray[indexes[i]].getString(1)]);
						gMail.decryptMs(jsonArray[indexes[i]]);
					}
					catch (...)
					{
						continue;
					}
				}
				else if (jsonArray[indexes[i]].getString(size_t(0)).compare("stu") == 0)
				{
					for (size_t j = 0; j < jsonArray[indexes[i]][2].size(); j++)
					{
						try
						{
							gmail::GMail gMail = static_cast<gmail::GMail&>(gMails[jsonArray[indexes[i]][2][j][1].getString(size_t(0))]);
							gMail.decryptSummary(jsonArray[indexes[i]][2][j][1]);
						}
						catch (...)
						{
							continue;
						}
					}
				}
				else if (jsonArray[indexes[i]].getString(size_t(0)).compare("tb") == 0)
				{
					try
					{
						decryptSummary(jsonArray[indexes[i]][2]);
					}
					catch (...)
					{
					}
				}
			}
			catch (...)
			{
				Logger::errorMessage("GMailUIWrapper", "decryptFull", "for outer second round", nsresult(1));
				return;
			}
		}
	}
	catch(...)
	{
		Logger::errorMessage("GMailUIWrapper", "decryptFull", "outer ...", nsresult(1));
		throw exception::domain::AlgorithmException(NITE_GMAIL, decryption);
	}
}

void gmail::GMailUIWrapper::getSentDraftObjectID(std::string reqId, std::string data)
{
	// check for out of bound indexes
	std::string dataCopy = data;
	gmail::GMail gMail = static_cast<gmail::GMail&>(base::ObjectStorage::getInstance().getTempObject(reqId, NITE_GMAIL));

	while (dataCopy.size() > 0)
	{
		try
		{
			json::JSON response = json::JSON(dataCopy);
			try
			{
				for (size_t i = 0; i < response[(size_t)0].size(); i++)
				{
					if (response[(size_t)0][i].getString(size_t(0)).compare("a") == 0)
					{
						try
						{
							gmail::GMail gMailNew(response[(size_t)0][i][3].getString(size_t(0)), gMail.objectKey, gMail.objectInitChain, gMail.objectCheckWord, gMail.objectDraftId);
							base::UsersList users;
							try
							{
								for (size_t j = 0; j < response[(size_t)0][i][3][3].size(); j++)
									gMailNew.attachments.push_back(response[(size_t)0][i][3][3][j].getString(5));
							}
							catch (...)
							{
							}
							base::ObjectStorage::getInstance().addObject(gMailNew, users);
							base::ObjectStorage::getInstance().removeTempObject(reqId);

							return;
						}
						catch (json::JsonTypeError)
						{
							continue;
						}
					}
				}
			}
			catch (...)
			{
				try
				{
					for (size_t i = 0; i < response.size(); i++)
					{
						if (response[i].getString(size_t(0)).compare("a") == 0)
						{
							try
							{
								gmail::GMail gMailNew(response[i][3].getString(size_t(0)), gMail.objectKey, gMail.objectInitChain, gMail.objectCheckWord, gMail.objectDraftId);
								base::UsersList users;
								try
								{
									for (size_t j = 0; j < response[i][3][3][(size_t)0].size(); j++)
										gMailNew.attachments.push_back(response[i][3][3][(size_t)0][j].getString(5));
								}
								catch (...)
								{
								}
								base::ObjectStorage::getInstance().addObject(gMailNew, users);
								base::ObjectStorage::getInstance().removeTempObject(reqId);

								return;
							}
							catch (json::JsonTypeError)
							{
								continue;
							}
						}
					}
				}
				catch (...)
				{
					throw exception::domain::AlgorithmException(NITE_GMAIL, encryption);
				}
			}
		}
		catch (...)
		{
		}
	}
}

std::string gmail::GMailUIWrapper::getSentMailObjectID(std::string reqId, std::string data)
{
	// check for out of bound indexes
	try
	{
		gmail::GMail gMail = static_cast<gmail::GMail&>(base::ObjectStorage::getInstance().getTempObject(reqId, NITE_GMAIL));
		std::string dataCopy = data;
		
		bool first = true;
		std::string returnString = "";

		while (dataCopy.size() > 0)
		{
			try
			{
				json::JSON response = json::JSON(dataCopy);
				bool done = false;

				for (size_t i = 0; i<response.size(); i++)
				{
					if (response[i].getString(size_t(0)).compare("a") == 0)
					{
						try
						{
							if(!done)
							{
								gmail::GMail gMailNew(response[i][response[i].size() - 1].getString(size_t(0)), gMail.objectKey, gMail.objectInitChain, gMail.objectCheckWord, gMail.objectDraftId);

								base::ObjectStorage::getInstance().addObject(gMailNew, gMail.users);
								base::ObjectStorage::getInstance().removeTempObject(reqId);
								done = true;
							}
						}
						catch(...)
						{
							break;

						}
					}
					else if (response[i].getString(size_t(0)).compare("csd") == 0)
					{
						if(!done)
						{
							gmail::GMail gMailNew(response[i][2].getString(1), gMail.objectKey, gMail.objectInitChain, gMail.objectCheckWord, gMail.objectDraftId);
							if(gMail.users.empty())
							{
								// Logger::logMessage("GMailUIWrapper", "getSentMailObjectID", "users is empty");
							}

							base::ObjectStorage::getInstance().addObject(gMailNew, gMail.users);
							base::ObjectStorage::getInstance().removeTempObject(reqId);
							done = true;
						}
						gMail.decryptMs(response[i][2]);
					}
				}
				std::string recalculatedPrefix;

				if(first)
				{
					recalculatedPrefix = response.getPrefix().substr(0, 6);
					first = false;
				}
				response.setPrefix("");
				size_t length = utf8Length(response.print()) + 2;

				std::ostringstream convert;
				convert << length;
				std::string lengthString = convert.str();
				recalculatedPrefix += lengthString + "\n";
				response.setPrefix(recalculatedPrefix);

				returnString += response.print() + "\n";
			}
			catch(json::JsonParsingError)
			{
				break;
			}
		}

		return returnString;
	}
	catch(...)
	{
		throw exception::domain::AlgorithmException(NITE_GMAIL, encryption);
		return data;
	}
}

// maybe needs unicode length not utf8
size_t gmail::GMailUIWrapper::utf8Length(const std::string str)
{
	try
	{
		std::size_t len = 0;
		std::string::const_iterator begin = str.begin();
		std::string::const_iterator end = str.end();
		while (begin != end)
		{
			unsigned char c = *begin;
			int n;
			if ((c & 0x80) == 0)
				n = 1;
			else if ((c & 0xE0) == 0xC0)
				n = 2;
			else if ((c & 0xF0) == 0xE0)
				n = 3;
			else if ((c & 0xF8) == 0xF0)
				n = 4;
			else
			{
				Logger::logMessage("", "", "utf8 length error");
				throw exception::domain::AlgorithmException(NITE_GMAIL, decryption);
			}


			// quick fix for emojis
			if (n == 4)
				len++;

			len++;
			begin += n;
		}

		return len;
	}
	catch (...)
	{
		return str.size();
	}
}

