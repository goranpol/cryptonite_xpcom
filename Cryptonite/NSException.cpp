#include <mozilla/Char16.h>

#include "NSException.h"

exception::technical::NSException::NSException(const nsresult errNum, const std::string act) : errorNumber(errNum), action(act)
{
}

exception::technical::NSException::~NSException() throw()
{
}

const char* exception::technical::NSException::what() const throw()
{
	return "";
}