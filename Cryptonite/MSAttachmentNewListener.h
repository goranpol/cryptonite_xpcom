#ifndef _MSAttachmentNewListener_h
#define _MSAttachmentNewListener_h

#include "HttpListener.h"

namespace hotmail
{
	class MSAttachmentNewListener : public base::HttpListener
	{
	public:
		MSAttachmentNewListener(const std::string attid);

		NS_DECL_ISUPPORTS
		NS_DECL_NSISTREAMLISTENER
		NS_DECL_NSIREQUESTOBSERVER

	private:
		~MSAttachmentNewListener();

		const std::string attachmentId;
	};
}

#endif
