#ifndef _GDriveFile_h
#define _GDriveFile_h

#include "DriveFile.h"

namespace gdrive
{
	class GDriveFile : public base::DriveFile
	{
	public:
		GDriveFile();
		GDriveFile(const std::string objID, bool single = false);
		GDriveFile(const std::string objID, const std::string objKey, const std::string checkWord, const std::string initChain);
		GDriveFile(const base::Object &obj);
		GDriveFile(const gdrive::GDriveFile &obj);
		~GDriveFile();

		GDriveFile &operator=(Object &obj);
		GDriveFile &operator=(GDriveFile &obj);
		bool operator==(Object &obj);
		bool operator!=(Object &obj);

		std::string encryptOutgoingString(std::string data);
		std::string encryptText(std::string textString);
		std::string encrypt(std::string textString);

		/* for fragmented data */
		void setIsFirst(const bool val);
		void setIsLast(const bool val);

	private:

		/* for fragmented data */
		bool isSingle;
		bool isFirst;
		bool isLast;
		std::string lastBlock;
		std::string initBlock;
		std::string remainder;

	};
}

#endif