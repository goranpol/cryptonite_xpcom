#include <mozilla/Char16.h>

#include "GDriveObserver.h"
#include "GDriveListener.h"
#include "GDriveFile.h"

#include "Logger.h"

#include "ObjectStorage.h"
#include "GDocument.h"

#include "NSException.h"
#include "AlgorithmException.h"
#include "SingletoneException.h"
#include "JsonError.h"

#include "Parser.h"

#include <math.h>
#include <sstream>

NS_IMPL_ISUPPORTS(gdrive::GDriveObserver, nsIObserver)

static const std::string base64Chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
										"abcdefghijklmnopqrstuvwxyz"
										"0123456789+/";

gdrive::GDriveObserver::GDriveObserver(bool encrypt) : HttpObserver(encrypt)
{
}

gdrive::GDriveObserver::~GDriveObserver()
{
}

NS_IMETHODIMP gdrive::GDriveObserver::Observe(nsISupports *aSubject, const char *aTopic, const char16_t *aData)
{
	nsresult rv;

	nsCOMPtr<nsIHttpChannel> httpChannel = do_QueryInterface(aSubject, &rv);
	if(NS_FAILED(rv))
	{
		// notify javascript
		Logger::errorMessage("GDriveObserver", "Observe", "Observe FAILED: couldn't query interface to nsIHttpChannel", rv);
		return NS_OK;
	}

	nsAutoCString requestMethodACString;
	httpChannel->GetRequestMethod(requestMethodACString);

	std::string method = requestMethodACString.get();
	if (method.compare("OPTIONS") == 0)
	{
		return NS_OK;
	}

	nsAutoCString urlPath;
	rv = getUri(httpChannel, urlPath);
	if(NS_FAILED(rv))
	{
		// notify javascript
		Logger::errorMessage("GDriveObserver", "Observe", "Observe FAILED: couldn't get uri", rv);
		return NS_OK;
	}
		
	if(!strcmp(aTopic, "http-on-modify-request"))
	{
		if(urlPath.Find("drive.google.com") != -1 && urlPath.Find("commonshare") != -1)
		{
			nsCOMPtr<nsIUploadChannel> uploadChannel = do_QueryInterface(httpChannel, &rv);
			if(NS_FAILED(rv))
			{
				// notify javascript
				Logger::errorMessage("GDriveObserver", "Observe:http-on-modify-request", "Observe FAILED: couldn't query interface to nsIUploadChannel", rv);
				return NS_OK;
			}

			try
			{
				std::string data = readSendingData(uploadChannel);

				try
				{
					changeUsersWithAccess(data);
					replaceSendingData(uploadChannel, data);
				}
				catch (...)
				{
					replaceSendingData(uploadChannel, data);
					return NS_OK;
				}
			}
			catch(exception::technical::NSException)
			{
				// notify javascript
			}
			catch(exception::technical::SingletoneException)
			{
				// notify javascript
			}
			catch(exception::domain::AlgorithmException)
			{
				// notify javascript
			}
			catch(...)
			{
				// notify javascript
			}

			return NS_OK;
		}
		else if (urlPath.Find("google.com/upload/drive") != -1 && doEncrypt)
		{
			nsCOMPtr<nsIUploadChannel> uploadChannel = do_QueryInterface(httpChannel, &rv);
			if (NS_FAILED(rv))
			{
				// notify javascript
				Logger::errorMessage("GDriveObserver", "Observe:http-on-modify-request", "Observe FAILED: couldn't query interface to nsIUploadChannel", rv);
				return NS_OK;
			}

			try
			{
				nsAutoCString method;
				httpChannel->GetRequestMethod(method);
				if (method.Compare("OPTIONS") == 0)
					return NS_OK;

				if (urlPath.Find("v2internal/files") != -1)
				{
					if (urlPath.Find("upload_id=") == -1)
					{
						std::string contentLengthString = getRequestHeader(httpChannel, "x-upload-content-length");
						std::string newContentLengthString = recalculateSize(contentLengthString);
						setRequestHeader(httpChannel, "x-upload-content-length", newContentLengthString);
						return NS_OK;
					}
					else
					{
						try
						{
							if (urlPath.Find("upload_id=") == -1)
								return NS_OK;

							std::string contentRangeString = getRequestHeader(httpChannel, "Content-Range");
							std::string data = readSendingData(uploadChannel);
							if (data.length() == 0)
							{
								replaceSendingData(uploadChannel, data);
								return NS_OK;
							}

							std::string url = urlPath.get();
							size_t andPos = url.rfind("&");
							size_t idPos = url.rfind("upload_id=");
							std::string uploadId = "";

							if (andPos < idPos)
							{
								uploadId = url.substr(idPos + 10);
							}
							else
							{
								uploadId = url.substr(idPos + 10, andPos - idPos - 10);
							}

							if (isSingleFragment(contentRangeString))
							{
								try
								{
									gui::GUIObserver::getInstance().notifyObserver(NITE_ENCRYPT, "file");

									std::string encryptedData = "";
									gdrive::GDriveFile gDriveFile = gdrive::GDriveFile(uploadId, true);
									encryptedData = gDriveFile.encryptOutgoingString(data);

									base::ObjectStorage::getInstance().addTempObject(gDriveFile);
									replaceSendingData(uploadChannel, encryptedData);

									std::string contentRangeRecalculated = recalculateContentRangeSingleFragment(contentRangeString);
									setRequestHeader(httpChannel, "Content-Range", contentRangeRecalculated);

									return NS_OK;
								}
								catch (...)
								{
									Logger::errorMessage("GDriveObserver", "Observe:http-on-modify-request in uploading single fragment", "...", nsresult(1));
									replaceSendingData(uploadChannel, data);
								}
							}
							else if (isFirstFragment(contentRangeString))
							{
								try
								{
									gui::GUIObserver::getInstance().notifyObserver(NITE_ENCRYPT, "file");

									std::string encryptedData = "";
									gdrive::GDriveFile gDriveFile = gdrive::GDriveFile(uploadId);
									encryptedData = gDriveFile.encryptOutgoingString(data);
									gDriveFile.setIsFirst(false);

									base::ObjectStorage::getInstance().addTempObject(gDriveFile);

									replaceSendingData(uploadChannel, encryptedData);

									std::string contentRangeRecalculated = recalculateContentRangeFirstFragment(contentRangeString);
									setRequestHeader(httpChannel, "Content-Range", contentRangeRecalculated);

									return NS_OK;
								}
								catch (...)
								{
									Logger::errorMessage("GDriveObserver", "Observe:http-on-modify-request in uploading first fragment", "...", nsresult(1));
									replaceSendingData(uploadChannel, data);
								}
							}
							else if (isLastFragment(contentRangeString))
							{
								try
								{
									gui::GUIObserver::getInstance().notifyObserver(NITE_ENCRYPT, "file");

									std::string encryptedData = "";
									gdrive::GDriveFile gDriveFile = static_cast<gdrive::GDriveFile&>(base::ObjectStorage::getInstance().getTempObject(uploadId, NITE_ONEDRIVEFILE));
									base::ObjectStorage::getInstance().removeTempObject(uploadId);
									gDriveFile.setIsLast(true);
									gDriveFile.setIsFirst(false);
									encryptedData = gDriveFile.encryptOutgoingString(data);

									base::ObjectStorage::getInstance().addTempObject(gDriveFile);

									replaceSendingData(uploadChannel, encryptedData);

									std::string contentRangeRecalculated = recalculateContentRangeLastFragment(contentRangeString);
									setRequestHeader(httpChannel, "Content-Range", contentRangeRecalculated);

									return NS_OK;
								}
								catch (...)
								{
									Logger::errorMessage("GDriveObserver", "Observe:http-on-modify-request in uploading last fragment", "...", nsresult(1));
									replaceSendingData(uploadChannel, data);
								}
							}
							else
							{
								try
								{
									std::string encryptedData = "";
									gdrive::GDriveFile gDriveFile = static_cast<gdrive::GDriveFile&>(base::ObjectStorage::getInstance().getTempObject(uploadId, NITE_ONEDRIVEFILE));
									base::ObjectStorage::getInstance().removeTempObject(uploadId);
									gDriveFile.setIsLast(false);
									gDriveFile.setIsFirst(false);
									encryptedData = gDriveFile.encryptOutgoingString(data);

									base::ObjectStorage::getInstance().addTempObject(gDriveFile);

									replaceSendingData(uploadChannel, encryptedData);

									std::string contentRangeRecalculated = recalculateContentRange(contentRangeString);
									setRequestHeader(httpChannel, "Content-Range", contentRangeRecalculated);

									return NS_OK;
								}
								catch (...)
								{
									Logger::errorMessage("OneDriveObserver", "Observe:http-on-modify-request in uploading middle fragment", "...", nsresult(1));
									replaceSendingData(uploadChannel, data);
								}
							}
						}
						catch (exception::technical::NSException)
						{
							return NS_OK;
						}
						catch (...)
						{
							return NS_OK;
						}
					}
				}
				else
				{
					std::string data = readSendingData(uploadChannel);
					if (data.length() == 0)
					{
						replaceSendingData(uploadChannel, data);
						return NS_OK;
					}

					std::string dataCopy = data;
					try
					{
						json::JSON propsJson(dataCopy);
						std::string id = propsJson.getString("id");

						gdrive::GDriveFile gDriveFile(id);
						std::string encryptedData = "";

						size_t encodingPos = data.find("content-transfer-encoding: ");
						std::string encodingPart = "";
						if (encodingPos != -1)
						{
							encodingPart = data.substr(encodingPos + 27);
							encodingPart = encodingPart.substr(0, encodingPart.find("\r\n"));
						}

						std::string file = data.substr(data.rfind("\r\n\r\n") + 4);
						file = file.substr(0, file.find("\r\n"));

						gui::GUIObserver::getInstance().notifyObserver(NITE_ENCRYPT, "file");
						std::string encryptedFile = "";
						if (encodingPart.compare("base64") == 0)
						{
							std::string plainFile = fromBase64WithPadding(file);
							std::string encryptedFilePlain = gDriveFile.encryptOutgoingString(plainFile);
							encryptedFile = toBase64WithPadding(encryptedFilePlain, encryptedFilePlain.size());
						}
						else
						{
							encryptedFile = gDriveFile.encryptOutgoingString(file);
						}

						encryptedData = data.substr(0, data.rfind("\r\n\r\n") + 4);
						
						std::string lastPart = data.substr(data.rfind("\r\n\r\n") + 4);
						lastPart = lastPart.substr(lastPart.find("\r\n"));

						encryptedData += encryptedFile;
						encryptedData += lastPart;

						replaceSendingData(uploadChannel, encryptedData);

						base::ObjectStorage::getInstance().addObject(gDriveFile, base::UsersList());
						return NS_OK;
					}
					catch (...)
					{
						replaceSendingData(uploadChannel, data);
						return NS_OK;
					}
				}

				return NS_OK;
			}
			catch (exception::technical::NSException)
			{
				Logger::errorMessage("GDriveObserver", "Observe:http-on-modify-request", "...", nsresult(2));
				// notify javascript
			}
			catch (exception::technical::SingletoneException)
			{
				Logger::errorMessage("GDriveObserver", "Observe:http-on-modify-request", "...", nsresult(3));
				// notify javascript
			}
			catch (exception::domain::AlgorithmException)
			{
				Logger::errorMessage("GDriveObserver", "Observe:http-on-modify-request", "...", nsresult(4));
				// notify javascript
			}
			catch (...)
			{
				Logger::errorMessage("GDriveObserver", "Observe:http-on-modify-request", "...", nsresult(5));
				// notify javascript
			}

			return NS_OK;
		}
	}
	else if(!strcmp(aTopic, "http-on-examine-response"))
	{
		if((urlPath.Find("drive.google.com/ar") != -1))
		{
			nsCOMPtr<nsITraceableChannel> traceableChannel = do_QueryInterface(aSubject, &rv);
			if(NS_FAILED(rv))
			{
				// notify javascript
				Logger::errorMessage("GDriveObserver", "Observe:http-on-examine-response", "Observe FAILED: couldn't query interface to nsITraceableChannel", rv);
				return rv;
			}

			RefPtr<base::HttpListener> gDriveListener = new gdrive::GDriveListener(NITE_deleteType);

			try
			{
				replaceStreamListener(traceableChannel, gDriveListener);
			}
			catch(exception::technical::NSException)
			{
				// notify javascript
			}
			catch(...)
			{
				// notify javascript
			}

			return NS_OK;
		}
		else if (urlPath.Find(".googleusercontent.") != -1 && urlPath.Find("e=download") != -1)
		{
			nsCOMPtr<nsITraceableChannel> traceableChannel = do_QueryInterface(aSubject, &rv);
			if (NS_FAILED(rv))
			{
				// notify javascript
				Logger::errorMessage("GDriveObserver", "Observe:http-on-examine-response", "Observe FAILED: couldn't query interface to nsITraceableChannel", rv);
				return rv;
			}

			std::string id = urlPath.get();
			id = id.substr(id.rfind("/") + 1);
			id = id.substr(0, id.find("?"));
			RefPtr<base::HttpListener> gDriveListener = new gdrive::GDriveListener(id);

			try
			{
				replaceStreamListener(traceableChannel, gDriveListener);
			}
			catch (exception::technical::NSException)
			{
				// notify javascript
			}
			catch (...)
			{
				// notify javascript
			}
		}
		else if (urlPath.Find("google.com/upload/drive") != -1 && urlPath.Find("v2internal/files") != -1 && doEncrypt)
		{
			nsCOMPtr<nsITraceableChannel> traceableChannel = do_QueryInterface(aSubject, &rv);
			if (NS_FAILED(rv))
			{
				// notify javascript
				Logger::errorMessage("GDriveObserver", "Observe:http-on-examine-response", "Observe FAILED: couldn't query interface to nsITraceableChannel", rv);
				return rv;
			}

			try
			{
				nsAutoCString method;
				httpChannel->GetRequestMethod(method);
				if (method.Compare("OPTIONS") == 0)
					return NS_OK;

				if (urlPath.Find("upload_id=") == -1)
					return NS_OK;

				std::string id = getResponseHeader(httpChannel, "x-guploader-uploadid");
				RefPtr<base::HttpListener> gDriveListener = new gdrive::GDriveListener(NITE_getIdType, id);
				replaceStreamListener(traceableChannel, gDriveListener);
			}
			catch (exception::technical::NSException)
			{
				Logger::errorMessage("GDriveObserver", "Observe:http-on-examine-response", "get id", nsresult(1));
				// notify javascript
				return NS_OK;
			}
			catch (...)
			{
				Logger::errorMessage("GDriveObserver", "Observe:http-on-examine-response", "get id", nsresult(2));
				// notify javascript
				return NS_OK;
			}
		}
	}

	return NS_OK;
}

void gdrive::GDriveObserver::changeUsersWithAccess(std::string data)
{
	std::vector<std::string> attributes = split(data, "&");

	std::vector<base::ObjectRequest> reqs;
	base::UsersList users;
	std::string type;

	try
	{
		std::string modelChanges;
		for (size_t i = 0; i<attributes.size(); i++)
		{
			if (attributes[i].find("requestType=") == 0)
			{
				type = attributes[i].substr(12);
				if (type.compare("data") == 0)
					return;
			}
			else if (attributes[i].find("itemIds=") == 0)
			{
				std::string itemIds = attributes[i].substr(8);
				std::vector<std::string> itemIdsVector = split(itemIds, ",");

				for (size_t j = 0; j<itemIdsVector.size(); j++)
					reqs.push_back(base::ObjectRequest(itemIdsVector[j]));
			}
			else if (attributes[i].find("modelChanges=") == 0)
			{
				modelChanges = attributes[i].substr(13);
			}
			else if (attributes[i].find("notificationInfo=") == 0)
			{
				modelChanges = attributes[i].substr(17);
			}
		}

		json::JSON modelChangesJson = json::JSON(modelChanges);
		if (type.compare("invite") == 0)
		{
			if (modelChangesJson.getString("messageType").compare("invite") == 0)
			{
				type = "a";
				std::string recipient = modelChangesJson.getString("recipient");
				while (true)
				{
					if (recipient.find("<") == -1)
						break;

					std::string user = recipient.substr(recipient.find("<") + 1, recipient.find(">") - recipient.find("<") - 1);
					recipient = recipient.substr(recipient.find(">") + 1);
					users.pushBack(user);
				}
			}
		}
		else if (type.compare("aclChange") == 0)
		{
			type = "r";
			for (size_t i = 0; i<modelChangesJson["aclEntries"].size(); i++)
			{
				if (modelChangesJson["aclEntries"][i].getInt("role") == 0)
					users.pushBack(modelChangesJson["aclEntries"][i]["scope"].getString("email"));
			}
		}
		else
			return;

		if (!users.empty())
		{
			if (reqs.empty())
				return;
			else if (reqs.size() == 1)
			{
				base::ObjectStorage::getInstance().changeAccess(reqs[0], users, type, NITE_GDRIVEFILE);
				return;
			}

			base::ObjectStorage::getInstance().changeAccess(reqs, users, type, NITE_GDRIVEFILE);
			return;
		}
	}
	catch (json::JsonError)
	{
		throw exception::domain::AlgorithmException(NITE_GDRIVEFILE, useraccess);
	}
}

std::string gdrive::GDriveObserver::recalculateSize(std::string sizeStr)
{
	size_t size = atoi(sizeStr.c_str());
	if (size % 16 == 0 || size % 16 == 15)
		size = size + 16;

	// int into size_t
	// Calculate size of encrypted file
	size_t newSize = ceil(double(size) / 16) * 16; // encrypted size
	size_t remainder = newSize % 3;
	newSize = newSize / 3 * 4;					// base64 size without remainder
	if (remainder == 1)
		newSize += 2;
	else if (remainder == 2)
		newSize += 3;							// base64 size with remainder
	newSize += 86;								// with added checkword;

	return std::to_string(newSize);
}

std::string gdrive::GDriveObserver::recalculateContentRangeSingleFragment(std::string data)
{
	std::string returnString = "";
	std::vector<std::string> rangeSplitted = split(data, "-");

	returnString = rangeSplitted[0];

	std::string len = split(rangeSplitted[1], "/")[1];
	size_t length = atoi(len.c_str());
	if (length % 16 == 0 || length % 16 == 15)
		length = length + 16;

	// Calculate size of encrypted file
	size_t newSize = ceil(double(length) / 16) * 16; // encrypted size
	size_t remainder = newSize % 3;
	newSize = newSize / 3 * 4;					// base64 size without remainder
	if (remainder == 1)
		newSize += 2;
	else if (remainder == 2)
		newSize += 3;							// base64 size with remainder

	newSize += 86;								// with added checkword

	returnString += "-" + std::to_string(newSize - 1) + "/" + std::to_string(newSize);

	return returnString;
}

std::string gdrive::GDriveObserver::recalculateContentRangeFirstFragment(std::string data)
{
	std::string dataCpy = data.substr(6);

	std::string from = "";
	size_t i = 0;
	while (dataCpy[i] != '-')
	{
		from.push_back(dataCpy[i]);
		i++;
	}

	if (from.compare("0") != 0)
	{
		Logger::errorMessage("OneDriveObserver", "recalculateContentRangeFirstFragment", "not first fragment", nsresult(1));
		throw std::exception();
	}

	dataCpy = dataCpy.substr(dataCpy.find("-") + 1);
	i = 0;
	std::string to = "";
	while (dataCpy[i] != '/')
	{
		to.push_back(dataCpy[i]);
		i++;
	}

	std::string length = dataCpy.substr(dataCpy.find("/") + 1);

	size_t toSizeT = atoi(to.c_str());
	size_t newToSizeT = floor(double(toSizeT + 1) / 48) * 48;	// encrypted size
	newToSizeT = (newToSizeT / 3 * 4) - 1;						// base64 size without remainder
	newToSizeT += 86;											// with added checkword
	std::string toRecalculated = std::to_string(newToSizeT);

	size_t len = atoi(length.c_str());
	if (len % 16 == 0 || len % 16 == 15)
		len = len + 16;

	// Calculate size of encrypted file
	size_t newSize = ceil(double(len) / 16) * 16; // encrypted size
	size_t remainder = newSize % 3;
	newSize = newSize / 3 * 4;					// base64 size without remainder
	if (remainder == 1)
		newSize += 2;
	else if (remainder == 2)
		newSize += 3;							// base64 size with remainder

	newSize += 86;								// with added checkword
	std::string lengthRecalculated = std::to_string(newSize);

	std::string returnString = "bytes " + from + "-" + toRecalculated + "/" + lengthRecalculated;

	return returnString;
}

std::string gdrive::GDriveObserver::recalculateContentRangeLastFragment(std::string data)
{
	std::string dataCpy = data.substr(6);

	std::string from = "";
	size_t i = 0;
	while (dataCpy[i] != '-')
	{
		from.push_back(dataCpy[i]);
		i++;
	}

	if (from.compare("0") == 0)
	{
		Logger::errorMessage("OneDriveObserver", "recalculateContentRangeLastFragment", "is first fragment", nsresult(1));
		throw std::exception();
	}

	dataCpy = dataCpy.substr(dataCpy.find("-") + 1);
	i = 0;
	std::string to = "";
	while (dataCpy[i] != '/')
	{
		to.push_back(dataCpy[i]);
		i++;
	}

	std::string length = dataCpy.substr(dataCpy.find("/") + 1);

	size_t fromSizeT = atoi(from.c_str());
	size_t newFromSizeT = floor(double(fromSizeT) / 48) * 48;	// encrypted size
	newFromSizeT = (newFromSizeT / 3 * 4);						// base64 size without remainder
	newFromSizeT += 86;											// with added checkword
	std::string fromRecalculated = std::to_string(newFromSizeT);

	size_t len = atoi(length.c_str());
	if (len % 16 == 0 || len % 16 == 15)
		len = len + 16;

	// Calculate size of encrypted file
	size_t newSize = ceil(double(len) / 16) * 16; // encrypted size
	size_t remainder = newSize % 3;
	newSize = newSize / 3 * 4;					// base64 size without remainder
	if (remainder == 1)
		newSize += 2;
	else if (remainder == 2)
		newSize += 3;							// base64 size with remainder

	newSize += 86;								// with added checkword
	std::string lengthRecalculated = std::to_string(newSize);

	std::string returnString = "bytes " + fromRecalculated + "-" + std::to_string(newSize - 1) + "/" + lengthRecalculated;

	return returnString;
}

std::string gdrive::GDriveObserver::recalculateContentRange(std::string data)
{
	std::string dataCpy = data.substr(6);

	std::string from = "";
	size_t i = 0;
	while (dataCpy[i] != '-')
	{
		from.push_back(dataCpy[i]);
		i++;
	}

	if (from.compare("0") == 0)
	{
		Logger::errorMessage("OneDriveObserver", "recalculateContentRange", "is first fragment", nsresult(1));
		throw std::exception();
	}

	dataCpy = dataCpy.substr(dataCpy.find("-") + 1);
	i = 0;
	std::string to = "";
	while (dataCpy[i] != '/')
	{
		to.push_back(dataCpy[i]);
		i++;
	}

	std::string length = dataCpy.substr(dataCpy.find("/") + 1);

	size_t fromSizeT = atoi(from.c_str());
	size_t newFromSizeT = floor(double(fromSizeT) / 48) * 48;	// encrypted size
	newFromSizeT = (newFromSizeT / 3 * 4);						// base64 size without remainder
	newFromSizeT += 86;											// with added checkword
	std::string fromRecalculated = std::to_string(newFromSizeT);

	size_t toSizeT = atoi(to.c_str());
	size_t newToSizeT = floor(double(toSizeT + 1) / 48) * 48;	// encrypted size
	newToSizeT = (newToSizeT / 3 * 4) - 1;					// base64 size without remainder
	newToSizeT += 86;										// with added checkword
	std::string toRecalculated = std::to_string(newToSizeT);

	size_t len = atoi(length.c_str());
	if (len % 16 == 0 || len % 16 == 15)
		len = len + 16;

	// Calculate size of encrypted file
	size_t newSize = ceil(double(len) / 16) * 16; // encrypted size
	size_t remainder = newSize % 3;
	newSize = newSize / 3 * 4;					// base64 size without remainder
	if (remainder == 1)
		newSize += 2;
	else if (remainder == 2)
		newSize += 3;							// base64 size with remainder

	newSize += 86;								// with added checkword
	std::string lengthRecalculated = std::to_string(newSize);

	std::string returnString = "bytes " + fromRecalculated + "-" + toRecalculated + "/" + lengthRecalculated;

	return returnString;
}

bool gdrive::GDriveObserver::isSingleFragment(std::string data)
{
	std::string range = data.substr(6);

	std::vector<std::string> rangeVector = split(range, "-");
	if (rangeVector.size() != 2)
		throw std::exception();

	std::vector<std::string>  rangeVector2 = split(rangeVector[1], "/");
	if (rangeVector2.size() != 2)
		throw std::exception();

	std::string from = rangeVector[0];
	std::string to = rangeVector2[0];
	std::string size = rangeVector2[1];

	if (from.compare("0") == 0)
	{
		size_t toUInt = atoi(to.c_str());
		size_t sizeUInt = atoi(size.c_str());

		if ((toUInt + 1) == sizeUInt)
			return true;
	}

	return false;
}

bool gdrive::GDriveObserver::isFirstFragment(std::string data)
{
	std::string range = data.substr(6);

	std::vector<std::string> rangeVector = split(range, "-");
	if (rangeVector.size() != 2)
		throw std::exception();

	if (rangeVector[0].compare("0") == 0)
		return true;

	return false;
}

bool gdrive::GDriveObserver::isLastFragment(std::string data)
{
	std::vector<std::string> rangeVector = split(data, "/");
	if (rangeVector.size() != 2)
		throw std::exception();

	std::vector<std::string> rangeVector2 = split(rangeVector[0], "-");
	if (rangeVector2.size() != 2)
		throw std::exception();

	std::string to = rangeVector2[1];
	std::string size = rangeVector[1];

	size_t toUInt = atoi(to.c_str());
	size_t sizeUInt = atoi(size.c_str());

	if ((toUInt + 1) == sizeUInt)
		return true;

	return false;
}

std::string gdrive::GDriveObserver::toBase64WithPadding(const std::string input, size_t len)
{
	std::string ret;
	int i = int(0);
	int j = int(0);
	unsigned char char_array_3[3];
	unsigned char char_array_4[4];
	unsigned char *inputCString = (unsigned char*)input.c_str();

	while (len--)
	{
		char_array_3[i++] = *(inputCString++);
		if (i == 3)
		{
			char_array_4[int(0)] = (char_array_3[int(0)] & 0xfc) >> 2;
			char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
			char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
			char_array_4[3] = char_array_3[2] & 0x3f;

			for (i = int(0); i < 4; i++)
				ret += base64Chars[char_array_4[i]];
			i = int(0);
		}
	}

	if (i)
	{
		for (j = i; j < 3; j++)
			char_array_3[j] = '\0';

		char_array_4[int(0)] = (char_array_3[int(0)] & 0xfc) >> 2;
		char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
		char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
		char_array_4[3] = char_array_3[2] & 0x3f;

		for (j = int(0); j < i + 1; j++)
			ret += base64Chars[char_array_4[j]];

		while (i++ < 3)
		{
			ret += '=';
		}
	}

	return ret;
}

std::string gdrive::GDriveObserver::fromBase64WithPadding(const std::string input)
{
	int len = input.size();
	int i = int(0);
	int j = int(0);
	int in_ = int(0);
	unsigned char char_array_4[4];
	unsigned char char_array_3[3];
	std::string ret;

	while (len-- && input[in_] != '=')
	{
		char_array_4[i++] = input[in_];
		j++;

		in_++;
		if (i == 4)
		{
			for (i = int(0); i < 4; i++)
				char_array_4[i] = base64Chars.find(char_array_4[i]);

			char_array_3[int(0)] = (char_array_4[int(0)] << 2) + ((char_array_4[1] & 0x30) >> 4);
			char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
			char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

			for (i = int(0); i < j - 1; i++)
				ret += char_array_3[i];
			i = 0;
			j = 0;
		}
	}

	if (i)
	{
		for (j = i; j < 4; j++)
			char_array_4[j] = int(0);

		for (j = int(0); j < 4; j++)
			char_array_4[j] = base64Chars.find(char_array_4[j]);

		char_array_3[int(0)] = (char_array_4[int(0)] << 2) + ((char_array_4[1] & 0x30) >> 4);
		char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
		char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

		for (j = int(0); j < i - 1; j++)
			ret += char_array_3[j];
	}

	return ret;
}
