#ifndef _NotImplementedException_h_
#define _NotImplementedException_h_

#include <exception>
#include <string>

namespace exception
{
	namespace domain
	{
		class NotImplementedException : public std::exception
		{
		public:
			NotImplementedException(const std::string clss, const std::string funct);
			virtual ~NotImplementedException() throw();

			const char* what() const throw();

		private:
			const std::string className;
			const std::string functionName;
		};
	}
}

#endif
