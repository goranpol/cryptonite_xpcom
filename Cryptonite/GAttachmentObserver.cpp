#include <mozilla/Char16.h>

#include "GAttachmentObserver.h"
#include "GAttachmentListener.h"

#include "NSException.h"

#include "Logger.h"


NS_IMPL_ISUPPORTS(gmail::GAttachmentObserver, nsIObserver)

gmail::GAttachmentObserver::GAttachmentObserver(bool encrypt) : HttpObserver(encrypt)
{
}

gmail::GAttachmentObserver::~GAttachmentObserver()
{
}

NS_IMETHODIMP gmail::GAttachmentObserver::Observe(nsISupports *aSubject, const char *aTopic, const char16_t *aData)
{
	nsresult rv;

	nsCOMPtr<nsIHttpChannel> httpChannel = do_QueryInterface(aSubject, &rv);
	if (NS_FAILED(rv))
	{
		// notify javascript
		Logger::errorMessage("GMailObserver", "Observe", "Observe FAILED: couldn't query interface to nsIHttpChannel", rv);
		return NS_OK;
	}

	nsAutoCString urlPath;
	rv = getUri(httpChannel, urlPath);
	if (NS_FAILED(rv))
	{
		// notify javascript
		Logger::errorMessage("GMailObserver", "Observe", "Observe FAILED: couldn't get uri", rv);
		return NS_OK;
	}

	if (!strcmp(aTopic, "http-on-examine-response"))
	{
		RefPtr<gmail::GAttachmentListener> gAttachmentListener;

		if (urlPath.Find("mail-attachment.googleusercontent.com/attachment/") != -1)
		{
			if (urlPath.Find("realattid=") != -1)
			{
				std::string url = urlPath.get();
				std::string realattid = url.substr(urlPath.Find("&realattid=") + 11);
				realattid = realattid.substr(0, realattid.find("&"));

				gui::GUIObserver::getInstance().notifyObserver(NITE_DECRYPT, "google attachment");
				gAttachmentListener = new gmail::GAttachmentListener(realattid);
			}
			else
			{
				gAttachmentListener = new gmail::GAttachmentListener("-1");
			}
		}
		else
		{
			return NS_OK;
		}

		nsCOMPtr<nsIHttpChannel> httpChannel = do_QueryInterface(aSubject, &rv);
		if (NS_FAILED(rv))
		{
			// notify javascript
			Logger::errorMessage("GMailObserver", "Observe:http-on-examine-response", "Observe FAILED: couldn't query interface aSubject to nsIHttpChannel", rv);
			return NS_OK;
		}

		nsCOMPtr<nsITraceableChannel> traceableChannel = do_QueryInterface(aSubject, &rv);
		if (NS_FAILED(rv))
		{
			// notify javascript
			Logger::errorMessage("GMailObserver", "Observe:http-on-examine-response", "Observe FAILED: couldn't query interface to nsITraceableChannel", rv);
			return NS_OK;
		}

		try
		{
			replaceStreamListener(traceableChannel, gAttachmentListener);
		}
		catch (exception::technical::NSException)
		{
			// notify javascript
		}
		catch (...)
		{
			// notify javascript
		}

		return NS_OK;
	}
	return NS_OK;
}
