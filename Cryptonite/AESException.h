#ifndef _AESException_h_
#define _AESException_h_

#include <exception>

#define keynull			0
#define keylen			1
#define blocklen		2
#define keyinit			3
#define cipherstrempty	4

namespace exception
{
	namespace domain
	{
		class AESException : public std::exception
		{
		public:
			AESException(const int typ);
			virtual ~AESException() throw();

			const char* what() const throw();

		private:
			const int type;
		};
	}
}

#endif
