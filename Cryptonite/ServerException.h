#ifndef _ServerException_h_
#define _ServerException_h_

#include <exception>
#include <string>

namespace exception
{
	namespace domain
	{
		class ServerException : public std::exception
		{
		public:
			ServerException(const int errNum, const std::string errMsg);
			virtual ~ServerException() throw();

			const char* what() const throw();

		private:
			const int errorNumber;
			const std::string errorMessage;
		};
	}
}

#endif
