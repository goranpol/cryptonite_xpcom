#include <mozilla/Char16.h>

#include "AESException.h"

exception::domain::AESException::AESException(const int typ) : type(typ)
{
}

exception::domain::AESException::~AESException() throw()
{
}

const char* exception::domain::AESException::what() const throw()
{
	return "";
}