#ifndef _Mail_h_
#define _Mail_h_

#include "Object.h"
#include "UsersList.h"

namespace base
{
	class Mail : public base::Object
	{
	public:
		Mail(const std::string objId, const std::string objKey, const std::string objInitChain, const std::string objCheckWord, const int objType);
		Mail(const std::string objId, const std::string objKey, const std::string objInitChain, const std::string objCheckWord, const int objType, const std::string objDraftId);
		Mail(const std::string objId, const std::string objKey, const std::string objInitChain, const std::string objCheckWord, const int objType, const std::string objDraftId, std::vector<std::string> attachmentIds);
		Mail(const Object &obj);
		Mail(const Mail &obj);

		Mail &operator=(Object &obj);
		Mail &operator=(Mail &obj);
		bool operator==(Mail &obj);
		bool operator!=(Mail &obj);

		std::string getObjectInitChain();
		std::string getObjectCheckWord();

		json::Value &toJson() const;

	protected:
		virtual ~Mail();

		std::string getInitChain(const base::Object &obj);
		std::string getCheckWord(const base::Object &obj);

		std::string generateCheckWord();

		virtual std::string encryptSubject(std::string subject);
		virtual std::string decryptSubject(std::string subject);
		virtual std::string encryptBody(std::string body);
		virtual std::string	decryptBody(std::string body);

		std::string encryptText(std::string textString);
		std::string decryptText(std::string cipherTextString);
		std::string encrypt(std::string textString);
		std::string decrypt(std::string cipherTextString);

		/* MAIL MEMBERS */
		const std::string objectInitChain;
		const std::string objectCheckWord;
		const std::string objectDraftId;
		std::vector<std::string> attachments;
		base::UsersList users;
	};
}

#endif
