#include <mozilla/Char16.h>

#include "GUIObserver.h"

#include "nsIServiceManager.h"
#include "nsIObserverService.h"
#include <nsStringAPI.h>

#include "Object.h"
#include "ObjectStorage.h"
#include "GDocument.h"
#include "GMail.h"

#include "Logger.h"

#include "NSException.h"
#include "ObjectNotFoundException.h"
#include "ObjectTypeException.h"

RefPtr<gui::GUIObserver> gui::GUIObserver::mGUIObserver = nullptr;
NS_IMPL_ISUPPORTS(gui::GUIObserver, nsIObserver)

gui::GUIObserver::GUIObserver()
{
}

gui::GUIObserver::~GUIObserver()
{
}

gui::GUIObserver &gui::GUIObserver::getInstance()
{
	if(mGUIObserver == nullptr)
		mGUIObserver = new GUIObserver;

	return *mGUIObserver;
}

void gui::GUIObserver::registerObserver()
{
	std::string action = "registering gui observer";
	nsresult rv;

	nsCOMPtr<nsIServiceManager> servMan;
	rv = NS_GetServiceManager(getter_AddRefs(servMan));
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	nsCOMPtr<nsIObserverService> observerService;
	rv = servMan->GetServiceByContractID("@mozilla.org/observer-service;1", NS_GET_IID(nsIObserverService), getter_AddRefs(observerService));
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}
	
	// all of the topics
	rv = observerService->AddObserver(&GUIObserver::getInstance(), "crypt-o-nite-encrypt-object-yes", false);
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	rv = observerService->AddObserver(&GUIObserver::getInstance(), "crypt-o-nite-encrypt-object-no", false);
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}
}

void gui::GUIObserver::unregisterObserver()
{
	std::string action = "unregistering gui observer";
	nsresult rv;

	nsCOMPtr<nsIServiceManager> servMan;
	rv = NS_GetServiceManager(getter_AddRefs(servMan));
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	nsCOMPtr<nsIObserverService> observerService;
	rv = servMan->GetServiceByContractID("@mozilla.org/observer-service;1", NS_GET_IID(nsIObserverService), getter_AddRefs(observerService));
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	// all of the topics
	rv = observerService->RemoveObserver(&GUIObserver::getInstance(), "crypt-o-nite-encrypt-object-yes");
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	rv = observerService->RemoveObserver(&GUIObserver::getInstance(), "crypt-o-nite-encrypt-object-no");
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}
}

void gui::GUIObserver::notifyObserver(const char* aTopic, std::string message)
{
	std::string action = "notifying javascript observer with topic and message";
	nsresult rv;

	nsCOMPtr<nsIServiceManager> servMan;
	rv = NS_GetServiceManager(getter_AddRefs(servMan));
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	nsCOMPtr<nsIObserverService> observerService;
	rv = servMan->GetServiceByContractID("@mozilla.org/observer-service;1", NS_GET_IID(nsIObserverService), getter_AddRefs(observerService));
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	rv = observerService->NotifyObservers(NULL, aTopic, NS_ConvertASCIItoUTF16(message.c_str()).get());
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}
}

NS_IMETHODIMP gui::GUIObserver::Observe(nsISupports *aSubject, const char *aTopic, const char16_t *aData)
{
	// not needed anymore
	return NS_OK;
}

std::string gui::GUIObserver::toString(nsEmbedString s)
{
	PRUnichar *start = (PRUnichar *)s.get();
	PRUnichar *end = (PRUnichar *)s.get() + s.Length();
	wchar_t wca[4096];
	wchar_t *wstart = wca;
	wchar_t *wpr = wstart;

	for(; start < end; ++start)
	{
		*wstart = (wchar_t) *start;
		++wstart;
	}
	*wstart = 0;


	std::string ptr;
	ptr.resize(4096);
	size_t size = wcstombs((char*)ptr.data(), wpr, 4096);
	ptr.resize(size);

	return ptr;
}
