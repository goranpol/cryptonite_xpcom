#include <mozilla/Char16.h>

#include "JsonTypeError.h"

#include <sstream>
#include "Value.h"
#include "Enumeration.h"
#include "Number.h"

json::JsonTypeError::JsonTypeError(const int expectedTyp, const int givenTyp) : expectedType(expectedTyp), givenType(givenTyp)
{
}

json::JsonTypeError::~JsonTypeError()
{
}

const char* json::JsonTypeError::what() const throw()
{
	std::string returnString = "Json error: expected type: ";
	if(expectedType == JsonEnumeration)
		returnString += "json::Enumeration";
	else if(expectedType == NITE_ArrayType)
		returnString += "json::Enumeration::Array";
	else if(expectedType == NITE_JavascriptType)
		returnString += "json::Enumeration::Javascript";
	else if(expectedType == ObjectType)
		returnString += "json::Enumeration::Object";
	else if(expectedType == JsonString)
		returnString += "json::String ";
	else if(expectedType == JsonBoolean)
		returnString += "json::Bool ";
	else if(expectedType == JsonNull)
		returnString += "json::Value(NULL)";
	else if(expectedType == JsonEmpty)
		returnString += "json::Value(Empty)";
	else if(expectedType == JsonNumber)
		returnString += "json::Number";
	else if(expectedType == NITE_INTEGER)
		returnString += "json::Number NITE_INTEGER";
	else if(expectedType == NITE_DOUBLE)
		returnString += "json::Number NITE_DOUBLE";

	returnString += " given type: ";
	if(givenType == JsonEnumeration)
		returnString += "json::Enumeration";
	else if(givenType == NITE_ArrayType)
		returnString += "json::Enumeration::Array";
	else if(givenType == NITE_JavascriptType)
		returnString += "json::Enumeration::Javascript";
	else if(givenType == ObjectType)
		returnString += "json::Enumeration::Object";
	else if(givenType == JsonString)
		returnString += "json::String ";
	else if(givenType == JsonBoolean)
		returnString += "json::Bool ";
	else if(givenType == JsonNull)
		returnString += "json::Value(NULL)";
	else if(givenType == JsonEmpty)
		returnString += "json::Value(Empty)";
	else if(givenType == JsonNumber)
		returnString += "json::Number";
	else if(expectedType == NITE_INTEGER)
		returnString += "json::Number NITE_INTEGER";
	else if(expectedType == NITE_DOUBLE)
		returnString += "json::Number NITE_DOUBLE";
	else
		returnString += "type error";

	return fromStdString(returnString);
}
