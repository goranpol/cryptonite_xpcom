#include <mozilla/Char16.h>

#include "GMailObserver.h"
#include "GMailListener.h"
#include "GAttachmentListener.h"

#include "Logger.h"

#include "GMailAttachment.h"
#include "GMailUIWrapper.h"
#include "ObjectStorage.h"
#include "EscapeEncoding.h"

#include "AlgorithmException.h"
#include "NSException.h"
#include "ObjectRequestException.h"
#include "ObjectTypeException.h"
#include "ServerException.h"
#include "SingletoneException.h"

NS_IMPL_ISUPPORTS(gmail::GMailObserver, nsIObserver)

gmail::GMailObserver::GMailObserver(bool encrypt) : HttpObserver(encrypt)
{
}

gmail::GMailObserver::~GMailObserver()
{
}

NS_IMETHODIMP gmail::GMailObserver::Observe(nsISupports *aSubject, const char *aTopic, const char16_t *aData)
{
	nsresult rv;

	nsCOMPtr<nsIHttpChannel> httpChannel = do_QueryInterface(aSubject, &rv);
	if(NS_FAILED(rv))
	{
		// notify javascript
		Logger::errorMessage("GMailObserver", "Observe", "Observe FAILED: couldn't query interface to nsIHttpChannel", rv);
		return NS_OK;
	}

	nsAutoCString urlPath;
	rv = getUri(httpChannel, urlPath);
	if(NS_FAILED(rv))
	{
		// notify javascript
		Logger::errorMessage("GMailObserver", "Observe", "Observe FAILED: couldn't get uri", rv);
		return NS_OK;
	}

	if(!strcmp(aTopic, "http-on-modify-request"))
	{
		nsCOMPtr<nsIUploadChannel> uploadChannel = do_QueryInterface(httpChannel, &rv);
		if(NS_FAILED(rv))
		{
			// notify javascript
			Logger::errorMessage("GMailObserver", "Observe:http-on-modify-request", "Observe FAILED: couldn't query interface to nsIUploadChannel", rv);
			return NS_OK;
		}

		if(urlPath.Find("/mail/") != -1)
		{
			if((urlPath.Find("act=sm") != -1) || (urlPath.Find("act=sd") != -1) && doEncrypt)
			{
				std::string data = "";
				try
				{
					data = readSendingData(uploadChannel);
					if (data.length() == 0)
					{
						replaceSendingData(uploadChannel, data);
						return NS_OK;
					}
				}
				catch (...)
				{
					return NS_OK;
				}

				try
				{
					data = base::EscapeEncoding::decode(data);

					std::string url = urlPath.get();
					std::string _reqId = url.substr(urlPath.Find("&_reqid=") + 1);
					_reqId = _reqId.substr(0, _reqId.find("&"));

					std::string encryptedData;
					encryptedData = gmail::GMailUIWrapper().encryptOutgoingString(data, _reqId);

					replaceSendingData(uploadChannel, encryptedData);
					return NS_OK;
				}
				catch(exception::technical::NSException)
				{
					Logger::errorMessage("GMailObserver", "Observe:http-on-modify-request", "ns exception", nsresult(1));
					// notify javascript
				}
				catch(exception::domain::ObjectRequestException)
				{
					Logger::errorMessage("GMailObserver", "Observe:http-on-modify-request", "obj req exception", nsresult(2));
					// notify javascript
				}
				catch(exception::domain::ObjectTypeException)
				{
					Logger::errorMessage("GMailObserver", "Observe:http-on-modify-request", "obj type exception", nsresult(3));
					// notify javascript
				}
				catch(exception::domain::ServerException)
				{
					Logger::errorMessage("GMailObserver", "Observe:http-on-modify-request", "server exception", nsresult(4));
					// notify javascript
				}
				catch(exception::technical::SingletoneException)
				{
					Logger::errorMessage("GMailObserver", "Observe:http-on-modify-request", "singletone exception", nsresult(5));
					// notify javascript
				}
				catch(...)
				{
					Logger::errorMessage("GMailObserver", "Observe:http-on-modify-request", "...", nsresult(6));
					// notify javascript
				}

				replaceSendingData(uploadChannel, data);
				return NS_OK;
			}
			else if (urlPath.Find("act=fup") != -1 && doEncrypt)
			{
				std::string data = "";
				try
				{
					data = readSendingData(uploadChannel);
					if (data.length() == 0)
					{
						replaceSendingData(uploadChannel, data);
						return NS_OK;
					}
				}
				catch (...)
				{
					return NS_OK;
				}

				try
				{
					std::string url = urlPath.get();
					std::string attid = url.substr(url.find("&attid=") + 7);
					attid = attid.substr(0, attid.find("&"));

					std::string encryptedData;
					gmail::GMailAttachment gAttachment(attid);
					encryptedData = gAttachment.encryptOutgoingString(data);

					gui::GUIObserver::getInstance().notifyObserver(NITE_ENCRYPT, "google attachment");

					base::ObjectStorage::getInstance().addObject(gAttachment, base::UsersList());

					replaceSendingData(uploadChannel, encryptedData);
					return NS_OK;
				}
				catch (...)
				{
					Logger::errorMessage("GMailObserver", "Observe:http-on-modify-request", "...", nsresult(1));
					// notify javascript
				}

				replaceSendingData(uploadChannel, data);
				return NS_OK;
			}
			else if(urlPath.Find("act=dl") != -1 || urlPath.Find("act=dd") != -1)
			{
				std::string data = "";
				try
				{
					data = readSendingData(uploadChannel);
					if (data.length() == 0)
					{
						replaceSendingData(uploadChannel, data);
						return NS_OK;
					}

					gmail::GMailUIWrapper().deleteMails(data);
					replaceSendingData(uploadChannel, data);
					return NS_OK;
				}
				catch(exception::technical::NSException)
				{
					// notify javascript
				}
				catch(exception::domain::AlgorithmException)
				{
					// notify javascript
				}
				catch(...)
				{
					// notify javascript
				}
				
				replaceSendingData(uploadChannel, data);
				return NS_OK;
			}
			else
			{
				return NS_OK;
			}
		}

		return NS_OK;
	}
	else if(!strcmp(aTopic, "http-on-examine-response"))
	{
		RefPtr<gmail::GMailListener> gMailListener;

		if(urlPath.Find("mail/") != -1)
		{
			if((urlPath.Find("act=sm") != -1) || (urlPath.Find("act=sd") != -1) && doEncrypt)
			{
				std::string url = urlPath.get();
				std::string _reqId = url.substr(urlPath.Find("&_reqid=") + 1);
				_reqId = _reqId.substr(0, _reqId.find("&"));

				if(urlPath.Find("act=sm") != -1)
					gMailListener = new gmail::GMailListener(_reqId, true);
				else
					gMailListener = new gmail::GMailListener(_reqId, false);
			}
			else if(urlPath.Find("&search=") != -1)
			{
				gui::GUIObserver::getInstance().notifyObserver(NITE_DECRYPT, "google mails");
				gMailListener = new gmail::GMailListener(NITE_DecryptMail);
			}
			else if (std::string(urlPath.get()).compare("https://mail.google.com/mail/") == 0 || std::string(urlPath.get()).compare("https://mail.google.com/mail/u/0/") == 0 || urlPath.Find("auth=") != -1 || urlPath.Find("tab=") != -1)
			{
				gui::GUIObserver::getInstance().notifyObserver(NITE_DECRYPT, "google mails");
				gMailListener = new gmail::GMailListener(NITE_DecryptMailSummary);
			}
			else
			{
				if(urlPath.Find("#") == -1)
					return NS_OK;

				gui::GUIObserver::getInstance().notifyObserver(NITE_DECRYPT, "google mails");
				gMailListener = new gmail::GMailListener(NITE_DecryptMailSummary);
			}
		}
		else if (urlPath.Find("foreignService=gmailAttachment") != -1)
		{
			gMailListener = new gmail::GMailListener(NITE_AttachmentToDrive);
		}
		else
		{
			return NS_OK;
		}

		nsCOMPtr<nsIHttpChannel> httpChannel = do_QueryInterface(aSubject, &rv);
		if(NS_FAILED(rv))
		{
			// notify javascript
			Logger::errorMessage("GMailObserver", "Observe:http-on-examine-response", "Observe FAILED: couldn't query interface aSubject to nsIHttpChannel", rv);
			return NS_OK;
		}
		
		nsCOMPtr<nsITraceableChannel> traceableChannel = do_QueryInterface(aSubject, &rv);
		if(NS_FAILED(rv))
		{
			// notify javascript
			Logger::errorMessage("GMailObserver", "Observe:http-on-examine-response", "Observe FAILED: couldn't query interface to nsITraceableChannel", rv);
			return NS_OK;
		}

		try
		{
			replaceStreamListener(traceableChannel, gMailListener);
		}
		catch(exception::technical::NSException)
		{
			// notify javascript
		}
		catch(...)
		{
			// notify javascript
		}

		return NS_OK;
	}

	return NS_OK;
}
