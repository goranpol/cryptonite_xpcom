#include <mozilla/Char16.h>

#include "OneDriveListener.h"
#include "OneDriveFile.h"
#include "Logger.h"

#include "Parser.h"
#include "JsonError.h"

#include "ObjectStorage.h"
#include "ObjectRequest.h"

#include "NSException.h"
#include "SingletoneException.h"

NS_IMPL_ISUPPORTS(onedrive::OneDriveListener, nsIRequestObserver, nsIStreamListener)

onedrive::OneDriveListener::OneDriveListener(const int typ) : type(typ), fileId("")
{
}

onedrive::OneDriveListener::OneDriveListener(std::string id) : type(NITE_downloadType), fileId(id)
{
}


onedrive::OneDriveListener::~OneDriveListener()
{
}

NS_IMETHODIMP onedrive::OneDriveListener::OnStartRequest(nsIRequest *aRequest, nsISupports *aContext)
{
	nsresult rv;

	try
	{
		rv = HttpListener::OnStartRequest(aRequest, aContext);
	}
	catch (exception::technical::NSException)
	{
		// notify javascript
	}

	return NS_OK;
}

NS_IMETHODIMP onedrive::OneDriveListener::OnDataAvailable(nsIRequest *aRequest, nsISupports *aContext, nsIInputStream *aInputStream, uint64_t aOffset, uint32_t aCount)
{
	nsresult rv;

	try
	{
		rv = HttpListener::OnDataAvailable(aRequest, aContext, aInputStream, aOffset, aCount);
	}
	catch (exception::technical::NSException)
	{
		// notify javascript
	}

	return NS_OK;
}

NS_IMETHODIMP onedrive::OneDriveListener::OnStopRequest(nsIRequest *aRequest, nsISupports *aContext, nsresult aStatusCode)
{
	nsresult rv;

	mData = (char*)NS_Realloc((void*)mData, sizeof(char) * (mRead + 1));
	if (!mData)
	{
		// notify javascript
		Logger::errorMessage("GDriveListener", "OnStopRequest", "OnStopRequest FAILED: couldn't reallocate memory for mData", nsresult(-1));
		return NS_OK;
	}

	memcpy(mData + mRead, "\0", 1);

	if (type == NITE_downloadType)
	{
		std::string data = mData;
		std::string checkWord = data.substr(0, 86);

		try
		{
			onedrive::OneDriveFile oneDriveFile = static_cast<onedrive::OneDriveFile&>(base::ObjectStorage::getInstance().getObject(base::ObjectRequest(fileId, checkWord), NITE_ONEDRIVEFILE));
			std::string decryptedData = oneDriveFile.decryptIncomingString(data);

			mDecryptedRead = decryptedData.size();
			mDecryptedData = (char*)NS_Alloc(sizeof(char) * mDecryptedRead);
			memcpy(mDecryptedData, decryptedData.c_str(), mDecryptedRead);
		}
		catch (...)
		{
			mDecryptedRead = mRead;
			mDecryptedData = (char*)NS_Alloc(sizeof(char) * mRead);
			memcpy(mDecryptedData, mData, mDecryptedRead);
		}

		try
		{
			rv = HttpListener::OnStopRequest(aRequest, aContext, aStatusCode);
		}
		catch (exception::technical::NSException)
		{
			// notify javascript
		}
		catch (exception::technical::SingletoneException)
		{
			// notify javascript
		}
		catch (...)
		{
			// notify javascript
		}
	}

	return NS_OK;
}
