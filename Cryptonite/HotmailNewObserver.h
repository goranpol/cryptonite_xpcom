#ifndef _HotmailNewObserver_h_
#define _HotmailNewObserver_h_

#include "HttpObserver.h"

namespace hotmail
{
	class HotmailNewObserver : public base::HttpObserver
	{
	public:
		HotmailNewObserver(bool encrypt);

		NS_DECL_ISUPPORTS
		NS_DECL_NSIOBSERVER

	protected:

	private:
		~HotmailNewObserver();

		std::string recalculateMetaData(std::string data, size_t length);

		std::string toBase64WithPadding(const std::string input, size_t len);
		std::string fromBase64WithPadding(const std::string input);
	};
}

#endif
