#include <mozilla/Char16.h>

#include "HotmailNew.h"

#include "ObjectNotFoundException.h"
#include "NotImplementedException.h"
#include "AESException.h"
#include "AlgorithmException.h"
#include "ObjectTypeException.h"

#include "Logger.h"


hotmail::HotmailNew::HotmailNew() : base::Mail("-1", generateKey(), generateKey(), generateCheckWord(), NITE_HOTMAILNEW)
{
}

hotmail::HotmailNew::HotmailNew(const std::string objId, const std::string objDraftId) : base::Mail(objId, generateKey(), generateKey(), generateCheckWord(), NITE_HOTMAILNEW, objDraftId)
{
}

hotmail::HotmailNew::HotmailNew(const std::string objId, const std::string objKey, const std::string objInitChain, const std::string objCheckWord) : base::Mail(objId, objKey, objInitChain, objCheckWord, NITE_HOTMAILNEW)
{
}

hotmail::HotmailNew::HotmailNew(const std::string objId, const std::string objKey, const std::string objInitChain, const std::string objCheckWord, const std::string objDraftId) : base::Mail(objId, objKey, objInitChain, objCheckWord, NITE_HOTMAILNEW, objDraftId)
{
}

hotmail::HotmailNew::HotmailNew(const std::string objId, const std::string objKey, const std::string objInitChain, const std::string objCheckWord, const std::string objDraftId, std::vector<std::string> objAttachmentIds) : base::Mail(objId, objKey, objInitChain, objCheckWord, NITE_HOTMAILNEW, objDraftId, objAttachmentIds)
{
}

hotmail::HotmailNew::HotmailNew(const base::Object &obj) : base::Mail(obj.getObjectId(), obj.getObjectKey(), getInitChain(obj), getCheckWord(obj), NITE_HOTMAILNEW)
{
}

hotmail::HotmailNew::HotmailNew(const hotmail::HotmailNew &obj) : base::Mail(obj.objectId, obj.objectKey, obj.objectInitChain, obj.objectCheckWord, NITE_HOTMAILNEW, obj.objectDraftId)
{
	for (size_t i = 0; i<obj.users.size(); i++)
		users.pushBack(obj.users[i]);

	for (size_t i = 0; i<obj.attachments.size(); i++)
		attachments.push_back(obj.attachments[i]);
}

hotmail::HotmailNew::~HotmailNew()
{
}

hotmail::HotmailNew &hotmail::HotmailNew::operator=(base::Object &obj)
{
	if (obj.getObjectType() != NITE_HOTMAILNEW)
		throw exception::domain::ObjectTypeException(NITE_HOTMAILNEW, obj.getObjectType());

	hotmail::HotmailNew hotmailNew = static_cast<hotmail::HotmailNew&>(obj);

	if (this == &hotmailNew)
		return *this;

	const_cast<int&>(objectType) = NITE_HOTMAILNEW;
	const_cast<std::string&>(objectId) = hotmailNew.objectId;
	const_cast<std::string&>(objectKey) = hotmailNew.objectKey;
	const_cast<std::string&>(objectInitChain) = hotmailNew.objectInitChain;
	const_cast<std::string&>(objectCheckWord) = hotmailNew.objectCheckWord;

	return *this;
}

hotmail::HotmailNew &hotmail::HotmailNew::operator=(hotmail::HotmailNew &obj)
{
	if (this == &obj)
		return *this;

	const_cast<int&>(objectType) = NITE_HOTMAILNEW;
	const_cast<std::string&>(objectId) = obj.objectId;
	const_cast<std::string&>(objectKey) = obj.objectKey;
	const_cast<std::string&>(objectInitChain) = obj.objectInitChain;
	const_cast<std::string&>(objectCheckWord) = obj.objectCheckWord;

	return *this;
}

bool hotmail::HotmailNew::operator==(base::Object &obj)
{
	if (objectType != NITE_HOTMAILNEW)
		return false;

	hotmail::HotmailNew hotmailNew = static_cast<hotmail::HotmailNew&>(obj);

	if (objectId.compare(hotmailNew.objectId) == 0 && objectKey.compare(hotmailNew.objectKey) == 0 && objectInitChain.compare(hotmailNew.objectInitChain) == 0 && objectCheckWord.compare(hotmailNew.objectCheckWord) == 0)
		return true;

	return false;
}

bool hotmail::HotmailNew::operator!=(base::Object &obj)
{
	return !(*this == obj);
}

std::string hotmail::HotmailNew::encryptOutgoingString(std::string data)
{
	std::string returnString = data;

	try
	{
		json::JSON dataJson(data);

		bool create = false;
		if (dataJson["Body"].getString("__type").compare("CreateItemRequest:#Exchange") == 0)
			create = true;
		else if (dataJson["Body"].getString("__type").compare("UpdateItemRequest:#Exchange") == 0)
			create = false;
		else
			return returnString;

		bool saveAndSend = false;
		if (dataJson["Body"].getString("MessageDisposition").compare("SendAndSaveCopy") == 0)
			saveAndSend = true;
		else if (dataJson["Body"].getString("MessageDisposition").compare("SaveOnly") == 0)
			saveAndSend = false;
		else
			return returnString;

		if (create)
		{
			try
			{
				if (saveAndSend)
				{
					try
					{
						for (size_t j = 0; j < dataJson["Body"]["Items"][size_t(0)]["CcRecipients"].size(); j++)
						{
							users.pushBack(dataJson["Body"]["Items"][size_t(0)]["CcRecipients"][j].getString("EmailAddress"));
						}
						for (size_t j = 0; j < dataJson["Body"]["Items"][size_t(0)]["BccRecipients"].size(); j++)
						{
							users.pushBack(dataJson["Body"]["Items"][size_t(0)]["BccRecipients"][j].getString("EmailAddress"));
						}
						for (size_t j = 0; j < dataJson["Body"]["Items"][size_t(0)]["ToRecipients"].size(); j++)
						{
							users.pushBack(dataJson["Body"]["Items"][size_t(0)]["ToRecipients"][j].getString("EmailAddress"));
						}
					}
					catch (...)
					{
						Logger::logMessage("HotmailNew", "encryptOutgoingString", "get receivers catch");
					}
				}

				std::string subject = dataJson["Body"]["Items"][size_t(0)].getString("Subject");
				if (!subject.empty())
				{
					std::string encryptedSubject = "";

					if (subject.find("Re: ") == 0)
					{
						encryptedSubject = "Re: ";
						subject = subject.substr(4);
					}
					else if (subject.find("Fw: ") == 0)
					{
						encryptedSubject = "Fw: ";
						subject = subject.substr(4);
					}

					encryptedSubject += encryptSubject(subject);
					dataJson["Body"]["Items"][size_t(0)].replace("Subject", encryptedSubject);
				}


				try
				{
					std::string body = dataJson["Body"]["Items"][size_t(0)]["Body"].getString("Value");
					if (!body.empty())
					{
						std::string encryptedBody = encryptBody(body);
						dataJson["Body"]["Items"][(size_t)0]["Body"].replace("Value", encryptedBody);
					}
				}
				catch (...)
				{
					std::string body = dataJson["Body"]["Items"][size_t(0)]["NewBodyContent"].getString("Value");
					if (!body.empty())
					{
						std::string encryptedBody = encryptBody(body);
						dataJson["Body"]["Items"][size_t(0)]["NewBodyContent"].replace("Value", encryptedBody);
					}
				}

				return dataJson.print();
			}
			catch (...)
			{
				Logger::logMessage("HotmailNew", "encryptOutgoingString", "create catch");
				return returnString;
			}
		}
		else
		{
			try
			{
				for (size_t i = 0; i < dataJson["Body"]["ItemChanges"][size_t(0)]["Updates"].size(); i++)
				{
					try
					{
						std::string fieldURI = dataJson["Body"]["ItemChanges"][size_t(0)]["Updates"][i]["Path"].getString("FieldURI");

						if (saveAndSend)
						{
							if (fieldURI.compare("CcRecipients") == 0)
							{
								for (size_t j = 0; j < dataJson["Body"]["ItemChanges"][size_t(0)]["Updates"][i]["Item"]["CcRecipients"].size(); j++)
								{
									users.pushBack(dataJson["Body"]["ItemChanges"][size_t(0)]["Updates"][i]["Item"]["CcRecipients"][j].getString("EmailAddress"));
								}
							}
							else if (fieldURI.compare("BccRecipients") == 0)
							{
								for (size_t j = 0; j < dataJson["Body"]["ItemChanges"][size_t(0)]["Updates"][i]["Item"]["BccRecipients"].size(); j++)
								{
									users.pushBack(dataJson["Body"]["ItemChanges"][(size_t)0]["Updates"][i]["Item"]["BccRecipients"][j].getString("EmailAddress"));
								}
							}
							else if (fieldURI.compare("ToRecipients") == 0)
							{
								for (size_t j = 0; j < dataJson["Body"]["ItemChanges"][size_t(0)]["Updates"][i]["Item"]["ToRecipients"].size(); j++)
								{
									users.pushBack(dataJson["Body"]["ItemChanges"][size_t(0)]["Updates"][i]["Item"]["ToRecipients"][j].getString("EmailAddress"));
								}
							}
						}

						if (fieldURI.compare("Subject") == 0)
						{
							std::string subject = dataJson["Body"]["ItemChanges"][size_t(0)]["Updates"][i]["Item"].getString("Subject");
							std::string encryptedSubject = "";

							if (subject.find("Re: ") == 0)
							{
								encryptedSubject = "Re: ";
								subject = subject.substr(4);
							}
							else if (subject.find("Fw: ") == 0)
							{
								encryptedSubject = "Fw: ";
								subject = subject.substr(4);
							}

							encryptedSubject += encryptSubject(subject);
							dataJson["Body"]["ItemChanges"][size_t(0)]["Updates"][i]["Item"].replace("Subject", encryptedSubject);
						}
						else if (fieldURI.compare("Body") == 0)
						{
							std::string body = dataJson["Body"]["ItemChanges"][size_t(0)]["Updates"][i]["Item"]["Body"].getString("Value");
							if (!body.empty())
							{
								std::string encryptedBody = encryptBody(body);
								dataJson["Body"]["ItemChanges"][size_t(0)]["Updates"][i]["Item"]["Body"].replace("Value", encryptedBody);
							}
						}
					}
					catch (...)
					{
					}
				}

				return dataJson.print();
			}
			catch (...)
			{
				Logger::logMessage("HotmailNew", "encryptOutgoingString", "update catch");
				return returnString;
			}
		}
	}
	catch (...)
	{
		return returnString;
	}

	return returnString;
}

std::string hotmail::HotmailNew::decryptIncomingString(std::string data)
{
	throw std::exception();
}

void hotmail::HotmailNew::decryptFull(json::Value &jsonFull)
{
	try
	{
		std::string subject = jsonFull.getString("Subject");
		std::string decryptedSubject = decryptSubject(subject);
		jsonFull.replace("Subject", decryptedSubject);
	}
	catch (...)
	{
		Logger::errorMessage("HotmailNew", "decryptFull", "error decrypting subject", nsresult(1));
	}

	try
	{
		for (size_t i = 0; i < jsonFull["ExtendedProperty"].size(); i++)
		{
			try
			{
				if (jsonFull["ExtendedProperty"][i]["ExtendedFieldURI"].getString("PropertyTag").compare("0xe1d") == 0)
				{
					std::string sub = jsonFull["ExtendedProperty"][i].getString("Value");
					std::string decryptedSub = decryptSubject(sub);
					jsonFull["ExtendedProperty"][i].replace("Value", decryptedSub);

					break;
				}
			}
			catch (...)
			{
				Logger::errorMessage("HotmailNew", "decryptFull", "error decrypting subject in extended property", nsresult(5));
			}
		}
	}
	catch (...)
	{
		Logger::errorMessage("HotmailNew", "decryptFull", "error decrypting subject in extended property", nsresult(2));
	}

	try
	{
		std::string body = jsonFull["UniqueBody"].getString("Value");
		std::string decryptedBody = decryptBody(body);
		jsonFull["UniqueBody"].replace("Value", decryptedBody);
	}
	catch (...)
	{
		Logger::errorMessage("HotmailNew", "decryptFull", "error decrypting body", nsresult(3));
	}

	try
	{
		jsonFull.replace("Preview", "Cryptonite encrypted");
	}
	catch (...)
	{
		Logger::errorMessage("HotmailNew", "decryptFull", "error decrypting preview", nsresult(4));
	}
}

void hotmail::HotmailNew::decryptSummary(json::Value &jsonSummary)
{
	try
	{
		std::string subject = jsonSummary.getString("ConversationTopic");
		std::string decryptedSubject = decryptSubject(subject);

		jsonSummary.replace("ConversationTopic", decryptedSubject);
		jsonSummary.replace("Preview", "Cryptonite encrypted");
	}
	catch (...)
	{
		Logger::errorMessage("HotmailNew", "decryptSummary", "some error", nsresult(1));
	}
}

void hotmail::HotmailNew::decryptReply(json::Value &jsonReply)
{
	try
	{
		std::string subject = jsonReply.getString("Subject");
		std::string decryptedSubject = decryptSubject(subject);
		jsonReply.replace("Subject", decryptedSubject);
	}
	catch (...)
	{
		Logger::errorMessage("HotmailNew", "decryptReply", "error decrypting subject", nsresult(1));
	}

	try
	{
		for (size_t i = 0; i < jsonReply["ExtendedProperty"].size(); i++)
		{
			try
			{
				if (jsonReply["ExtendedProperty"][i]["ExtendedFieldURI"].getString("PropertyTag").compare("0xe1d") == 0)
				{
					std::string sub = jsonReply["ExtendedProperty"][i].getString("Value");
					std::string decryptedSub = decryptSubject(sub);
					jsonReply["ExtendedProperty"][i].replace("Value", decryptedSub);
				}
			}
			catch (...)
			{
				Logger::errorMessage("HotmailNew", "decryptSummary", "error decrypting subject in extended property", nsresult(3));
			}
		}
	}
	catch (...)
	{
		Logger::errorMessage("HotmailNew", "decryptReply", "error decrypting subject in extended property", nsresult(2));
	}

	try
	{
		std::string body = jsonReply["Body"].getString("Value");
		std::string decryptedBody = decryptBody(body);
		decryptedBody = encryptBody(decryptedBody);
		jsonReply["Body"].replace("Value", decryptedBody);
	}
	catch (...)
	{
		Logger::errorMessage("HotmailNew", "decryptReply", "error decrypting body", nsresult(3));
	}
}

void hotmail::HotmailNew::decryptDraftFull(json::Value &jsonDraftFull)
{
	try
	{
		std::string subject = jsonDraftFull.getString("Subject");
		std::string decryptedSubject = decryptSubject(subject);
		jsonDraftFull.replace("Subject", decryptedSubject);
	}
	catch (...)
	{
		Logger::errorMessage("HotmailNew", "decryptDraftFull", "error decrypting subject", nsresult(1));
	}

	try
	{
		for (size_t i = 0; i < jsonDraftFull["ExtendedProperty"].size(); i++)
		{
			try
			{
				if (jsonDraftFull["ExtendedProperty"][i]["ExtendedFieldURI"].getString("PropertyTag").compare("0xe1d") == 0)
				{
					std::string sub = jsonDraftFull["ExtendedProperty"][i].getString("Value");
					std::string decryptedSub = decryptSubject(sub);
					jsonDraftFull["ExtendedProperty"][i].replace("Value", decryptedSub);

					break;
				}
			}
			catch (...)
			{
				Logger::errorMessage("HotmailNew", "decryptDraftFull", "error decrypting subject in extended property", nsresult(3));
			}
		}
	}
	catch (...)
	{
		Logger::errorMessage("HotmailNew", "decryptDraftFull", "error decrypting subject in extended property", nsresult(2));
	}

	try
	{
		std::string body = jsonDraftFull["NormalizedBody"].getString("Value");
		std::string decryptedBody = decryptBody(body);
		jsonDraftFull["NormalizedBody"].replace("Value", decryptedBody);
	}
	catch (...)
	{
		try
		{
			std::string body = jsonDraftFull["Body"].getString("Value");
			std::string decryptedBody = decryptBody(body);
			jsonDraftFull["Body"].replace("Value", decryptedBody);
		}
		catch (...)
		{
			Logger::errorMessage("HotmailNew", "decryptDraftFull", "error decrypting body", nsresult(3));
		}
	}

	try
	{
		jsonDraftFull.replace("Preview", "Cryptonite encrypted");
	}
	catch (...)
	{
		Logger::errorMessage("HotmailNew", "decryptDraftFull", "error decrypting preview", nsresult(4));
	}
}

void hotmail::HotmailNew::decryptDraftSummary(json::Value &jsonDraftSummary)
{
	try
	{
		std::string subject = jsonDraftSummary.getString("Subject");
		std::string decryptedSubject = decryptSubject(subject);

		jsonDraftSummary.replace("Subject", decryptedSubject);
		jsonDraftSummary.replace("Preview", "Cryptonite encrypted");
	}
	catch (...)
	{
		Logger::errorMessage("HotmailNew", "decryptDraftSummary", "error decrypting subject or replaceing preview", nsresult(1));
	}
	try
	{
		for (size_t i = 0; i < jsonDraftSummary["ExtendedProperty"].size(); i++)
		{
			try
			{
				if (jsonDraftSummary["ExtendedProperty"][i]["ExtendedFieldURI"].getString("PropertyTag").compare("0xe1d") == 0)
				{
					std::string sub = jsonDraftSummary["ExtendedProperty"][i].getString("Value");
					std::string decryptedSub = decryptSubject(sub);
					jsonDraftSummary["ExtendedProperty"][i].replace("Value", decryptedSub);

					break;
				}
			}
			catch (...)
			{
				Logger::errorMessage("HotmailNew", "decryptDraftSummary", "error within extended properties", nsresult(1));
			}
		}
	}
	catch (...)
	{
		Logger::errorMessage("HotmailNew", "decryptDraftSummary", "error decrypting subject in extended property", nsresult(2));
	}
}

std::string hotmail::HotmailNew::decryptSubject(std::string subject)
{
	std::string decryptedSubject = Mail::decryptSubject(subject);
	decryptedSubject = replace(decryptedSubject, "\\", "");
	decryptedSubject = replace(decryptedSubject, "\"", "\\\"");

	return decryptedSubject;
}

std::string hotmail::HotmailNew::decryptBody(std::string body)
{
	std::string returnString = "";
	std::vector<std::string> splitted = split(body, "*");

	if (splitted.size() <= 2)
		return body;

	returnString = splitted[0];

	for (size_t i = 1; i<splitted.size() - 1; i++)
	{
		if (isBase64(splitted[i]) && splitted[i].size() != 0)
		{
			try
			{
				if (splitted[i].compare(objectCheckWord) == 0)
					continue;
				
				returnString += decryptText(splitted[i]);
			}
			catch (...)
			{
			}
		}
		else
		{
			returnString += splitted[i];
		}
	}

	returnString += splitted[splitted.size() - 1];

	// remove the new line signs
	returnString = replace(returnString, "\\u000d", "");
	returnString = replace(returnString, "\\u000a", "");
	returnString = replace(returnString, "\\u0009", "");

	// remove slashes (\\) and replace " with \"
	returnString = replace(returnString, "\\", "");
	returnString = replace(returnString, "\"", "\\\"");

	// change \n and \t for ""
	returnString = replace(returnString, "\n", "");
	returnString = replace(returnString, "\t", "");

	return returnString;
}
