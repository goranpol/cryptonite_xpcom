#ifndef _Number_h_
#define _Number_h_

#define NITE_INTEGER 9
#define NITE_DOUBLE	10

#include "Value.h"

namespace json
{
	class Number : public Value
	{
	public:
		Number(const int64_t value);
		Number(const int value);
		Number(const double value);
		Number(const float value);
		Number(const Number &val);
		virtual ~Number();

		Number &operator=(const Value &val);
		Number &operator=(const Number &val);
		Number &operator=(const int64_t val);
		Number &operator=(const int val);
		Number &operator=(const float val);
		Number &operator=(const double val);

		bool operator==(const Value &val);
		bool operator==(const Number &val);
		bool operator==(const int64_t val);
		bool operator==(const int val);
		bool operator==(const float val);
		bool operator==(const double val);

		bool operator!=(const Value &val);
		bool operator!=(const Number &val);
		bool operator!=(const int64_t val);
		bool operator!=(const int val);
		bool operator!=(const float val);
		bool operator!=(const double val);

		/* print */
		std::string print();

		int64_t getIntValue() const;
		double getDoubleValue() const;

		friend class Enumeration;

	protected:

	private:
		/********************************************************************************************/
		/************************************Number members******************************************/
		/********************************************************************************************/
		const int numberType;						/* the type of number (0 - int, 1 - double) */
		const int64_t valueInt;						/* the value of number json value if it is an integer */
		const double valueDouble;					/* the value of number json value if it is a double */
		/********************************************************************************************/
		/************************************Number members******************************************/
		/********************************************************************************************/
	};
}

#endif
