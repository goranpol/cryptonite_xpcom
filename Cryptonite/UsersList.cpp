#include <mozilla/Char16.h>

#include "UsersList.h"
#include "Parser.h"

#include "UserNotFoundException.h"

base::UsersList::UsersList() : length(0)
{
}

base::UsersList::~UsersList()
{
}

base::UsersList &base::UsersList::operator=(const base::UsersList &list)
{
	length = list.length;
	users = list.users;

	return *this;
}

std::string base::UsersList::operator[](const size_t index) const
{
	if (length == 0)
		throw exception::domain::UserNotFoundException();
	if(index > length-1)
		throw exception::domain::UserNotFoundException();

	return users[index];
}

std::string base::UsersList::get(const size_t index)
{
	return (*this)[index];
}

void base::UsersList::replace(const size_t index, const std::string user)
{
	if (length == 0)
		throw exception::domain::UserNotFoundException();
	if(index > length-1)
		throw exception::domain::UserNotFoundException();

	users.erase(users.begin() + index);
	users.insert(users.begin() + index, user);
}

void base::UsersList::insert(const size_t index, const std::string user)
{
	if(index > length-1)
		throw exception::domain::UserNotFoundException();

	users.insert(users.begin() + index, user);
	length++;
}

void base::UsersList::pushBack(const std::string user)
{
	users.push_back(user);
	length++;
}

void base::UsersList::remove(const size_t index)
{
	if (length == 0)
		throw exception::domain::UserNotFoundException();
	if(index > length-1)
		throw exception::domain::UserNotFoundException();

	users.erase(users.begin() + index);
	length--;
}

void base::UsersList::remove(const std::string user)
{
	if (length == 0)
		throw exception::domain::UserNotFoundException();

	for(size_t i=0; i<length; i++)
	{
		if(users[i].compare(user) == 0)
		{
			users.erase(users.begin() + i);
			length--;
			return;
		}
	}
	
	throw exception::domain::UserNotFoundException();
}

bool base::UsersList::empty() const
{
	if (length != 0)
		return false;

	return users.empty();
}

void base::UsersList::clear()
{
	users.clear();
	length = 0;
}

size_t base::UsersList::size() const
{
	return length;
}

json::Value &base::UsersList::toJson()
{
	json::JSON jsonArray = json::JSON(NITE_ArrayType);

	for (size_t i = 0; i<length; i++)
		jsonArray.pushBack(users[i]);

	return jsonArray.get();
}