#ifndef _Enumeration_h_
#define _Enumeration_h_

#define NITE_ArrayType 6
#define ObjectType 7
#define NITE_JavascriptType 8

#include "Value.h"
#include "String.h"
#include "Bool.h"
#include "Number.h"
#include <memory>

namespace json
{
	class Enumeration : public Value
	{
	public:
		Enumeration(const Enumeration &val);
		Enumeration(const int enumType, const std::string nm = "");
		virtual ~Enumeration();

		virtual Enumeration &operator=(const Enumeration &val);
		virtual Value &operator=(const Value &val);

		bool operator==(const Value &val);
		bool operator==(const Enumeration &val);
		bool operator!=(const Value &val);
		bool operator!=(const Enumeration &val);

		/* access operators */
		Value &operator[](const size_t index);
		Value &operator[](const std::string key);
		Value &operator[](const char* key);

		int getType() const;

		/* gets if array */
		Value &get(const size_t index);
		std::string getString(const size_t index);
		bool getBool(const size_t index);
		int64_t getInt64(const size_t index);
		int getInt(const size_t index);
		double getDouble(const size_t index);

		/* gets if object */
		Value &get(const std::string key);
		std::string getString(const std::string key);
		bool getBool(const std::string key);
		int64_t getInt64(const std::string key);
		int getInt(const std::string key);
		double getDouble(const std::string key);

		/* replaces if array */
		void replace(const size_t index, const Value &val);
		void replace(const size_t index, const std::string val);
		void replace(const size_t index, const char* val);
		void replace(const size_t index, const bool val);
		void replace(const size_t index, const int64_t val);
		void replace(const size_t index, const int val);
		void replace(const size_t index, const float val);
		void replace(const size_t index, const double val);
		void replace(const size_t index);
		void replaceArray(const size_t index);
		void replaceObject(const size_t index);
		void replaceJavascript(const size_t index, const std::string name = "");

		/* replaces if object */
		void replace(const std::string key, const Value &val);
		void replace(const std::string key, const std::string val);
		void replace(const std::string key, const char* val);
		void replace(const std::string key, const bool val);
		void replace(const std::string key, const int64_t val);
		void replace(const std::string key, const int val);
		void replace(const std::string key, const float val);
		void replace(const std::string key, const double val);
		void replace(const std::string key);
		void replaceArray(const std::string key);
		void replaceObject(const std::string key);
		void replaceJavascript(const std::string key, const std::string name = "");

		/* inserts if array */
		void insert(const size_t index, const Value &val);
		void insert(const size_t index, const std::string val);
		void insert(const size_t index, const char* val);
		void insert(const size_t index, const bool val);
		void insert(const size_t index, const int64_t val);
		void insert(const size_t index, const int val);
		void insert(const size_t index, const float val);
		void insert(const size_t index, const double val);
		void insert(const size_t index);
		void insertArray(const size_t index);
		void insertObject(const size_t index);
		void insertJavascript(const size_t index, const std::string name = "");

		/* inserts if object */
		void insert(const std::string key, const Value &val, const size_t index);
		void insert(const std::string key, const std::string val, const size_t index);
		void insert(const std::string key, const char* val, const size_t index);
		void insert(const std::string key, const bool val, const size_t index);
		void insert(const std::string key, const int64_t val, const size_t index);
		void insert(const std::string key, const int val, const size_t index);
		void insert(const std::string key, const float val, const size_t index);
		void insert(const std::string key, const double val, const size_t index);
		void insert(const std::string key, const size_t index);
		void insertArray(const std::string key, const size_t index);
		void insertObject(const std::string key, const size_t index);
		void insertJavascript(const std::string key, const size_t index, const std::string name = "");

		/* pushBacks if array */
		void pushBack(const Value &val);
		void pushBack(const bool val);
		void pushBack(const int64_t val);
		void pushBack(const int val);
		void pushBack(const float val);
		void pushBack(const double val);
		void pushBack();
		void pushBackArray();
		void pushBackObject();

		/* pushBack for array (String, char*) or object (key and null) */
		void pushBack(const std::string val);
		void pushBack(const char* val);
		void pushBackJavascript(const std::string key, const std::string name = "");

		/* pushBacks if object */
		/**** put it for NITE_JavascriptType and NITE_ArrayType too ***/
		/* just push back another enumeration of ObjectType */
		/******* and put this key value pair in there *******/
		void pushBack(const std::string key, const Value &val);
		void pushBack(const std::string key, const std::string val);
		void pushBack(const std::string key, const char* val);
		void pushBack(const std::string key, const bool val);
		void pushBack(const std::string key, const int64_t val);
		void pushBack(const std::string key, const int val);
		void pushBack(const std::string key, const float val);
		void pushBack(const std::string key, const double val);
		void pushBackArray(const std::string key);
		void pushBackObject(const std::string key);

		/* removes for array and object */
		void remove(const size_t index);
		void remove(const std::string key);

		/* empty & clear */
		bool empty();
		void clear();

		/* check name if javascript function */
		bool checkName(const std::string nm);
		bool checkName(const char* nm);

		size_t size();
		std::string print();

		friend class JSON;

	protected:

	private:
		/* parse values privates */
		Value *parse(std::string &jsonStr);
		Value *parseArray(std::string &jsonStr);
		Value *parseObject(std::string &jsonStr);
		Value *parseString(std::string &jsonStr);
		Value *parseNumber(std::string &jsonStr);
		Value *parseJavascript(std::string &jsonStr);

		/*************************************************************************************************/
		/************************************Enumeration members******************************************/
		/*************************************************************************************************/
		typedef std::shared_ptr<json::Value> valuePtr;
		typedef std::shared_ptr<json::String> stringPtr;
		typedef std::shared_ptr<json::Bool> boolPtr;
		typedef std::shared_ptr<json::Number> numberPtr;
		typedef std::shared_ptr<json::Enumeration> enumerationPtr;

		const int enumerationType;										/* Array - 6, Object - 7, JavascriptObject - 8 */
		std::vector<valuePtr> arrayMembers;								/* vector of pointers to values for Array type */
		std::vector<std::pair<std::string, valuePtr>> objectMembers;	/* vector of pairs (key, pointer to object) for Object type */
		std::string name;												/* for javacript call (new HM.FppReturnPackage for example) */
		/*************************************************************************************************/
		/************************************Enumeration members******************************************/
		/*************************************************************************************************/
	};
}

#endif
