#include <mozilla/Char16.h>

#include "Enumeration.h"
#include "String.h"
#include "Bool.h"
#include "Number.h"
#include "Value.h"

#include "Logger.h"
#include <sstream>
#include <limits.h>

#include "JsonTypeError.h"
#include "JsonTypeNotDefined.h"
#include "JsonBadCast.h"
#include "JsonBadAlloc.h"
#include "JsonKeyNotFound.h"
#include "JsonOutOfRange.h"
#include "JsonParsingError.h"

/* this one is private */
json::Enumeration::Enumeration(const int enumType, const std::string nm) : Value(JsonEnumeration), enumerationType(enumType), name(nm)
{
	if(enumType != ObjectType && enumType != NITE_ArrayType && enumType != NITE_JavascriptType)
	{
		throw json::JsonTypeError(enumerationType, enumType);
	}
	else if(name.empty())
		name = "new noname";
}

json::Enumeration::Enumeration(const json::Enumeration &val) : Value(val.type), enumerationType(val.enumerationType), arrayMembers(val.arrayMembers), objectMembers(val.objectMembers), name(val.name)
{
}

json::Enumeration::~Enumeration()
{
	objectMembers.clear();
	arrayMembers.clear();
}

json::Value &json::Enumeration::operator=(const json::Value &val)
{
	if (val.type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, val.type);

	if (this == &val)
		return *this;

	const json::Enumeration enumeration = static_cast<const json::Enumeration&>(val);

	const_cast<int&>(this->type) = enumeration.type;
	const_cast<int&>(this->enumerationType) = enumeration.enumerationType;
	this->name = enumeration.name;
	this->arrayMembers = enumeration.arrayMembers;
	this->objectMembers = enumeration.objectMembers;

	return *this;
}

json::Enumeration &json::Enumeration::operator=(const json::Enumeration &val)
{
	if (val.type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, val.type);

	if (this == &val)
		return *this;

	const_cast<int&>(this->type) = JsonEnumeration;
	const_cast<int&>(this->enumerationType) = val.enumerationType;
	this->name = val.name;
	this->objectMembers = val.objectMembers;
	this->arrayMembers = val.arrayMembers;

	return *this;
}

bool json::Enumeration::operator==(const json::Value &val)
{
	if(type != JsonEnumeration || val.getType() != JsonEnumeration)
		return false;

	json::Enumeration enumVal = static_cast<json::Enumeration&>(const_cast<json::Value&>(val));
	
	if(enumerationType != enumVal.enumerationType)
		return false;

	if(enumerationType == ObjectType)
	{
		for (size_t i = 0; i<objectMembers.size(); i++)
		{
			if(objectMembers[i].first.compare(enumVal.objectMembers[i].first) != 0)
				return false;

			if(objectMembers[i].second->getType() != enumVal.objectMembers[i].second->getType())
				return false;

			if(objectMembers[i].second->getType() == JsonString)
			{
				stringPtr strThis = std::static_pointer_cast<String>(objectMembers[i].second);
				stringPtr strEnumVal = std::static_pointer_cast<String>(enumVal.objectMembers[i].second);

				if(*strThis != *strEnumVal)
					return false;
			}
			else if(objectMembers[i].second->getType() == JsonNull)
			{
				if(objectMembers[i].second->getType() != enumVal.objectMembers[i].second->getType())
					return false;
			}
			else if(objectMembers[i].second->getType() == JsonBoolean)
			{
				boolPtr boolThis = std::static_pointer_cast<Bool>(objectMembers[i].second);
				boolPtr boolEnumVal = std::static_pointer_cast<Bool>(enumVal.objectMembers[i].second);

				if(*boolThis != *boolEnumVal)
					return false;
			}
			else if(objectMembers[i].second->getType() == JsonNumber)
			{
				numberPtr numThis = std::static_pointer_cast<Number>(objectMembers[i].second);
				numberPtr numEnumVal = std::static_pointer_cast<Number>(enumVal.objectMembers[i].second);

				if(*numThis != *numEnumVal)
					return false;
			}
			else if(objectMembers[i].second->getType() == JsonEnumeration)
			{
				enumerationPtr enumThis = std::static_pointer_cast<Enumeration>(objectMembers[i].second);
				enumerationPtr enumEnumVal = std::static_pointer_cast<Enumeration>(enumVal.objectMembers[i].second);

				if(*enumThis != *enumEnumVal)
					return false;
			}
			else
				return false;
		}
	}
	else if(enumerationType == NITE_ArrayType)
	{
		for (size_t i = 0; i<arrayMembers.size(); i++)
		{
			if(arrayMembers[i]->getType() != enumVal.arrayMembers[i]->getType())
				return false;

			if(arrayMembers[i]->getType() == JsonString)
			{
				stringPtr strThis = std::static_pointer_cast<String>(arrayMembers[i]);
				stringPtr strEnumVal = std::static_pointer_cast<String>(enumVal.arrayMembers[i]);

				if(*strThis != *strEnumVal)
					return false;
			}
			else if(arrayMembers[i]->getType() == JsonEmpty || arrayMembers[i]->getType() == JsonNull)
			{
				if(arrayMembers[i]->getType() != enumVal.arrayMembers[i]->getType())
					return false;
			}
			else if(arrayMembers[i]->getType() == JsonBoolean)
			{
				boolPtr boolThis = std::static_pointer_cast<Bool>(arrayMembers[i]);
				boolPtr boolEnumVal = std::static_pointer_cast<Bool>(enumVal.arrayMembers[i]);

				if(*boolThis != *boolEnumVal)
					return false;
			}
			else if(arrayMembers[i]->getType() == JsonNumber)
			{
				numberPtr numThis = std::static_pointer_cast<Number>(arrayMembers[i]);
				numberPtr numEnumVal = std::static_pointer_cast<Number>(enumVal.arrayMembers[i]);

				if(*numThis != *numEnumVal)
					return false;
			}
			else if(arrayMembers[i]->getType() == JsonEnumeration)
			{
				enumerationPtr enumThis = std::static_pointer_cast<Enumeration>(arrayMembers[i]);
				enumerationPtr enumEnumVal = std::static_pointer_cast<Enumeration>(enumVal.arrayMembers[i]);

				if(*enumThis != *enumEnumVal)
					return false;
			}
			else
				return false;
		}
	}
	else if(enumerationType == NITE_JavascriptType)
	{
		for (size_t i = 0; i<arrayMembers.size(); i++)
		{
			if(arrayMembers[i]->getType() != enumVal.arrayMembers[i]->getType())
				return false;

			if(arrayMembers[i]->getType() == JsonString)
			{
				stringPtr strThis = std::static_pointer_cast<String>(arrayMembers[i]);
				stringPtr strEnumVal = std::static_pointer_cast<String>(enumVal.arrayMembers[i]);

				if(*strThis != *strEnumVal)
					return false;
			}
			else if(arrayMembers[i]->getType() == JsonEmpty || arrayMembers[i]->getType() == JsonNull)
			{
				if(arrayMembers[i]->getType() != enumVal.arrayMembers[i]->getType())
					return false;
			}
			else if(arrayMembers[i]->getType() == JsonBoolean)
			{
				boolPtr boolThis = std::static_pointer_cast<Bool>(arrayMembers[i]);
				boolPtr boolEnumVal = std::static_pointer_cast<Bool>(enumVal.arrayMembers[i]);

				if(*boolThis != *boolEnumVal)
					return false;
			}
			else if(arrayMembers[i]->getType() == JsonNumber)
			{
				numberPtr numThis = std::static_pointer_cast<Number>(arrayMembers[i]);
				numberPtr numEnumVal = std::static_pointer_cast<Number>(enumVal.arrayMembers[i]);

				if(*numThis != *numEnumVal)
					return false;
			}
			else if(arrayMembers[i]->getType() == JsonEnumeration)
			{
				enumerationPtr enumThis = std::static_pointer_cast<Enumeration>(arrayMembers[i]);
				enumerationPtr enumEnumVal = std::static_pointer_cast<Enumeration>(enumVal.arrayMembers[i]);

				if(*enumThis != *enumEnumVal)
					return false;
			}
			else
				return false;
		}
	}
	else
		return false;

	return true;
}

bool json::Enumeration::operator==(const json::Enumeration &val)
{
	if(type != JsonEnumeration || val.getType() != JsonEnumeration)
		return false;

	if(enumerationType != val.enumerationType)
		return false;

	if(enumerationType == ObjectType)
	{
		for (size_t i = 0; i<objectMembers.size(); i++)
		{
			if(objectMembers[i].first.compare(val.objectMembers[i].first) != 0)
				return false;

			if(objectMembers[i].second->getType() != val.objectMembers[i].second->getType())
				return false;

			if(objectMembers[i].second->getType() == JsonString)
			{
				stringPtr strThis = std::static_pointer_cast<String>(objectMembers[i].second);
				stringPtr strEnumVal = std::static_pointer_cast<String>(val.objectMembers[i].second);

				if(*strThis != *strEnumVal)
					return false;
			}
			else if(objectMembers[i].second->getType() == JsonNull)
			{
				if(objectMembers[i].second->getType() != val.objectMembers[i].second->getType())
					return false;
			}
			else if(objectMembers[i].second->getType() == JsonBoolean)
			{
				boolPtr boolThis = std::static_pointer_cast<Bool>(objectMembers[i].second);
				boolPtr boolEnumVal = std::static_pointer_cast<Bool>(val.objectMembers[i].second);

				if(*boolThis != *boolEnumVal)
					return false;
			}
			else if(objectMembers[i].second->getType() == JsonNumber)
			{
				numberPtr numThis = std::static_pointer_cast<Number>(objectMembers[i].second);
				numberPtr numEnumVal = std::static_pointer_cast<Number>(val.objectMembers[i].second);

				if(*numThis != *numEnumVal)
					return false;
			}
			else if(objectMembers[i].second->getType() == JsonEnumeration)
			{
				enumerationPtr enumThis = std::static_pointer_cast<Enumeration>(objectMembers[i].second);
				enumerationPtr enumEnumVal = std::static_pointer_cast<Enumeration>(val.objectMembers[i].second);

				if(*enumThis != *enumEnumVal)
					return false;
			}
			else
				return false;
		}
	}
	else if(enumerationType == NITE_ArrayType)
	{
		for (size_t i = 0; i<arrayMembers.size(); i++)
		{
			if(arrayMembers[i]->getType() != val.arrayMembers[i]->getType())
				return false;

			if(arrayMembers[i]->getType() == JsonString)
			{
				stringPtr strThis = std::static_pointer_cast<String>(arrayMembers[i]);
				stringPtr strEnumVal = std::static_pointer_cast<String>(val.arrayMembers[i]);

				if(*strThis != *strEnumVal)
					return false;
			}
			else if(arrayMembers[i]->getType() == JsonEmpty || arrayMembers[i]->getType() == JsonNull)
			{
				if(arrayMembers[i]->getType() != val.arrayMembers[i]->getType())
					return false;
			}
			else if(arrayMembers[i]->getType() == JsonBoolean)
			{
				boolPtr boolThis = std::static_pointer_cast<Bool>(arrayMembers[i]);
				boolPtr boolEnumVal = std::static_pointer_cast<Bool>(val.arrayMembers[i]);

				if(*boolThis != *boolEnumVal)
					return false;
			}
			else if(arrayMembers[i]->getType() == JsonNumber)
			{
				numberPtr numThis = std::static_pointer_cast<Number>(arrayMembers[i]);
				numberPtr numEnumVal = std::static_pointer_cast<Number>(val.arrayMembers[i]);

				if(*numThis != *numEnumVal)
					return false;
			}
			else if(arrayMembers[i]->getType() == JsonEnumeration)
			{
				enumerationPtr enumThis = std::static_pointer_cast<Enumeration>(arrayMembers[i]);
				enumerationPtr enumEnumVal = std::static_pointer_cast<Enumeration>(val.arrayMembers[i]);

				if(*enumThis != *enumEnumVal)
					return false;
			}
			else
				return false;
		}
	}
	else if(enumerationType == NITE_JavascriptType)
	{
		for (size_t i = 0; i<arrayMembers.size(); i++)
		{
			if(arrayMembers[i]->getType() != val.arrayMembers[i]->getType())
				return false;

			if(arrayMembers[i]->getType() == JsonString)
			{
				stringPtr strThis = std::static_pointer_cast<String>(arrayMembers[i]);
				stringPtr strEnumVal = std::static_pointer_cast<String>(val.arrayMembers[i]);

				if(*strThis != *strEnumVal)
					return false;
			}
			else if(arrayMembers[i]->getType() == JsonEmpty || arrayMembers[i]->getType() == JsonNull)
			{
				if(arrayMembers[i]->getType() != val.arrayMembers[i]->getType())
					return false;
			}
			else if(arrayMembers[i]->getType() == JsonBoolean)
			{
				boolPtr boolThis = std::static_pointer_cast<Bool>(arrayMembers[i]);
				boolPtr boolEnumVal = std::static_pointer_cast<Bool>(val.arrayMembers[i]);

				if(*boolThis != *boolEnumVal)
					return false;
			}
			else if(arrayMembers[i]->getType() == JsonNumber)
			{
				numberPtr numThis = std::static_pointer_cast<Number>(arrayMembers[i]);
				numberPtr numEnumVal = std::static_pointer_cast<Number>(val.arrayMembers[i]);

				if(*numThis != *numEnumVal)
					return false;
			}
			else if(arrayMembers[i]->getType() == JsonEnumeration)
			{
				enumerationPtr enumThis = std::static_pointer_cast<Enumeration>(arrayMembers[i]);
				enumerationPtr enumEnumVal = std::static_pointer_cast<Enumeration>(val.arrayMembers[i]);

				if(*enumThis != *enumEnumVal)
					return false;
			}
			else
				return false;
		}
	}
	else
		return false;

	return true;
}

bool json::Enumeration::operator!=(const json::Value &val)
{
	return !(*this == val);
}

bool json::Enumeration::operator!=(const json::Enumeration &val)
{
	return !(*this == val);
}

/* access operators */
json::Value &json::Enumeration::operator[](const size_t index)
{
	size_t length = 0;
	if (enumerationType == NITE_ArrayType || enumerationType == NITE_JavascriptType)
		length = arrayMembers.size();
	else if (enumerationType == ObjectType)
		length = objectMembers.size();
	else
		throw json::JsonTypeNotDefined(10);

	if (length == 0)
		throw json::JsonOutOfRange(index, 0);
	if(index > length-1)
		throw json::JsonOutOfRange(index, length);

	if(enumerationType != NITE_ArrayType && enumerationType != NITE_JavascriptType)
		return *objectMembers[index].second;

	return *arrayMembers[index];
}

json::Value &json::Enumeration::operator[](const std::string key)
{
	size_t length = objectMembers.size();
	if(enumerationType != ObjectType)
		throw json::JsonTypeError(ObjectType, enumerationType);
	if (length == 0)
		throw json::JsonKeyNotFound(key);

	for (size_t i = 0; i<length; i++)
	{
		if(objectMembers[i].first.compare(key) == 0)
			return *objectMembers[i].second;
	}

	throw json::JsonKeyNotFound(key);
}

json::Value &json::Enumeration::operator[](const char* key)
{
	size_t length = objectMembers.size();
	if (enumerationType != ObjectType)
		throw json::JsonTypeError(ObjectType, enumerationType);
	if (length == 0)
		throw json::JsonKeyNotFound(std::string(key));

	for (size_t i = 0; i<length; i++)
	{
		if (objectMembers[i].first.compare(std::string(key)) == 0)
			return *objectMembers[i].second;
	}

	throw json::JsonKeyNotFound(std::string(key));
}

int json::Enumeration::getType() const
{
	return enumerationType;
}

/* gets if array */
json::Value &json::Enumeration::get(const size_t index)
{
	return (*this)[index];
}

std::string json::Enumeration::getString(const size_t index)
{
	size_t length = arrayMembers.size();
	if(enumerationType != NITE_ArrayType && enumerationType != NITE_JavascriptType)
		throw json::JsonTypeError(NITE_ArrayType, enumerationType);
	if (length == 0)
		throw json::JsonOutOfRange(index, 0);
	if(index > length-1)
		throw json::JsonOutOfRange(index, length);

	if(arrayMembers[index]->type != JsonString)
		throw json::JsonTypeError(JsonString, arrayMembers[index]->type);

	stringPtr str = std::static_pointer_cast<String>(arrayMembers[index]);

	return str->stringValue;
}

bool json::Enumeration::getBool(const size_t index)
{
	size_t length = arrayMembers.size();
	if(enumerationType != NITE_ArrayType && enumerationType != NITE_JavascriptType)
		throw json::JsonTypeError(NITE_ArrayType, enumerationType);
	if (length == size_t(0))
		throw json::JsonOutOfRange(index, 0);
	if(index > length-1)
		throw json::JsonOutOfRange(index, length);

	if(arrayMembers[index]->type != JsonBoolean)
		throw json::JsonTypeError(JsonBoolean, arrayMembers[index]->type);

	boolPtr boolean = std::static_pointer_cast<Bool>(arrayMembers[index]);

	return boolean->boolValue;
}

int64_t json::Enumeration::getInt64(const size_t index)
{
	size_t length = arrayMembers.size();
	if(enumerationType != NITE_ArrayType && enumerationType != NITE_JavascriptType)
		throw json::JsonTypeError(NITE_ArrayType, enumerationType);
	if (length == 0)
		throw json::JsonOutOfRange(index, 0);
	if(index > length-1)
		throw json::JsonOutOfRange(index, length);

	if(arrayMembers[index]->type != JsonNumber)
		throw json::JsonTypeError(JsonNumber, arrayMembers[index]->type);

	numberPtr num = std::static_pointer_cast<Number>(arrayMembers[index]);

	if(num->numberType != NITE_INTEGER)
		throw json::JsonTypeError(NITE_INTEGER, num->numberType);

	return num->valueInt;
}

int json::Enumeration::getInt(const size_t index)
{
	size_t length = arrayMembers.size();
	if(enumerationType != NITE_ArrayType && enumerationType != NITE_JavascriptType)
		throw json::JsonTypeError(NITE_ArrayType, enumerationType);
	if (length == 0)
		throw json::JsonOutOfRange(index, 0);
	if(index > length-1)
		throw json::JsonOutOfRange(index, length);

	if(arrayMembers[index]->type != JsonNumber)
		throw json::JsonTypeError(JsonNumber, arrayMembers[index]->type);

	numberPtr num = std::static_pointer_cast<Number>(arrayMembers[index]);

	if(num->numberType != NITE_INTEGER)
		throw json::JsonTypeError(NITE_INTEGER, num->numberType);

	return (int)num->valueInt;
}

double json::Enumeration::getDouble(const size_t index)
{
	size_t length = arrayMembers.size();
	if(enumerationType != NITE_ArrayType && enumerationType != NITE_JavascriptType)
		throw json::JsonTypeError(NITE_ArrayType, enumerationType);
	if (length == 0)
		throw json::JsonOutOfRange(index, 0);
	if(index > length-1)
		throw json::JsonOutOfRange(index, length);

	if(arrayMembers[index]->type != JsonNumber)
		throw json::JsonTypeError(JsonNumber, arrayMembers[index]->type);

	numberPtr num = std::static_pointer_cast<Number>(arrayMembers[index]);

	if(num->numberType != NITE_DOUBLE)
		throw json::JsonTypeError(NITE_DOUBLE, num->numberType);

	return num->valueDouble;
}

/* gets if object */
json::Value &json::Enumeration::get(const std::string key)
{
	return (*this)[key];
}

std::string json::Enumeration::getString(const std::string key)
{
	size_t length = objectMembers.size();
	if(enumerationType != ObjectType)
		throw json::JsonTypeError(ObjectType, enumerationType);
	if (length == 0)
		throw json::JsonKeyNotFound(key);

	for (size_t i = 0; i<length; i++)
	{
		if(objectMembers[i].first.compare(key) == 0)
		{
			if(objectMembers[i].second->type != JsonString)
				throw json::JsonTypeError(JsonString, objectMembers[i].second->type);

			stringPtr str = std::static_pointer_cast<String>(objectMembers[i].second);
			
			return str->stringValue;
		}
	}

	throw json::JsonKeyNotFound(key);
}

bool json::Enumeration::getBool(const std::string key)
{
	size_t length = objectMembers.size();
	if(enumerationType != ObjectType)
		throw json::JsonTypeError(ObjectType, enumerationType);
	if (length == 0)
		throw json::JsonKeyNotFound(key);

	for (size_t i = 0; i<length; i++)
	{
		if(objectMembers[i].first.compare(key) == 0)
		{
			if(objectMembers[i].second->type != JsonBoolean)
				throw json::JsonTypeError(JsonBoolean, objectMembers[i].second->type);

			boolPtr boolean = std::static_pointer_cast<Bool>(objectMembers[i].second);
			
			return boolean->boolValue;
		}
	}

	throw json::JsonKeyNotFound(key);
}

int64_t json::Enumeration::getInt64(const std::string key)
{
	size_t length = objectMembers.size();
	if(enumerationType != ObjectType)
		throw json::JsonTypeError(ObjectType, enumerationType);
	if (length == 0)
		throw json::JsonKeyNotFound(key);

	for (size_t i = 0; i<length; i++)
	{
		if(objectMembers[i].first.compare(key) == 0)
		{
			if(objectMembers[i].second->type != JsonNumber)
				throw json::JsonTypeError(JsonNumber, objectMembers[i].second->type);

			numberPtr num = std::static_pointer_cast<Number>(objectMembers[i].second);
			
			if(num->numberType != NITE_INTEGER)
				throw json::JsonTypeError(NITE_INTEGER, num->numberType);
			
			return num->valueInt;
		}
	}

	throw json::JsonKeyNotFound(key);
}

int json::Enumeration::getInt(const std::string key)
{
	size_t length = objectMembers.size();
	if(enumerationType != ObjectType)
		throw json::JsonTypeError(ObjectType, enumerationType);
	if (length == 0)
		throw json::JsonKeyNotFound(key);

	for (size_t i = 0; i<length; i++)
	{
		if(objectMembers[i].first.compare(key) == 0)
		{
			if(objectMembers[i].second->type != JsonNumber)
				throw json::JsonTypeError(JsonNumber, objectMembers[i].second->type);

			numberPtr num = std::static_pointer_cast<Number>(objectMembers[i].second);

			if(num->numberType != NITE_INTEGER)
				throw json::JsonTypeError(NITE_INTEGER, num->numberType);
			
			return (int)num->valueInt;
		}
	}

	throw json::JsonKeyNotFound(key);
}

double json::Enumeration::getDouble(const std::string key)
{
	size_t length = objectMembers.size();
	if(enumerationType != ObjectType)
		throw json::JsonTypeError(ObjectType, enumerationType);
	if (length == 0)
		throw json::JsonKeyNotFound(key);

	for (size_t i = 0; i<length; i++)
	{
		if(objectMembers[i].first.compare(key) == 0)
		{
			if(objectMembers[i].second->type != JsonNumber)
				throw json::JsonTypeError(JsonNumber, objectMembers[i].second->type);

			numberPtr num = std::static_pointer_cast<Number>(objectMembers[i].second);
			
			if(num->numberType != NITE_DOUBLE)
				throw json::JsonTypeError(NITE_DOUBLE, num->numberType);
			
			return num->valueDouble;
		}
	}

	throw json::JsonKeyNotFound(key);
}

/* replaces if array */
void json::Enumeration::replace(const size_t index, const json::Value &val)
{
	size_t length = arrayMembers.size();
	if (length == 0)
		throw json::JsonOutOfRange(index, 0);
	if(index > length-1)
		throw json::JsonOutOfRange(index, length);
	if(enumerationType != NITE_ArrayType && enumerationType != NITE_JavascriptType)
		throw json::JsonTypeError(NITE_ArrayType, enumerationType);

	if(val.type == JsonString)
	{
		String strVal = static_cast<String&>(const_cast<Value&>(val));

		try
		{
			arrayMembers.erase(arrayMembers.begin() + index);
			arrayMembers.insert(arrayMembers.begin() + index, std::make_shared<String>(strVal.stringValue, strVal.singleQuotes));
		}
		catch(std::bad_alloc)
		{
			throw json::JsonBadAlloc();
		}
	}
	else if(val.type == JsonNull)
	{
		try
		{
			arrayMembers.erase(arrayMembers.begin() + index);
			arrayMembers.insert(arrayMembers.begin() + index, std::make_shared<Value>());
		}
		catch(std::bad_alloc)
		{
			throw json::JsonBadAlloc();
		}
	}
	else if(val.type == JsonBoolean)
	{
		Bool boolVal = static_cast<Bool&>(const_cast<Value&>(val));

		try
		{
			arrayMembers.erase(arrayMembers.begin() + index);
			arrayMembers.insert(arrayMembers.begin() + index, std::make_shared<Bool>(boolVal.boolValue));
		}
		catch(std::bad_alloc)
		{
			throw json::JsonBadAlloc();
		}
	}
	else if(val.type == JsonNumber)
	{
		Number numVal = static_cast<Number&>(const_cast<Value&>(val));

		try
		{
			arrayMembers.erase(arrayMembers.begin() + index);
			if (numVal.numberType == NITE_INTEGER)
				arrayMembers.insert(arrayMembers.begin() + index, std::make_shared<Number>(numVal.valueInt));
			else if (numVal.numberType == NITE_DOUBLE)
				arrayMembers.insert(arrayMembers.begin() + index, std::make_shared<Number>(numVal.valueDouble));
			else
				throw json::JsonTypeError(JsonNumber, numVal.numberType);
		}
		catch(std::bad_alloc)
		{
			throw json::JsonBadAlloc();
		}
	}
	else if(val.type == JsonEnumeration)
	{
		Enumeration enumVal = static_cast<Enumeration&>(const_cast<Value&>(val));

		try
		{
			arrayMembers.erase(arrayMembers.begin() + index);
			if (enumVal.enumerationType == NITE_ArrayType)
				arrayMembers.insert(arrayMembers.begin() + index, std::make_shared<Enumeration>(enumVal));
			else if (enumVal.enumerationType == ObjectType)
				arrayMembers.insert(arrayMembers.begin() + index, std::make_shared<Enumeration>(enumVal));
			else if (enumVal.enumerationType == NITE_JavascriptType)
				arrayMembers.insert(arrayMembers.begin() + index, std::make_shared<Enumeration>(enumVal));
			else
				throw json::JsonTypeError(JsonEnumeration, enumVal.enumerationType);
		}
		catch(std::bad_alloc)
		{
			throw json::JsonBadAlloc();
		}
	}
	else
		throw json::JsonTypeNotDefined(val.type);
}

void json::Enumeration::replace(const size_t index, const std::string val)
{
	size_t length = arrayMembers.size();
	if (length == 0)
		throw json::JsonOutOfRange(index, 0);
	if(index > length-1)
		throw json::JsonOutOfRange(index, length);
	if(enumerationType != NITE_ArrayType && enumerationType != NITE_JavascriptType)
		throw json::JsonTypeError(NITE_ArrayType, enumerationType);

	try
	{
		arrayMembers.erase(arrayMembers.begin() + index);
		arrayMembers.insert(arrayMembers.begin() + index, std::make_shared<String>(val));
	}
	catch(std::bad_alloc)
	{
		throw json::JsonBadAlloc();
	}
}

void json::Enumeration::replace(const size_t index, const char* val)
{
	size_t length = arrayMembers.size();
	if (length == 0)
		throw json::JsonOutOfRange(index, 0);
	if(index > length-1)
		throw json::JsonOutOfRange(index, length);
	if(enumerationType != NITE_ArrayType && enumerationType != NITE_JavascriptType)
		throw json::JsonTypeError(NITE_ArrayType, enumerationType);

	try
	{
		arrayMembers.erase(arrayMembers.begin() + index);
		arrayMembers.insert(arrayMembers.begin() + index, std::make_shared<String>(val));
	}
	catch(std::bad_alloc)
	{
		throw json::JsonBadAlloc();
	}
}

void json::Enumeration::replace(const size_t index, const bool val)
{
	size_t length = arrayMembers.size();
	if (length == 0)
		throw json::JsonOutOfRange(index, 0);
	if(index > length-1)
		throw json::JsonOutOfRange(index, length);
	if(enumerationType != NITE_ArrayType && enumerationType != NITE_JavascriptType)
		throw json::JsonTypeError(NITE_ArrayType, enumerationType);

	try
	{
		arrayMembers.erase(arrayMembers.begin() + index);
		arrayMembers.insert(arrayMembers.begin() + index, std::make_shared<Bool>(val));
	}
	catch(std::bad_alloc)
	{
		throw json::JsonBadAlloc();
	}
}

void json::Enumeration::replace(const size_t index, const int64_t val)
{
	size_t length = arrayMembers.size();
	if (length == 0)
		throw json::JsonOutOfRange(index, 0);
	if(index > length-1)
		throw json::JsonOutOfRange(index, length);
	if(enumerationType != NITE_ArrayType && enumerationType != NITE_JavascriptType)
		throw json::JsonTypeError(NITE_ArrayType, enumerationType);

	try
	{
		arrayMembers.erase(arrayMembers.begin() + index);
		arrayMembers.insert(arrayMembers.begin() + index, std::make_shared<Number>(val));
	}
	catch(std::bad_alloc)
	{
		throw json::JsonBadAlloc();
	}
}

void json::Enumeration::replace(const size_t index, const int val)
{
	size_t length = arrayMembers.size();
	if (length == 0)
		throw json::JsonOutOfRange(index, 0);
	if(index > length-1)
		throw json::JsonOutOfRange(index, length);
	if(enumerationType != NITE_ArrayType && enumerationType != NITE_JavascriptType)
		throw json::JsonTypeError(NITE_ArrayType, enumerationType);

	try
	{
		arrayMembers.erase(arrayMembers.begin() + index);
		arrayMembers.insert(arrayMembers.begin() + index, std::make_shared<Number>(val));
	}
	catch(std::bad_alloc)
	{
		throw json::JsonBadAlloc();
	}
}

void json::Enumeration::replace(const size_t index, const float val)
{
	size_t length = arrayMembers.size();
	if (length == 0)
		throw json::JsonOutOfRange(index, 0);
	if(index > length-1)
		throw json::JsonOutOfRange(index, length);
	if(enumerationType != NITE_ArrayType && enumerationType != NITE_JavascriptType)
		throw json::JsonTypeError(NITE_ArrayType, enumerationType);

	try
	{
		arrayMembers.erase(arrayMembers.begin() + index);
		arrayMembers.insert(arrayMembers.begin() + index, std::make_shared<Number>(val));
	}
	catch(std::bad_alloc)
	{
		throw json::JsonBadAlloc();
	}
}

void json::Enumeration::replace(const size_t index, const double val)
{
	size_t length = arrayMembers.size();
	if (length == 0)
		throw json::JsonOutOfRange(index, 0);
	if(index > length-1)
		throw json::JsonOutOfRange(index, length);
	if(enumerationType != NITE_ArrayType && enumerationType != NITE_JavascriptType)
		throw json::JsonTypeError(NITE_ArrayType, enumerationType);

	try
	{
		arrayMembers.erase(arrayMembers.begin() + index);
		arrayMembers.insert(arrayMembers.begin() + index, std::make_shared<Number>(val));
	}
	catch(std::bad_alloc)
	{
		throw json::JsonBadAlloc();
	}
}

void json::Enumeration::replace(const size_t index)
{
	size_t length = arrayMembers.size();
	if (length == 0)
		throw json::JsonOutOfRange(index, 0);
	if(index > length-1)
		throw json::JsonOutOfRange(index, length);
	if(enumerationType != NITE_ArrayType && enumerationType != NITE_JavascriptType)
		throw json::JsonTypeError(NITE_ArrayType, enumerationType);

	try
	{
		arrayMembers.erase(arrayMembers.begin() + index);
		arrayMembers.insert(arrayMembers.begin() + index, std::make_shared<Value>());
	}
	catch(std::bad_alloc)
	{
		throw json::JsonBadAlloc();
	}
}

void json::Enumeration::replaceArray(const size_t index)
{
	size_t length = arrayMembers.size();
	if (length == 0)
		throw json::JsonOutOfRange(index, 0);
	if(index > length-1)
		throw json::JsonOutOfRange(index, length);
	if(enumerationType != NITE_ArrayType && enumerationType != NITE_JavascriptType)
		throw json::JsonTypeError(NITE_ArrayType, enumerationType);

	try
	{
		arrayMembers.erase(arrayMembers.begin() + index);
		arrayMembers.insert(arrayMembers.begin() + index, std::make_shared<Enumeration>(NITE_ArrayType));
	}
	catch(std::bad_alloc)
	{
		throw json::JsonBadAlloc();
	}
}

void json::Enumeration::replaceJavascript(const size_t index, const std::string name)
{
	size_t length = arrayMembers.size();
	if (length == 0)
		throw json::JsonOutOfRange(index, 0);
	if(index > length-1)
		throw json::JsonOutOfRange(index, length);
	if(enumerationType != NITE_ArrayType && enumerationType != NITE_JavascriptType)
		throw json::JsonTypeError(NITE_ArrayType, enumerationType);

	try
	{
		arrayMembers.erase(arrayMembers.begin() + index);
		arrayMembers.insert(arrayMembers.begin() + index, std::make_shared<Enumeration>(NITE_JavascriptType, name));
	}
	catch(std::bad_alloc)
	{
		throw json::JsonBadAlloc();
	}
}

void json::Enumeration::replaceObject(const size_t index)
{
	size_t length = arrayMembers.size();
	if (length == 0)
		throw json::JsonOutOfRange(index, 0);
	if(index > length-1)
		throw json::JsonOutOfRange(index, length);
	if(enumerationType != NITE_ArrayType && enumerationType != NITE_JavascriptType)
		throw json::JsonTypeError(NITE_ArrayType, enumerationType);

	try
	{
		arrayMembers.erase(arrayMembers.begin() + index);
		arrayMembers.insert(arrayMembers.begin() + index, std::make_shared<Enumeration>(ObjectType));
	}
	catch(std::bad_alloc)
	{
		throw json::JsonBadAlloc();
	}
}

/* replaces if object */
void json::Enumeration::replace(const std::string key, const json::Value &val)
{
	size_t length = objectMembers.size();
	if (length == 0)
		throw json::JsonKeyNotFound(key);
	if(enumerationType != ObjectType)
		throw json::JsonTypeError(ObjectType, enumerationType);

	if(val.type == JsonString)
	{
		String strVal = static_cast<String&>(const_cast<Value&>(val));

		for (size_t i = 0; i<length; i++)
		{
			if(objectMembers[i].first.compare(key) == 0)
			{
				try
				{
					objectMembers.erase(objectMembers.begin() + i);
					objectMembers.insert(objectMembers.begin() + i, std::pair<std::string, valuePtr>(key, std::make_shared<String>(strVal.stringValue, strVal.singleQuotes)));
				}
				catch(std::bad_alloc)
				{
					throw json::JsonBadAlloc();
				}
				return;
			}
		}
		throw json::JsonKeyNotFound(key);
	}
	else if(val.type == JsonNull)
	{
		for (size_t i = 0; i<length; i++)
		{
			if(objectMembers[i].first.compare(key) == 0)
			{
				try
				{
					objectMembers.erase(objectMembers.begin() + i);
					objectMembers.insert(objectMembers.begin() + i, std::pair<std::string, valuePtr>(key, std::make_shared<Value>()));
				}
				catch(std::bad_alloc)
				{
					throw json::JsonBadAlloc();
				}
				return;
			}
		}
		throw json::JsonKeyNotFound(key);
	}
	else if(val.type == JsonBoolean)
	{
		Bool boolVal = static_cast<Bool&>(const_cast<Value&>(val));

		for (size_t i = 0; i<length; i++)
		{
			if(objectMembers[i].first.compare(key) == 0)
			{
				try
				{
					objectMembers.erase(objectMembers.begin() + i);
					objectMembers.insert(objectMembers.begin() + i, std::pair<std::string, valuePtr>(key, std::make_shared<Bool>(boolVal.boolValue)));
				}
				catch(std::bad_alloc)
				{
					throw json::JsonBadAlloc();
				}
				return;
			}
		}
		throw json::JsonKeyNotFound(key);
	}
	else if(val.type == JsonNumber)
	{
		Number numVal = static_cast<Number&>(const_cast<Value&>(val));

		for (size_t i = 0; i<length; i++)
		{
			if(objectMembers[i].first.compare(key) == 0)
			{
				try
				{
					objectMembers.erase(objectMembers.begin() + i);
					if (numVal.numberType == NITE_INTEGER)
						objectMembers.insert(objectMembers.begin() + i, std::pair<std::string, valuePtr>(key, std::make_shared<Number>(numVal.getIntValue())));
					else if (numVal.numberType == NITE_DOUBLE)
						objectMembers.insert(objectMembers.begin() + i, std::pair<std::string, valuePtr>(key, std::make_shared<Number>(numVal.getDoubleValue())));
					else
						throw json::JsonTypeError(JsonNumber, numVal.numberType);
				}
				catch(std::bad_alloc)
				{
					throw json::JsonBadAlloc();
				}
				return;
			}
		}
		throw json::JsonKeyNotFound(key);
	}
	else if(val.type == JsonEnumeration)
	{
		Enumeration enumVal = static_cast<Enumeration&>(const_cast<Value&>(val));

		for (size_t i = 0; i<length; i++)
		{
			if(objectMembers[i].first.compare(key) == 0)
			{
				try
				{
					objectMembers.erase(objectMembers.begin() + i);
					if (enumVal.enumerationType == NITE_ArrayType)
						objectMembers.insert(objectMembers.begin() + i, std::pair<std::string, valuePtr>(key, std::make_shared<Enumeration>(enumVal)));
					else if (enumVal.enumerationType == ObjectType)
						objectMembers.insert(objectMembers.begin() + i, std::pair<std::string, valuePtr>(key, std::make_shared<Enumeration>(enumVal)));
					else if (enumVal.enumerationType == NITE_JavascriptType)
						objectMembers.insert(objectMembers.begin() + i, std::pair<std::string, valuePtr>(key, std::make_shared<Enumeration>(enumVal)));
					else
						throw json::JsonTypeError(JsonEnumeration, enumVal.enumerationType);
				}
				catch(std::bad_alloc)
				{
					throw json::JsonBadAlloc();
				}
				return;
			}
		}
		throw json::JsonKeyNotFound(key);
	}
	else
		throw json::JsonTypeNotDefined(val.type);
}

void json::Enumeration::replace(const std::string key, const std::string val)
{
	size_t length = objectMembers.size();
	if (length == 0)
		throw json::JsonKeyNotFound(key);
	if(enumerationType != ObjectType)
		throw json::JsonTypeError(ObjectType, enumerationType);

	for (size_t i = 0; i<length; i++)
	{
		if(objectMembers[i].first.compare(key) == 0)
		{
			try
			{
				objectMembers.erase(objectMembers.begin() + i);
				objectMembers.insert(objectMembers.begin() + i, std::pair<std::string, valuePtr>(key, std::make_shared<String>(val)));
			}
			catch(std::bad_alloc)
			{
				throw json::JsonBadAlloc();
			}
			return;
		}
	}
	throw json::JsonKeyNotFound(key);
}

void json::Enumeration::replace(const std::string key, const char* val)
{
	size_t length = objectMembers.size();
	if (length == 0)
		throw json::JsonKeyNotFound(key);
	if(enumerationType != ObjectType)
		throw json::JsonTypeError(ObjectType, enumerationType);

	for (size_t i = 0; i<length; i++)
	{
		if(objectMembers[i].first.compare(key) == 0)
		{
			try
			{
				objectMembers.erase(objectMembers.begin() + i);
				objectMembers.insert(objectMembers.begin() + i, std::pair<std::string, valuePtr>(key, std::make_shared<String>(val)));
			}
			catch(std::bad_alloc)
			{
				throw json::JsonBadAlloc();
			}
			return;
		}
	}
	throw json::JsonKeyNotFound(key);
}

void json::Enumeration::replace(const std::string key, const bool val)
{
	size_t length = objectMembers.size();
	if (length == 0)
		throw json::JsonKeyNotFound(key);
	if(enumerationType != ObjectType)
		throw json::JsonTypeError(ObjectType, enumerationType);

	for (size_t i = 0; i<length; i++)
	{
		if(objectMembers[i].first.compare(key) == 0)
		{
			try
			{
				objectMembers.erase(objectMembers.begin() + i);
				objectMembers.insert(objectMembers.begin() + i, std::pair<std::string, valuePtr>(key, std::make_shared<Bool>(val)));
			}
			catch(std::bad_alloc)
			{
				throw json::JsonBadAlloc();
			}
			return;
		}
	}
	throw json::JsonKeyNotFound(key);
}

void json::Enumeration::replace(const std::string key, const int64_t val)
{
	size_t length = objectMembers.size();
	if (length == 0)
		throw json::JsonKeyNotFound(key);
	if(enumerationType != ObjectType)
		throw json::JsonTypeError(ObjectType, enumerationType);

	for (size_t i = 0; i<length; i++)
	{
		if(objectMembers[i].first.compare(key) == 0)
		{
			try
			{
				objectMembers.erase(objectMembers.begin() + i);
				objectMembers.insert(objectMembers.begin() + i, std::pair<std::string, valuePtr>(key, std::make_shared<Number>(val)));
			}
			catch(std::bad_alloc)
			{
				throw json::JsonBadAlloc();
			}
			return;
		}
	}
	throw json::JsonKeyNotFound(key);
}

void json::Enumeration::replace(const std::string key, const int val)
{
	size_t length = objectMembers.size();
	if (length == 0)
		throw json::JsonKeyNotFound(key);
	if(enumerationType != ObjectType)
		throw json::JsonTypeError(ObjectType, enumerationType);

	for (size_t i = 0; i<length; i++)
	{
		if(objectMembers[i].first.compare(key) == 0)
		{
			try
			{
				objectMembers.erase(objectMembers.begin() + i);
				objectMembers.insert(objectMembers.begin() + i, std::pair<std::string, valuePtr>(key, std::make_shared<Number>(val)));
			}
			catch(std::bad_alloc)
			{
				throw json::JsonBadAlloc();
			}
			return;
		}
	}
	throw json::JsonKeyNotFound(key);
}

void json::Enumeration::replace(const std::string key, const float val)
{
	size_t length = objectMembers.size();
	if (length == 0)
		throw json::JsonKeyNotFound(key);
	if(enumerationType != ObjectType)
		throw json::JsonTypeError(ObjectType, enumerationType);

	for (size_t i = 0; i<length; i++)
	{
		if(objectMembers[i].first.compare(key) == 0)
		{
			try
			{
				objectMembers.erase(objectMembers.begin() + i);
				objectMembers.insert(objectMembers.begin() + i, std::pair<std::string, valuePtr>(key, std::make_shared<Number>(val)));
			}
			catch(std::bad_alloc)
			{
				throw json::JsonBadAlloc();
			}
			return;
		}
	}
	throw json::JsonKeyNotFound(key);
}

void json::Enumeration::replace(const std::string key, const double val)
{
	size_t length = objectMembers.size();
	if (length == 0)
		throw json::JsonKeyNotFound(key);
	if(enumerationType != ObjectType)
		throw json::JsonTypeError(ObjectType, enumerationType);

	for (size_t i = 0; i<length; i++)
	{
		if(objectMembers[i].first.compare(key) == 0)
		{
			try
			{
				objectMembers.erase(objectMembers.begin() + i);
				objectMembers.insert(objectMembers.begin() + i, std::pair<std::string, valuePtr>(key, std::make_shared<Number>(val)));
			}
			catch(std::bad_alloc)
			{
				throw json::JsonBadAlloc();
			}
			return;
		}
	}
	throw json::JsonKeyNotFound(key);
}

void json::Enumeration::replace(const std::string key)
{
	size_t length = objectMembers.size();
	if (length == 0)
		throw json::JsonKeyNotFound(key);
	if(enumerationType != ObjectType)
		throw json::JsonTypeError(ObjectType, enumerationType);

	for (size_t i = 0; i<length; i++)
	{
		if(objectMembers[i].first.compare(key) == 0)
		{
			try
			{
				objectMembers.erase(objectMembers.begin() + i);
				objectMembers.insert(objectMembers.begin() + i, std::pair<std::string, valuePtr>(key, std::make_shared<Value>()));
			}
			catch(std::bad_alloc)
			{
				throw json::JsonBadAlloc();
			}
			return;
		}
	}
	throw json::JsonKeyNotFound(key);
}

void json::Enumeration::replaceArray(const std::string key)
{
	size_t length = objectMembers.size();
	if (length == 0)
		throw json::JsonKeyNotFound(key);
	if(enumerationType != ObjectType)
		throw json::JsonTypeError(ObjectType, enumerationType);

	for (size_t i = 0; i<length; i++)
	{
		if(objectMembers[i].first.compare(key) == 0)
		{
			try
			{
				objectMembers.erase(objectMembers.begin() + i);
				objectMembers.insert(objectMembers.begin() + i, std::pair<std::string, valuePtr>(key, std::make_shared<Enumeration>(NITE_ArrayType)));
			}
			catch(std::bad_alloc)
			{
				throw json::JsonBadAlloc();
			}
			return;
		}
	}
	throw json::JsonKeyNotFound(key);
}

void json::Enumeration::replaceJavascript(const std::string key, const std::string name)
{
	size_t length = objectMembers.size();
	if (length == 0)
		throw json::JsonKeyNotFound(key);
	if(enumerationType != ObjectType)
		throw json::JsonTypeError(ObjectType, enumerationType);

	for (size_t i = 0; i<length; i++)
	{
		if(objectMembers[i].first.compare(key) == 0)
		{
			try
			{
				objectMembers.erase(objectMembers.begin() + i);
				objectMembers.insert(objectMembers.begin() + i, std::pair<std::string, valuePtr>(key, std::make_shared<Enumeration>(NITE_JavascriptType, name)));
			}
			catch(std::bad_alloc)
			{
				throw json::JsonBadAlloc();
			}
			return;
		}
	}
	throw json::JsonKeyNotFound(key);
}

void json::Enumeration::replaceObject(const std::string key)
{
	size_t length = objectMembers.size();
	if (length == 0)
		throw json::JsonKeyNotFound(key);
	if(enumerationType != ObjectType)
		throw json::JsonTypeError(ObjectType, enumerationType);

	for (size_t i = 0; i<length; i++)
	{
		if(objectMembers[i].first.compare(key) == 0)
		{
			try
			{
				objectMembers.erase(objectMembers.begin() + i);
				objectMembers.insert(objectMembers.begin() + i, std::pair<std::string, valuePtr>(key, std::make_shared<Enumeration>(ObjectType)));
			}
			catch(std::bad_alloc)
			{
				throw json::JsonBadAlloc();
			}
			return;
		}
	}
	throw json::JsonKeyNotFound(key);
}

/* inserts if array */
void json::Enumeration::insert(const size_t index, const json::Value &val)
{
	if (index != 0 && arrayMembers.size() == 0)
		throw json::JsonOutOfRange(index, arrayMembers.size());
	if (index > arrayMembers.size() - 1)
		throw json::JsonOutOfRange(index, arrayMembers.size());
	if(enumerationType != NITE_ArrayType && enumerationType != NITE_JavascriptType)
		throw json::JsonTypeError(NITE_ArrayType, enumerationType);

	if(val.type == JsonString)
	{
		String strVal = static_cast<String&>(const_cast<Value&>(val));

		try
		{
			arrayMembers.insert(arrayMembers.begin() + index, std::make_shared<String>(strVal.stringValue, strVal.singleQuotes));
		}
		catch(std::bad_alloc)
		{
			throw json::JsonBadAlloc();
		}
	}
	else if(val.type == JsonNull)
	{
		try
		{
			arrayMembers.insert(arrayMembers.begin() + index, std::make_shared<Value>());
		}
		catch(std::bad_alloc)
		{
			throw json::JsonBadAlloc();
		}
	}
	else if(val.type == JsonBoolean)
	{
		Bool boolVal = static_cast<Bool&>(const_cast<Value&>(val));

		try
		{
			arrayMembers.insert(arrayMembers.begin() + index, std::make_shared<Bool>(boolVal.boolValue));
		}
		catch(std::bad_alloc)
		{
			throw json::JsonBadAlloc();
		}
	}
	else if(val.type == JsonNumber)
	{
		Number numVal = static_cast<Number&>(const_cast<Value&>(val));

		try
		{
			if (numVal.numberType == NITE_INTEGER)
				arrayMembers.insert(arrayMembers.begin() + index, std::make_shared<Number>(numVal.valueInt));
			else if (numVal.numberType == NITE_DOUBLE)
				arrayMembers.insert(arrayMembers.begin() + index, std::make_shared<Number>(numVal.valueDouble));
			else
				throw json::JsonTypeError(JsonNumber, numVal.numberType);
		}
		catch(std::bad_alloc)
		{
			throw json::JsonBadAlloc();
		}
	}
	else if(val.type == JsonEnumeration)
	{
		Enumeration enumVal = static_cast<Enumeration&>(const_cast<Value&>(val));

		try
		{
			if (enumVal.enumerationType == NITE_ArrayType)
				arrayMembers.insert(arrayMembers.begin() + index, std::make_shared<Enumeration>(enumVal));
			else if (enumVal.enumerationType == ObjectType)
				arrayMembers.insert(arrayMembers.begin() + index, std::make_shared<Enumeration>(enumVal));
			else if (enumVal.enumerationType == NITE_JavascriptType)
				arrayMembers.insert(arrayMembers.begin() + index, std::make_shared<Enumeration>(enumVal));
			else
				throw json::JsonTypeError(JsonEnumeration, enumVal.enumerationType);
		}
		catch(std::bad_alloc)
		{
			throw json::JsonBadAlloc();
		}
	}
	else
		throw json::JsonTypeNotDefined(val.type);
}

void json::Enumeration::insert(const size_t index, const std::string val)
{
	if (index != 0 && arrayMembers.size() == 0)
		throw json::JsonOutOfRange(index, arrayMembers.size());
	if (index > arrayMembers.size() - 1)
		throw json::JsonOutOfRange(index, arrayMembers.size());;
	if(enumerationType != NITE_ArrayType && enumerationType != NITE_JavascriptType)
		throw json::JsonTypeError(NITE_ArrayType, enumerationType);

	try
	{
		arrayMembers.insert(arrayMembers.begin() + index, std::make_shared<String>(val));
	}
	catch(std::bad_alloc)
	{
		throw json::JsonBadAlloc();
	}
}

void json::Enumeration::insert(const size_t index, const char* val)
{
	if (index != 0 && arrayMembers.size() == 0)
		throw json::JsonOutOfRange(index, arrayMembers.size());
	if (index > arrayMembers.size() - 1)
		throw json::JsonOutOfRange(index, arrayMembers.size());
	if(enumerationType != NITE_ArrayType && enumerationType != NITE_JavascriptType)
		throw json::JsonTypeError(NITE_ArrayType, enumerationType);

	try
	{
		arrayMembers.insert(arrayMembers.begin() + index, std::make_shared<String>(val));
	}
	catch(std::bad_alloc)
	{
		throw json::JsonBadAlloc();
	}
}

void json::Enumeration::insert(const size_t index, const bool val)
{
	if (index != 0 && arrayMembers.size() == 0)
		throw json::JsonOutOfRange(index, arrayMembers.size());
	if (index > arrayMembers.size() - 1)
		throw json::JsonOutOfRange(index, arrayMembers.size());
	if(enumerationType != NITE_ArrayType && enumerationType != NITE_JavascriptType)
		throw json::JsonTypeError(NITE_ArrayType, enumerationType);

	try
	{
		arrayMembers.insert(arrayMembers.begin() + index, std::make_shared<Bool>(val));
	}
	catch(std::bad_alloc)
	{
		throw json::JsonBadAlloc();
	}
}

void json::Enumeration::insert(const size_t index, const int64_t val)
{
	if (index != 0 && arrayMembers.size() == 0)
		throw json::JsonOutOfRange(index, arrayMembers.size());
	if (index > arrayMembers.size() - 1)
		throw json::JsonOutOfRange(index, arrayMembers.size());
	if(enumerationType != NITE_ArrayType && enumerationType != NITE_JavascriptType)
		throw json::JsonTypeError(NITE_ArrayType, enumerationType);

	try
	{
		arrayMembers.insert(arrayMembers.begin() + index, std::make_shared<Number>(val));
	}
	catch(std::bad_alloc)
	{
		throw json::JsonBadAlloc();
	}
}

void json::Enumeration::insert(const size_t index, const int val)
{
	if (index != 0 && arrayMembers.size() == 0)
		throw json::JsonOutOfRange(index, arrayMembers.size());
	if (index > arrayMembers.size() - 1)
		throw json::JsonOutOfRange(index, arrayMembers.size());
	if(enumerationType != NITE_ArrayType && enumerationType != NITE_JavascriptType)
		throw json::JsonTypeError(NITE_ArrayType, enumerationType);

	try
	{
		arrayMembers.insert(arrayMembers.begin() + index, std::make_shared<Number>(val));
	}
	catch(std::bad_alloc)
	{
		throw json::JsonBadAlloc();
	}
}

void json::Enumeration::insert(const size_t index, const float val)
{
	if (index != 0 && arrayMembers.size() == 0)
		throw json::JsonOutOfRange(index, arrayMembers.size());
	if (index > arrayMembers.size() - 1)
		throw json::JsonOutOfRange(index, arrayMembers.size());
	if(enumerationType != NITE_ArrayType && enumerationType != NITE_JavascriptType)
		throw json::JsonTypeError(NITE_ArrayType, enumerationType);

	try
	{
		arrayMembers.insert(arrayMembers.begin() + index, std::make_shared<Number>(val));
	}
	catch(std::bad_alloc)
	{
		throw json::JsonBadAlloc();
	}
}

void json::Enumeration::insert(const size_t index, const double val)
{
	if (index != 0 && arrayMembers.size() == 0)
		throw json::JsonOutOfRange(index, arrayMembers.size());
	if (index > arrayMembers.size() - 1)
		throw json::JsonOutOfRange(index, arrayMembers.size());
	if(enumerationType != NITE_ArrayType && enumerationType != NITE_JavascriptType)
		throw json::JsonTypeError(NITE_ArrayType, enumerationType);

	try
	{
		arrayMembers.insert(arrayMembers.begin() + index, std::make_shared<Number>(val));
	}
	catch(std::bad_alloc)
	{
		throw json::JsonBadAlloc();
	}
}

void json::Enumeration::insert(const size_t index)
{
	if (index != 0 && arrayMembers.size() == 0)
		throw json::JsonOutOfRange(index, arrayMembers.size());
	if (index > arrayMembers.size() - 1)
		throw json::JsonOutOfRange(index, arrayMembers.size());
	if(enumerationType != NITE_ArrayType && enumerationType != NITE_JavascriptType)
		throw json::JsonTypeError(NITE_ArrayType, enumerationType);

	try
	{
		arrayMembers.insert(arrayMembers.begin() + index, std::make_shared<Value>());
	}
	catch(std::bad_alloc)
	{
		throw json::JsonBadAlloc();
	}
}

void json::Enumeration::insertArray(const size_t index)
{
	if (index != 0 && arrayMembers.size() == 0)
		throw json::JsonOutOfRange(index, arrayMembers.size());
	if (index > arrayMembers.size() - 1)
		throw json::JsonOutOfRange(index, arrayMembers.size());
	if(enumerationType != NITE_ArrayType && enumerationType != NITE_JavascriptType)
		throw json::JsonTypeError(NITE_ArrayType, enumerationType);

	try
	{
		arrayMembers.insert(arrayMembers.begin() + index, std::make_shared<Enumeration>(NITE_ArrayType));
	}
	catch(std::bad_alloc)
	{
		throw json::JsonBadAlloc();
	}
}

void json::Enumeration::insertJavascript(const size_t index, const std::string name)
{
	if (index != 0 && arrayMembers.size() == 0)
		throw json::JsonOutOfRange(index, arrayMembers.size());
	if (index > arrayMembers.size() - 1)
		throw json::JsonOutOfRange(index, arrayMembers.size());
	if(enumerationType != NITE_ArrayType && enumerationType != NITE_JavascriptType)
		throw json::JsonTypeError(NITE_ArrayType, enumerationType);

	try
	{
		arrayMembers.insert(arrayMembers.begin() + index, std::make_shared<Enumeration>(NITE_JavascriptType, name));
	}
	catch(std::bad_alloc)
	{
		throw json::JsonBadAlloc();
	}
}

void json::Enumeration::insertObject(const size_t index)
{
	if (index != 0 && arrayMembers.size() == 0)
		throw json::JsonOutOfRange(index, arrayMembers.size());
	if (index > arrayMembers.size() - 1)
		throw json::JsonOutOfRange(index, arrayMembers.size());
	if(enumerationType != NITE_ArrayType && enumerationType != NITE_JavascriptType)
		throw json::JsonTypeError(NITE_ArrayType, enumerationType);

	try
	{
		arrayMembers.insert(arrayMembers.begin() + index, std::make_shared<Enumeration>(ObjectType));
	}
	catch(std::bad_alloc)
	{
		throw json::JsonBadAlloc();
	}
}

/* inserts if object */
void json::Enumeration::insert(const std::string key, const json::Value &val, const size_t index)
{
	if (index != 0 && objectMembers.size() == 0)
		throw json::JsonOutOfRange(index, objectMembers.size());
	if (index > objectMembers.size() - 1)
		throw json::JsonOutOfRange(index, objectMembers.size());
	if(enumerationType != ObjectType)
		throw json::JsonTypeError(ObjectType, enumerationType);

	if(val.type == JsonString)
	{
		String strVal = static_cast<String&>(const_cast<Value&>(val));

		try
		{
			objectMembers.insert(objectMembers.begin() + index, std::pair<std::string, valuePtr>(key, std::make_shared<String>(strVal.stringValue, strVal.singleQuotes)));
		}
		catch(std::bad_alloc)
		{
			throw json::JsonBadAlloc();
		}
	}
	else if(val.type == JsonNull)
	{
		try
		{
			objectMembers.insert(objectMembers.begin() + index, std::pair<std::string, valuePtr>(key, std::make_shared<Value>()));
		}
		catch(std::bad_alloc)
		{
			throw json::JsonBadAlloc();
		}
	}
	else if(val.type == JsonBoolean)
	{
		Bool boolVal = static_cast<Bool&>(const_cast<Value&>(val));

		try
		{
			objectMembers.insert(objectMembers.begin() + index, std::pair<std::string, valuePtr>(key, std::make_shared<Bool>(boolVal.boolValue)));
		}
		catch(std::bad_alloc)
		{
			throw json::JsonBadAlloc();
		}
	}
	else if(val.type == JsonNumber)
	{
		Number numVal = static_cast<Number&>(const_cast<Value&>(val));

		try
		{
			if (numVal.numberType == NITE_INTEGER)
				objectMembers.insert(objectMembers.begin() + index, std::pair<std::string, valuePtr>(key, std::make_shared<Number>(numVal.valueInt)));
			else if(numVal.numberType == NITE_DOUBLE)
				objectMembers.insert(objectMembers.begin() + index, std::pair<std::string, valuePtr>(key, std::make_shared<Number>(numVal.valueDouble)));
			else
				throw json::JsonTypeError(JsonNumber, numVal.numberType);
		}
		catch(std::bad_alloc)
		{
			throw json::JsonBadAlloc();
		}
	}
	else if(val.type == JsonEnumeration)
	{
		Enumeration enumVal = static_cast<Enumeration&>(const_cast<Value&>(val));

		try
		{
			if (enumVal.enumerationType == NITE_ArrayType)
				objectMembers.insert(objectMembers.begin() + index, std::pair<std::string, valuePtr>(key, std::make_shared<Enumeration>(enumVal)));
			else if (enumVal.enumerationType == ObjectType)
				objectMembers.insert(objectMembers.begin() + index, std::pair<std::string, valuePtr>(key, std::make_shared<Enumeration>(enumVal)));
			else if (enumVal.enumerationType == NITE_JavascriptType)
				objectMembers.insert(objectMembers.begin() + index, std::pair<std::string, valuePtr>(key, std::make_shared<Enumeration>(enumVal)));
			else
				throw json::JsonTypeError(JsonEnumeration, enumVal.enumerationType);
		}
		catch(std::bad_alloc)
		{
			throw json::JsonBadAlloc();
		}
	}
	else
		throw json::JsonTypeNotDefined(val.type);
}

void json::Enumeration::insert(const std::string key, const std::string val, const size_t index)
{
	if (index != 0 && objectMembers.size() == 0)
		throw json::JsonOutOfRange(index, objectMembers.size());
	if (index > objectMembers.size() - 1)
		throw json::JsonOutOfRange(index, objectMembers.size());
	if(enumerationType != ObjectType)
		throw json::JsonTypeError(ObjectType, enumerationType);

	try
	{
		objectMembers.insert(objectMembers.begin() + index, std::pair<std::string, valuePtr>(key, std::make_shared<String>(val)));
	}
	catch(std::bad_alloc)
	{
		throw json::JsonBadAlloc();
	}
}

void json::Enumeration::insert(const std::string key, const char* val, const size_t index)
{
	if (index != 0 && objectMembers.size() == 0)
		throw json::JsonOutOfRange(index, objectMembers.size());
	if (index > objectMembers.size() - 1)
		throw json::JsonOutOfRange(index, objectMembers.size());
	if(enumerationType != ObjectType)
		throw json::JsonTypeError(ObjectType, enumerationType);

	try
	{
		objectMembers.insert(objectMembers.begin() + index, std::pair<std::string, valuePtr>(key, std::make_shared<String>(val)));
	}
	catch(std::bad_alloc)
	{
		throw json::JsonBadAlloc();
	}
}

void json::Enumeration::insert(const std::string key, const bool val, const size_t index)
{
	if (index != 0 && objectMembers.size() == 0)
		throw json::JsonOutOfRange(index, objectMembers.size());
	if (index > objectMembers.size() - 1)
		throw json::JsonOutOfRange(index, objectMembers.size());
	if(enumerationType != ObjectType)
		throw json::JsonTypeError(ObjectType, enumerationType);

	try
	{
		objectMembers.insert(objectMembers.begin() + index, std::pair<std::string, valuePtr>(key, std::make_shared<Bool>(val)));
	}
	catch(std::bad_alloc)
	{
		throw json::JsonBadAlloc();
	}
}

void json::Enumeration::insert(const std::string key, const int64_t val, const size_t index)
{
	if (index != 0 && objectMembers.size() == 0)
		throw json::JsonOutOfRange(index, objectMembers.size());
	if (index > objectMembers.size() - 1)
		throw json::JsonOutOfRange(index, objectMembers.size());
	if(enumerationType != ObjectType)
		throw json::JsonTypeError(ObjectType, enumerationType);

	try
	{
		objectMembers.insert(objectMembers.begin() + index, std::pair<std::string, valuePtr>(key, std::make_shared<Number>(val)));
	}
	catch(std::bad_alloc)
	{
		throw json::JsonBadAlloc();
	}
}

void json::Enumeration::insert(const std::string key, const int val, const size_t index)
{
	if (index != 0 && objectMembers.size() == 0)
		throw json::JsonOutOfRange(index, objectMembers.size());
	if (index > objectMembers.size() - 1)
		throw json::JsonOutOfRange(index, objectMembers.size());
	if(enumerationType != ObjectType)
		throw json::JsonTypeError(ObjectType, enumerationType);

	try
	{
		objectMembers.insert(objectMembers.begin() + index, std::pair<std::string, valuePtr>(key, std::make_shared<Number>(val)));
	}
	catch(std::bad_alloc)
	{
		throw json::JsonBadAlloc();
	}
}

void json::Enumeration::insert(const std::string key, const float val, const size_t index)
{
	if (index != 0 && objectMembers.size() == 0)
		throw json::JsonOutOfRange(index, objectMembers.size());
	if (index > objectMembers.size() - 1)
		throw json::JsonOutOfRange(index, objectMembers.size());
	if(enumerationType != ObjectType)
		throw json::JsonTypeError(ObjectType, enumerationType);

	try
	{
		objectMembers.insert(objectMembers.begin() + index, std::pair<std::string, valuePtr>(key, std::make_shared<Number>(val)));
	}
	catch(std::bad_alloc)
	{
		throw json::JsonBadAlloc();
	}
}

void json::Enumeration::insert(const std::string key, const double val, const size_t index)
{
	if (index != 0 && objectMembers.size() == 0)
		throw json::JsonOutOfRange(index, objectMembers.size());
	if (index > objectMembers.size() - 1)
		throw json::JsonOutOfRange(index, objectMembers.size());
	if(enumerationType != ObjectType)
		throw json::JsonTypeError(ObjectType, enumerationType);

	try
	{
		objectMembers.insert(objectMembers.begin() + index, std::pair<std::string, valuePtr>(key, std::make_shared<Number>(val)));
	}
	catch(std::bad_alloc)
	{
		throw json::JsonBadAlloc();
	}
}

void json::Enumeration::insert(const std::string key, const size_t index)
{
	if (index != 0 && objectMembers.size() == 0)
		throw json::JsonOutOfRange(index, objectMembers.size());
	if (index > objectMembers.size() - 1)
		throw json::JsonOutOfRange(index, objectMembers.size());
	if(enumerationType != ObjectType)
		throw json::JsonTypeError(ObjectType, enumerationType);

	try
	{
		objectMembers.insert(objectMembers.begin() + index, std::pair<std::string, valuePtr>(key, std::make_shared<Value>()));
	}
	catch(std::bad_alloc)
	{
		throw json::JsonBadAlloc();
	}
}

void json::Enumeration::insertArray(const std::string key, const size_t index)
{
	if (index != 0 && objectMembers.size() == 0)
		throw json::JsonOutOfRange(index, objectMembers.size());
	if (index > objectMembers.size() - 1)
		throw json::JsonOutOfRange(index, objectMembers.size());
	if(enumerationType != ObjectType)
		throw json::JsonTypeError(ObjectType, enumerationType);

	try
	{
		objectMembers.insert(objectMembers.begin() + index, std::pair<std::string, valuePtr>(key, std::make_shared<Enumeration>(NITE_ArrayType)));
	}
	catch(std::bad_alloc)
	{
		throw json::JsonBadAlloc();
	}
}

void json::Enumeration::insertJavascript(const std::string key, const size_t index, const std::string name)
{
	if (index != 0 && objectMembers.size() == 0)
		throw json::JsonOutOfRange(index, objectMembers.size());
	if (index > objectMembers.size() - 1)
		throw json::JsonOutOfRange(index, objectMembers.size());
	if(enumerationType != ObjectType)
		throw json::JsonTypeError(ObjectType, enumerationType);

	try
	{
		objectMembers.insert(objectMembers.begin() + index, std::pair<std::string, valuePtr>(key, std::make_shared<Enumeration>(NITE_JavascriptType, name)));
	}
	catch(std::bad_alloc)
	{
		throw json::JsonBadAlloc();
	}
}

void json::Enumeration::insertObject(const std::string key, const size_t index)
{
	if (index != 0 && objectMembers.size() == 0)
		throw json::JsonOutOfRange(index, objectMembers.size());
	if (index > objectMembers.size() - 1)
		throw json::JsonOutOfRange(index, objectMembers.size());
	if(enumerationType != ObjectType)
		throw json::JsonTypeError(ObjectType, enumerationType);

	try
	{
		objectMembers.insert(objectMembers.begin() + index, std::pair<std::string, valuePtr>(key, std::make_shared<Enumeration>(ObjectType)));
	}
	catch(std::bad_alloc)
	{
		throw json::JsonBadAlloc();
	}
}

/* pushBacks if array */
void json::Enumeration::pushBack(const json::Value &val)
{
	if(enumerationType != NITE_ArrayType && enumerationType != NITE_JavascriptType)
		throw json::JsonTypeError(NITE_ArrayType, enumerationType);

	if(val.type == JsonString)
	{
		String strVal = static_cast<String&>(const_cast<Value&>(val));

		try
		{
			arrayMembers.push_back(std::make_shared<String>(strVal.stringValue, strVal.singleQuotes));
		}
		catch(std::bad_alloc)
		{
			throw json::JsonBadAlloc();
		}
	}
	else if(val.type == JsonNull)
	{
		try
		{
			arrayMembers.push_back(std::make_shared<Value>());
		}
		catch(std::bad_alloc)
		{
			throw json::JsonBadAlloc();
		}
	}
	else if(val.type == JsonBoolean)
	{
		Bool boolVal = static_cast<Bool&>(const_cast<Value&>(val));

		try
		{
			arrayMembers.push_back(std::make_shared<Bool>(boolVal.boolValue));
		}
		catch(std::bad_alloc)
		{
			throw json::JsonBadAlloc();
		}
	}
	else if(val.type == JsonNumber)
	{
		Number numVal = static_cast<Number&>(const_cast<Value&>(val));

		try
		{
			if (numVal.numberType == NITE_INTEGER)
				arrayMembers.push_back(std::make_shared<Number>(numVal.valueInt));
			else if (numVal.numberType == NITE_DOUBLE)
				arrayMembers.push_back(std::make_shared<Number>(numVal.valueDouble));
			else
				throw json::JsonTypeError(JsonNumber, numVal.numberType);
		}
		catch(std::bad_alloc)
		{
			throw json::JsonBadAlloc();
		}
	}
	else if(val.type == JsonEnumeration)
	{
		Enumeration enumVal = static_cast<Enumeration&>(const_cast<Value&>(val));

		try
		{
			if (enumVal.enumerationType == NITE_ArrayType)
				arrayMembers.push_back(std::make_shared<Enumeration>(enumVal));
			else if (enumVal.enumerationType == ObjectType)
				arrayMembers.push_back(std::make_shared<Enumeration>(enumVal));
			else if (enumVal.enumerationType == NITE_JavascriptType)
				arrayMembers.push_back(std::make_shared<Enumeration>(enumVal));
			else
				throw json::JsonTypeError(JsonEnumeration, enumVal.enumerationType);
		}
		catch(std::bad_alloc)
		{
			throw json::JsonBadAlloc();
		}
	}
	else
		throw json::JsonTypeNotDefined(val.type);
}

void json::Enumeration::pushBack(const bool val)
{
	if(enumerationType != NITE_ArrayType && enumerationType != NITE_JavascriptType)
		throw json::JsonTypeError(NITE_ArrayType, enumerationType);

	try
	{
		arrayMembers.push_back(std::make_shared<Bool>(val));
	}
	catch(std::bad_alloc)
	{
		throw json::JsonBadAlloc();
	}
}

void json::Enumeration::pushBack(const int64_t val)
{
	if(enumerationType != NITE_ArrayType && enumerationType != NITE_JavascriptType)
		throw json::JsonTypeError(NITE_ArrayType, enumerationType);

	try
	{
		arrayMembers.push_back(std::make_shared<Number>(val));
	}
	catch(std::bad_alloc)
	{
		throw json::JsonBadAlloc();
	}
}

void json::Enumeration::pushBack(const int val)
{
	if(enumerationType != NITE_ArrayType && enumerationType != NITE_JavascriptType)
		throw json::JsonTypeError(NITE_ArrayType, enumerationType);

	try
	{
		arrayMembers.push_back(std::make_shared<Number>(val));
	}
	catch(std::bad_alloc)
	{
		throw json::JsonBadAlloc();
	}
}

void json::Enumeration::pushBack(const float val)
{
	if(enumerationType != NITE_ArrayType && enumerationType != NITE_JavascriptType)
		throw json::JsonTypeError(NITE_ArrayType, enumerationType);

	try
	{
		arrayMembers.push_back(std::make_shared<Number>(val));
	}
	catch(std::bad_alloc)
	{
		throw json::JsonBadAlloc();
	}
}

void json::Enumeration::pushBack(const double val)
{
	if(enumerationType != NITE_ArrayType && enumerationType != NITE_JavascriptType)
		throw json::JsonTypeError(NITE_ArrayType, enumerationType);

	try
	{
		arrayMembers.push_back(std::make_shared<Number>(val));
	}
	catch(std::bad_alloc)
	{
		throw json::JsonBadAlloc();
	}
}

void json::Enumeration::pushBack()
{
	if(enumerationType != NITE_ArrayType && enumerationType != NITE_JavascriptType)
		throw json::JsonTypeError(NITE_ArrayType, enumerationType);

	try
	{
		arrayMembers.push_back(std::make_shared<Value>());
	}
	catch(std::bad_alloc)
	{
		throw json::JsonBadAlloc();
	}
}

void json::Enumeration::pushBackArray()
{
	if(enumerationType != NITE_ArrayType && enumerationType != NITE_JavascriptType)
		throw json::JsonTypeError(NITE_ArrayType, enumerationType);

	try
	{
		arrayMembers.push_back(std::make_shared<Enumeration>(NITE_ArrayType));
	}
	catch(std::bad_alloc)
	{
		throw json::JsonBadAlloc();
	}
}

void json::Enumeration::pushBackObject()
{
	if(enumerationType != NITE_ArrayType && enumerationType != NITE_JavascriptType)
		throw json::JsonTypeError(NITE_ArrayType, enumerationType);

	try
	{
		arrayMembers.push_back(std::make_shared<Enumeration>(ObjectType));
	}
	catch(std::bad_alloc)
	{
		throw json::JsonBadAlloc();
	}
}

/* pushBack for array (String, char*) or object (key and null) */
void json::Enumeration::pushBack(const std::string val)
{
	if(enumerationType == NITE_ArrayType || enumerationType == NITE_JavascriptType)
	{
		try
		{
			arrayMembers.push_back(std::make_shared<String>(val));
		}
		catch(std::bad_alloc)
		{
			throw json::JsonBadAlloc();
		}
	}
	else if(enumerationType == ObjectType)
	{
		try
		{
			objectMembers.push_back(std::pair<std::string, valuePtr>(val, std::make_shared<Value>()));
		}
		catch(std::bad_alloc)
		{
			throw json::JsonBadAlloc();
		}
	}
	else
		throw json::JsonTypeError(JsonEnumeration, enumerationType);
}

void json::Enumeration::pushBack(const char *val)
{
	if(enumerationType == NITE_ArrayType || enumerationType == NITE_JavascriptType)
	{
		try
		{
			arrayMembers.push_back(std::make_shared<String>(val));
		}
		catch(std::bad_alloc)
		{
			throw json::JsonBadAlloc();
		}
	}
	else if(enumerationType == ObjectType)
	{
		try
		{
			objectMembers.push_back(std::pair<std::string, valuePtr>(val, std::make_shared<Value>()));
		}
		catch(std::bad_alloc)
		{
			throw json::JsonBadAlloc();
		}
	}
	else
		throw json::JsonTypeError(JsonEnumeration, enumerationType);
}

void json::Enumeration::pushBackJavascript(const std::string key, const std::string name)
{
	if(enumerationType == ObjectType)
	{
		try
		{
			objectMembers.push_back(std::pair<std::string, valuePtr>(key, std::make_shared<Enumeration>(NITE_JavascriptType, name)));
		}
		catch(std::bad_alloc)
		{
			throw json::JsonBadAlloc();
		}
	}
	else if(enumerationType == NITE_ArrayType || enumerationType == NITE_JavascriptType)
	{
		try
		{
			arrayMembers.push_back(std::make_shared<Enumeration>(NITE_JavascriptType, key));
		}
		catch(std::bad_alloc)
		{
			throw json::JsonBadAlloc();
		}
	}
	else
		throw json::JsonTypeError(JsonEnumeration, enumerationType);
}

/* pushBacks if object */
void json::Enumeration::pushBack(const std::string key, const json::Value &val)
{
	if(enumerationType != ObjectType)
		throw json::JsonTypeError(ObjectType, enumerationType);

	if(val.type == JsonString)
	{
		String strVal = static_cast<String&>(const_cast<Value&>(val));

		try
		{
			objectMembers.push_back(std::pair<std::string, valuePtr>(key, std::make_shared<String>(strVal.stringValue, strVal.singleQuotes)));
		}
		catch(std::bad_alloc)
		{
			throw json::JsonBadAlloc();
		}
	}
	else if(val.type == JsonNull)
	{
		try
		{
			objectMembers.push_back(std::pair<std::string, valuePtr>(key, std::make_shared<Value>()));
		}
		catch(std::bad_alloc)
		{
			throw json::JsonBadAlloc();
		}
	}
	else if(val.type == JsonBoolean)
	{
		Bool boolVal = static_cast<Bool&>(const_cast<Value&>(val));

		try
		{
			objectMembers.push_back(std::pair<std::string, valuePtr>(key, std::make_shared<Bool>(boolVal.boolValue)));
		}
		catch(std::bad_alloc)
		{
			throw json::JsonBadAlloc();
		}
	}
	else if(val.type == JsonNumber)
	{
		Number numVal = static_cast<Number&>(const_cast<Value&>(val));

		try
		{
			if (numVal.numberType == NITE_INTEGER)
				objectMembers.push_back(std::pair<std::string, valuePtr>(key, std::make_shared<Number>(numVal.valueInt)));
			else if (numVal.numberType == NITE_DOUBLE)
				objectMembers.push_back(std::pair<std::string, valuePtr>(key, std::make_shared<Number>(numVal.valueDouble)));
			else
				throw json::JsonTypeError(JsonNumber, numVal.numberType);
		}
		catch(std::bad_alloc)
		{
			throw json::JsonBadAlloc();
		}
	}
	else if(val.type == JsonEnumeration)
	{
		Enumeration enumVal = static_cast<Enumeration&>(const_cast<Value&>(val));
		
		try
		{
			if (enumVal.enumerationType == NITE_ArrayType)
				objectMembers.push_back(std::pair<std::string, valuePtr>(key, std::make_shared<Enumeration>(enumVal)));
			else if (enumVal.enumerationType == ObjectType)
				objectMembers.push_back(std::pair<std::string, valuePtr>(key, std::make_shared<Enumeration>(enumVal)));
			else if (enumVal.enumerationType == NITE_JavascriptType)
				objectMembers.push_back(std::pair<std::string, valuePtr>(key, std::make_shared<Enumeration>(enumVal)));
			else
				throw json::JsonTypeError(JsonEnumeration, enumVal.enumerationType);
		}
		catch(std::bad_alloc)
		{
			throw json::JsonBadAlloc();
		}
	}
	else
		throw json::JsonTypeNotDefined(val.type);
}

void json::Enumeration::pushBack(const std::string key, const std::string val)
{
	if(enumerationType != ObjectType)
		throw json::JsonTypeError(ObjectType, enumerationType);

	try
	{
		objectMembers.push_back(std::pair<std::string, valuePtr>(key, std::make_shared<String>(val)));
	}
	catch(std::bad_alloc)
	{
		throw json::JsonBadAlloc();
	}
}

void json::Enumeration::pushBack(const std::string key, const char* val)
{
	if(enumerationType != ObjectType)
		throw json::JsonTypeError(ObjectType, enumerationType);

	try
	{
		objectMembers.push_back(std::pair<std::string, valuePtr>(key, std::make_shared<String>(val)));
	}
	catch(std::bad_alloc)
	{
		throw json::JsonBadAlloc();
	}
}

void json::Enumeration::pushBack(const std::string key, const bool val)
{
	if(enumerationType != ObjectType)
		throw json::JsonTypeError(ObjectType, enumerationType);

	try
	{
		objectMembers.push_back(std::pair<std::string, valuePtr>(key, std::make_shared<Bool>(val)));
	}
	catch(std::bad_alloc)
	{
		throw json::JsonBadAlloc();
	}
}

void json::Enumeration::pushBack(const std::string key, const int64_t val)
{
	if(enumerationType != ObjectType)
		throw json::JsonTypeError(ObjectType, enumerationType);

	try
	{
		objectMembers.push_back(std::pair<std::string, valuePtr>(key, std::make_shared<Number>(val)));
	}
	catch(std::bad_alloc)
	{
		throw json::JsonBadAlloc();
	}
}

void json::Enumeration::pushBack(const std::string key, const int val)
{
	if(enumerationType != ObjectType)
		throw json::JsonTypeError(ObjectType, enumerationType);

	try
	{
		objectMembers.push_back(std::pair<std::string, valuePtr>(key, std::make_shared<Number>(val)));
	}
	catch(std::bad_alloc)
	{
		throw json::JsonBadAlloc();
	}
}

void json::Enumeration::pushBack(const std::string key, const float val)
{
	if(enumerationType != ObjectType)
		throw json::JsonTypeError(ObjectType, enumerationType);

	try
	{
		objectMembers.push_back(std::pair<std::string, valuePtr>(key, std::make_shared<Number>(val)));
	}
	catch(std::bad_alloc)
	{
		throw json::JsonBadAlloc();
	}
}

void json::Enumeration::pushBack(const std::string key, const double val)
{
	if(enumerationType != ObjectType)
		throw json::JsonTypeError(ObjectType, enumerationType);

	try
	{
		objectMembers.push_back(std::pair<std::string, valuePtr>(key, std::make_shared<Number>(val)));
	}
	catch(std::bad_alloc)
	{
		throw json::JsonBadAlloc();
	}
}

void json::Enumeration::pushBackArray(const std::string key)
{
	if(enumerationType != ObjectType)
		throw json::JsonTypeError(ObjectType, enumerationType);

	try
	{
		objectMembers.push_back(std::pair<std::string, valuePtr>(key, std::make_shared<Enumeration>(NITE_ArrayType)));
	}
	catch(std::bad_alloc)
	{
		throw json::JsonBadAlloc();
	}
}

void json::Enumeration::pushBackObject(const std::string key)
{
	if(enumerationType != ObjectType)
		throw json::JsonTypeError(ObjectType, enumerationType);

	try
	{
		objectMembers.push_back(std::pair<std::string, valuePtr>(key, std::make_shared<Enumeration>(ObjectType)));
	}
	catch(std::bad_alloc)
	{
		throw json::JsonBadAlloc();
	}
}

/* removes for array and object */
void json::Enumeration::remove(const size_t index)
{
	if (arrayMembers.size() == 0)
		throw json::JsonOutOfRange(index, 0);
	if (index > arrayMembers.size() - 1)
		throw json::JsonOutOfRange(index, arrayMembers.size());
	if(enumerationType != NITE_ArrayType && enumerationType != NITE_JavascriptType)
		throw json::JsonTypeError(NITE_ArrayType, enumerationType);

	arrayMembers.erase(arrayMembers.begin() + index);
}

void json::Enumeration::remove(const std::string key)
{
	if (objectMembers.size() == 0)
		throw json::JsonKeyNotFound(key);
	if(enumerationType != ObjectType)
		throw json::JsonTypeError(ObjectType, enumerationType);

	for (size_t i = 0; i<objectMembers.size(); i++)
	{
		if(objectMembers[i].first.compare(key) == 0)
		{
			objectMembers.erase(objectMembers.begin() + i);
			return;
		}
	}
	throw json::JsonKeyNotFound(key);
}

/* empty & clear */
bool json::Enumeration::empty()
{
	if(enumerationType == NITE_ArrayType || enumerationType == NITE_JavascriptType)
		return arrayMembers.empty();
	else if(enumerationType == ObjectType)
		return objectMembers.empty();
	else
		throw json::JsonTypeError(JsonEnumeration, enumerationType);
}

void json::Enumeration::clear()
{
	arrayMembers.clear();
	objectMembers.clear();
}

/* check name if javascript function */
bool json::Enumeration::checkName(const std::string nm)
{
	if(enumerationType != NITE_JavascriptType)
	{
		throw json::JsonTypeError(NITE_JavascriptType, type);
	}

	if(name.compare(nm) == 0)
	{
		return true;
	}

	return false;
}

bool json::Enumeration::checkName(const char* nm)
{
	if(enumerationType != NITE_JavascriptType)
	{
		throw json::JsonTypeError(NITE_JavascriptType, type);
	}

	if(name.compare(nm) == 0)
	{
		return true;
	}

	return false;
}

/* size */
size_t json::Enumeration::size()
{
	if (enumerationType == ObjectType)
		return objectMembers.size();
	else if (enumerationType == NITE_ArrayType || enumerationType == NITE_JavascriptType)
		return arrayMembers.size();

	throw json::JsonTypeError(4, 5);
}

/* print */
std::string json::Enumeration::print()
{
	std::string result;

	if(enumerationType == ObjectType)
	{
		if (objectMembers.size() == 0)
			return "{}";

		result = "{";
		for (size_t i = 0; i<objectMembers.size() - 1; i++)
			result += "\"" + objectMembers[i].first + "\":" + objectMembers[i].second->print() + ",";

		result += "\"" + objectMembers.back().first + "\":" + objectMembers.back().second->print() + "}";
		return result;
	}
	else if(enumerationType == NITE_ArrayType)
	{
		if (arrayMembers.size() == 0)
			return "[]";

		result = "[";
		for (size_t i = 0; i<arrayMembers.size() - 1; i++)
			result += arrayMembers[i]->print() + ",";

		result += arrayMembers.back()->print() + "]";
		return result;
	}
	else if(enumerationType == NITE_JavascriptType)
	{
		std::string prefix = "";
		if(name.empty())
			prefix = "new noname.noname";
		else
			prefix = name;
		
		if (arrayMembers.size() == 0)
			return prefix + "()";

		result = prefix + "(";
		for (size_t i = 0; i<arrayMembers.size() - 1; i++)
			result += arrayMembers[i]->print() + ",";

		result += arrayMembers.back()->print() + ")";
		return result;
	}
	else
	{
		throw json::JsonTypeError(JsonEnumeration, enumerationType);
	}
}

/* privates */
json::Value *json::Enumeration::parse(std::string &jsonStr)
{
	skipSpaces(jsonStr);
	if (jsonStr[0] == '[')
	{
		return parseArray(jsonStr);
	}
	else if (jsonStr[0] == '{')
	{
		return parseObject(jsonStr);
	}
	else if (jsonStr.find("new ") == 0 && jsonStr.find("(") >= 5)
	{
		return parseJavascript(jsonStr);
	}
	else
	{
		throw json::JsonParsingError(startingJSONSignNotFound, jsonStr);
	}
}

json::Value *json::Enumeration::parseArray(std::string &jsonStr)
{
	skipSpaces(jsonStr);
	if (jsonStr[0] == '[')
	{
		bool comma = true;
		jsonStr = jsonStr.substr(1);
		while(true)
		{
			if (jsonStr.size() == 0)
			{
				throw json::JsonParsingError(stringNotComplete, jsonStr);
			}

			skipSpaces(jsonStr);
			if (jsonStr[0] == '{')
			{
				try
				{
					json::Enumeration *enumeration = new Enumeration(ObjectType);
					arrayMembers.push_back(valuePtr(enumeration->parseObject(jsonStr)));

					comma = false;
				}
				catch(std::bad_alloc)
				{
					throw json::JsonBadAlloc();
				}
			}
			else if (jsonStr[0] == '[')
			{
				try
				{
					json::Enumeration *enumeration = new Enumeration(NITE_ArrayType);
					arrayMembers.push_back(valuePtr(enumeration->parseArray(jsonStr)));

					comma = false;
				}
				catch(std::bad_alloc)
				{
					throw json::JsonBadAlloc();
				}
			}
			else if (jsonStr.find("new ") == 0 && jsonStr.find("(") >= 5)
			{
				try
				{
					json::Enumeration *enumeration = new Enumeration(NITE_JavascriptType);
					arrayMembers.push_back(valuePtr(enumeration->parseJavascript(jsonStr)));

					comma = false;
				}
				catch(std::bad_alloc)
				{
					throw json::JsonBadAlloc();
				}
			}
			else if (jsonStr[0] == '\"' || jsonStr[0] == '\'')
			{
				arrayMembers.push_back(valuePtr(parseString(jsonStr)));

				comma = false;
			}
			else if (jsonStr.find("false") == 0)
			{
				try
				{
					arrayMembers.push_back(boolPtr(new Bool(false)));
				}
				catch(std::bad_alloc)
				{
					throw json::JsonBadAlloc();
				}

				jsonStr = jsonStr.substr(5);

				comma = false;
			}
			else if (jsonStr.find("true") == 0)
			{
				try
				{
					arrayMembers.push_back(boolPtr(new Bool(true)));
				}
				catch(std::bad_alloc)
				{
					throw json::JsonBadAlloc();
				}

				jsonStr = jsonStr.substr(4);

				comma = false;
			}
			else if (jsonStr.find("null") == 0)
			{
				try
				{
					arrayMembers.push_back(valuePtr(new Value()));
				}
				catch(std::bad_alloc)
				{
					throw json::JsonBadAlloc();
				}

				jsonStr = jsonStr.substr(4);

				comma = false;
			}
			else if (isdigit(jsonStr[0]) || jsonStr[0] == '-')
			{
				arrayMembers.push_back(valuePtr(parseNumber(jsonStr)));

				comma = false;
			}

			skipSpaces(jsonStr);
			if (jsonStr[0] == ',')
			{
				if(comma)
				{
					try
					{
						arrayMembers.push_back(valuePtr(new Value(JsonEmpty)));
					}
					catch(std::bad_alloc)
					{
						throw json::JsonBadAlloc();
					}
				}
				comma = true;
				jsonStr = jsonStr.substr(1);
			}
			else if (jsonStr[0] == ']')
			{
				if (comma && arrayMembers.size() != 0)
				{
					try
					{
						arrayMembers.push_back(valuePtr(new Value(JsonEmpty)));
					}
					catch (std::bad_alloc)
					{
						throw json::JsonBadAlloc();
					}
				}

				jsonStr = jsonStr.substr(1);
				skipSpaces(jsonStr);
				return this;
			}
			else
			{
				throw json::JsonParsingError(endingOrSeparatorArraySignNotFound, jsonStr);
			}
		}
	}
	else
	{
		throw json::JsonParsingError(startingArraySignNotFound, jsonStr);
	}
}

json::Value *json::Enumeration::parseJavascript(std::string &jsonStr)
{
	skipSpaces(jsonStr);
	if (jsonStr.find("new ") == 0 && jsonStr.find("(") >= 5)
	{
		bool comma = true;
		name = jsonStr.substr(0, jsonStr.find("("));
		jsonStr = jsonStr.substr(jsonStr.find("(") + 1);
		while(true)
		{
			if (jsonStr.size() == 0)
			{
				throw json::JsonParsingError(stringNotComplete, jsonStr);
			}

			skipSpaces(jsonStr);
			if (jsonStr[0] == '{')
			{
				try
				{
					json::Enumeration *enumeration = new Enumeration(ObjectType);
					arrayMembers.push_back(valuePtr(enumeration->parseObject(jsonStr)));

					comma = false;
				}
				catch(std::bad_alloc)
				{
					throw json::JsonBadAlloc();
				}
			}
			else if (jsonStr[0] == '[')
			{
				try
				{
					json::Enumeration *enumeration = new Enumeration(NITE_ArrayType);
					arrayMembers.push_back(valuePtr(enumeration->parseArray(jsonStr)));

					comma = false;
				}
				catch(std::bad_alloc)
				{
					throw json::JsonBadAlloc();
				}
			}
			else if (jsonStr.find("new ") == 0 && jsonStr.find("(") >= 5)
			{
				try
				{
					json::Enumeration *enumeration = new Enumeration(NITE_JavascriptType);
					arrayMembers.push_back(valuePtr(enumeration->parseJavascript(jsonStr)));

					comma = false;
				}
				catch(std::bad_alloc)
				{
					throw json::JsonBadAlloc();
				}
			}
			else if (jsonStr[0] == '\"' || jsonStr[0] == '\'')
			{
				arrayMembers.push_back(valuePtr(parseString(jsonStr)));

				comma = false;
			}
			else if (jsonStr.find("false") == 0)
			{
				try
				{
					arrayMembers.push_back(boolPtr(new Bool(false)));
				}
				catch(std::bad_alloc)
				{
					throw json::JsonBadAlloc();
				}

				jsonStr = jsonStr.substr(5);

				comma = false;
			}
			else if (jsonStr.find("true") == 0)
			{
				try
				{
					arrayMembers.push_back(boolPtr(new Bool(true)));
				}
				catch(std::bad_alloc)
				{
					throw json::JsonBadAlloc();
				}

				jsonStr = jsonStr.substr(4);

				comma = false;
			}
			else if (jsonStr.find("null") == 0)
			{
				try
				{
					arrayMembers.push_back(valuePtr(new Value()));
				}
				catch(std::bad_alloc)
				{
					throw json::JsonBadAlloc();
				}

				jsonStr = jsonStr.substr(4);

				comma = false;
			}
			else if (isdigit(jsonStr[0]) || jsonStr[0] == '-')
			{
				arrayMembers.push_back(valuePtr(parseNumber(jsonStr)));

				comma = false;
			}

			skipSpaces(jsonStr);
			if (jsonStr[0] == ',')
			{
				if(comma)
				{
					try
					{
						arrayMembers.push_back(valuePtr(new Value(JsonEmpty)));
					}
					catch(std::bad_alloc)
					{
						throw json::JsonBadAlloc();
					}
				}
				comma = true;
				jsonStr = jsonStr.substr(1);
			}
			else if (jsonStr[0] == ')')
			{
				if (comma && arrayMembers.size() != 0)
				{
					try
					{
						arrayMembers.push_back(valuePtr(new Value(JsonEmpty)));
					}
					catch (std::bad_alloc)
					{
						throw json::JsonBadAlloc();
					}
				}

				jsonStr = jsonStr.substr(1);
				skipSpaces(jsonStr);
				return this;
			}
			else
			{
				throw json::JsonParsingError(endingOrSeparatorArraySignNotFound, jsonStr);
			}
		}
	}
	else
	{
		throw json::JsonParsingError(startingArraySignNotFound, jsonStr);
	}
}

json::Value *json::Enumeration::parseObject(std::string &jsonStr)
{
	skipSpaces(jsonStr);
	if (jsonStr[0] == '{')
	{
		jsonStr = jsonStr.substr(1);

		while(true)
		{
			if (jsonStr.size() == 0)
			{
				throw json::JsonParsingError(stringNotComplete, jsonStr);
			}

			skipSpaces(jsonStr);
			if (jsonStr[0] == '\"')
			{
				jsonStr = jsonStr.substr(1);
				std::string key;
				while(true)
				{
					size_t index = jsonStr.find('\"');
					if (index == 0)
					{
						key = "";
						jsonStr = jsonStr.substr(1);
						break;
					}
					else if(index == -1)
					{
						throw json::JsonParsingError(endingSignKeyNotFound, jsonStr);
					}
					if(jsonStr[index-1] == '\\')
					{
						key += jsonStr.substr(0, index + 1);
						jsonStr = jsonStr.substr(index+1);
					}
					else
					{
						key += jsonStr.substr(0, index);
						jsonStr = jsonStr.substr(index+1);
						break;
					}
				}

				skipSpaces(jsonStr);
				if (jsonStr[0] == ':')
				{
					jsonStr = jsonStr.substr(1);
					skipSpaces(jsonStr);
					if (jsonStr[0] == '{')
					{
						try
						{
							json::Enumeration *enumeration = new json::Enumeration(ObjectType);
							objectMembers.push_back(std::pair<std::string, valuePtr>(key, valuePtr(enumeration->parseObject(jsonStr))));
						}
						catch(std::bad_alloc)
						{
							throw json::JsonBadAlloc();
						}
					}
					else if (jsonStr[0] == '[')
					{
						try
						{
							json::Enumeration *enumeration = new json::Enumeration(NITE_ArrayType);
							objectMembers.push_back(std::pair<std::string, valuePtr>(key, valuePtr(enumeration->parseArray(jsonStr))));
						}
						catch(std::bad_alloc)
						{
							throw json::JsonBadAlloc();
						}
					}
					else if (jsonStr.find("new ") == 0 && jsonStr.find("(") >= 5)
					{
						try
						{
							json::Enumeration *enumeration = new json::Enumeration(NITE_JavascriptType);
							objectMembers.push_back(std::pair<std::string, valuePtr>(key, valuePtr(enumeration->parseJavascript(jsonStr))));
						}
						catch(std::bad_alloc)
						{
							throw json::JsonBadAlloc();
						}
					}
					else if (jsonStr[0] == '\"' || jsonStr[0] == '\'')
					{
						objectMembers.push_back(std::pair<std::string, valuePtr>(key, valuePtr(parseString(jsonStr))));
					}
					else if (jsonStr.find("false") == 0)
					{
						try
						{
							objectMembers.push_back(std::pair<std::string, valuePtr>(key, valuePtr(new Bool(false))));
						}
						catch(std::bad_alloc)
						{
							throw json::JsonBadAlloc();
						}

						jsonStr = jsonStr.substr(5);
					}
					else if (jsonStr.find("true") == 0)
					{
						try
						{
							objectMembers.push_back(std::pair<std::string, valuePtr>(key, valuePtr(new Bool(true))));
						}
						catch(std::bad_alloc)
						{
							throw json::JsonBadAlloc();
						}

						jsonStr = jsonStr.substr(4);
					}
					else if (jsonStr.find("null") == 0)
					{
						try
						{
							objectMembers.push_back(std::pair<std::string, valuePtr>(key, valuePtr(new Value())));
						}
						catch(std::bad_alloc)
						{
							throw json::JsonBadAlloc();
						}

						jsonStr = jsonStr.substr(4);
					}
					else if (isdigit(jsonStr[0]) || jsonStr[0] == '-')
					{
						objectMembers.push_back(std::pair<std::string, valuePtr>(key, valuePtr(parseNumber(jsonStr))));
					}
					else
					{
						throw json::JsonParsingError(noValueFound, jsonStr);
					}

					skipSpaces(jsonStr);
					if (jsonStr[0] == ',')
					{
						jsonStr = jsonStr.substr(1);
					}
					else if (jsonStr[0] == '}')
					{
						jsonStr = jsonStr.substr(1);
						skipSpaces(jsonStr);
						return this;
					}
					else
					{
						throw json::JsonParsingError(endingOrSeparatorObjectNotFound, jsonStr);
					}
				}
				else
				{
					throw json::JsonParsingError(keyValueSeparatorNotFound, jsonStr);
				}
			}
			else if (jsonStr[0] == '}')
			{
				jsonStr = jsonStr.substr(1);
				skipSpaces(jsonStr);
				return this;
			}
			else
			{
				throw json::JsonParsingError(startingSignKeyNotFound, jsonStr);
			}
		}
	}
	else
	{
		throw json::JsonParsingError(startingObjectSignNotFound, jsonStr);
	}
}

json::Value *json::Enumeration::parseString(std::string &jsonStr)
{
	skipSpaces(jsonStr);
	if (jsonStr[0] != '\"' && jsonStr[0] != '\'')
	{
		throw json::JsonParsingError(startQuoteNotFound, jsonStr);
	}
	else
	{
		char quotes = jsonStr[0];
		bool singleQuotes;
		if(quotes == '\'')
			singleQuotes = true;
		else
			singleQuotes = false;

		jsonStr = jsonStr.substr(1);
		std::string strVal;
		while(true)
		{
			size_t index = jsonStr.find(quotes);
			if (index == 0)
			{
				strVal = "";
				jsonStr = jsonStr.substr(1);
				try
				{
					return new json::String(strVal, singleQuotes);
				}
				catch(std::bad_alloc)
				{
					throw json::JsonBadAlloc();
				}
			}
			else if(index == -1)
			{
				throw json::JsonParsingError(endQuoteNotFound, jsonStr);
			}
			if(jsonStr[index-1] == '\\')
			{
				if (index == 1)
				{
					strVal += jsonStr.substr(0, index + 1);
					jsonStr = jsonStr.substr(index + 1);
				}
				else if (jsonStr[index - 2] == '\\')
				{
					strVal += jsonStr.substr(0, index);
					jsonStr = jsonStr.substr(index + 1);
					try
					{
						return new json::String(strVal, singleQuotes);
					}
					catch (std::bad_alloc)
					{
						throw json::JsonBadAlloc();
					}
				}
				else
				{
					strVal += jsonStr.substr(0, index + 1);
					jsonStr = jsonStr.substr(index + 1);
				}
			}
			else
			{
				strVal += jsonStr.substr(0, index);
				jsonStr = jsonStr.substr(index+1);
				try
				{
					return new json::String(strVal, singleQuotes);
				}
				catch(std::bad_alloc)
				{
					throw json::JsonBadAlloc();
				}
			}
		}
	}
}

json::Value *json::Enumeration::parseNumber(std::string &jsonStr)
{
	std::string resultString;
	skipSpaces(jsonStr);

	char signs[7] = {']', ',', '}', ' ', '\t', '\n', ')'};
	int index = 0;
	int minIndex = INT_MAX;
	for(int i=0; i<7; i++)
	{
		index = jsonStr.find(signs[i]);
		if(index == -1)
			continue;
		else if(minIndex > index)
			minIndex = index;
	}
	if(minIndex == INT_MAX)
	{
		throw json::JsonParsingError(numberParsingError, jsonStr);
	}

	resultString = jsonStr.substr(0, minIndex);
	std::stringstream result(resultString);
	jsonStr = jsonStr.substr(minIndex);
	skipSpaces(jsonStr);

	for (size_t i = 0; i<resultString.size(); i++)
	{
		if(!(isdigit(resultString[i]) || resultString[i] == 'E' || resultString[i] == 'e' || resultString[i] == '-' || resultString[i] == '+' || resultString[i] == '.'))
			throw json::JsonParsingError(numberParsingError, resultString + jsonStr);
	}

	double val;
	result >> val ? val : throw json::JsonParsingError(numberParsingError, resultString + jsonStr);

	try
	{
		if(val - (int64_t)val > int64_t(0))
			return new json::Number(val);
		else if(val - (int64_t)val == int64_t(0))
			return new json::Number((int64_t)val);
		else
			throw json::JsonParsingError(numberParsingError, resultString + jsonStr);
	}
	catch(std::bad_alloc)
	{
		throw json::JsonBadAlloc();
	}
}
