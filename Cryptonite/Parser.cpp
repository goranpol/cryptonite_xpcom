#include <mozilla/Char16.h>

#include "Parser.h"

#include "JsonTypeNotDefined.h"
#include "JsonTypeError.h"
#include "JsonOutOfRange.h"
#include "JsonKeyNotFound.h"
#include "JsonBadAlloc.h"
#include "JsonBadCast.h"
#include "JsonParsingError.h"

#include "Logger.h"

json::JSON::JSON(std::string &jsonStr) : enumeration(json::Enumeration(ObjectType))
{
	parse(jsonStr);
}

json::JSON::JSON(const JSON &val) : enumeration(val.enumeration), prefix(val.prefix)
{
}

json::JSON::JSON(const int type) : prefix(""), enumeration(json::Enumeration(type))
{
}

json::JSON::JSON(const std::string &prfx, const int type) : prefix(prfx), enumeration(json::Enumeration(type))
{
}

json::JSON::JSON(const Enumeration &val) : prefix(""), enumeration(json::Enumeration(val))
{
}

json::JSON::JSON(const std::string &prfx, const Enumeration &val) : prefix(prfx), enumeration(json::Enumeration(val))
{
}

json::JSON::~JSON()
{
}

json::JSON &json::JSON::operator=(const json::JSON &val)
{
	if (this == &val)
		return *this;

	this->prefix = val.prefix;
	this->enumeration = json::Enumeration(val.enumeration);

	return *this;
}

json::JSON &json::JSON::operator=(const json::Enumeration &val)
{
	this->prefix = "";
	this->enumeration = json::Enumeration(val);

	return *this;
}

bool json::JSON::operator==(const json::JSON &val)
{
	if (enumeration.enumerationType != val.enumeration.enumerationType)
		return false;

	if(enumeration != val.enumeration)
		return false;

	return true;
}

bool json::JSON::operator==(const json::Enumeration &val)
{
	if (enumeration.enumerationType != val.enumerationType)
		return false;

	if (enumeration != val)
		return false;

	return true;
}

bool json::JSON::operator!=(const json::JSON &val)
{
	return !(*this == val);
}

bool json::JSON::operator!=(const json::Enumeration &val)
{
	return !(*this == val);
}

/* access operators */
json::Value &json::JSON::operator[](const size_t index)
{
	return enumeration[index];
}

json::Value &json::JSON::operator[](const std::string key)
{
	return enumeration[key];
}

json::Value &json::JSON::operator[](const char* key)
{
	return enumeration[std::string(key)];
}

/*
get root
POSSIBLE MEMORY LEAK (caller has to delete it)
*/
json::Value &json::JSON::get()
{
	return *new Enumeration(enumeration);
}

/* gets if array */
json::Value &json::JSON::get(const size_t index)
{
	return (*this)[index];
}

std::string json::JSON::getString(const size_t index)
{
	return enumeration.getString(index);
}

bool json::JSON::getBool(const size_t index)
{
	return enumeration.getBool(index);
}

int64_t json::JSON::getInt64(const size_t index)
{
	return enumeration.getInt64(index);
}

int json::JSON::getInt(const size_t index)
{
	return enumeration.getInt(index);
}

double json::JSON::getDouble(const size_t index)
{
	return enumeration.getDouble(index);
}

/* gets if object */
json::Value &json::JSON::get(const std::string &key)
{
	return (*this)[key];
}

std::string json::JSON::getString(const std::string &key)
{
	return enumeration.getString(key);
}

bool json::JSON::getBool(const std::string &key)
{
	return enumeration.getBool(key);
}

int64_t json::JSON::getInt64(const std::string &key)
{
	return enumeration.getInt64(key);
}

int json::JSON::getInt(const std::string &key)
{
	return enumeration.getInt(key);
}

double json::JSON::getDouble(const std::string &key)
{
	return enumeration.getDouble(key);
}

/* replaces if array */
void json::JSON::replace(const size_t index, const json::Value &val)
{
	enumeration.replace(index, val);
}

void json::JSON::replace(const size_t index, const std::string val)
{
	enumeration.replace(index, val);
}

void json::JSON::replace(const size_t index, const char* val)
{
	enumeration.replace(index, val);
}

void json::JSON::replace(const size_t index, const bool val)
{
	enumeration.replace(index, val);
}

void json::JSON::replace(const size_t index, const int64_t val)
{
	enumeration.replace(index, val);
}

void json::JSON::replace(const size_t index, const float val)
{
	enumeration.replace(index, val);
}

void json::JSON::replace(const size_t index, const double val)
{
	enumeration.replace(index, val);
}

void json::JSON::replace(const size_t index)
{
	enumeration.replace(index);
}

void json::JSON::replaceArray(const size_t index)
{
	enumeration.replaceArray(index);
}

void json::JSON::replaceObject(const size_t index)
{
	enumeration.replaceObject(index);
}

void json::JSON::replaceJavascript(const size_t index, const std::string name)
{
	enumeration.replaceJavascript(index, name);
}

/* replaces if object */
void json::JSON::replace(const std::string key, const json::Value &val)
{
	enumeration.replace(key, val);
}

void json::JSON::replace(const std::string key, const std::string val)
{
	enumeration.replace(key, val);
}

void json::JSON::replace(const std::string key, const char* val)
{
	enumeration.replace(key, val);
}

void json::JSON::replace(const std::string key, const bool val)
{
	enumeration.replace(key, val);
}

void json::JSON::replace(const std::string key, const int64_t val)
{
	enumeration.replace(key, val);
}

void json::JSON::replace(const std::string key, const float val)
{
	enumeration.replace(key, val);
}

void json::JSON::replace(const std::string key, const double val)
{
	enumeration.replace(key, val);
}

void json::JSON::replace(const std::string key)
{
	enumeration.replace(key);
}

void json::JSON::replaceArray(const std::string key)
{
	enumeration.replaceArray(key);
}

void json::JSON::replaceObject(const std::string key)
{
	enumeration.replaceObject(key);
}

void json::JSON::replaceJavascript(const std::string key, const std::string name)
{
	enumeration.replaceJavascript(key, name);
}

/* inserts if array */
void json::JSON::insert(const size_t index, const json::Value &val)
{
	enumeration.insert(index, val);
}

void json::JSON::insert(const size_t index, const std::string val)
{
	enumeration.insert(index, val);
}

void json::JSON::insert(const size_t index, const char* val)
{
	enumeration.insert(index, val);
}

void json::JSON::insert(const size_t index, const bool val)
{
	enumeration.insert(index, val);
}

void json::JSON::insert(const size_t index, const int64_t val)
{
	enumeration.insert(index, val);
}

void json::JSON::insert(const size_t index, const float val)
{
	enumeration.insert(index, val);
}

void json::JSON::insert(const size_t index, const double val)
{
	enumeration.insert(index, val);
}

void json::JSON::insert(const size_t index)
{
	enumeration.insert(index);
}

void json::JSON::insertArray(const size_t index)
{
	enumeration.insertArray(index);
}

void json::JSON::insertObject(const size_t index)
{
	enumeration.insertObject(index);
}

void json::JSON::insertJavascript(const size_t index, const std::string name)
{
	enumeration.insertJavascript(index, name);
}

/* inserts if object */
void json::JSON::insert(const std::string key, const json::Value &val, const size_t index)
{
	enumeration.insert(key, val, index);
}

void json::JSON::insert(const std::string key, const std::string val, const size_t index)
{
	enumeration.insert(key, val, index);
}

void json::JSON::insert(const std::string key, const char* val, const size_t index)
{
	enumeration.insert(key, val, index);
}

void json::JSON::insert(const std::string key, const bool val, const size_t index)
{
	enumeration.insert(key, val, index);
}

void json::JSON::insert(const std::string key, const int64_t val, const size_t index)
{
	enumeration.insert(key, val, index);
}

void json::JSON::insert(const std::string key, const float val, const size_t index)
{
	enumeration.insert(key, val, index);
}

void json::JSON::insert(const std::string key, const double val, const size_t index)
{
	enumeration.insert(key, val, index);
}

void json::JSON::insert(const std::string key, const size_t index)
{
	enumeration.insert(key, index);
}

void json::JSON::insertArray(const std::string key, const size_t index)
{
	enumeration.insertArray(key, index);
}

void json::JSON::insertObject(const std::string key, const size_t index)
{
	enumeration.insertObject(key, index);
}

void json::JSON::insertJavascript(const std::string key, const size_t index, const std::string name)
{
	enumeration.insertJavascript(key, index);
}

/* pushBacks if array */
void json::JSON::pushBack(const json::Value &val)
{
	enumeration.pushBack(val);
}

void json::JSON::pushBack(const bool val)
{
	enumeration.pushBack(val);
}

void json::JSON::pushBack(const int64_t val)
{
	enumeration.pushBack(val);
}

void json::JSON::pushBack(const float val)
{
	enumeration.pushBack(val);
}

void json::JSON::pushBack(const double val)
{
	enumeration.pushBack(val);
}

void json::JSON::pushBack()
{
	enumeration.pushBack();
}

void json::JSON::pushBackArray()
{
	enumeration.pushBackArray();
}

void json::JSON::pushBackObject()
{
	enumeration.pushBackObject();
}

/* pushBack for array (String, char*) or object (key and null) */
void json::JSON::pushBack(const std::string val)
{
	enumeration.pushBack(val);
}

void json::JSON::pushBack(const char* val)
{
	enumeration.pushBack(val);
}

void json::JSON::pushBackJavascript(const std::string key, const std::string name)
{
	enumeration.pushBackJavascript(key, name);
}

/* pushBacks if object */
void json::JSON::pushBack(const std::string key, const json::Value &val)
{
	enumeration.pushBack(key, val);
}

void json::JSON::pushBack(const std::string key, const std::string val)
{
	enumeration.pushBack(key, val);
}

void json::JSON::pushBack(const std::string key, const char* val)
{
	enumeration.pushBack(key, val);
}

void json::JSON::pushBack(const std::string key, const bool val)
{
	enumeration.pushBack(key, val);
}

void json::JSON::pushBack(const std::string key, const int64_t val)
{
	enumeration.pushBack(key, val);
}

void json::JSON::pushBack(const std::string key, const int val)
{
	enumeration.pushBack(key, val);
}

void json::JSON::pushBack(const std::string key, const float val)
{
	enumeration.pushBack(key, val);
}

void json::JSON::pushBack(const std::string key, const double val)
{
	enumeration.pushBack(key, val);
}

void json::JSON::pushBackArray(const std::string key)
{
	enumeration.pushBackArray(key);
}

void json::JSON::pushBackObject(const std::string key)
{

	enumeration.pushBackObject(key);
}

/* removes for array and object */
void json::JSON::remove(const size_t index)
{
	enumeration.remove(index);
}

void json::JSON::remove(const std::string key)
{
	enumeration.remove(key);
}

/* empty & clear */
bool json::JSON::empty()
{
	return enumeration.empty();
}

void json::JSON::clear()
{
	enumeration.clear();
}

/* check name if javascript function */
bool json::JSON::checkName(const std::string nm)
{
	return enumeration.checkName(nm);
}

bool json::JSON::checkName(const char* nm)
{
	return enumeration.checkName(nm);
}

/* other */
size_t json::JSON::size()
{
	return enumeration.size();
}

std::string json::JSON::print()
{
	if(prefix.empty())
	{
		return enumeration.print();
	}

	return prefix + enumeration.print();
}

void json::JSON::parse(std::string &jsonStr)
{
	if(!parsePrefix(jsonStr))
		throw json::JsonParsingError(notJson, jsonStr);

	enumeration = static_cast<json::Enumeration&>(*enumeration.parse(jsonStr));
}

std::string json::JSON::getPrefix()
{
	if(prefix.empty())
		return "";
	else
		return prefix;
}

void json::JSON::setPrefix(std::string prfx)
{
	prefix = prfx;
}

/* privates */
bool json::JSON::parsePrefix(std::string &jsonStr)
{
	int arrayPosition = jsonStr.find('[');
	int objectPosition = jsonStr.find('{');
	////////////////////////////////////////////////////
	int javascriptNewPosition = jsonStr.find("new ");
	int javascriptPosition = jsonStr.find("(");
	////////////////////////////////////////////////////

	if(arrayPosition == -1 && objectPosition == -1 && (javascriptNewPosition == -1 || javascriptPosition == -1))
		return false;

	if (arrayPosition == -1)
	{
		arrayPosition = jsonStr.size() + 1;
	}
	if (objectPosition == -1)
	{
		objectPosition = jsonStr.size() + 1;
	}
	if (javascriptNewPosition == -1 || javascriptPosition == -1)
	{
		javascriptNewPosition = jsonStr.size() + 1;
		javascriptPosition = jsonStr.size() + 1;
	}

	if((arrayPosition < objectPosition) && (arrayPosition < javascriptNewPosition))
	{
		enumeration = json::Enumeration(NITE_ArrayType);
		prefix = jsonStr.substr(0, arrayPosition);
		jsonStr = jsonStr.substr(arrayPosition);

		return true;
	}
	else if((objectPosition < arrayPosition) && (objectPosition < javascriptNewPosition))
	{
		enumeration = json::Enumeration(ObjectType);
		prefix = jsonStr.substr(0, objectPosition);
		jsonStr = jsonStr.substr(objectPosition);

		return true;
	}
	else if((javascriptNewPosition < arrayPosition) && (javascriptNewPosition < objectPosition) && (javascriptNewPosition <= javascriptPosition - 5))
	{
		enumeration = json::Enumeration(NITE_JavascriptType);
		prefix = jsonStr.substr(0, javascriptNewPosition);
		jsonStr = jsonStr.substr(javascriptNewPosition);

		return true;
	}
	else
	{
		return false;
	}

	return false;
}
