#include <mozilla/Char16.h>

#include "GAttachmentListener.h"
#include "NSException.h"
#include "AlgorithmException.h"

#include "ObjectRequest.h"
#include "ObjectStorage.h"

#include "GMailAttachment.h"

#include "Logger.h"


NS_IMPL_ISUPPORTS(gmail::GAttachmentListener, nsIRequestObserver, nsIStreamListener)

gmail::GAttachmentListener::GAttachmentListener(const std::string attid) : attachmentId(attid)
{
}


gmail::GAttachmentListener::~GAttachmentListener()
{
}

NS_IMETHODIMP gmail::GAttachmentListener::OnStartRequest(nsIRequest *aRequest, nsISupports *aContext)
{
	try
	{
		HttpListener::OnStartRequest(aRequest, aContext);
	}
	catch (exception::technical::NSException)
	{
		// notify javascript
	}

	return NS_OK;
}

NS_IMETHODIMP gmail::GAttachmentListener::OnDataAvailable(nsIRequest *aRequest, nsISupports *aContext, nsIInputStream *aInputStream, uint64_t aOffset, uint32_t aCount)
{
	try
	{
		HttpListener::OnDataAvailable(aRequest, aContext, aInputStream, aOffset, aCount);
	}
	catch (exception::technical::NSException)
	{
		// notify javascript
	}

	return NS_OK;
}

NS_IMETHODIMP gmail::GAttachmentListener::OnStopRequest(nsIRequest *aRequest, nsISupports *aContext, nsresult aStatusCode)
{
	mData = (char*)NS_Realloc((void*)mData, sizeof(char) * (mRead + 1));
	if (!mData)
	{
		Logger::errorMessage("GAttachmentListener", "OnStopRequest", "OnStopRequest FAILED: couldn't reallocate memory for mData", nsresult(-1));
		return NS_OK;
	}

	memcpy(mData + mRead, "\0", 1);

	std::string data = mData;
	if (data.size() < 108 || !isBase64(data))
	{
		mDecryptedRead = mRead;
		mDecryptedData = (char*)NS_Alloc(sizeof(char) * mRead);
		memcpy(mDecryptedData, mData, mDecryptedRead);

		try
		{
			HttpListener::OnStopRequest(aRequest, aContext, aStatusCode);
		}
		catch (exception::technical::NSException)
		{
			// inform javascript
		}
		catch (...)
		{
			// inform javascript
		}

		return NS_OK;
	}

	std::string checkWord = data.substr(0, 86);
	std::string attId = attachmentId;
	try
	{
		if (attachmentId.compare("-1") == 0)
			attId = checkWord;
		
		gmail::GMailAttachment gAttachment = static_cast<gmail::GMailAttachment&>(base::ObjectStorage::getInstance().getObject(base::ObjectRequest(attId, checkWord), NITE_GATTACH));

		std::string decryptedData = gAttachment.decryptIncomingString(data);

		mDecryptedRead = decryptedData.size();
		mDecryptedData = (char*)NS_Alloc(sizeof(char) * mDecryptedRead);
		memcpy(mDecryptedData, decryptedData.c_str(), mDecryptedRead);
	}
	catch (...)
	{
		mDecryptedRead = mRead;
		mDecryptedData = (char*)NS_Alloc(sizeof(char) * mRead);
		memcpy(mDecryptedData, mData, mDecryptedRead);
	}

	try
	{
		HttpListener::OnStopRequest(aRequest, aContext, aStatusCode);
	}
	catch (exception::technical::NSException)
	{
		// inform javascript
	}
	catch (...)
	{
		// inform javascript
	}

	return NS_OK;
}
