#ifndef _UnknownException_h_
#define _UnknownException_h_

#include <exception>
#include <string>

namespace exception
{
	namespace domain
	{
		class UnknownException : public std::exception
		{
		public:
			UnknownException(const std::string functSign);
			virtual ~UnknownException() throw();

			const char* what() const throw();

		private:
			const std::string functionSignature;
		};
	}
}

#endif
