#ifndef _HotmailObserver_h_
#define _HotmailObserver_h_

#include "HttpObserver.h"

namespace hotmail
{
	class HotmailObserver : public base::HttpObserver
	{
	public:
		HotmailObserver(bool encrypt);

		NS_DECL_ISUPPORTS
		NS_DECL_NSIOBSERVER

	protected:

	private:
		~HotmailObserver();

		std::string recalculateFileLength(std::string data);
	};
}

#endif