#ifndef _GDriveListener_h_
#define _GDriveListener_h_

#include "HttpListener.h"
#include <string>

#define NITE_deleteType		0
#define NITE_getIdType		1
#define NITE_downloadType	2

namespace gdrive
{
	class GDriveListener : public base::HttpListener
	{
	public:
		GDriveListener(const int typ);
		GDriveListener(const int type, const std::string id);
		GDriveListener(const std::string id);

		NS_DECL_ISUPPORTS
		NS_DECL_NSISTREAMLISTENER 
		NS_DECL_NSIREQUESTOBSERVER

	protected:

	private:
		~GDriveListener();

		void deleteFromGDrive();

		const int type;
		const std::string fileId;
	};
}

#endif
