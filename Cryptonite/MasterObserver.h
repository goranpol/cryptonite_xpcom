#ifndef _MasterObserver_h_
#define _MasterObserver_h_

#include "nsCOMPtr.h"
#include "nsIHttpChannel.h"
#include "nsIObserver.h"
#include "nsEmbedString.h"
#include "nsIUploadChannel.h"

#include "Test.h"

#include <string>

class Cryptonite;

namespace base
{
	class MasterObserver : public nsIObserver
	{
	public:
		static MasterObserver &getInstance();

		NS_DECL_ISUPPORTS
		NS_DECL_NSIOBSERVER

		friend class ::Cryptonite;
		friend char* ::CLogin(const char * username, const char * password);
		friend void ::CLogout();

	private:
		static RefPtr<MasterObserver> mMasterObserver;

		MasterObserver();
		~MasterObserver();

		void registerObserver();
		void unregisterObserver();

		void getUri(nsIHttpChannel *httpChannel, nsACString& url);
		std::string toString(nsEmbedString s);

		/* MASTER OBSERVER MEMBERS */

		// flag if encrypt stuff = true if don't encrypt = false
		bool doEncrypt;

		// flags if the login of addon and webpage match
		bool gMailFlag;
		bool gDriveFlag;
		bool hotmailFlag;
		bool oneDriveFlag;
		// others still to come
	};
}

#endif
