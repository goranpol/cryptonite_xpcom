#ifndef _OneDriveListener_h_
#define _OneDriveListener_h_

#include "HttpListener.h"

#include <string>

#define NITE_deleteType		0
#define NITE_downloadType	1

namespace onedrive
{
	class OneDriveListener : public base::HttpListener
	{
	public:
		OneDriveListener(const int typ);
		OneDriveListener(const std::string id);

		NS_DECL_ISUPPORTS
		NS_DECL_NSISTREAMLISTENER
		NS_DECL_NSIREQUESTOBSERVER

	protected:

	private:
		~OneDriveListener();

		const std::string fileId;
		const int type;
	};
}

#endif
