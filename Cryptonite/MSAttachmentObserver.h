#ifndef _MSAttachmentObserver_h_
#define _MSAttachmentObserver_h_

#include "HttpObserver.h"

namespace hotmail
{
	class MSAttachmentObserver : public base::HttpObserver
	{
	public:
		MSAttachmentObserver(bool encrypt);

		NS_DECL_ISUPPORTS
		NS_DECL_NSIOBSERVER

	protected:

	private:
		~MSAttachmentObserver();
	};
}

#endif