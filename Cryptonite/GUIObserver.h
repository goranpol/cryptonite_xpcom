#ifndef _GUIObserver_h_
#define _GUIObserver_h_

#include "nsIObserver.h"
#include "nsCOMPtr.h"
#include "nsEmbedString.h"

#include <string>

// clean this because you don't need all of them (thigs changed)
#define NITE_ENCRYPT				"crypt-o-nite-encrypt"
#define NITE_DECRYPT				"crypt-o-nite-decrypt"
#define NITE_DELETE				"crypt-o-nite-delete"
#define NITE_REMOVE				"crypt-o-nite-remove"
#define NITE_NEWOBJECT			"crypt-o-nite-new-object"
#define NITE_ENCRYPTOBJECTYES	"crypt-o-nite-encrypt-object-yes"
#define NITE_ENCRYPTOBJECTNO		"crypt-o-nite-encrypt-object-no"
#define NITE_USERS				"crypt-o-nite-users"

namespace gui
{
	class GUIObserver : public nsIObserver
	{
	public:
		static GUIObserver &getInstance();

		NS_DECL_ISUPPORTS
		NS_DECL_NSIOBSERVER

		void registerObserver();
		void unregisterObserver();
		void notifyObserver(const char* aTopic, std::string message);

	private:
		static RefPtr<GUIObserver> mGUIObserver;

		GUIObserver();
		~GUIObserver();
		std::string toString(nsEmbedString s);
	};
}

#endif
