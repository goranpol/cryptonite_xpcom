#include "GDriveFile.h"

#include "Attachment.h"
#include "AESRijndael.h"
#include "Logger.h"

#include <sstream>
#include "Parser.h"

#include "ObjectNotFoundException.h"
#include "NotImplementedException.h"
#include "AESException.h"
#include "AlgorithmException.h"
#include "ObjectTypeException.h"


gdrive::GDriveFile::GDriveFile() : base::DriveFile("-1", generateKey(), generateCheckWord(), generateKey(), NITE_GDRIVEFILE)
{
}

gdrive::GDriveFile::GDriveFile(const std::string objID, bool single) : base::DriveFile(objID, generateKey(), generateCheckWord(), generateKey(), NITE_GDRIVEFILE), isSingle(single), isFirst(true), isLast(false), lastBlock(""), remainder(""), initBlock("")
{
}

gdrive::GDriveFile::GDriveFile(const std::string objID, const std::string objKey, const std::string checkWord, const std::string initChain) : base::DriveFile(objID, objKey, checkWord, initChain, NITE_GDRIVEFILE)
{
}

gdrive::GDriveFile::GDriveFile(const base::Object &obj) : base::DriveFile(obj.getObjectId(), obj.getObjectKey(), getCheckWord(obj), getInitChain(obj), NITE_GDRIVEFILE)
{
}

gdrive::GDriveFile::GDriveFile(const gdrive::GDriveFile &obj) : base::DriveFile(obj.getObjectId(), obj.getObjectKey(), obj.objectCheckWord, obj.objectInitChain, NITE_GDRIVEFILE), isSingle(obj.isSingle), isFirst(obj.isFirst), isLast(obj.isLast), lastBlock(obj.lastBlock), remainder(obj.remainder), initBlock(obj.initBlock)
{
}

gdrive::GDriveFile::~GDriveFile()
{
}

gdrive::GDriveFile &gdrive::GDriveFile::operator=(base::Object &obj)
{
	if (obj.getObjectType() != NITE_GDRIVEFILE)
		throw exception::domain::ObjectTypeException(NITE_GDRIVEFILE, obj.getObjectType());

	gdrive::GDriveFile file = static_cast<gdrive::GDriveFile&>(obj);
	
	if (this == &file)
		return *this;

	const_cast<int&>(objectType) = NITE_GDRIVEFILE;
	const_cast<std::string&>(objectId) = file.objectId;
	const_cast<std::string&>(objectKey) = file.objectKey;
	const_cast<std::string&>(objectInitChain) = file.objectInitChain;
	const_cast<std::string&>(objectCheckWord) = file.objectCheckWord;

	return *this;
}

gdrive::GDriveFile &gdrive::GDriveFile::operator=(gdrive::GDriveFile &obj)
{
	if (this == &obj)
		return *this;

	const_cast<int&>(objectType) = NITE_GDRIVEFILE;
	const_cast<std::string&>(objectId) = obj.objectId;
	const_cast<std::string&>(objectKey) = obj.objectKey;
	const_cast<std::string&>(objectInitChain) = obj.objectInitChain;
	const_cast<std::string&>(objectCheckWord) = obj.objectCheckWord;

	return *this;
}

bool gdrive::GDriveFile::operator==(base::Object &obj)
{
	gdrive::GDriveFile gDriveFile = static_cast<gdrive::GDriveFile&>(obj);

	if (objectId.compare(gDriveFile.objectId) == 0 && objectKey.compare(gDriveFile.objectKey) == 0 && objectInitChain.compare(gDriveFile.objectInitChain) == 0 && objectCheckWord.compare(gDriveFile.objectCheckWord) == 0)
		return true;

	return false;
}

bool gdrive::GDriveFile::operator!=(base::Object &obj)
{
	return !(*this == obj);
}

void gdrive::GDriveFile::setIsFirst(const bool val)
{
	isFirst = val;
}

void gdrive::GDriveFile::setIsLast(const bool val)
{
	isLast = val;
}

std::string gdrive::GDriveFile::encryptOutgoingString(std::string data)
{
	if (isSingle)
	{
		std::string paddedText;

		size_t textStringSize = data.size();
		size_t blockLeftOver = textStringSize % 16;
		if (blockLeftOver == 0)
		{
			paddedText = data + randomNumbers(13);
			paddedText += "/16";
		}
		else if (blockLeftOver == 15)
		{
			paddedText = data + randomNumbers(14);
			paddedText += "/17";
		}
		else
		{
			size_t padding = 16 - blockLeftOver;

			if (padding > 9)
			{
				paddedText = data;
				paddedText += randomNumbers(padding - 3);
			}
			else
			{
				paddedText = data;
				paddedText += randomNumbers(padding - 2);
			}

			paddedText += "/";
			std::ostringstream convertNumber;
			convertNumber << padding;
			paddedText += convertNumber.str();
		}

		return objectCheckWord + this->encryptText(paddedText);
	}
	else if (isFirst)
	{
		size_t len = floor(data.size() / 48) * 48;
		std::string plainText = data.substr(0, len);
		this->remainder = data.substr(len);
		this->lastBlock = plainText.substr(plainText.size() - 16);

		return objectCheckWord + this->encryptText(plainText);
	}
	else if (isLast)
	{
		std::string plainText = this->remainder + data;
		this->remainder = "";
		this->initBlock = this->lastBlock;

		std::string paddedText;
		size_t textStringSize = plainText.size();
		size_t blockLeftOver = textStringSize % 16;
		if (blockLeftOver == 0)
		{
			paddedText = plainText + randomNumbers(13);
			paddedText += "/16";
		}
		else if (blockLeftOver == 15)
		{
			paddedText = plainText + randomNumbers(14);
			paddedText += "/17";
		}
		else
		{
			size_t padding = 16 - blockLeftOver;

			if (padding > 9)
			{
				paddedText = plainText;
				paddedText += randomNumbers(padding - 3);
			}
			else
			{
				paddedText = plainText;
				paddedText += randomNumbers(padding - 2);
			}

			paddedText += "/";
			std::ostringstream convertNumber;
			convertNumber << padding;
			paddedText += convertNumber.str();
		}

		return encryptText(paddedText);
	}
	else
	{
		std::string plainText = this->remainder + data;
		size_t len = floor(plainText.size() / 48) * 48;
		std::string text = plainText.substr(0, len);
		this->remainder = plainText.substr(len);
		this->initBlock = this->lastBlock;
		this->lastBlock = text.substr(text.size() - 16);

		return encryptText(text);
	}
}

std::string gdrive::GDriveFile::encryptText(std::string textString)
{
	return this->encrypt(textString);
}

std::string gdrive::GDriveFile::encrypt(std::string textString)
{
	// if textString.size() % 48 == 0
	std::string result;

	char *cipherText = (char*)NS_Alloc(sizeof(char) * textString.size() + 1);
	memset(cipherText, 0, textString.size() + 1);

	base::AESRijndael r;

	if (isFirst)
		r.makeKey((char*)fromBase64(objectKey).c_str(), (char*)fromBase64(objectInitChain).c_str(), 32);
	else
		r.makeKey((char*)fromBase64(objectKey).c_str(), (char*)initBlock.c_str(), 32);

	r.encrypt((char*)textString.c_str(), cipherText, textString.size(), r.CBC);

	char *tempBuf = cipherText;

	for (size_t i = 0; i<textString.size(); i++)
	{
		result.push_back(*tempBuf);
		tempBuf++;
	}

	NS_Free(cipherText);

	return toBase64(result, textString.size());
}
