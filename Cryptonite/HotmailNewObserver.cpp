#include <mozilla/Char16.h>

#include "HotmailNewObserver.h"
#include "HotmailNewListener.h"
#include "HotmailNewUIWrapper.h"
#include "HotmailAttachment.h"

#include "ObjectStorage.h"
#include "EscapeEncoding.h"
#include "Parser.h"

#include "NSException.h"

#include "Logger.h"

NS_IMPL_ISUPPORTS(hotmail::HotmailNewObserver, nsIObserver)

static const std::string base64Chars =	"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
										"abcdefghijklmnopqrstuvwxyz"
										"0123456789+/";

hotmail::HotmailNewObserver::HotmailNewObserver(bool encrypt) : HttpObserver(encrypt)
{
}

hotmail::HotmailNewObserver::~HotmailNewObserver()
{
}

NS_IMETHODIMP hotmail::HotmailNewObserver::Observe(nsISupports *aSubject, const char *aTopic, const char16_t *aData)
{
	nsresult rv;

	nsCOMPtr<nsIHttpChannel> httpChannel = do_QueryInterface(aSubject, &rv);
	if (NS_FAILED(rv))
	{
		// inform javascript
		Logger::errorMessage("HotmailNewObserver", "Observe", "Observe FAILED: couldn't query interface to nsIHttpChannel", rv);
		return NS_OK;
	}

	nsAutoCString urlPath;
	rv = getUri(httpChannel, urlPath);
	if (NS_FAILED(rv))
	{
		// inform javascript
		Logger::errorMessage("HotmailNewObserver", "Observe", "Observe FAILED: couldn't get uri", rv);
		return NS_OK;
	}

	if (!strcmp(aTopic, "http-on-modify-request"))
	{
		if (urlPath.Find("service.svc") != -1 && (urlPath.Find("action=CreateItem") != -1 || urlPath.Find("action=UpdateItem") != -1))
		{
			nsCOMPtr<nsIUploadChannel> uploadChannel = do_QueryInterface(httpChannel, &rv);
			if (NS_FAILED(rv))
			{
				// inform javascript
				Logger::errorMessage("HotmailNewObserver", "Observe:http-on-modify-request", "Observe FAILED: couldn't query interface to nsIUploadChannel", rv);
				return NS_OK;
			}

			try
			{
				gui::GUIObserver::getInstance().notifyObserver(NITE_ENCRYPT, "hotmail mail");

				std::string data = readSendingData(uploadChannel);
				std::string encryptedData = hotmail::HotmailNewUIWrapper().encryptOutgoingString(data, 1);
				replaceSendingData(uploadChannel, encryptedData);
			}
			catch (...)
			{
				Logger::logMessage("HotmailNewObserver", "Observe:http-on-modify-request", "catch ecrypting mail");
				// infrom javascript
			}

			return NS_OK;
		}
		else if (urlPath.Find("service.svc") != -1 && urlPath.Find("CreateAttachmentFromLocalFile") != -1)
		{
			nsCOMPtr<nsIUploadChannel> uploadChannel = do_QueryInterface(httpChannel, &rv);
			if (NS_FAILED(rv))
			{
				// inform javascript
				Logger::errorMessage("HotmailNewObserver", "Observe:http-on-modify-request", "Observe FAILED: couldn't query interface to nsIUploadChannel", rv);
				return NS_OK;
			}

			try
			{
				gui::GUIObserver::getInstance().notifyObserver(NITE_ENCRYPT, "microsoft attachment");

				// encrypt attachment
				hotmail::HotmailAttachment hotmailAttachment("hotmail_attachment");
				std::string data = readSendingData(uploadChannel);

				std::string encryptedData = hotmailAttachment.encryptOutgoingString(data);
				size_t length = encryptedData.size();

				replaceSendingData(uploadChannel, encryptedData);

				// recalculate metadata
				std::string metaData = getRequestHeader(httpChannel, "X-OWA-UrlPostData");
				
				std::string metaDataDecoded = base::EscapeEncoding::decode(metaData);
				std::string encryptedMetaData = recalculateMetaData(metaDataDecoded, length);

				encryptedMetaData = base::EscapeEncoding::encode(encryptedMetaData, true);
				encryptedMetaData = replace(encryptedMetaData, "%21", "!");

				setRequestHeader(httpChannel, "X-OWA-UrlPostData", encryptedMetaData);

				base::ObjectStorage::getInstance().addTempObject(hotmailAttachment);

				return NS_OK;
			}
			catch (...)
			{
				// inform javascript
				return NS_OK;
			}
		}
		else if (urlPath.Find("action=DeleteItem") != -1)
		{
			nsCOMPtr<nsIUploadChannel> uploadChannel = do_QueryInterface(httpChannel, &rv);
			if (NS_FAILED(rv))
			{
				// inform javascript
				Logger::errorMessage("HotmailNewObserver", "Observe:http-on-modify-request", "Observe FAILED: couldn't query interface to nsIUploadChannel", rv);
				return NS_OK;
			}

			try
			{
				std::string data = readSendingData(uploadChannel);
				hotmail::HotmailNewUIWrapper().deleteMails(data);

				return NS_OK;
			}
			catch (...)
			{
				return NS_OK;
			}
		}
		else if (urlPath.Find("action=ApplyConversationAction") != -1)
		{
			nsCOMPtr<nsIUploadChannel> uploadChannel = do_QueryInterface(httpChannel, &rv);
			if (NS_FAILED(rv))
			{
				// inform javascript
				Logger::errorMessage("HotmailNewObserver", "Observe:http-on-modify-request", "Observe FAILED: couldn't query interface to nsIUploadChannel", rv);
				return NS_OK;
			}

			try
			{
				std::string data = readSendingData(uploadChannel);
				hotmail::HotmailNewUIWrapper().moveMails(data);

				return NS_OK;
			}
			catch (...)
			{
				return NS_OK;
			}
		}

		return NS_OK;
	}
	else if (!strcmp(aTopic, "http-on-examine-response"))
	{
		RefPtr<hotmail::HotmailNewListener> hotmailNewListener;

		if (urlPath.Find("outlook.live.com") != -1 && urlPath.Find("service.svc") != -1)
		{
			if (urlPath.Find("action=GetConversationItems") != -1)
			{
				hotmailNewListener = new hotmail::HotmailNewListener(NITE_Full);
				gui::GUIObserver::getInstance().notifyObserver(NITE_DECRYPT, "microsoft mails");
			}
			else if (urlPath.Find("action=FindConversation") != -1)
			{
				hotmailNewListener = new hotmail::HotmailNewListener(NITE_Summary);
				gui::GUIObserver::getInstance().notifyObserver(NITE_DECRYPT, "microsoft mails");
			}
			else if (urlPath.Find("action=CreateItem") != -1)
			{
				hotmailNewListener = new hotmail::HotmailNewListener(NITE_Reply);
			}
			else if (urlPath.Find("action=FindItem") != -1)
			{
				hotmailNewListener = new hotmail::HotmailNewListener(NITE_DraftSummary);
				gui::GUIObserver::getInstance().notifyObserver(NITE_DECRYPT, "microsoft mails");
			}
			else if (urlPath.Find("action=GetItem") != -1)
			{
				hotmailNewListener = new hotmail::HotmailNewListener(NITE_DraftFull);
				gui::GUIObserver::getInstance().notifyObserver(NITE_DECRYPT, "microsoft mails");
			}
			else if (urlPath.Find("service.svc") != -1 && urlPath.Find("CreateAttachmentFromLocalFile") != -1)
			{
				hotmailNewListener = new hotmail::HotmailNewListener(NITE_attachment);
			}
			else if (urlPath.Find("action=ApplyConversationAction") != -1)
			{
				hotmailNewListener = new hotmail::HotmailNewListener(NITE_MoveOrDelete);
			}
			else
			{
				return NS_OK;
			}
		}
		else if (urlPath.Find("outlook.live.com") != -1 && urlPath.Find("sessiondata.ashx") != -1 && urlPath.Find("appcacheclient") != -1)
		{
			hotmailNewListener = new hotmail::HotmailNewListener(NITE_SessionData);
			gui::GUIObserver::getInstance().notifyObserver(NITE_DECRYPT, "microsoft mails");
		}
		else if (urlPath.Find("outlook.live.com") != -1 && urlPath.Find("PendingNotificationRequest") != -1)
		{
			hotmailNewListener = new hotmail::HotmailNewListener(NITE_Notification);
			gui::GUIObserver::getInstance().notifyObserver(NITE_DECRYPT, "microsoft mails");
		}
		else
		{
			return NS_OK;
		}

		nsCOMPtr<nsIHttpChannel> httpChannel = do_QueryInterface(aSubject, &rv);
		if (NS_FAILED(rv))
		{
			// notify javascript
			Logger::errorMessage("HotmailNewObserver", "Observe:http-on-examine-response", "Observe FAILED: couldn't query interface aSubject to nsIHttpChannel", rv);
			return NS_OK;
		}

		nsCOMPtr<nsITraceableChannel> traceableChannel = do_QueryInterface(aSubject, &rv);
		if (NS_FAILED(rv))
		{
			// notify javascript
			Logger::errorMessage("HotmailNewObserver", "Observe:http-on-examine-response", "Observe FAILED: couldn't query interface to nsITraceableChannel", rv);
			return NS_OK;
		}

		try
		{
			replaceStreamListener(traceableChannel, hotmailNewListener);
		}
		catch (exception::technical::NSException)
		{
			// notify javascript
		}
		catch (...)
		{
			// notify javascript
		}
	}

	return NS_OK;
}

std::string hotmail::HotmailNewObserver::recalculateMetaData(std::string data, size_t length)
{
	std::string dataCopy = data;

	try
	{
		json::JSON dataJson(dataCopy);

		for (size_t i = 0; i < dataJson["Body"]["Attachments"].size(); i++)
		{
			dataJson["Body"]["Attachments"][i].replace("Size", int64_t(length));
		}

		return dataJson.print();
	}
	catch (...)
	{
		Logger::errorMessage("HotmailNewObserver", "encryptAttachments", "error encrypting attachments", nsresult(1));
		return data;
	}
}

std::string hotmail::HotmailNewObserver::toBase64WithPadding(const std::string input, size_t len)
{
	std::string ret;
	int i = int(0);
	int j = int(0);
	unsigned char char_array_3[3];
	unsigned char char_array_4[4];
	unsigned char *inputCString = (unsigned char*)input.c_str();

	while (len--)
	{
		char_array_3[i++] = *(inputCString++);
		if (i == 3)
		{
			char_array_4[int(0)] = (char_array_3[int(0)] & 0xfc) >> 2;
			char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
			char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
			char_array_4[3] = char_array_3[2] & 0x3f;

			for (i = int(0); i < 4; i++)
				ret += base64Chars[char_array_4[i]];
			i = int(0);
		}
	}

	if (i)
	{
		for (j = i; j < 3; j++)
			char_array_3[j] = '\0';

		char_array_4[int(0)] = (char_array_3[int(0)] & 0xfc) >> 2;
		char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
		char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
		char_array_4[3] = char_array_3[2] & 0x3f;

		for (j = int(0); j < i + 1; j++)
			ret += base64Chars[char_array_4[j]];

		while (i++ < 3)
		{
			ret += '=';
		}
	}

	return ret;
}

std::string hotmail::HotmailNewObserver::fromBase64WithPadding(const std::string input)
{
	int len = input.size();
	int i = int(0);
	int j = int(0);
	int in_ = int(0);
	unsigned char char_array_4[4];
	unsigned char char_array_3[3];
	std::string ret;

	while (len-- && input[in_] != '=')
	{
		char_array_4[i++] = input[in_];
		j++;

		in_++;
		if (i == 4)
		{
			for (i = int(0); i < 4; i++)
				char_array_4[i] = base64Chars.find(char_array_4[i]);

			char_array_3[int(0)] = (char_array_4[int(0)] << 2) + ((char_array_4[1] & 0x30) >> 4);
			char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
			char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

			for (i = int(0); i < j - 1; i++)
				ret += char_array_3[i];
			i = 0;
			j = 0;
		}
	}

	if (i)
	{
		for (j = i; j < 4; j++)
			char_array_4[j] = int(0);

		for (j = int(0); j < 4; j++)
			char_array_4[j] = base64Chars.find(char_array_4[j]);

		char_array_3[int(0)] = (char_array_4[int(0)] << 2) + ((char_array_4[1] & 0x30) >> 4);
		char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
		char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

		for (j = int(0); j < i - 1; j++)
			ret += char_array_3[j];
	}

	return ret;
}
