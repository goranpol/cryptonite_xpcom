#ifndef _Hotmail_h
#define _Hotmail_h

#include "UsersList.h"
#include "Mail.h"

namespace hotmail
{
	class Hotmail : public base::Mail
	{
	public:
		Hotmail();
		Hotmail(const std::string objDraftId);
		Hotmail(const std::string objDraftId, std::vector<std::string> objAttachmentIds);
		Hotmail(const std::string objId, const std::string objKey, const std::string objInitChain, const std::string objCheckWord);
		Hotmail(const std::string objId, const std::string objKey, const std::string objInitChain, const std::string objCheckWord, const std::string objDraftId);
		Hotmail(const std::string objId, const std::string objKey, const std::string objInitChain, const std::string objCheckWord, const std::string objDraftId, std::vector<std::string> objAttachmentIds);
		Hotmail(const Object &obj);
		Hotmail(const Hotmail &obj);
		~Hotmail();

		Hotmail &operator=(Object &obj);
		Hotmail &operator=(Hotmail &obj);
		bool operator==(Object &obj);
		bool operator!=(Object &obj);

		std::string encryptOutgoingString(std::string data);
		std::string decryptIncomingString(std::string data);

		friend class HotmailUIWrapper;

	private:

		std::string encryptDraft(std::string data);
		
		void decryptRollup(json::Value &jsonRollup);
		void decryptMessage(json::Value &jsonMessage);
		
		std::string decryptSubject(std::string subject);
		std::string decryptBody(std::string body);
	};
}

#endif
