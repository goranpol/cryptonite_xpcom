#ifndef _GDocumentListener_h
#define _GDocumentListener_h

#include "HttpListener.h"

#include "Object.h"
#include "GDocument.h"


namespace gdocument
{
	class GDocumentListener : public base::HttpListener
	{
	public:
		GDocumentListener();
		GDocumentListener(base::Object &gDoc);

		NS_DECL_ISUPPORTS
		NS_DECL_NSISTREAMLISTENER
		NS_DECL_NSIREQUESTOBSERVER

	private:
		~GDocumentListener();

		gdocument::GDocument GDocument;
	};
}

#endif