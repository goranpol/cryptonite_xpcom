#ifndef _DriveFile_h
#define _DriveFile_h

#include "Object.h"
#include <string>

namespace base
{
	class DriveFile : public base::Object
	{
	public:
		DriveFile(const int type);
		DriveFile(const std::string objID, const std::string objKey, const std::string checkWord, const std::string initChain, const int type);
		DriveFile(const Object &obj);
		DriveFile(DriveFile &obj);
		virtual ~DriveFile();

		DriveFile &operator=(Object &obj);
		DriveFile &operator=(DriveFile &obj);
		bool operator==(DriveFile &obj);
		bool operator!=(DriveFile &obj);

		std::string encryptOutgoingString(std::string data);
		std::string decryptIncomingString(std::string data);

		std::string getObjectInitChain();
		std::string getObjectCheckWord();

		json::Value &toJson() const;

	protected:

		std::string generateCheckWord();

		std::string getInitChain(const base::Object &obj);
		std::string getCheckWord(const base::Object &obj);

		std::string encryptText(std::string textString);
		std::string decryptText(std::string cipherTextString);
		std::string encrypt(std::string textString);
		std::string decrypt(std::string cipherTextString);

		/* DRIVEFILE MEMBERS */
		const std::string objectInitChain;
		const std::string objectCheckWord;

	private:

	};
}

#endif
