#ifndef _Attachment_h_
#define _Attachment_h_

#include "Object.h"
#include <string>

namespace base
{
	class Attachment : public base::Object
	{
	public:
		Attachment(const std::string objId, const std::string objKey, const std::string objInitChain, const std::string objCheckWord, const int objType);
		Attachment(const Object &obj);
		Attachment(Attachment &obj);
		virtual ~Attachment();

		Attachment &operator=(Object &obj);
		Attachment &operator=(Attachment &obj);
		bool operator==(Attachment &obj);
		bool operator!=(Attachment &obj);

		std::string encryptOutgoingString(std::string data);
		std::string decryptIncomingString(std::string data);

		std::string getObjectInitChain();
		std::string getObjectCheckWord();

		json::Value &toJson() const;

	protected:
		std::string getInitChain(const base::Object &obj);
		std::string getCheckWord(const base::Object &obj);

		std::string generateCheckWord();

		std::string encryptText(std::string textString);
		std::string decryptText(std::string cipherTextString);
		std::string encrypt(std::string textString);
		std::string decrypt(std::string cipherTextString);

		/* ATTACHMENT MEMBERS */
		const std::string objectInitChain;
		const std::string objectCheckWord;
	};
}

#endif
