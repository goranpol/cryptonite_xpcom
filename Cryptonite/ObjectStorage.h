#ifndef _ObjectStorage_h_
#define _ObjectStorage_h_

#include "SessionInfo.h"
#include "ObjectList.h"
#include "Object.h"
#include "UsersList.h"
#include "ObjectRequest.h"

#include "Test.h"
#include <memory>

namespace base
{
	class ObjectStorage
	{
	public:
		static ObjectStorage &getInstance();

		void addObject(const Object &obj, const UsersList &users);
		void addObjects(ObjectList &objs, std::vector<UsersList> users);
		Object &getObject(const ObjectRequest &objReq, const int objType);
		ObjectList &getObjects(std::vector<ObjectRequest> objReqs, const int objType);
		void removeObject(const ObjectRequest &objReq, const int objType);
		void removeObjects(const std::vector<ObjectRequest> objReqs, const int objType);
		void changeAccess(ObjectRequest objReq, UsersList &usrList, const std::string type, const int objType);
		void changeAccess(std::vector<ObjectRequest> objReqs, UsersList &usrList, const std::string type, const int objType);

		/* thoose are helpers for GMail objects */
		void addTempObject(Object &obj);
		void removeTempObject(const std::string reqId);
		Object &getTempObject(const std::string reqId, int objType);

		friend char* ::CLogin(const char * username, const char * password);
		friend void ::CLogout();

	private:
		static ObjectStorage *mObjectStorage;

		ObjectStorage(std::string sessKey);
		~ObjectStorage();

		void saveKey(const Object &obj, UsersList userList);
		void saveKeys(ObjectList &objList, std::vector<UsersList> usrsList);
		Object &getKey(const ObjectRequest &objReq, const int objType);
		ObjectList &getKeys(std::vector<ObjectRequest> objReqs, const int objType);
		void deleteKey(const ObjectRequest &objReq, const int objType);
		void deleteKeys(const std::vector<ObjectRequest> objReqs, const int objType);
		void changeUsersWithAccess(ObjectRequest objReq, UsersList &usrList, const std::string type, const int objType);
		void changeUsersWithAccess(std::vector<ObjectRequest> objReqs, UsersList &usrList, const std::string type, const int objType);

		std::vector<std::string> split(const std::string str, const std::string delimiters = " ");

		/* OBJECT STORAGE MEMBERS */
		ObjectList objects;
		ObjectList tempObjects;
		std::vector<std::pair<std::string, int>> notEncryptedObjects;
		SessionInfo session;

		std::shared_ptr<ObjectList> returnObjects;
		std::shared_ptr<Object> returnObject;
	};
}

#endif
