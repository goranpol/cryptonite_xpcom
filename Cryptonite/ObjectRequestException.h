#ifndef _ObjectRequestException_h_
#define _ObjectRequestException_h_

#include <exception>

namespace exception
{
	namespace domain
	{
		class ObjectRequestException : public std::exception
		{
		public:
			ObjectRequestException();
			virtual ~ObjectRequestException() throw();

			const char* what() const throw();
		};
	}
}

#endif
