#include <mozilla/Char16.h>
#include "nsStringAPI.h"

#include "JsonError.h"

json::JsonError::JsonError()
{
}

json::JsonError::~JsonError()
{
}

const char* json::JsonError::what() const throw()
{
	std::string returnString = "Json error: details unknown";
	return fromStdString(returnString);
}

char* json::JsonError::fromStdString(const std::string str) const
{
	char *cStr = (char*)NS_Alloc(sizeof(char) * str.size() + 1);
	memset(cStr, 0, str.size() + 1);
	
	char *tempBuf = cStr;
	for (size_t i = 0; i<str.size(); i++)
	{
		*tempBuf = str[i];
		tempBuf++;
	}

	return cStr;
}
