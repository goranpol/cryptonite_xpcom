#include <mozilla/Char16.h>

#include "HttpListener.h"

#include "nsIBinaryInputStream.h"
#include "nsIBinaryOutputStream.h"
#include "nsIStorageStream.h"
#include "nsIComponentManager.h"
#include "nsStringAPI.h"
#include "nsISeekableStream.h"
#include "nsIStringStream.h"
#include "nsIScriptableInputStream.h"

#include "nsIAsyncInputStream.h"

#include "Logger.h"

#include "NSException.h"

NS_IMPL_ISUPPORTS(base::HttpListener, nsIRequestObserver, nsIStreamListener)

static const std::string base64Chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
										"abcdefghijklmnopqrstuvwxyz"
										"0123456789-_";

base::HttpListener::HttpListener()
{
}

base::HttpListener::~HttpListener()
{
}

NS_IMETHODIMP base::HttpListener::OnDataAvailable(nsIRequest *aRequest, nsISupports *aContext, nsIInputStream *aInputStream, uint64_t aOffset, uint32_t aCount)
{
	std::string action = "receiving response data";
	nsresult rv;

	nsCOMPtr<nsIComponentManager> componentManager;
	rv = NS_GetComponentManager(getter_AddRefs(componentManager));
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	nsCOMPtr<nsIBinaryInputStream> binaryInputStream;
	rv = componentManager->CreateInstanceByContractID("@mozilla.org/binaryinputstream;1", nullptr, NS_GET_IID(nsIBinaryInputStream), getter_AddRefs(binaryInputStream));
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	rv = binaryInputStream->SetInputStream(aInputStream);
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	mData = (char*)NS_Realloc((void*)mData, sizeof(char) * (aCount + mRead));
	if(!mData)
	{
		throw exception::technical::NSException(rv, action + " !!!response data empty!!!");
	}
	
	char* readData = (char*)NS_Alloc(sizeof(char) * aCount);
	if(!readData)
	{
		throw exception::technical::NSException(rv, action + " !!!response data empty!!!");
	}

	rv = binaryInputStream->ReadBytes(aCount, &readData);
	if (NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	memcpy(mData + mRead, readData, aCount);

	mRead += aCount;
	NS_Free(readData);

	return NS_OK;
}

NS_IMETHODIMP base::HttpListener::OnStartRequest(nsIRequest *aRequest, nsISupports *aContext)
{
	std::string action = "starting response";
	nsresult rv;
	
	mData = 0;
	mRead = 0;
	
	rv = mListener->OnStartRequest(aRequest, aContext);
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}
	return NS_OK;
}

NS_IMETHODIMP base::HttpListener::OnStopRequest(nsIRequest *aRequest, nsISupports *aContext, nsresult aStatusCode)
{
	std::string action = "ending response";
	nsresult rv;
	
	nsCOMPtr<nsIComponentManager> componentManager;
	rv = NS_GetComponentManager(getter_AddRefs(componentManager));
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	nsCOMPtr<nsIStorageStream> storageStream;
	rv = componentManager->CreateInstanceByContractID("@mozilla.org/storagestream;1", nullptr, NS_GET_IID(nsIStorageStream), getter_AddRefs(storageStream));
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	rv = storageStream->Init(8192, mDecryptedRead);
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	nsCOMPtr<nsIBinaryOutputStream> binaryOutputStream;
	rv = componentManager->CreateInstanceByContractID("@mozilla.org/binaryoutputstream;1", nullptr, NS_GET_IID(nsIBinaryOutputStream), getter_AddRefs(binaryOutputStream));
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	nsCOMPtr<nsIOutputStream> outputStream;
	rv = storageStream->GetOutputStream(int32_t(0), getter_AddRefs(outputStream));
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}
	
	rv = binaryOutputStream->SetOutputStream(outputStream);
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	rv = binaryOutputStream->WriteBytes(mDecryptedData, mDecryptedRead);
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	nsCOMPtr<nsIInputStream> inputStream;
	rv = storageStream->NewInputStream(int32_t(0), getter_AddRefs(inputStream));
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	/* call OnDataAvailable */
	rv = mListener->OnDataAvailable(aRequest, aContext, inputStream, 0, mDecryptedRead);
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	/* call OnStopRequest */
	rv = mListener->OnStopRequest(aRequest, aContext, aStatusCode);
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	if (mData != nullptr)
		NS_Free(mData);
	if (mDecryptedData != nullptr)
		NS_Free(mDecryptedData);
	
	return NS_OK;
}

nsIStreamListener *base::HttpListener::getMListener()
{
	return mListener;
}

void base::HttpListener::setMListener(nsIStreamListener *listener)
{
	mListener = listener;
}

bool base::HttpListener::isBase64(const std::string input)
{
	for (size_t i = 0; i < input.size(); i++)
	{
		if (isalnum(input[i]) == 0 && input[i] != '-' && input[i] != '_')
			return false;
	}

	return true;
}
