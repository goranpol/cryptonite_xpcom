#include <mozilla/Char16.h>

#include "MSWordObserver.h"

NS_IMPL_ISUPPORTS(msword::MSWordObserver, nsIObserver)

msword::MSWordObserver::MSWordObserver(bool encrypt) : HttpObserver(encrypt)
{
}

msword::MSWordObserver::~MSWordObserver()
{
}

NS_IMETHODIMP msword::MSWordObserver::Observe(nsISupports *aSubject, const char *aTopic, const char16_t *aData)
{
	return NS_OK;
}
