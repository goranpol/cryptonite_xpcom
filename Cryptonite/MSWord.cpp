#include <mozilla/Char16.h>

#include "MSWord.h"


msword::MSWord::MSWord() : base::Object(NITE_WORD, "", generateKey())
{
}

msword::MSWord::~MSWord()
{
}

msword::MSWord &msword::MSWord::operator=(base::Object &obj)
{
	msword::MSWord *word = dynamic_cast<msword::MSWord*>(&obj);
	if(word != NULL)
		return *word;
	else
		throw std::exception();
}

bool msword::MSWord::operator==(base::Object &obj)
{
	return false;
}

bool msword::MSWord::operator!=(base::Object &obj)
{
	return !(*this == obj);
}

std::string msword::MSWord::encryptOutgoingString(std::string data)
{
	return data;
}

std::string msword::MSWord::decryptIncomingString(std::string data)
{
	return data;
}

void msword::MSWord::changeUsersWithAccess(std::string data)
{
}

std::string msword::MSWord::encryptText(std::string textString)
{
	throw std::exception();
}

std::string msword::MSWord::decryptText(std::string cipherTextString)
{
	throw std::exception();
}

std::string msword::MSWord::encrypt(std::string textString)
{
	throw std::exception();
}

std::string msword::MSWord::decrypt(std::string cipherTextString)
{
	throw std::exception();
}
