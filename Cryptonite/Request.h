#ifndef _Request_h_
#define _Request_h_

#include <string>

namespace network
{
	class Request
	{
	public:
		Request(const std::string mess);
		~Request();

		std::string sendRequest();
		void sendAsyncRequest();

	private:

		const std::string message;
	};
}

#endif