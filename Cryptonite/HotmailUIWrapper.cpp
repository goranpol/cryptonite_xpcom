#include <mozilla/Char16.h>

#include "HotmailUIWrapper.h"

#include "Hotmail.h"
#include "ObjectStorage.h"
#include "GUIObserver.h"

#include "ObjectRequest.h"
#include "Object.h"
#include "ObjectList.h"

#include "JsonError.h"
#include "JsonTypeError.h"
#include "JsonParsingError.h"
#include "JsonOutOfRange.h"

#include "GUIObserver.h"

#include "Logger.h"
#include <sstream>

#include "ObjectNotFoundException.h"
#include "AlgorithmException.h"
#include "ObjectRequestException.h"

#include "EscapeEncoding.h"

#include "Logger.h"

hotmail::HotmailUIWrapper::HotmailUIWrapper()
{
}

hotmail::HotmailUIWrapper::~HotmailUIWrapper()
{
}

std::string hotmail::HotmailUIWrapper::encryptOutgoingString(std::string data, int encryptType)
{
	data = base::EscapeEncoding::decode(data);
	if (encryptType == NITE_draft)
		return encryptDraft(data);
	else if (encryptType != NITE_mail)
		throw std::exception();

	// if na 10 mestu != empty
	// if na 10 mestu != "mgAAAAAAAAAAAAAAAAAAAAAA2"
	// 10 mesto = reply id
	// if 8 mesto != empty
	// 8 mesto = draft id

	std::vector<std::string> splittedTemp = split(data, "&");
	std::vector<std::string> splittedData;
	for(size_t i=0; i<splittedTemp.size(); i++)
	{
		if (splittedTemp[i].find("cn=") == 0)
			splittedData.push_back(splittedTemp[i] + "&");
		else if (splittedTemp[i].find("mn=") == 0)
			splittedData.push_back(splittedTemp[i] + "&");
		else if (splittedTemp[i].find("d=") == 0)
			splittedData.push_back(splittedTemp[i] + "&");
		else if (splittedTemp[i].find("v=") == 0)
			splittedData.push_back(splittedTemp[i] + "&");
		else
			splittedData[splittedData.size()-1] += splittedTemp[i] + "&";
	}
	size_t size = splittedData.size();
	splittedData[size - 1] = splittedData[size - 1].substr(0, splittedData[size - 1].size() - 1);

	std::string returnString = "";
	for (size_t i = 0; i<splittedData.size(); i++)
	{
		if (splittedData[i].find("d=") == 0)
		{
			try
			{
				std::string replyEmailId = getReplyToMail(splittedData[i]);
				std::string draftId = "";
				
				try
				{
					draftId = getDraftId(splittedData[i]);
				}
				catch(...)
				{
				}

				// add attachments
				hotmail::Hotmail tmp(base::ObjectStorage::getInstance().getObject(base::ObjectRequest(replyEmailId), NITE_HOTMAIL));
				hotmail::Hotmail hotMail(hotmail::Hotmail("-1", tmp.objectKey, tmp.objectInitChain, tmp.objectCheckWord, draftId, getAttachments(splittedData[i])));
				returnString += hotMail.encryptOutgoingString(splittedData[i]) + "&";
				base::ObjectStorage::getInstance().addTempObject(hotMail);

				gui::GUIObserver::getInstance().notifyObserver(NITE_ENCRYPT, "hotmail mail");

				// na sedmem mestu
			}	
			catch(...)
			{
				std::string draftId = "";
				try
				{
					draftId = getDraftId(splittedData[i]);
					try
					{
						base::ObjectStorage::getInstance().getObject(base::ObjectRequest(draftId), NITE_HOTMAIL);
					}
					catch(...)
					{
						return data;
					}
				}
				catch(...)
				{
				}

				hotmail::Hotmail hotMail(draftId, getAttachments(splittedData[i]));
				returnString += hotMail.encryptOutgoingString(splittedData[i]) + "&";

				base::ObjectStorage::getInstance().addTempObject(hotMail);

				gui::GUIObserver::getInstance().notifyObserver(NITE_ENCRYPT, "hotmail mail");
			}
		}
		else
			returnString += splittedData[i] + "&";
	}

	returnString = returnString.substr(0, returnString.size() - 1);
	return returnString;
}

std::string hotmail::HotmailUIWrapper::decryptIncomingString(std::string data, const int decryptType)
{
	std::string returnString = data;
	try
	{
		if (decryptType == NITE_decryptHtml)
		{
			if(data.find("ol.initialData") == -1)
				return data;

			std::vector<std::string> splitedHTML = split(data, "ol.initialData");
			// if splitedHTML.length() != 2 throw error
			// also check if find return != -1
			returnString = splitedHTML[0] + "ol.initialData" + splitedHTML[1].substr(0, splitedHTML[1].find("return"));
			splitedHTML[1] = splitedHTML[1].substr(splitedHTML[1].find("return"));
			json::JSON jsonData = json::JSON(splitedHTML[1]);
			decryptArray(jsonData.get());
			returnString += jsonData.print() + splitedHTML[1];
		}
		else if (decryptType == NITE_decryptJavascript)
		{
			json::JSON jsonData = json::JSON(data);
			if(!jsonData.checkName("new HM.FppReturnPackage"))
			{
				return returnString;
			}

			decryptArray(jsonData[1]);
			returnString = jsonData.print();
		}
		else if (decryptType == NITE_decryptCompose)
		{
			json::JSON jsonData = json::JSON(data);
			if(!jsonData.checkName("new HM.FppReturnPackage"))
			{
				return returnString;
			}

			decryptComposeData(jsonData.get());
			returnString = jsonData.print();
		}
		else
		{
			// throw exception
			Logger::logMessage("", "", "else error");
			return returnString;
		}

		return returnString;
	}
	catch(json::JsonError &je)
	{
		Logger::errorMessage("HotmailUIWrapper", "decryptIncomingString", (char*)je.what(), nsresult(1));
		return returnString;
	}
	catch (...)
	{
		Logger::errorMessage("HotmailUIWrapper", "decryptIncomingString", "...", nsresult(2));
		return returnString;
	}
}

std::string hotmail::HotmailUIWrapper::encryptDraft(std::string data)
{
	// if na 14 mestu != empty
	// if na 14 mestu != "mgAAAAAAAAAAAAAAAAAAAAAA2"
	// 14 mesto = reply id
	// if 13 mesto != empty
	// 13 mesto = draft id
	std::vector<std::string> splittedTemp = split(data, "&");
	std::vector<std::string> splittedData;
	for (size_t i = 0; i<splittedTemp.size(); i++)
	{
		if (splittedTemp[i].find("cn=") == 0)
			splittedData.push_back(splittedTemp[i] + "&");
		else if (splittedTemp[i].find("mn=") == 0)
			splittedData.push_back(splittedTemp[i] + "&");
		else if (splittedTemp[i].find("d=") == 0)
			splittedData.push_back(splittedTemp[i] + "&");
		else if (splittedTemp[i].find("v=") == 0)
			splittedData.push_back(splittedTemp[i] + "&");
		else
			splittedData[splittedData.size()-1] += splittedTemp[i] + "&";
	}
	size_t size = splittedData.size();
	splittedData[size - 1] = splittedData[size - 1].substr(0, splittedData[size - 1].size() - 1);

	std::string returnString = "";
	for (size_t i = 0; i<splittedData.size(); i++)
	{
		if (splittedData[i].find("d=") == 0)
		{
			try
			{
				std::string replyEmailId = getReplyToMailDraft(splittedData[i]);
				std::string draftId = "";
				
				try
				{
					draftId = getDraftIdDraft(splittedData[i]);
				}
				catch(...)
				{
				}

				hotmail::Hotmail tmp(base::ObjectStorage::getInstance().getObject(base::ObjectRequest(replyEmailId), NITE_HOTMAIL));

				hotmail::Hotmail hotMail("-1", tmp.objectKey, tmp.objectInitChain, tmp.objectCheckWord, draftId);
				returnString += hotMail.encryptDraft(splittedData[i]) + "&";
				base::ObjectStorage::getInstance().addTempObject(hotMail);

				gui::GUIObserver::getInstance().notifyObserver(NITE_ENCRYPT, "hotmail mail");
			}	
			catch(...)
			{
				std::string draftId = "";
				try
				{
					draftId = getDraftIdDraft(splittedData[i]);

					try
					{
						base::ObjectStorage::getInstance().getObject(base::ObjectRequest(draftId), NITE_HOTMAIL);
					}
					catch(...)
					{
						return data;
					}
				}
				catch(...)
				{
				}

				hotmail::Hotmail hotMail(draftId);
				returnString += hotMail.encryptDraft(splittedData[i]) + "&";
				base::ObjectStorage::getInstance().addTempObject(hotMail);

				gui::GUIObserver::getInstance().notifyObserver(NITE_ENCRYPT, "hotmail mail");
			}
		}
		else
			returnString += splittedData[i] + "&";
	}

	returnString = returnString.substr(0, returnString.size() - 1);
	return returnString;
}

void hotmail::HotmailUIWrapper::deleteMails(std::string data)
{
	try
	{
		std::vector<std::string> splittedData = split(data, "&");
		std::vector<base::ObjectRequest> objReqs;
		
		if (splittedData[2].find("d=") != 0)
			return;

		std::string d = splittedData[2].substr(2);
		d = "[" + d + "]";
		d = replace(d, "{", "[");
		d = replace(d, "}", "]");

		json::JSON dJson = json::JSON(d);
		if (dJson.getString(size_t(0)).compare("fltrash") != 0)
			return;

		for (size_t i = 0; i<dJson[1].size(); i++)
		{
			if(dJson[3][i].getString(1).compare("fltrash") == 0)
				objReqs.push_back(getId(dJson[1].getString(i)));
		}

		if(objReqs.empty())
			return;
		else if(objReqs.size() == 1)
			base::ObjectStorage::getInstance().removeObject(objReqs[0], NITE_HOTMAIL);
		else
			base::ObjectStorage::getInstance().removeObjects(objReqs, NITE_HOTMAIL);
	}
	catch(...)
	{
	}
}

void hotmail::HotmailUIWrapper::decryptComposeData(json::Value &jsonData)
{
	try
	{
		for (size_t i = 0; i<jsonData[1].size(); i++)
		{
			if(!jsonData[1][i].checkName("new HM.ComposeData"))
				break;

			// 4 body
			// 5 subject
			// 10 draft id
			// 12 reply mail id
			try
			{
				// Decrypt subject

				// Check if subject string contains a checkword.
				std::string chWd = encryptCheck(jsonData[1][i].getString(5));
				
				// Get object from storage with mail id and checkword.
				base::ObjectRequest objReq = base::ObjectRequest(getId(jsonData[1][i].getString(12)), chWd);
				hotmail::Hotmail hotMail(base::ObjectStorage::getInstance().getObject(objReq, NITE_HOTMAIL));

				// Decrypt the subject and replace it in json.
				// Replace '\x2a' with '*' because decryptSubject expects it.
				jsonData[1][i].replace(5, hotMail.decryptSubject(replace(jsonData[1][i].getString(5), "\\x2a", "*")));

				// Decrypt body
				std::string decryptedBody = "";
				std::string body = replace(jsonData[1][i].getString(4), "\\x2a", "*");

				std::vector<std::string> bodySplitted = split(body, "*");
				decryptedBody = bodySplitted[0];

				for(size_t j=1; j<bodySplitted.size(); j++)
				{
					try
					{
						if (bodySplitted[j].compare(hotMail.objectCheckWord) == 0)
							continue;

						if (bodySplitted[j].find("\\x") != -1)
						{
							decryptedBody += bodySplitted[j];
							continue;
						}

						decryptedBody += hotMail.decryptText(bodySplitted[j]);
					}
					catch(...)
					{
						decryptedBody += bodySplitted[j];
					}
				}

				std::vector<std::string> splitedDecryptedBody = split(decryptedBody, "\"");
				decryptedBody = splitedDecryptedBody[0];
				for(size_t j=1; j<splitedDecryptedBody.size(); j++)
				{
					if(splitedDecryptedBody[j-1][splitedDecryptedBody[j-1].size()-1] == '\\')
						decryptedBody += "\"" + splitedDecryptedBody[j];
					else
						decryptedBody += "\\\"" + splitedDecryptedBody[j];
				}

				decryptedBody = replace(decryptedBody, "\n", "<br>");
				jsonData[1][i].replace(4, decryptedBody);
			}
			catch(...)
			{
				continue;
			}
		}
	}
	catch(...)
	{
		return;
	}
}

void hotmail::HotmailUIWrapper::getSentDraftObjectID(std::string data)
{
	try
	{
		hotmail::Hotmail hotMail = static_cast<hotmail::Hotmail&>(base::ObjectStorage::getInstance().getTempObject("-1", NITE_HOTMAIL));
		json::JSON response = json::JSON(data);
		hotmail::Hotmail hotMailNew(getId(response[1][(size_t)0].getString(1)), hotMail.objectKey, hotMail.objectInitChain, hotMail.objectCheckWord, hotMail.objectDraftId);
		base::UsersList users;
		base::ObjectStorage::getInstance().addObject(hotMailNew, users);
		base::ObjectStorage::getInstance().removeTempObject("-1");
	}
	catch(...)
	{
		throw exception::domain::AlgorithmException(NITE_GMAIL, encryption);
	}
}

void hotmail::HotmailUIWrapper::getSentMail()
{
	try
	{
		hotmail::Hotmail hotMail = static_cast<hotmail::Hotmail&>(base::ObjectStorage::getInstance().getTempObject("-1", NITE_HOTMAIL));
		base::UsersList users = hotMail.users;
		base::ObjectStorage::getInstance().addObject(hotMail, users);
		base::ObjectStorage::getInstance().removeTempObject("-1");
	}
	catch (...)
	{
		throw exception::domain::AlgorithmException(NITE_GMAIL, encryption);
	}
}

std::string hotmail::HotmailUIWrapper::getReplyToMail(std::string data)
{
	// 10 reply id
	// 8 draft id

	data = "[" + data.substr(2, data.size() - 3) + "]";
	data = replace(data, "{", "[");
	data = replace(data, "}", "]");
	json::JSON dataJson(data);

	if (dataJson.getString(10).compare("") == 0 || dataJson.getString(10).compare("mgAAAAAAAAAAAAAAAAAAAAAA2") == 0)
		throw std::exception();
	else
		return dataJson.getString(10).substr(2);
}

std::string hotmail::HotmailUIWrapper::getDraftId(std::string data)
{
	// 10 reply id
	// 8 draft id

	data = "[" + data.substr(2, data.size() - 3) + "]";
	data = replace(data, "{", "[");
	data = replace(data, "}", "]");
	json::JSON dataJson(data);

	if (dataJson.getString(8).compare("") == 0)
		throw std::exception();
	else
		return dataJson.getString(8).substr(2);
}

std::string hotmail::HotmailUIWrapper::getReplyToMailDraft(std::string data)
{
	// 14 reply id
	// 13 draft id

	data = "[" + data.substr(2, data.size() - 3) + "]";
	data = replace(data, "{", "[");
	data = replace(data, "}", "]");
	json::JSON dataJson(data);

	if (dataJson.getString(14).compare("") == 0 || dataJson.getString(14).compare("mgAAAAAAAAAAAAAAAAAAAAAA2") == 0)
		throw std::exception();
	else
		return dataJson.getString(14).substr(2);
}

std::string hotmail::HotmailUIWrapper::getDraftIdDraft(std::string data)
{
	// 14 reply id
	// 13 draft id

	data = "[" + data.substr(2, data.size() - 3) + "]";
	data = replace(data, "{", "[");
	data = replace(data, "}", "]");
	json::JSON dataJson(data);

	if (dataJson.getString(13).compare("") == 0 || dataJson.getString(13).compare("mgAAAAAAAAAAAAAAAAAAAAAA2") == 0)
		throw std::exception();
	else
		return dataJson.getString(13).substr(2);
}

std::vector<std::string> hotmail::HotmailUIWrapper::getAttachments(std::string data)
{
	std::vector<std::string> returnVector;
	data = "[" + data.substr(2, data.size() - 3) + "]";
	data = replace(data, "{", "[");
	data = replace(data, "}", "]");
	json::JSON dataJson(data);

	try
	{
		for (size_t i = 0; i < dataJson[7].size(); i++)
		{
			std::vector<std::string> tmp = split(dataJson[7].getString(i), "\\|");
			returnVector.push_back(tmp[2]);
		}
	}
	catch (...)
	{
	}

	return returnVector;
}

void hotmail::HotmailUIWrapper::decryptArray(json::Value &jsonArray)
{
	std::vector<base::ObjectRequest> objReqs;
	std::vector<std::pair<size_t, size_t>> indexes;

	try
	{
		for (size_t i = 0; i<jsonArray.size(); i++)
		{
			try
			{
				if(jsonArray[i].checkName("new HM.ItemListData"))
				{
					if (jsonArray[i].getInt(size_t(0)) == 0)
					{
						if(jsonArray[i][2].empty())
							continue;

						for (size_t j = 0; j<jsonArray[i][2].size(); j++)
						{
							try
							{
								if(!jsonArray[i][2][j].checkName("new HM.RollupData"))
									continue;

								if (!jsonArray[i][2][j][(size_t)0].checkName("new HM.Rollup"))
									continue;

								std::string checkWord = encryptCheck(jsonArray[i][2][j][(size_t)0].getString(9));
								if (checkWord.compare(encryptCheck(jsonArray[i][2][j][(size_t)0].getString(10))) != 0)
								{
									Logger::errorMessage("HotmailUIWrapper", "decryptArray", "ERROR!!! encryptCheck summary error", nsresult(-1));
								}

								// substr 2 because hotmail puts "mg" or "cm" prefixes (maybe more of them) "cv" you save the whole
								objReqs.push_back(base::ObjectRequest(getId(jsonArray[i][2][j][(size_t)0].getString(size_t(0))), checkWord));
								indexes.push_back(std::pair<size_t, size_t>(i, j));
							}
							catch(...)
							{
								continue;
							}
						}
					}
					else if (jsonArray[i].getInt(size_t(0)) == 1)
					{
						try
						{
							if (jsonArray[i][1].getType() != JsonNull)
							{
								if (!jsonArray[i][1].checkName("new HM.RollupData"))
									continue;

								if (!jsonArray[i][1][(size_t)0].checkName("new HM.Rollup"))
									continue;

								std::string checkWord = encryptCheck(jsonArray[i][1][(size_t)0].getString(9));
								if (checkWord.compare(encryptCheck(jsonArray[i][1][(size_t)0].getString(10))) != 0)
								{
									Logger::errorMessage("HotmailUIWrapper", "decryptArray", "ERROR!!! encryptCheck full error", nsresult(-1));
								}

								indexes.push_back(std::pair<size_t, size_t>(i, 0));
								for (size_t j = 0; j < jsonArray[i][1][(size_t)0][2].size(); j++)
								{
									// substr 2 because hotmail puts "mg" or "cm" prefixes (maybe more of them) "cv" you save the whole
									objReqs.push_back(base::ObjectRequest(getId(jsonArray[i][1][(size_t)0][2].getString(j)), checkWord));
								}
							}
							else
							{
								for (size_t j = 0; j < jsonArray[i][2].size(); j++)
								{
									std::string checkWord = encryptCheck(jsonArray[i][2][j][(size_t)0].getString(10));
									// substr 2 because hotmail puts "mg" or "cm" prefixes (maybe more of them) "cv" you save the whole
									objReqs.push_back(base::ObjectRequest(getId(jsonArray[i][2][j][(size_t)0].getString(size_t(0))), checkWord));
									if (checkWord.compare(encryptCheck(jsonArray[i][2][(size_t)0][(size_t)0].getString(11))) != 0)
									{
										Logger::errorMessage("HotmailUIWrapper", "decryptArray", "ERROR!!! encryptCheck full error", nsresult(-1));
									}
								}

								indexes.push_back(std::pair<size_t, size_t>(i, 0));
							}
						}
						catch(...)
						{
							continue;
						}
					}
				}
			}
			catch(...)
			{
				continue;
			}
		}
		
		if(indexes.empty())
		{
			return;
		}

		base::ObjectList hotmails;
		try
		{
			if (!objReqs.empty())
				hotmails = base::ObjectStorage::getInstance().getObjects(objReqs, NITE_HOTMAIL);
		}
		catch (...)
		{
			Logger::logMessage("HotmailUIWrapper", "decryptArray", "Error getting keys from server");
		}

		if(hotmails.empty())
		{
			// throw some exception
			return;
		}

		for (size_t i = 0; i < indexes.size(); i++)
		{
			if (jsonArray[indexes[i].first].getInt(size_t(0)) == 0)
			{
				try
				{
					// substr 2 because hotmail puts "mg" or "cm" prefixes (maybe more of them) "cv" you save the whole
					Hotmail hotMail(hotmails[getId(jsonArray[indexes[i].first][2][indexes[i].second][(size_t)0].getString(size_t(0)))]);
					hotMail.decryptRollup(jsonArray[indexes[i].first][2][indexes[i].second][(size_t)0]);
				}
				catch (...)
				{
					continue;
				}
			}
			else if (jsonArray[indexes[i].first].getInt(size_t(0)) == 1)
			{
				try
				{
					// substr 2 because hotmail puts "mg" or "cm" prefixes (maybe more of them) "cv" you save the whole
					Hotmail hotMail(hotmails[getId(jsonArray[indexes[i].first][2][indexes[i].second][(size_t)0].getString(size_t(0)))]);
					hotMail.decryptRollup(jsonArray[indexes[i].first][1][(size_t)0]);
					
					for (size_t j = size_t(0); j < jsonArray[indexes[i].first][2].size(); j++)
					{
						try
						{
							hotMail = hotmails[getId(jsonArray[indexes[i].first][2][j][(size_t)0].getString(size_t(0)))];
							hotMail.decryptMessage(jsonArray[indexes[i].first][2][j][(size_t)0]);
						}
						catch (...)
						{
						}
					}
				}
				catch (...)
				{
					for (size_t j = 0; j < jsonArray[indexes[i].first][2].size(); j++)
					{
						try
						{
							Hotmail hotMail(hotmails[getId(jsonArray[indexes[i].first][2][j][(size_t)0].getString(size_t(0)))]);
							hotMail.decryptMessage(jsonArray[indexes[i].first][2][j][(size_t)0]);
						}
						catch (...)
						{
						}
					}
				}
			}
		}
	}
	catch(...)
	{
		Logger::logMessage("HotmailUIWrapper", "decryptArray", "error outer catch");
	}
}

std::string hotmail::HotmailUIWrapper::encryptCheck(const std::string subject)
{
	std::string subjectBuffer = subject;

	// this is hard coded use replace(subjectBuffer, "\\x2a", "*");
	// or use replace(subjectBuffer, "\\x26\\x2342\\x3b", "*");
	if(subjectBuffer.find("\\x2a") != -1)
	{
		subjectBuffer = subjectBuffer.substr(subjectBuffer.find("\\x2a") + 4);
		if(subjectBuffer.find("\\x2a") < 27 && subjectBuffer.find("\\x2a") > 13)
			return subjectBuffer.substr(0, subjectBuffer.find("\\x2a"));
	}
	else if(subjectBuffer.find("\\x26\\x2342\\x3b") != -1)
	{
		subjectBuffer = subjectBuffer.substr(subjectBuffer.find("\\x26\\x2342\\x3b") + 14);
		if(subjectBuffer.find("\\x26\\x2342\\x3b") < 27 && subjectBuffer.find("\\x26\\x2342\\x3b") > 13)
			return subjectBuffer.substr(0, subjectBuffer.find("\\x26\\x2342\\x3b"));
	}

	throw std::exception();		// notEncryptedException
}


// substr 2 because hotmail puts "mg" or "cm" prefixes (maybe more of them) "cv" you save the whole
std::string hotmail::HotmailUIWrapper::getId(std::string id)
{
	if (id.size() == 25 && id.find("cv") == 0)
		return id;

	return id.substr(2);
}