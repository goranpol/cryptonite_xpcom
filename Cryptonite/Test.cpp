#include <mozilla/Char16.h>

#include "MasterObserver.h"
#include "nsStringAPI.h"
#include "Logger.h"

#include <string>

#include "Request.h"

#include "Value.h"
#include "Bool.h"
#include "String.h"
#include "Number.h"
#include "Enumeration.h"
#include "Parser.h"

#include "JsonTypeError.h"
#include "JsonTypeNotDefined.h"
#include "JsonBadCast.h"
#include "JsonBadAlloc.h"
#include "JsonKeyNotFound.h"
#include "JsonOutOfRange.h"

#include "Object.h"
#include "GDocument.h"
#include "ObjectStorage.h"

#include "GUIObserver.h"

#include "NSException.h"
#include "SingletoneException.h"

#include "nsMemory.h"

#include"Test.h"

char* CLogin(const char * username, const char * password)
{
	try
	{
		gui::GUIObserver::getInstance().registerObserver();
	}
	catch (exception::technical::NSException)
	{
		Logger::errorMessage("main", "CLogin", "registering GUIObserver FAILED", nsresult(1));
		return "{\"errm\":\"failed to register GUIObserver\",\"errn\":1}";
	}

	// username is username from login
	// password is eather password or persistent cookie from login

	Logger::logMessage("main", "CLogin", "registering MasterObserver START");

	try
	{
		std::string usernameString = username;
		std::string passwordString = password;

		json::JSON loginJson = json::JSON(ObjectType);
		loginJson.pushBack("rty", "li");
		loginJson.pushBackObject("lid");
		loginJson["lid"].pushBack("un", usernameString);
		loginJson["lid"].pushBack("p", passwordString);
		loginJson["lid"].pushBack("psk");

		std::string response = network::Request(loginJson.print()).sendRequest();		// send request fails if redirected
		json::JSON jsonResponse = json::JSON(response);
		try
		{
			std::string errorMessage = jsonResponse.getString("errm");
			int errorNumber = jsonResponse.getInt("errn");

			Logger::errorMessage("main", "CLogin", (char*)errorMessage.c_str(), nsresult(errorNumber));

			// notify javascript

			Logger::logMessage("main", "CLogin", "registering MasterObserver NOT SUCESSESFULL (username, password incorrect)");
			
			// maybe memory leak
			std::string returnString = jsonResponse.print();
			char* _retval = (char*)NS_Alloc(sizeof(char) * returnString.size() + 1);
			memcpy(_retval, returnString.c_str(), returnString.size() + 1);

			return _retval;
		}
		catch (json::JsonKeyNotFound)
		{
			// make it unique_ptr
			base::ObjectStorage::mObjectStorage = new base::ObjectStorage(jsonResponse.getString("sek"));
			try
			{
				base::MasterObserver::mMasterObserver = new base::MasterObserver();
				base::MasterObserver::getInstance().registerObserver();
			}
			catch (exception::technical::NSException)
			{
				Logger::errorMessage("main", "CLogin", "registering MasterObserver FAILED", nsresult(2));
				return "{\"errm\":\"failed to register MasterObserver\",\"errn\":2}";
			}

			Logger::logMessage("main", "CLogin", "registering MasterObserver SUCCEEDED");
			json::JSON returnJson = json::JSON(ObjectType);
			returnJson.pushBack("username", usernameString);
			returnJson.pushBack("ud", jsonResponse["ud"]);
			std::string returnString = returnJson.print();

			// maybe memory leak
			char* _retval = (char*)NS_Alloc(sizeof(char) * returnString.size() + 1);
			memcpy(_retval, returnString.c_str(), returnString.size() + 1);

			return _retval;
		}
	}
	catch (exception::technical::NSException)
	{
		Logger::errorMessage("main", "CLogin", "server error", nsresult(3));
		return "{\"errm\":\"server error\",\"errn\":3}";
	}
	catch (...)
	{
		Logger::logMessage("main", "CLogin", "registering MasterObserver NOT SUCESSESFULL (unknown error)");
		return "{\"errm\":\"unknown error\",\"errn\":4}";
	}
}

void CLogout()
{
	try
	{
		gui::GUIObserver::getInstance().unregisterObserver();
	}
	catch (exception::technical::NSException)
	{
		Logger::errorMessage("main", "CLogout", "unregistering GUIObserver FAILED", nsresult(1));
		return;
	}

	// send logout request

	Logger::logMessage("main", "CLogout", "unregistering MasterObserver START");

	try
	{
		// delete base::ObjectStorage::mObjectStorage;
	}
	catch (exception::technical::SingletoneException)
	{
		Logger::errorMessage("main", "CLogout", "removing ObjectStorage FAILED", nsresult(2));
		return;
	}

	try
	{
		base::MasterObserver::getInstance().unregisterObserver();
	}
	catch (exception::technical::SingletoneException)
	{
		Logger::errorMessage("main", "CLogout", "MasterObserver is NULL", nsresult(3));
		return;
	}
	catch (exception::technical::NSException)
	{
		Logger::errorMessage("main", "CLogout", "unregistering MasterObserver FAILED", nsresult(4));
		return;
	}

	Logger::logMessage("main", "CLogout", "unregistering MasterObserver SUCCEEDED");
}

