#ifndef _GMail_h
#define _GMail_h

#include "UsersList.h"
#include "Mail.h"

namespace gmail
{
	class GMail : public base::Mail
	{
	public:
		GMail(const std::string reqId);
		GMail(const std::string reqId, const std::string objDraftId);
		GMail(const std::string reqId, const std::string objDraftId, std::vector<std::string> objAttachmentIds);
		GMail(const std::string objId, const std::string objKey, const std::string objInitChain, const std::string objCheckWord);
		GMail(const std::string objId, const std::string objKey, const std::string objInitChain, const std::string objCheckWord, const std::string objDraftId);
		GMail(const std::string objId, const std::string objKey, const std::string objInitChain, const std::string objCheckWord, const std::string objDraftId, std::vector<std::string> objAttachmentIds);
		GMail(const Object &obj);
		GMail(const GMail &obj);
		~GMail();

		GMail &operator=(Object &obj);
		GMail &operator=(GMail &obj);
		bool operator==(Object &obj);
		bool operator!=(Object &obj);

		std::string encryptOutgoingString(std::string data);
		std::string decryptIncomingString(std::string data);

		friend class GMailUIWrapper;

	private:

		void decryptCs(json::Value &jsonArray);
		void decryptMs(json::Value &jsonArray);
		void decryptSummary(json::Value &jsonArray);

		std::string decryptSubject(std::string subject);
		std::string decryptBody(std::string body);
	};
}

#endif
