#include <mozilla/Char16.h>

#include "ObjectList.h"
#include "Parser.h"

#include "GDocument.h"
#include "GMail.h"
#include "Hotmail.h"
#include "GMailAttachment.h"
#include "HotmailAttachment.h"
#include "GDriveFile.h"
#include "OneDriveFile.h"
#include "HotmailNew.h"

#include "Logger.h"

#include "ObjectNotFoundException.h"
#include "ObjectTypeException.h"

base::ObjectList::ObjectList()
{
}

base::ObjectList::~ObjectList()
{
}

base::ObjectList &base::ObjectList::operator=(const base::ObjectList &objList)
{
	if (this == &objList)
		return *this;

	objectMembers = objList.objectMembers;

	return *this;
}

base::Object &base::ObjectList::operator[](const size_t index)
{
	if (objectMembers.empty())
		throw exception::domain::ObjectNotFoundException("size_t (no id)");
	if(index > objectMembers.size() - 1)
		throw exception::domain::ObjectNotFoundException("size_t (no id)");

	return *objectMembers[index];
}

base::Object &base::ObjectList::operator[](const std::string ooid)
{
	if (objectMembers.empty())
		throw exception::domain::ObjectNotFoundException(ooid);

	for (size_t i = 0; i < objectMembers.size(); i++)
	{
		if (objectMembers[i]->getObjectId().compare(ooid) == 0)
			return *objectMembers[i];
	}
	
	throw exception::domain::ObjectNotFoundException(ooid);
}

base::ObjectList &base::ObjectList::operator+(base::ObjectList &objList)
{
	if(objList.empty())
		return *this;

	ObjectList objectList = *this;
	objectList += objList;

	return objectList;
}

base::ObjectList &base::ObjectList::operator+=(base::ObjectList &objList)
{
	if(objList.empty())
		return *this;

	for (size_t i = 0; i < objList.size(); i++)
		pushBack(objList[i]);

	return *this;
}

base::Object &base::ObjectList::get(const size_t index)
{
	return (*this)[index];
}

base::Object &base::ObjectList::get(const std::string ooid)
{
	return(*this)[ooid];
}

void base::ObjectList::replace(const size_t index, base::Object &obj)
{
	if (objectMembers.empty())
		throw exception::domain::ObjectNotFoundException(obj.getObjectId());
	if(index > objectMembers.size() - 1)
		throw exception::domain::ObjectNotFoundException(obj.getObjectId());

	remove(index);
	insert(index, obj);
}

void base::ObjectList::insert(const size_t index, base::Object &obj)
{
	if(index > objectMembers.size() - 1)
		throw exception::domain::ObjectNotFoundException(obj.getObjectId());

	if (obj.getObjectType() == NITE_GDOC)
	{
		objectMembers.insert(objectMembers.begin() + index, std::make_shared<gdocument::GDocument>(obj.getObjectId(), obj.getObjectKey()));
	}
	else if (obj.getObjectType() == NITE_GMAIL)
	{
		gmail::GMail gMail = static_cast<gmail::GMail&>(obj);
		objectMembers.insert(objectMembers.begin() + index, std::make_shared<gmail::GMail>(gMail));
	}
	else if (obj.getObjectType() == NITE_HOTMAIL)
	{
		hotmail::Hotmail hotmail = static_cast<hotmail::Hotmail&>(obj);
		objectMembers.insert(objectMembers.begin() + index, std::make_shared<hotmail::Hotmail>(hotmail));
	}
	else if (obj.getObjectType() == NITE_GATTACH)
	{
		gmail::GMailAttachment gAttachment = static_cast<gmail::GMailAttachment&>(obj);
		objectMembers.insert(objectMembers.begin() + index, std::make_shared<gmail::GMailAttachment>(gAttachment));
	}
	else if (obj.getObjectType() == NITE_HOTATTACH)
	{
		hotmail::HotmailAttachment hotmailAttachment = static_cast<hotmail::HotmailAttachment&>(obj);
		objectMembers.insert(objectMembers.begin() + index, std::make_shared<hotmail::HotmailAttachment>(hotmailAttachment));
	}
	else if (obj.getObjectType() == NITE_GDRIVEFILE)
	{
		gdrive::GDriveFile gDriveFile = static_cast<gdrive::GDriveFile&>(obj);
		objectMembers.insert(objectMembers.begin() + index, std::make_shared<gdrive::GDriveFile>(gDriveFile));
	}
	else if (obj.getObjectType() == NITE_ONEDRIVEFILE)
	{
		onedrive::OneDriveFile oneDriveFile = static_cast<onedrive::OneDriveFile&>(obj);
		objectMembers.insert(objectMembers.begin() + index, std::make_shared<onedrive::OneDriveFile>(oneDriveFile));
	}
	else if (obj.getObjectType() == NITE_HOTMAILNEW)
	{
		hotmail::HotmailNew hotMailNew = static_cast<hotmail::HotmailNew&>(obj);
		objectMembers.insert(objectMembers.begin() + index, std::make_shared<hotmail::HotmailNew>(hotMailNew));
	}
	// others
	else
		throw exception::domain::ObjectTypeException(obj.getObjectType());
}

void base::ObjectList::pushBack(const base::Object &obj)
{
	if (obj.getObjectType() == NITE_GDOC)
	{
		objectMembers.push_back(std::make_shared<gdocument::GDocument>(obj.getObjectId(), obj.getObjectKey()));
	}
	else if (obj.getObjectType() == NITE_GMAIL)
	{
		const gmail::GMail gMail = static_cast<const gmail::GMail&>(obj);
		objectMembers.push_back(std::make_shared<gmail::GMail>(gMail));
	}
	else if (obj.getObjectType() == NITE_HOTMAIL)
	{
		const hotmail::Hotmail hotmail = static_cast<const hotmail::Hotmail&>(obj);
		objectMembers.push_back(std::make_shared<hotmail::Hotmail>(hotmail));
	}
	else if (obj.getObjectType() == NITE_GATTACH)
	{
		const gmail::GMailAttachment gAttachment = static_cast<const gmail::GMailAttachment&>(obj);
		objectMembers.push_back(std::make_shared<gmail::GMailAttachment>(gAttachment));
	}
	else if (obj.getObjectType() == NITE_HOTATTACH)
	{
		const hotmail::HotmailAttachment hotmailAttachment = static_cast<const hotmail::HotmailAttachment&>(obj);
		objectMembers.push_back(std::make_shared<hotmail::HotmailAttachment>(hotmailAttachment));
	}
	else if (obj.getObjectType() == NITE_GDRIVEFILE)
	{
		const gdrive::GDriveFile gDriveFile = static_cast<const gdrive::GDriveFile&>(obj);
		objectMembers.push_back(std::make_shared<gdrive::GDriveFile>(gDriveFile));
	}
	else if (obj.getObjectType() == NITE_ONEDRIVEFILE)
	{
		const onedrive::OneDriveFile oneDriveFile = static_cast<const onedrive::OneDriveFile&>(obj);
		objectMembers.push_back(std::make_shared<onedrive::OneDriveFile>(oneDriveFile));
	}
	else if (obj.getObjectType() == NITE_HOTMAILNEW)
	{
		const hotmail::HotmailNew hotMailNew = static_cast<const hotmail::HotmailNew&>(obj);
		objectMembers.push_back(std::make_shared<hotmail::HotmailNew>(hotMailNew));
	}
	// others
	else
		throw exception::domain::ObjectTypeException(obj.getObjectType());
}

void base::ObjectList::remove(const size_t index)
{
	if (objectMembers.empty())
		throw exception::domain::ObjectNotFoundException("size_t (no id)");
	if(index > objectMembers.size() - 1)
		throw exception::domain::ObjectNotFoundException("size_t (no id)");

	objectMembers.erase(objectMembers.begin() + index);
}

void base::ObjectList::remove(const std::string ooid)
{
	if (objectMembers.empty())
		throw exception::domain::ObjectNotFoundException(ooid);

	for (size_t i = 0; i < objectMembers.size(); i++)
	{
		if(objectMembers[i]->getObjectId().compare(ooid) == 0)
		{
			objectMembers.erase(objectMembers.begin() + i);
			return;
		}
	}

	throw exception::domain::ObjectNotFoundException(ooid);
}

bool base::ObjectList::empty()
{
	return objectMembers.empty();
}

void base::ObjectList::clear()
{
	objectMembers.clear();
}

size_t base::ObjectList::size()
{
	return objectMembers.size();
}

json::Value &base::ObjectList::toJson()
{
	json::JSON jsonArray = json::JSON(NITE_ArrayType);

	for (size_t i = 0; i < objectMembers.size(); i++)
		jsonArray.pushBack(objectMembers[i]->toJson());

	return jsonArray.get();
}
