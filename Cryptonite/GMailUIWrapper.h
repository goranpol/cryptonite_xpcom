#ifndef _GMailUIWrapper_h_
#define _GMailUIWrapper_h_

#include <string>
#include "Parser.h"
#include "GMail.h"
#include "MailUIWrapper.h"

#define NITE_DecryptSummary	0
#define NITE_Other			1

namespace gmail
{
	class GMailUIWrapper : public base::MailUIWrapper
	{
	public:
		GMailUIWrapper();
		~GMailUIWrapper();

		std::string encryptOutgoingString(std::string data, const std::string reqId);
		std::string decryptIncomingString(std::string data, const int decryptType);
		void changeUsersWithAccess(std::string data){};

		void getSentDraftObjectID(std::string reqId, std::string data);
		std::string getSentMailObjectID(std::string reqId, std::string data);

		void deleteMails(std::string data);

	private:

		std::string decryptHtml(std::string data);
		std::string decryptMailsOneJSON(json::JSON json);
		std::string decryptMailsMultipleJSONS(std::vector<json::JSON> jsons);

		void decryptSummary(json::Value &jsonArray);
		void decryptFull(json::Value &jsonArray);

		size_t utf8Length(const std::string str);
	};
}

#endif
