#include <mozilla/Char16.h>

#include "NotImplementedException.h"

exception::domain::NotImplementedException::NotImplementedException(const std::string clss, const std::string funct) : className(clss), functionName(funct)
{
}

exception::domain::NotImplementedException::~NotImplementedException() throw()
{
}

const char* exception::domain::NotImplementedException::what() const throw()
{
	return "";
}