#include <mozilla/Char16.h>

#include "Number.h"
#include "Logger.h"

#include <sstream>
#include <math.h>
#include <float.h>

#include "JsonTypeError.h"
#include "JsonBadCast.h"
#include "JsonBadAlloc.h"

json::Number::Number(const int64_t value) : json::Value(JsonNumber), numberType(NITE_INTEGER), valueInt(value), valueDouble(0)
{
}

json::Number::Number(const int value) : json::Value(JsonNumber), numberType(NITE_INTEGER), valueInt(int64_t(value)), valueDouble(0)
{
}

json::Number::Number(const double value) : json::Value(JsonNumber), numberType(NITE_DOUBLE), valueInt(0), valueDouble(value)
{
}

json::Number::Number(const float value) : json::Value(JsonNumber), numberType(NITE_DOUBLE), valueInt(0), valueDouble(value)
{
}

json::Number::Number(const json::Number &val) : json::Value(val.type), numberType(val.numberType), valueInt(val.valueInt), valueDouble(val.valueDouble)
{
}

json::Number::~Number()
{
}

json::Number &json::Number::operator=(const json::Value &val)
{
	if(val.getType() != JsonNumber)
		throw json::JsonTypeError(JsonNumber, val.getType());

	Number numberVal = static_cast<Number&>(const_cast<Value&>(val));

	if (&numberVal == this)
		return *this;

	const_cast<int&>(this->type) = JsonNumber;
	const_cast<int&>(this->numberType) = numberVal.numberType;
	const_cast<double&>(this->valueDouble) = numberVal.valueDouble;
	const_cast<int64_t&>(this->valueInt) = numberVal.valueInt;

	return *this;
}

json::Number &json::Number::operator=(const json::Number &val)
{
	if(val.getType() != JsonNumber)
		throw json::JsonTypeError(JsonNumber, val.getType());

	if (this == &val)
		return *this;
	
	const_cast<int&>(this->type) = JsonNumber;
	const_cast<int&>(this->numberType) = val.numberType;
	const_cast<double&>(this->valueDouble) = val.valueDouble;
	const_cast<int64_t&>(this->valueInt) = val.valueInt;

	return *this;
}

json::Number &json::Number::operator=(const int64_t val)
{
	const_cast<int&>(this->type) = JsonNumber;
	const_cast<int&>(this->numberType) = NITE_INTEGER;
	const_cast<double&>(this->valueDouble) = 0;
	const_cast<int64_t&>(this->valueInt) = val;

	return *this;
}

json::Number &json::Number::operator=(const int val)
{
	const_cast<int&>(this->type) = JsonNumber;
	const_cast<int&>(this->numberType) = NITE_INTEGER;
	const_cast<double&>(this->valueDouble) = 0;
	const_cast<int64_t&>(this->valueInt) = val;

	return *this;
}

json::Number &json::Number::operator=(const float val)
{
	const_cast<int&>(this->type) = JsonNumber;
	const_cast<int&>(this->numberType) = NITE_DOUBLE;
	const_cast<double&>(this->valueDouble) = val;
	const_cast<int64_t&>(this->valueInt) = 0;

	return *this;
}

json::Number &json::Number::operator=(const double val)
{
	const_cast<int&>(this->type) = JsonNumber;
	const_cast<int&>(this->numberType) = NITE_DOUBLE;
	const_cast<double&>(this->valueDouble) = val;
	const_cast<int64_t&>(this->valueInt) = 0;

	return *this;
}

bool json::Number::operator==(const json::Value &val)
{
	if(type != JsonNumber || val.getType() != JsonNumber)
		return false;

	Number numberVal = static_cast<Number&>(const_cast<Value&>(val));

	if(numberType != numberVal.numberType)
		return false;

	if(numberType == NITE_INTEGER)
	{
		if(numberVal.valueInt != valueInt)
			return false;
	}
	else if(numberType == NITE_DOUBLE)
	{
		if(fabs(numberVal.valueDouble - valueDouble) > DBL_EPSILON)
			return false;
	}
	else
		return false;

	return true;
}

bool json::Number::operator==(const json::Number &val)
{
	if(type != JsonNumber || val.getType() != JsonNumber)
		return false;
	
	if(numberType != val.numberType)
		return false;

	if(numberType == NITE_INTEGER)
	{
		if(val.valueInt != valueInt)
			return false;
	}
	else if(numberType == NITE_DOUBLE)
	{
		if(fabs(val.valueDouble - valueDouble) > DBL_EPSILON)
			return false;
	}
	else
		return false;

	return true;
}

bool json::Number::operator==(const int64_t val)
{
	if(type != JsonNumber)
		return false;

	if(numberType != NITE_INTEGER)
		return false;

	if(valueInt != val)
		return false;

	return true;
}

bool json::Number::operator==(const int val)
{
	if(type != JsonNumber)
		return false;

	if(numberType != NITE_INTEGER)
		return false;

	if(valueInt != (int64_t)val)
		return false;

	return true;
}

bool json::Number::operator==(const float val)
{
	if(type != JsonNumber)
		return false;

	if(numberType != NITE_DOUBLE)
		return false;

	if(fabs(valueDouble - static_cast<double>(val)) > FLT_EPSILON)
		return false;

	return true;
}

bool json::Number::operator==(const double val)
{
	if(type != JsonNumber)
		return false;

	if(numberType != NITE_DOUBLE)
		return false;

	if(fabs(valueDouble - val) > DBL_EPSILON)
		return false;

	return true;
}

bool json::Number::operator!=(const json::Value &val)
{
	return !(*this == val);
}

bool json::Number::operator!=(const json::Number &val)
{
	return !(*this == val);
}

bool json::Number::operator!=(const int64_t val)
{
	return !(*this == val);
}

bool json::Number::operator!=(const int val)
{
	return !(*this == val);
}

bool json::Number::operator!=(const float val)
{
	return !(*this == val);
}

bool json::Number::operator!=(const double val)
{
	return !(*this == val);
}

std::string json::Number::print()
{
	if(type != JsonNumber)
		throw json::JsonTypeError(JsonNumber, type);

	std::stringstream result;

	if(numberType == NITE_INTEGER)
	{
		result << valueInt;
	}
	else if(numberType == NITE_DOUBLE)
	{
		std::string resultString;
		result << valueDouble;
		resultString = result.str();
		
		if(valueDouble - (int)valueDouble <= int(0))
		{
			resultString += ".0";
			return resultString;
		}

		return resultString;
	}
	else
	{
		throw json::JsonTypeError(JsonNumber, numberType);
	}

	return result.str();
}

int64_t json::Number::getIntValue() const
{
	return valueInt;
}

double json::Number::getDoubleValue() const
{
	return valueDouble;
}
