#ifndef _Bool_h_
#define _Bool_h_

#include "Value.h"

namespace json
{
	class Bool : public Value
	{
	public:
		Bool();
		Bool(const bool val);
		Bool(const Bool &val);
		virtual ~Bool();

		Bool &operator=(const Value &val);
		Bool &operator=(const Bool &val);
		Bool &operator=(const bool val);

		bool operator==(const Value &val);
		bool operator==(const Bool &val);
		bool operator==(const bool val);

		bool operator!=(const Value &val);
		bool operator!=(const Bool &val);
		bool operator!=(const bool val);

		/* print */
		std::string print();

		bool getBoolValue() const;

		friend class Enumeration;

	protected:

	private:
		/******************************************************************************************/
		/************************************Bool members******************************************/
		/******************************************************************************************/
		const bool boolValue;								/* the value of true/false json value */
		/******************************************************************************************/
		/************************************Bool members******************************************/
		/******************************************************************************************/
	};
}

#endif
