#include <mozilla/Char16.h>

#include "Value.h"
#include "Logger.h"

#include "Enumeration.h"
#include "String.h"
#include "Number.h"
#include "Bool.h"

#include "JsonTypeNotDefined.h"
#include "JsonTypeError.h"
#include "JsonOutOfRange.h"
#include "JsonKeyNotFound.h"
#include "JsonBadAlloc.h"
#include "JsonBadCast.h"

json::Value::Value() : type(JsonNull)
{
}

json::Value::Value(const int type)  : type(type)
{
}

json::Value::Value(const json::Value &val) : type(val.type)
{
}

json::Value::~Value()
{
}

/* = operators */
json::Value &json::Value::operator=(const json::Value &val)
{
	if (this == &val)
		return *this;

	const_cast<int&>(this->type) = val.type;
	return *this;
}

/* == operators */
bool json::Value::operator==(const json::Value &val)
{
	if(type != val.type)
		return false;

	if(type == JsonNull)
		return true;
	else if(type == JsonEmpty)
		return true;
	else if(type == JsonString)
	{
		String str1 = static_cast<String&>(const_cast<Value&>(val));
		String str2 = static_cast<String&>(*this);

		return (str1 == str2);
	}
	else if(type == JsonNumber)
	{
		Number num1 = static_cast<Number&>(const_cast<Value&>(val));
		Number num2 = static_cast<Number&>(*this);

		return (num1 == num2);
	}
	else if(type == JsonBoolean)
	{
		Bool bool1 = static_cast<Bool&>(const_cast<Value&>(val));
		Bool bool2 = static_cast<Bool&>(*this);

		return (bool1 == bool2);
	}
	else if(type == JsonEnumeration)
	{
		Enumeration enum1 = static_cast<Enumeration&>(const_cast<Value&>(val));
		Enumeration enum2 = static_cast<Enumeration&>(*this);

		return (enum1 == enum2);
	}
	
	return false;
}

bool json::Value::operator==(const std::string val)
{
	if(type != JsonString)
		return false;

	String str = static_cast<String&>(*this);

	if(str != val)
		return false;

	return true;
}

bool json::Value::operator==(const char* val)
{
	if(type != JsonString)
		return false;

	String str = static_cast<String&>(*this);

	if(str != val)
		return false;

	return true;
}

bool json::Value::operator==(const bool val)
{
	if(type != JsonBoolean)
		return false;

	Bool boolean = static_cast<Bool&>(*this);

	if(boolean != val)
		return false;

	return true;
}

bool json::Value::operator==(const int64_t val)
{
	if(type != JsonNumber)
		return false;

	Number num = static_cast<Number&>(*this);

	if(num == val)
		return false;

	return true;
}

bool json::Value::operator==(const int val)
{
	if(type != JsonNumber)
		return false;

	Number num = static_cast<Number&>(*this);

	if(num == val)
		return false;

	return true;
}

bool json::Value::operator==(const float val)
{
	if(type != JsonNumber)
		return false;

	Number num = static_cast<Number&>(*this);

	if(num == val)
		return false;

	return true;
}

bool json::Value::operator==(const double val)
{
	if(type != JsonNumber)
		return false;

	Number num = static_cast<Number&>(*this);

	if(num == val)
		return false;

	return true;
}

bool json::Value::operator!=(const json::Value &val)
{
	return !(*this == val);
}

bool json::Value::operator!=(const std::string val)
{
	return !(*this == val);
}

bool json::Value::operator!=(const char* val)
{
	return !(*this == val);
}

bool json::Value::operator!=(const bool val)
{
	return !(*this == val);
}

bool json::Value::operator!=(const int64_t val)
{
	return !(*this == val);
}

bool json::Value::operator!=(const int val)
{
	return !(*this == val);
}

bool json::Value::operator!=(const float val)
{
	return !(*this == val);
}

bool json::Value::operator!=(const double val)
{
	return !(*this == val);
}

/* access operators */
json::Value &json::Value::operator[](const size_t index)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonNumber, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	return (enumeration)[index];
}

json::Value &json::Value::operator[](const std::string key)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	return (enumeration)[key];
}

json::Value &json::Value::operator[](const char* key)
{
	if (type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	return (enumeration)[std::string(key)];
}

/* gets if array */
json::Value &json::Value::get(const size_t index)
{
	return (*this)[index];
}

std::string json::Value::getString(const size_t index)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	return enumeration.getString(index);
}

bool json::Value::getBool(const size_t index)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	return enumeration.getBool(index);
}

int64_t json::Value::getInt64(const size_t index)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	return enumeration.getInt64(index);
}

int json::Value::getInt(const size_t index)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	return enumeration.getInt(index);
}

double json::Value::getDouble(const size_t index)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	return enumeration.getDouble(index);
}

/* gets if object */
json::Value &json::Value::get(const std::string key)
{
	return (*this)[key];
}

std::string json::Value::getString(const std::string key)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	return enumeration.getString(key);
}

bool json::Value::getBool(const std::string key)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	return enumeration.getBool(key);
}

int64_t json::Value::getInt64(const std::string key)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	return enumeration.getInt64(key);
}

int json::Value::getInt(const std::string key)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	return enumeration.getInt(key);
}

double json::Value::getDouble(const std::string key)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	return enumeration.getDouble(key);
}

/* replaces if array */
void json::Value::replace(const size_t index, const json::Value &val)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.replace(index, val);
}

void json::Value::replace(const size_t index, const std::string val)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.replace(index, val);
}

void json::Value::replace(const size_t index, const char* val)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.replace(index, val);
}

void json::Value::replace(const size_t index, const bool val)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.replace(index, val);
}

void json::Value::replace(const size_t index, const int64_t val)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.replace(index, val);
}

void json::Value::replace(const size_t index, const int val)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.replace(index, val);
}

void json::Value::replace(const size_t index, const float val)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.replace(index, val);
}

void json::Value::replace(const size_t index, const double val)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.replace(index, val);
}

void json::Value::replace(const size_t index)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.replace(index);
}

void json::Value::replaceArray(const size_t index)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.replaceArray(index);
}

void json::Value::replaceObject(const size_t index)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.replaceObject(index);
}

void json::Value::replaceJavascript(const size_t index, const std::string name)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.replaceJavascript(index, name);
}

/* replaces if object */
void json::Value::replace(const std::string key, const json::Value &val)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.replace(key, val);
}

void json::Value::replace(const std::string key, const std::string val)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.replace(key, val);
}

void json::Value::replace(const std::string key, const char* val)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.replace(key, val);
}

void json::Value::replace(const std::string key, const bool val)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.replace(key, val);
}

void json::Value::replace(const std::string key, const int64_t val)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.replace(key, val);
}

void json::Value::replace(const std::string key, const int val)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.replace(key, val);
}

void json::Value::replace(const std::string key, const float val)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.replace(key, val);
}

void json::Value::replace(const std::string key, const double val)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.replace(key, val);
}

void json::Value::replace(const std::string key)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.replace(key);
}

void json::Value::replaceArray(const std::string key)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.replaceArray(key);
}

void json::Value::replaceObject(const std::string key)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.replaceObject(key);
}

void json::Value::replaceJavascript(const std::string key, const std::string name)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.replaceJavascript(key, name);
}

/* inserts if array */
void json::Value::insert(const size_t index, const json::Value &val)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.insert(index, val);
}

void json::Value::insert(const size_t index, const std::string val)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.insert(index, val);
}

void json::Value::insert(const size_t index, const char* val)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.insert(index, val);
}

void json::Value::insert(const size_t index, const bool val)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.insert(index, val);
}

void json::Value::insert(const size_t index, const int64_t val)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.insert(index, val);
}

void json::Value::insert(const size_t index, const int val)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.insert(index, val);
}

void json::Value::insert(const size_t index, const float val)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.insert(index, val);
}

void json::Value::insert(const size_t index, const double val)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.insert(index, val);
}

void json::Value::insert(const size_t index)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.insert(index);
}

void json::Value::insertArray(const size_t index)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.insertArray(index);
}

void json::Value::insertObject(const size_t index)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.insertObject(index);
}

void json::Value::insertJavascript(const size_t index, const std::string name)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.insertJavascript(index, name);
}

/* inserts if object */
void json::Value::insert(const std::string key, const json::Value &val, const size_t index)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.insert(key, val, index);
}

void json::Value::insert(const std::string key, const std::string val, const size_t index)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.insert(key, val, index);
}

void json::Value::insert(const std::string key, const char* val, const size_t index)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.insert(key, val, index);
}

void json::Value::insert(const std::string key, const bool val, const size_t index)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.insert(key, val, index);
}

void json::Value::insert(const std::string key, const int64_t val, const size_t index)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.insert(key, val, index);
}

void json::Value::insert(const std::string key, const int val, const size_t index)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.insert(key, val, index);
}

void json::Value::insert(const std::string key, const float val, const size_t index)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.insert(key, val, index);
}

void json::Value::insert(const std::string key, const double val, const size_t index)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.insert(key, val, index);
}

void json::Value::insert(const std::string key, const size_t index)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.insert(key, index);
}

void json::Value::insertArray(const std::string key, const size_t index)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.insertArray(key, index);
}

void json::Value::insertObject(const std::string key, const size_t index)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.insertObject(key, index);
}

void json::Value::insertJavascript(const std::string key, const size_t index, const std::string name)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.insertJavascript(key, index, name);
}

/* pushBacks if array */
void json::Value::pushBack(const json::Value &val)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.pushBack(val);
}

void json::Value::pushBack(const bool val)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.pushBack(val);
}

void json::Value::pushBack(const int64_t val)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.pushBack(val);
}

void json::Value::pushBack(const int val)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.pushBack(val);
}

void json::Value::pushBack(const float val)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.pushBack(val);
}

void json::Value::pushBack(const double val)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.pushBack(val);
}

void json::Value::pushBack()
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.pushBack();
}

void json::Value::pushBackArray()
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.pushBackArray();
}

void json::Value::pushBackObject()
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.pushBackObject();
}

/* pushBack for array (String, char*) or object (key and null) */
void json::Value::pushBack(const std::string val)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.pushBack(val);
}

void json::Value::pushBack(const char* val)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.pushBack(val);
}

void json::Value::pushBackJavascript(const std::string key, const std::string name)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.pushBackJavascript(key, name);
}

/* pushBacks if object */
void json::Value::pushBack(const std::string key, const json::Value &val)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.pushBack(key, val);
}

void json::Value::pushBack(const std::string key, const std::string val)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.pushBack(key, val);
}

void json::Value::pushBack(const std::string key, const char* val)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.pushBack(key, val);
}

void json::Value::pushBack(const std::string key, const bool val)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.pushBack(key, val);
}

void json::Value::pushBack(const std::string key, const int64_t val)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.pushBack(key, val);
}

void json::Value::pushBack(const std::string key, const int val)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.pushBack(key, val);
}

void json::Value::pushBack(const std::string key, const float val)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.pushBack(key, val);
}

void json::Value::pushBack(const std::string key, const double val)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.pushBack(key, val);
}

void json::Value::pushBackArray(const std::string key)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.pushBackArray(key);
}

void json::Value::pushBackObject(const std::string key)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.pushBackObject(key);
}

/* removes for array and object */
void json::Value::remove(const size_t index)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.remove(index);
}

void json::Value::remove(const std::string key)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	enumeration.remove(key);
}

/* empty & clear */
bool json::Value::empty()
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	return enumeration.empty();
}

void json::Value::clear()
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	return enumeration.clear();
}

/* check name if javascript function */
bool json::Value::checkName(const std::string nm)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	return enumeration.checkName(nm);
}

bool json::Value::checkName(const char *nm)
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	return enumeration.checkName(nm);
}

std::string json::Value::print()
{
	if(type == JsonNull)
		return "null";
	else if(type == JsonEmpty)
		return "";
	else
		throw json::JsonTypeError(JsonNull, type);
}

size_t json::Value::size()
{
	if(type != JsonEnumeration)
		throw json::JsonTypeError(JsonEnumeration, type);

	Enumeration enumeration = static_cast<Enumeration&>(*this);

	return enumeration.size();
}

int json::Value::getType() const
{
	return type;
}

void json::Value::skipSpaces(std::string &jsonStr)
{
	for(size_t i=0; i<jsonStr.size(); i++)
	{
		if(jsonStr[i] == ' ' || jsonStr[i] == '\n' || jsonStr[i] == '\t' || jsonStr[i] == '\r')
		{
			continue;
		}
		else
		{
			jsonStr = jsonStr.substr(i);
			return;
		}
	}
}
