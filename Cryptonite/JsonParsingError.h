#ifndef _JsonParsingError_h_
#define _JsonParsingError_h_

#define startQuoteNotFound						0				// starting quote for string (' or ") not found
#define endQuoteNotFound						1				// ending quote for string (' or ") not found
#define stringNotComplete						2				// string not complete json (size = 0 before end of parsing)
#define startingArraySignNotFound				3				// starting sign for array ([) not found
#define endingOrSeparatorArraySignNotFound		4				// ending sign for array (]) not found or separator sign for array (,) not found
#define startingJSONSignNotFound				5				// starting sign for new JSON ([ or {) not found
#define startingObjectSignNotFound				6				// starting sign for object ({) not found
#define endingOrSeparatorObjectNotFound			7				// ending sign for object (}) not found or separator sign for object (,) not found
#define startingSignKeyNotFound					8				// starting sign for key (") in object pair not found
#define endingSignKeyNotFound					9				// ending sign for key (") in object pair not found
#define keyValueSeparatorNotFound				10				// separator for key value (:) not found
#define noValueFound							11				// no value found after "key" :
#define numberParsingError						12				// error parsing json number
#define notJson									13				// string is not json (no object or array found)

#include "JsonError.h"

namespace json
{
	class JsonParsingError : public JsonError
	{
	public:
		JsonParsingError(const int typ, const std::string jsonStr);
		~JsonParsingError();

		const char* what() const throw();

	private:
		/******************************************************************************************************/
		/************************************JsonParsingError members******************************************/
		/******************************************************************************************************/
		const int type;									/* type of parsing error */
		const std::string jsonString;					/* where the error occured */
		/******************************************************************************************************/
		/************************************JsonParsingError members******************************************/
		/******************************************************************************************************/

	};
}

#endif
