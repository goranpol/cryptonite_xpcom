#ifndef _HotmailListener_h
#define _HotmailListener_h

#include "HttpListener.h"
#include <string>
#include <vector>

#define NITE_decryptHtml			0
#define NITE_decryptJavascript	1
#define NITE_decryptCompose		2
#define NITE_sendDraft			3
#define NITE_sendMail			4
#define NITE_sendAttachment		5

namespace hotmail
{
	class HotmailListener : public base::HttpListener
	{
	public:
		HotmailListener(const int type);

		NS_DECL_ISUPPORTS
		NS_DECL_NSISTREAMLISTENER
		NS_DECL_NSIREQUESTOBSERVER

	private:
		~HotmailListener();

		const int urlType;
	};
}

#endif
