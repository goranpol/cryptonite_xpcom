#ifndef _HttpListener_h_
#define _HttpListener_h_

#include "nsCOMPtr.h"
#include "nsIStreamListener.h"
#include "mozilla/RefPTr.h"
#include <string>

namespace base
{
	class HttpListener: public nsIStreamListener
	{
	public:
		HttpListener();
		
		NS_DECL_ISUPPORTS
		NS_DECL_NSISTREAMLISTENER
		NS_DECL_NSIREQUESTOBSERVER

		nsIStreamListener *getMListener();
		void setMListener(nsIStreamListener *listener);

	protected:
		virtual ~HttpListener();

		bool isBase64(const std::string input);

		char *mData;
		char *mDecryptedData;
		PRUint32 mRead;
		PRUint32 mDecryptedRead;

	private:
		RefPtr<nsIStreamListener> mListener;			// next listener in chain
	};
}

#endif