#include <mozilla/Char16.h>

#include "HotmailNewListener.h"
#include "HotmailNewUIWrapper.h"

#include "NSException.h"

#include "Logger.h"

NS_IMPL_ISUPPORTS(hotmail::HotmailNewListener, nsIRequestObserver, nsIStreamListener)

hotmail::HotmailNewListener::HotmailNewListener(const int type) : urlType(type)
{
}


hotmail::HotmailNewListener::~HotmailNewListener()
{
}

NS_IMETHODIMP hotmail::HotmailNewListener::OnStartRequest(nsIRequest *aRequest, nsISupports *aContext)
{
	try
	{
		HttpListener::OnStartRequest(aRequest, aContext);
	}
	catch (exception::technical::NSException)
	{
		// notify javascript
	}

	return NS_OK;
}


NS_IMETHODIMP hotmail::HotmailNewListener::OnDataAvailable(nsIRequest *aRequest, nsISupports *aContext, nsIInputStream *aInputStream, uint64_t aOffset, uint32_t aCount)
{
	try
	{
		HttpListener::OnDataAvailable(aRequest, aContext, aInputStream, aOffset, aCount);
	}
	catch (exception::technical::NSException)
	{
		// notify javascript
	}

	return NS_OK;
}

NS_IMETHODIMP hotmail::HotmailNewListener::OnStopRequest(nsIRequest *aRequest, nsISupports *aContext, nsresult aStatusCode)
{
	mData = (char*)NS_Realloc((void*)mData, sizeof(char) * (mRead + 1));
	if (!mData)
	{
		Logger::errorMessage("GMailListener", "OnStopRequest", "OnStopRequest FAILED: couldn't reallocate memory for mData", nsresult(-1));
		return NS_OK;
	}

	memcpy(mData + mRead, "\0", 1);

	if (urlType == NITE_Reply)
	{
		try
		{
			std::string decryptedData = hotmail::HotmailNewUIWrapper().decryptIncomingString(mData, NITE_DecryptReply);
			mDecryptedRead = static_cast<unsigned int>(decryptedData.size());
			mDecryptedData = (char*)NS_Alloc(sizeof(char) * mDecryptedRead);
			memcpy(mDecryptedData, decryptedData.c_str(), mDecryptedRead);
		}
		catch (...)
		{
			// notify javascript
			mDecryptedRead = mRead;
			mDecryptedData = (char*)NS_Alloc(sizeof(char) * mRead);
			memcpy(mDecryptedData, mData, mDecryptedRead);
		}
	}
	else if (urlType == NITE_Notification)
	{
		try
		{
			std::string decryptedData = hotmail::HotmailNewUIWrapper().decryptIncomingString(mData, NITE_DecryptNotification);

			mDecryptedRead = static_cast<unsigned int>(decryptedData.size());
			mDecryptedData = (char*)NS_Alloc(sizeof(char) * mDecryptedRead);
			memcpy(mDecryptedData, decryptedData.c_str(), mDecryptedRead);
		}
		catch (...)
		{
			// notify javascript
			mDecryptedRead = mRead;
			mDecryptedData = (char*)NS_Alloc(sizeof(char) * mRead);
			memcpy(mDecryptedData, mData, mDecryptedRead);
		}
	}
	else if (urlType == NITE_Summary)
	{
		try
		{
			std::string decryptedData = hotmail::HotmailNewUIWrapper().decryptIncomingString(mData, NITE_DecryptSummary);
			mDecryptedRead = static_cast<unsigned int>(decryptedData.size());
			mDecryptedData = (char*)NS_Alloc(sizeof(char) * mDecryptedRead);
			memcpy(mDecryptedData, decryptedData.c_str(), mDecryptedRead);
		}
		catch (...)
		{
			// notify javascript
			mDecryptedRead = mRead;
			mDecryptedData = (char*)NS_Alloc(sizeof(char) * mRead);
			memcpy(mDecryptedData, mData, mDecryptedRead);
		}
	}
	else if (urlType == NITE_Full)
	{
		try
		{
			std::string decryptedData = hotmail::HotmailNewUIWrapper().decryptIncomingString(mData, NITE_DecryptFull);

			mDecryptedRead = static_cast<unsigned int>(decryptedData.size());
			mDecryptedData = (char*)NS_Alloc(sizeof(char) * mDecryptedRead);
			memcpy(mDecryptedData, decryptedData.c_str(), mDecryptedRead);
		}
		catch (...)
		{

			// notify javascript
			mDecryptedRead = mRead;
			mDecryptedData = (char*)NS_Alloc(sizeof(char) * mRead);
			memcpy(mDecryptedData, mData, mDecryptedRead);
		}
	}
	else if (urlType == NITE_SessionData)
	{
		try
		{
			std::string decryptedData = hotmail::HotmailNewUIWrapper().decryptIncomingString(mData, NITE_DecryptSessionData);

			mDecryptedRead = static_cast<unsigned int>(decryptedData.size());
			mDecryptedData = (char*)NS_Alloc(sizeof(char) * mDecryptedRead);
			memcpy(mDecryptedData, decryptedData.c_str(), mDecryptedRead);
		}
		catch (...)
		{
			mDecryptedRead = mRead;
			mDecryptedData = (char*)NS_Alloc(sizeof(char) * mRead);
			memcpy(mDecryptedData, mData, mDecryptedRead);
		}
	}
	else if (urlType == NITE_DraftSummary)
	{
		try
		{
			std::string decryptedData = hotmail::HotmailNewUIWrapper().decryptIncomingString(mData, NITE_DecryptDraftSummary);

			mDecryptedRead = static_cast<unsigned int>(decryptedData.size());
			mDecryptedData = (char*)NS_Alloc(sizeof(char) * mDecryptedRead);
			memcpy(mDecryptedData, decryptedData.c_str(), mDecryptedRead);
		}
		catch (...)
		{
			mDecryptedRead = mRead;
			mDecryptedData = (char*)NS_Alloc(sizeof(char) * mRead);
			memcpy(mDecryptedData, mData, mDecryptedRead);
		}
	}
	else if (urlType == NITE_DraftFull)
	{
		try
		{
			std::string decryptedData = hotmail::HotmailNewUIWrapper().decryptIncomingString(mData, NITE_DecryptDraftFull);

			mDecryptedRead = static_cast<unsigned int>(decryptedData.size());
			mDecryptedData = (char*)NS_Alloc(sizeof(char) * mDecryptedRead);
			memcpy(mDecryptedData, decryptedData.c_str(), mDecryptedRead);
		}
		catch (...)
		{
			mDecryptedRead = mRead;
			mDecryptedData = (char*)NS_Alloc(sizeof(char) * mRead);
			memcpy(mDecryptedData, mData, mDecryptedRead);
		}
	}
	else if (urlType == NITE_attachment)
	{
		try
		{
			hotmail::HotmailNewUIWrapper().getAttachmentIds(mData);

			mDecryptedRead = mRead;
			mDecryptedData = (char*)NS_Alloc(sizeof(char) * mRead);
			memcpy(mDecryptedData, mData, mDecryptedRead);
		}
		catch (...)
		{
			mDecryptedRead = mRead;
			mDecryptedData = (char*)NS_Alloc(sizeof(char) * mRead);
			memcpy(mDecryptedData, mData, mDecryptedRead);
		}
	}
	else if (urlType == NITE_MoveOrDelete)
	{
		try
		{
			hotmail::HotmailNewUIWrapper().getNewIds(mData);

			mDecryptedRead = mRead;
			mDecryptedData = (char*)NS_Alloc(sizeof(char) * mRead);
			memcpy(mDecryptedData, mData, mDecryptedRead);
		}
		catch (...)
		{
			mDecryptedRead = mRead;
			mDecryptedData = (char*)NS_Alloc(sizeof(char) * mRead);
			memcpy(mDecryptedData, mData, mDecryptedRead);
		}
	}
	else
	{
		mDecryptedRead = mRead;
		mDecryptedData = (char*)NS_Alloc(sizeof(char) * mRead);
		memcpy(mDecryptedData, mData, mDecryptedRead);
	}

	try
	{
		HttpListener::OnStopRequest(aRequest, aContext, aStatusCode);
	}
	catch (exception::technical::NSException)
	{
		// inform javascript
	}
	catch (...)
	{
		// inform javascript
	}

	return NS_OK;
}
