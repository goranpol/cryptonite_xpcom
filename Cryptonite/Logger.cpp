#include <mozilla/Char16.h>

#include "Logger.h"

#include <time.h>
#include <string>

void Logger::logMessage(char* object, char* function, char* action)
{
	/* FILE* log = fopen("D:\\cryptolog\\cryptoniteLog.txt", "a+");
	fprintf(log, "Log Message [%s]: Class: %s \tFunction: %s \tAction: %s\n", getTime(), object, function, action);
	fclose(log); */
}

void Logger::errorMessage(char* object, char* function, char* action, nsresult errorCode)
{
	/* FILE* log = fopen("D:\\cryptolog\\cryptoniteLog.txt", "a+");
	fprintf(log, "!!!!! Error Message [%s]: Class: %s \tFunction: %s \tAction: %s \terror code: %x !!!!!\n", getTime(), object, function, action, errorCode);
	fclose(log); */
}

char* Logger::getTime()
{
	time_t rawTime;
	struct tm * timeInfo;

	time(&rawTime);
	timeInfo = localtime(&rawTime);

	return strtok(asctime(timeInfo), "\n");;
}
