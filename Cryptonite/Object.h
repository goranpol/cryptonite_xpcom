#ifndef _Object_h_
#define _Object_h_

#include "Value.h"
#include <string>
#include <vector>

#define NITE_GDOC			0
#define NITE_GMAIL			1
#define NITE_HOTMAIL		2
#define NITE_WORD			3
#define NITE_GATTACH		4
#define NITE_HOTATTACH		5
#define NITE_GDRIVEFILE		6
#define NITE_ONEDRIVEFILE	7
#define NITE_HOTMAILNEW		8
/*
others
*/

namespace base
{
	class Object
	{
	public:
		Object(const int objType, const std::string objID, const std::string objKey);
		virtual ~Object();

		Object &operator=(Object &obj);
		virtual bool operator==(Object &obj);
		virtual bool operator!=(Object &obj);

		virtual std::string encryptOutgoingString(std::string data);
		virtual std::string decryptIncomingString(std::string data);

		virtual json::Value &toJson() const;

		std::string getObjectId() const;
		std::string getObjectKey() const;
		int getObjectType() const;

	protected:
		char* randomNumbers(const size_t num);
		std::string generateKey();

		virtual std::string encryptText(std::string textString);
		virtual std::string decryptText(std::string cipherTextString);
		virtual std::string encrypt(std::string textString);
		virtual std::string decrypt(std::string cipherTextString);

		/* string helper functions */
		std::vector<std::string> split(const std::string str, const std::string delimiters = " ");
		std::string replace(const std::string str, const std::string from, const std::string to);
		/* Base64 encoding functions */
		std::string toBase64(const std::string input, size_t len);
		std::string fromBase64(const std::string input);
		bool isBase64(const std::string input);

		/* OBJECT MEMBERS */
		const std::string objectId;
		const int objectType;
		const std::string objectKey;			// base64
	};
}

#endif
