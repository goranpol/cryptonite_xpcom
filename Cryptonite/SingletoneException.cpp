#include <mozilla/Char16.h>

#include "SingletoneException.h"

exception::technical::SingletoneException::SingletoneException(const std::string sing) : singletone(sing)
{
}

exception::technical::SingletoneException::~SingletoneException() throw()
{
}

const char* exception::technical::SingletoneException::what() const throw()
{
	return "";
}