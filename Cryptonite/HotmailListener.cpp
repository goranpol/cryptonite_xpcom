#include <mozilla/Char16.h>

#include "HotmailListener.h"

#include "NSException.h"
#include "AlgorithmException.h"
#include "HotmailUIWrapper.h"
#include "ObjectStorage.h"
#include "HotmailAttachment.h"

#include "Logger.h"

NS_IMPL_ISUPPORTS(hotmail::HotmailListener, nsIRequestObserver, nsIStreamListener)

hotmail::HotmailListener::HotmailListener(const int type) : urlType(type)
{
}

hotmail::HotmailListener::~HotmailListener()
{
}


NS_IMETHODIMP hotmail::HotmailListener::OnStartRequest(nsIRequest *aRequest, nsISupports *aContext)
{
	try
	{
		HttpListener::OnStartRequest(aRequest, aContext);
	}
	catch(exception::technical::NSException)
	{
		// notify javascript
	}

	return NS_OK;
}

NS_IMETHODIMP hotmail::HotmailListener::OnDataAvailable(nsIRequest *aRequest, nsISupports *aContext, nsIInputStream *aInputStream, uint64_t aOffset, uint32_t aCount)
{
	try
	{
		HttpListener::OnDataAvailable(aRequest, aContext, aInputStream, aOffset, aCount);
	}
	catch(exception::technical::NSException)
	{
		// notify javascript
	}

	return NS_OK;
}

NS_IMETHODIMP hotmail::HotmailListener::OnStopRequest(nsIRequest *aRequest, nsISupports *aContext, nsresult aStatusCode)
{
	mData = (char*)NS_Realloc((void*)mData, sizeof(char) * (mRead + 1));
	if(!mData)
	{
		Logger::errorMessage("GMailListener", "OnStopRequest", "OnStopRequest FAILED: couldn't reallocate memory for mData", nsresult(-1));
		return NS_OK;
	}

	memcpy(mData + mRead, "\0", 1);

	if (urlType == NITE_sendDraft)
	{
		try
		{
			hotmail::HotmailUIWrapper().getSentDraftObjectID(mData);

			mDecryptedRead = mRead;
			mDecryptedData = (char*)NS_Alloc(sizeof(char) * mRead);
			memcpy(mDecryptedData, mData, mDecryptedRead);
		}
		catch(...)
		{
			// notify javascript
			mDecryptedRead = mRead;
			mDecryptedData = (char*)NS_Alloc(sizeof(char) * mRead);
			memcpy(mDecryptedData, mData, mDecryptedRead);
		}
	}
	else if (urlType == NITE_sendMail)
	{
		try
		{
			hotmail::HotmailUIWrapper().getSentMail();

			mDecryptedRead = mRead;
			mDecryptedData = (char*)NS_Alloc(sizeof(char) * mRead);
			memcpy(mDecryptedData, mData, mDecryptedRead);
		}
		catch (...)
		{
			// notify javascript
			mDecryptedRead = mRead;
			mDecryptedData = (char*)NS_Alloc(sizeof(char) * mRead);
			memcpy(mDecryptedData, mData, mDecryptedRead);
		}
	}
	else if (urlType == NITE_decryptCompose || urlType == NITE_decryptHtml || urlType == NITE_decryptJavascript)
	{
		try
		{
			std::string decryptedData = hotmail::HotmailUIWrapper().decryptIncomingString(mData, urlType);

			mDecryptedRead = decryptedData.size();
			mDecryptedData = (char*)NS_Alloc(sizeof(char) * mDecryptedRead);
			memcpy(mDecryptedData, decryptedData.c_str(), mDecryptedRead);
		}
		catch(...)
		{
			// notify javascript
			mDecryptedRead = mRead;
			mDecryptedData = (char*)NS_Alloc(sizeof(char) * mRead);
			memcpy(mDecryptedData, mData, mDecryptedRead);
		}
	}
	else if (urlType == NITE_sendAttachment)
	{
		try
		{
			std::string data(mData);

			hotmail::HotmailAttachment hotmailAttachment = static_cast<hotmail::HotmailAttachment&>(base::ObjectStorage::getInstance().getTempObject("-1", NITE_HOTATTACH));
			hotmail::HotmailAttachment hotmailAttachmentNew(hotmailAttachment.getAttachmentId(data), hotmailAttachment.getObjectKey(), hotmailAttachment.getObjectInitChain(), hotmailAttachment.getObjectCheckWord());

			base::ObjectStorage::getInstance().addObject(hotmailAttachmentNew, base::UsersList());
			base::ObjectStorage::getInstance().removeTempObject("-1");

			mDecryptedRead = mRead;
			mDecryptedData = (char*)NS_Alloc(sizeof(char) * mRead);
			memcpy(mDecryptedData, mData, mDecryptedRead);
		}
		catch (...)
		{
			// notify javascript
			mDecryptedRead = mRead;
			mDecryptedData = (char*)NS_Alloc(sizeof(char) * mRead);
			memcpy(mDecryptedData, mData, mDecryptedRead);
		}
	}
	else
	{
		mDecryptedRead = mRead;
		mDecryptedData = (char*)NS_Alloc(sizeof(char) * mRead);
		memcpy(mDecryptedData, mData, mDecryptedRead);
	}

	try
	{
		HttpListener::OnStopRequest(aRequest, aContext, aStatusCode);
	}
	catch(exception::technical::NSException)
	{
		// inform javascript
	}
	catch(...)
	{
		// inform javascript
	}

	return NS_OK;
}