#ifndef _GMailAttachment_h_
#define _GMailAttachment_h_

#include "Attachment.h"

namespace gmail
{
	class GMailAttachment : public base::Attachment
	{
	public:
		GMailAttachment(const std::string id);
		GMailAttachment(const std::string objId, const std::string objKey, const std::string objInitChain, const std::string objCheckWord);
		GMailAttachment(const base::Object &obj);
		GMailAttachment(const gmail::GMailAttachment &obj);
		~GMailAttachment();

		GMailAttachment &operator=(Object &obj);
		GMailAttachment &operator=(GMailAttachment &obj);
		bool operator==(Object &obj);
		bool operator!=(Object &obj);

	private:
	};
}

#endif

