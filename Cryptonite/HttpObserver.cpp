#include <mozilla/Char16.h>

#include "HttpObserver.h"

#include "nsIRandomGenerator.h"
#include "nsIComponentManager.h"
#include "nsIURI.h"
#include "nsISeekableStream.h"
#include "nsIStringStream.h"
#include "nsIBinaryInputStream.h"

#include "Logger.h"
#include "EscapeEncoding.h"

#include "nsStringAPI.h"

#include "NSException.h"
#include "NotImplementedException.h"

NS_IMPL_ISUPPORTS(base::HttpObserver, nsIObserver)

base::HttpObserver::HttpObserver(bool encrypt) : requestMethod(""), contentType(""), empty(true), doEncrypt(encrypt)
{
}

base::HttpObserver::~HttpObserver()
{
}

NS_IMETHODIMP base::HttpObserver::Observe(nsISupports *aSubject, const char *aTopic, const char16_t *aData)
{
	return NS_OK;
}

nsresult base::HttpObserver::getUri(nsIHttpChannel *httpChannel, nsACString& url)
{
	std::string action = "getting url";
	nsresult rv;
	nsIURI *uri;
	nsAutoCString urlPrePath;
	nsAutoCString urlPath;

	rv = httpChannel->GetURI(&uri);
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	rv = uri->GetPrePath(urlPrePath);
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	rv = uri->GetPath(urlPath);
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	url.Append(urlPrePath);
	url.Append(urlPath);

	return NS_OK;
}

/*************** returns escaped string read from the outgoing stream (also rewinds stream) *****************/
std::string base::HttpObserver::readSendingData(nsIUploadChannel *aUploadChannel)
{
	std::string action = "reading request data";
	nsresult rv;
	nsCOMPtr<nsIInputStream> inputStream;
	rv = aUploadChannel->GetUploadStream(getter_AddRefs(inputStream));
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	/* get component manager */
	nsCOMPtr<nsIComponentManager> componentManager;
	rv = NS_GetComponentManager(getter_AddRefs(componentManager));
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	/* queryinterface from input stream to seekable stream */
	nsCOMPtr<nsISeekableStream> seekableStream = do_QueryInterface(inputStream, &rv);
	if (NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	/* get offset in bytes */
	int64_t prevOffset;
	rv = seekableStream->Tell(&prevOffset);
	if (NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	/* rewind  stream to start */
	rv = seekableStream->Seek(seekableStream->NS_SEEK_SET, 0);
	if (NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	/* create binary stream */
	nsCOMPtr<nsIBinaryInputStream> binaryInputStream;
	rv = componentManager->CreateInstanceByContractID("@mozilla.org/binaryinputstream;1", nullptr, NS_GET_IID(nsIBinaryInputStream), getter_AddRefs(binaryInputStream));
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}
	
	/* set input stream */
	rv = binaryInputStream->SetInputStream(inputStream);
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	/* queryinterface upload channel to channel */
	nsCOMPtr<nsIChannel> channel = do_QueryInterface(aUploadChannel, &rv);
	if (NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	/* queryinterface channel to http channel */
	nsCOMPtr<nsIHttpChannel> httpChannel = do_QueryInterface(channel, &rv);
	if (NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	/*******************************************************************************/

	/* get request method */
	rv = httpChannel->GetRequestMethod(requestMethod);
	if (NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	/* get content type */
	NS_NAMED_LITERAL_CSTRING(contentTypeHeader, "Content-Type");
	rv = httpChannel->GetRequestHeader(contentTypeHeader, contentType);
	if (NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	if (!std::string(contentType.get()).empty())
		empty = false;
	/*******************************************************************************/

	/* get content length */
	NS_NAMED_LITERAL_CSTRING(contentLengthHeader, "Content-Length");
	nsAutoCString len;
	rv = httpChannel->GetRequestHeader(contentLengthHeader, len);
	if (NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	/* alloc post data */
	uint64_t postLength = atoll(len.get());
	char *postDataEscapedString = (char*)NS_Alloc(sizeof(char) * (postLength) + 1);
	if (!postDataEscapedString)
	{
		throw exception::technical::NSException(nsresult(-1), action);
	}

	/* read bytes from binary input stream */
	rv = binaryInputStream->ReadBytes(postLength, &postDataEscapedString);
	if (NS_FAILED(rv))
	{
		throw exception::technical::NSException(nsresult(-1), action);
	}
	if (!postDataEscapedString)
	{
		throw exception::technical::NSException(nsresult(-1), action);
	}

	std::string returnString = "";
	std::string tempString = "";
	char *buf = postDataEscapedString;
	for (size_t i = 0; i < postLength; i++)
	{
		returnString.push_back(buf[i]);
	}

	if (prevOffset == 0)
	{
		/* rewind  stream to start if it was at the start before */
		rv = seekableStream->Seek(seekableStream->NS_SEEK_SET, 0);
		if (NS_FAILED(rv))
		{
			throw exception::technical::NSException(rv, action);
		}
	}

	NS_Free(postDataEscapedString);
	return returnString;
}

void base::HttpObserver::replaceSendingData(nsIUploadChannel *aUploadChannel, std::string encryptedMessage)
{
	std::string action = "replacing request data";
	nsresult rv;

	/* get component manager */
	nsCOMPtr<nsIComponentManager> componentManager;
	rv = NS_GetComponentManager(getter_AddRefs(componentManager));
	if (NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	/* create a string stream */
	nsCOMPtr<nsIStringInputStream> stringStream;
	rv = componentManager->CreateInstanceByContractID("@mozilla.org/io/string-input-stream;1", nullptr, NS_GET_IID(nsIStringInputStream), getter_AddRefs(stringStream));
	if (NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	/* allocate memory for cstring */
	size_t len = encryptedMessage.size();
	char *encryptedCString = (char*)NS_Alloc(sizeof(char) * (len)+1);
	if (!encryptedCString)
	{
		throw exception::technical::NSException(nsresult(-1), action);
	}

	/* set the cstring */
	char *buf = encryptedCString;
	for (size_t i = 0; i < len; i++)
	{
		*buf = encryptedMessage[i];
		buf++;
	}
	
	/* set cstring to string data */
	rv = stringStream->SetData(encryptedCString, len);
	if (NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	/************************************************************************************************************************************************/
	/*****************************************************************REWIND STREAM******************************************************************/
	/************************************************************************************************************************************************/
	nsCOMPtr<nsISeekableStream> seekableStream = do_QueryInterface(stringStream, &rv);
	if (NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	rv = seekableStream->Seek(seekableStream->NS_SEEK_SET, 0);
	if (NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}
	/************************************************************************************************************************************************/
	/*****************************************************************REWIND STREAM******************************************************************/
	/************************************************************************************************************************************************/

	/*************************************************************************************************************************************************/
	/*****************************************************************REPLACE STREAM******************************************************************/
	/*************************************************************************************************************************************************/

	/* set string stream as upload stream */
	if (empty == false)
	{
		rv = aUploadChannel->SetUploadStream(stringStream, contentType, -1);
		if (NS_FAILED(rv))
		{
			throw exception::technical::NSException(rv, action);
		}
	}
	else
	{
		// maybe nothing here instead
		nsAutoCString typeACString;
		char *type = "application/x-www-form-urlencoded;charset=utf-8";
		typeACString.Assign(type);

		rv = aUploadChannel->SetUploadStream(stringStream, typeACString, -1);
		if (NS_FAILED(rv))
		{
			throw exception::technical::NSException(rv, action);
		}
	}
	/*************************************************************************************************************************************************/
	/*****************************************************************REPLACE STREAM******************************************************************/
	/*************************************************************************************************************************************************/
	
	/**************************************************************************************************************************************************/
	/****************************************************************SET REQUEST METHOD****************************************************************/
	/**************************************************************************************************************************************************/
	
	/* queryinterface upload channel to http channel */
	nsCOMPtr<nsIHttpChannel> httpChannel = do_QueryInterface(aUploadChannel, &rv);
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	if (requestMethod.IsEmpty())
		requestMethod.Assign("POST");

	/* set request method to http channel */
	rv = httpChannel->SetRequestMethod(requestMethod);
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}
	/**************************************************************************************************************************************************/
	/****************************************************************SET REQUEST METHOD****************************************************************/
	/**************************************************************************************************************************************************/

	/* free the memory of cstring */
	NS_Free(encryptedCString);
}

void base::HttpObserver::replaceStreamListener(nsITraceableChannel *traceableChannel, base::HttpListener *listener)
{
	std::string action = "replacing stram listener";
	nsresult rv;

	nsCOMPtr<nsIStreamListener> oldListener;
	rv = traceableChannel->SetNewListener(listener, getter_AddRefs(oldListener));
	if(NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, action);
	}

	listener->setMListener(oldListener);
}

void base::HttpObserver::changeUsersWithAccess(std::string data)
{
	throw exception::domain::NotImplementedException("HttpObserver", "changeUsersWithAccess");
}

std::string base::HttpObserver::getRequestHeader(nsIHttpChannel *httpChannel, const std::string header)
{
	nsresult rv;
	NS_NAMED_LITERAL_CSTRING(headerCStr, header.c_str());
	nsAutoCString headerValue;
	rv = httpChannel->GetRequestHeader(headerCStr, headerValue);
	if (NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, "Couldn't get request header");
	}
	
	std::string returnString(headerValue.get());
	return returnString;
}

void base::HttpObserver::setRequestHeader(nsIHttpChannel *httpChannel, const std::string header, const std::string value)
{
	nsresult rv;
	NS_NAMED_LITERAL_CSTRING(headerCStr, header.c_str());

	rv = httpChannel->SetRequestHeader(headerCStr, nsAutoCString(value.c_str()), false);
	if (NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, "Couldn't set request header");
	}
}

std::string base::HttpObserver::getResponseHeader(nsIHttpChannel *httpChannel, const std::string header)
{
	nsresult rv;
	NS_NAMED_LITERAL_CSTRING(headerCStr, header.c_str());
	nsAutoCString headerValue;
	rv = httpChannel->GetResponseHeader(headerCStr, headerValue);
	if (NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, "Couldn't get response header");
	}

	std::string returnString(headerValue.get());
	return returnString;
}

void base::HttpObserver::setResponseHeader(nsIHttpChannel *httpChannel, const std::string header, std::string value)
{
	nsresult rv;
	NS_NAMED_LITERAL_CSTRING(headerCStr, header.c_str());

	rv = httpChannel->SetResponseHeader(headerCStr, nsAutoCString(value.c_str()), false);
	if (NS_FAILED(rv))
	{
		throw exception::technical::NSException(rv, "Couldn't set response header");
	}
}

/************splits string str with delimiters and returns the vector of splited strings************/
std::vector<std::string> base::HttpObserver::split(const std::string str, const std::string delimiters)
{
	std::string::size_type pos;
	std::string::size_type lastPos = 0;

	std::vector<std::string> result;
	while (true)
	{
		pos = str.find(delimiters, lastPos);

		if (pos == std::string::npos)
		{
			pos = str.size();
			if (pos != lastPos)
				result.push_back(str.substr(lastPos, pos - lastPos));
			else
				result.push_back(std::string(""));

			break;
		}
		else
		{
			if (pos != lastPos)
				result.push_back(str.substr(lastPos, pos - lastPos));
			else
				result.push_back(std::string(""));
		}

		lastPos = pos + delimiters.size();
	}

	return result;
}

/* replaces from substrings with to substrings and returns changed string */
std::string base::HttpObserver::replace(const std::string str, const std::string from, const std::string to)
{
	if (str.find(from) == -1)
		return str;

	std::vector<std::string> strTmp = split(str, from);
	std::string returnString = strTmp[0];
	for (size_t i = 1; i<strTmp.size(); i++)
	{
		returnString.append(to);
		returnString.append(strTmp[i]);
	}

	return returnString;
}